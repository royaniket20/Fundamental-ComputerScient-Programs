package com.aniket.multithreadingBasic.concurrentDs.problemsolving;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Create three threads . Print 1-50
 * <p>
 * Thread-2 prints all numbers divisible by 2
 * Thread 3 prints numbers divisible 3
 * Other numbers Thread 1 will print .
 * <p>
 * Also needs to be printed in sequence.
 */
public class SequntialPrintingByThread {


    public static void main(String[] args) {
        AtomicInteger currentPos = new AtomicInteger(1); //his Denotes what will get printed
        AtomicInteger printingTurn = new AtomicInteger(1); //This Denotes who Will Print Data
        PrinterThread printerThread = new PrinterThread(currentPos, printingTurn, 1); //Last param is Printer Number
        PrinterThread printerThread2 = new PrinterThread(currentPos, printingTurn, 2);//Last param is Printer Number
        PrinterThread printerThread3 = new PrinterThread(currentPos, printingTurn, 3);//Last param is Printer Number
        //Intentionally started thread in Random manner to showcase Whatever way you start it does Not matter
        new Thread(printerThread3).start();
        new Thread(printerThread).start();
        new Thread(printerThread2).start();
    }
}

class PrinterThread implements Runnable {
    private AtomicInteger position;
    private AtomicInteger printingTurn;
    private int divisor;
    public PrinterThread(AtomicInteger position, AtomicInteger printingTurn, int divisor) {
        this.position = position;
        this.printingTurn = printingTurn;
        this.divisor = divisor;
    }
    void setNextCandidate(AtomicInteger printingTurn, AtomicInteger position) {
        //Checking whats the Future Turn will be - Based on Next Pos Value
        //During Printing of Last Position we have called getAndIncrement() // So we are already in Next Position value
        if (position.get() % 2 == 0) { //If a Number is Divisible by 2 , 3 all - 2 will get priority
            printingTurn.set(2);
        } else if (position.get() % 3 == 0) {
            printingTurn.set(3);
        } else {
            printingTurn.set(1);
        }
    }
    @Override
    public void run() {
        while (position.get() <= 50) {
            if (printingTurn.get() == divisor) {
                System.out.println("This is my chance to Go ahead  - " + divisor);
                System.out.println("Printing -" + position.getAndIncrement() + " By Thread - " + divisor);
                setNextCandidate(printingTurn, position);
            }
        }
    }
}
