package com.aniket.multithreadingBasic.concurrentDs.problemsolving;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.Semaphore;

//New Token get added at 1 token per sec
//Requet need to acquire the token to go forward
public class ImplementRateLimitUsingTokenBucket {

    public static void main(String[] args) throws InterruptedException {
        TokenBucket tokenBucket = new TokenBucket();
        Thread th1 = new Thread(()->{
            while (true){
                try {
                    Thread.sleep(2000);
                    System.out.println("Token received by "+Thread.currentThread().getName()+" is "+tokenBucket.getToken());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Thread th2 = new Thread(()->{
            while (true){
                try {
                    Thread.sleep(1500);
                    System.out.println("Token received by "+Thread.currentThread().getName()+" is "+tokenBucket.getToken());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        th1.start();
        th2.start();
        tokenBucket.putToken();
        th1.join();
        th2.join();


    }
}

class TokenBucket{
    private Semaphore tokenController;

    public TokenBucket() {
        this.tokenController = new Semaphore(0);
    }


    public String getToken(){
        try {
            System.out.println("Trying to get token ----Available "+tokenController.availablePermits());
            tokenController.acquire();
            System.out.println("tOKEN aCQUIRED -----");
            return  "Some Token";
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public void putToken(){
        while (true){
            try {
                Thread.sleep(1200);
                System.out.println("Generating New token at - "+new Date());
                tokenController.release();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
