package com.aniket.multithreadingBasic.concurrentDs.problemsolving;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class BlockingQueueUsingSemaphore<E> {

	//Quere and it's size
	private Queue<E> queue;
	private int size;

	private Semaphore producerSem;
	private Semaphore consumerSem;




	public BlockingQueueUsingSemaphore(int size) {
		super();
		this.size = size;
		queue = new LinkedList<>();
		this.producerSem= new Semaphore(size);
		this.consumerSem  = new Semaphore(0); //Producer will control How Much you can consume
	}

	public  void put(E e)
	{
		try {
			//We are acquering the semaphore to Porduce the Data
			producerSem.acquire();
			queue.add(e);
			consumerSem.release(); //Releasing One Semaphore so atleast one data can be consumed as One Just got produced
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
		
	}
	
	public  E get()
	{
		try {
			consumerSem.acquire(); //Because Producer has released So I can acquire and proceed
			E e = queue.remove();
			producerSem.release(); //Informing producer to produce More
			return  e;	
		} catch (InterruptedException e1) {
			throw new RuntimeException(e1);
		}
		
	}
	
}
