package com.aniket.multithreadingBasic.concurrentDs.problemsolving;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;
//https://jvmaware.com/custom-thread-pool/
public class CustomThreadPool {
    public static void main(String[] args) {
        System.out.println("Creating custom Thread Pool ");
        MyThreadPool myThreadPool = new MyThreadPool(10);
        new Thread(()->{
            try {
                IntStream.range(0,10).forEach(item->{
                    Runnable task = ()->{
                        System.out.println("Doing some heavy  Computation " + item);
                    };
                    myThreadPool.submitTask(task);
                });
                Thread.sleep(5000);
               myThreadPool.terminate();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }).start();
        myThreadPool.awaitTermination();
    }
}

class MyThreadPool {
    private final int poolSize;
    private final List<Thread> threadList;
    private final Supplier<Thread> threadSupplier = MyThread::new;

    volatile  boolean isRunning = false;

    public MyThreadPool(int poolSize ){
        this.poolSize = poolSize;
        threadList = new ArrayList<>();
        //Add new threads
        IntStream.range(0,poolSize).forEach(count->{
            System.out.println("Adding thread No - "+ count);
            Thread newThread = threadSupplier.get();
            System.out.println("New Thread created - "+ newThread);
            threadList.add(newThread);
            newThread.start();
        });
        isRunning= true;
    }

    public void submitTask(Runnable runnable){
        int randomThread = new Random().nextInt(this.poolSize);

        MyThread myThread = (MyThread) threadList.get(randomThread);
        System.out.println("Add Task to thread - "+myThread);
        myThread.addTask(runnable);
    }
    public void awaitTermination(){
        while (isRunning);
    }

    public void terminate() {
        threadList.forEach(item->{
            item.interrupt();
            while (item.isAlive());
            System.out.println("Thread killed Now ");
        });
        isRunning = false;
    }
}

class MyThread extends  Thread {

    Queue<Runnable> runnableQueue = new ConcurrentLinkedQueue<>() ;

    public void addTask(Runnable task){
        runnableQueue.add(task);
    }

    @Override
    public void run() {
       while (true){
           //System.out.println(" I am waiting for work !!!" + Thread.currentThread());
           //ready to do work
           if(!runnableQueue.isEmpty()){
               System.out.println(" Got new Task to be executed " + Thread.currentThread());
               Runnable currentTask = runnableQueue.remove();
               //Executing the task
               currentTask.run();
           }
           if(Thread.interrupted()){
               System.out.println("Ending the thread");
               break;
           }

       }
    }
}
