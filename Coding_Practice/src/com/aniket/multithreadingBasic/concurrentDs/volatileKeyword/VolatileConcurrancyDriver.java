package com.aniket.multithreadingBasic.concurrentDs.volatileKeyword;

import java.util.Random;

public class VolatileConcurrancyDriver extends Thread {

	
	boolean isReader;
	VolatileConcurrancyExample volatileConcurrancyExample;

	/**
	 * Constructor
	 * 
	 * @param isReader
	 */
	public VolatileConcurrancyDriver(boolean isReader, VolatileConcurrancyExample volatileConcurrancyExample) {
		super();
		this.isReader = isReader;
		this.volatileConcurrancyExample = volatileConcurrancyExample;
		
	}

	
	/**
	 * Overridden Run implementation
	 */
	@Override
	public void run() {

		while (true) {
			if (isReader) {
				// Call the Reader thread
				volatileConcurrancyExample.readerMethod(this.getId());
			} else {
				// Otherwise call the writer thread
				if(new Random().nextInt(1000)%7 ==0 ) //Randomly Write sometimes
				{
					volatileConcurrancyExample.writerMethod(this.getId());
				}
			}
			super.run();
		}
		
	}

	public static void main(String[] args) {
		
		
		VolatileConcurrancyExample volatileConcurrancyExample = new VolatileConcurrancyExample();
		//Create reader and writer threads
		VolatileConcurrancyDriver readerThread1 = new VolatileConcurrancyDriver(true,volatileConcurrancyExample);
		VolatileConcurrancyDriver readerThread2 = new VolatileConcurrancyDriver(true,volatileConcurrancyExample);
		VolatileConcurrancyDriver readerThread3 = new VolatileConcurrancyDriver(true,volatileConcurrancyExample);
		VolatileConcurrancyDriver readerThread4 = new VolatileConcurrancyDriver(true,volatileConcurrancyExample);
		VolatileConcurrancyDriver writerThread = new VolatileConcurrancyDriver(false,volatileConcurrancyExample);
		
		//starting the threads
		writerThread.start();
		readerThread1.start();
		readerThread2.start();
		readerThread3.start();
		readerThread4.start();
		System.out.println("***ended***");
		
	}
}
