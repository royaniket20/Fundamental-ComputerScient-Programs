package com.aniket.multithreadingBasic.basics;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MissingSignalFixUsingLoop {
    static volatile boolean flag = false;

    public static void main(String[] args) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition condition = reentrantLock.newCondition();

        Thread th1 = new Thread(() -> {
            try {
                reentrantLock.lock();
                System.out.println("I will be waiting for Cond to fulfil");
                condition.await();
                flag = true;
                System.out.println("I will be free Now "); //This will not get called - as Signal is Lost
                reentrantLock.unlock();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread th2 = new Thread(() -> {
            while (!flag) {
                reentrantLock.lock();
                System.out.println("I will be trying  fulfilling the condition ");
                condition.signal();

                reentrantLock.unlock();
            }
        });

        th2.start();
        Thread.sleep(3000);
        //Now Signal is missed
        th1.start();
        th1.join();
        th2.join();
    }
}
