package com.aniket.multithreadingBasic.basics;

import java.util.concurrent.Callable;

//When single thread to Sum - It Take Longer time - Multiple thread can do it faster - specifically on Multi threaded system
public class PerformanceByMultiThreading {

    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        SimpleSum simpleSum1 = new SimpleSum();
        SimpleSum simpleSum2 = new SimpleSum();
        Thread th1 = new Thread(() ->{

          simpleSum1.giveSum(0 , Integer.MAX_VALUE/2);
        });
        Thread th2 = new Thread(() ->{

          simpleSum2.giveSum(Integer.MAX_VALUE/2+1 , Integer.MAX_VALUE);
        });
        th1.start();
        th2.start();
        th1.join();
        System.out.println("Thread 1 ended ");
        th2.join();
        System.out.println("Thread 2 ended ");
        long end = System.currentTimeMillis();
        long result = simpleSum1.sumPart +simpleSum2.sumPart;
        System.out.println("Time taken MultiThreading  - "+(end-start));
         start = System.currentTimeMillis();
        SimpleSum simpleSum = new SimpleSum();
        simpleSum.giveSum(0 , Integer.MAX_VALUE);
        end = System.currentTimeMillis();
        System.out.println("Time taken Single Thread  - "+(end-start));
    }



}

class SimpleSum {
    long sumPart = 0l;
    public void giveSum(int fromRange , int toRange){

        for (int i = fromRange; i < toRange; i++) {
            sumPart = sumPart + i;
        }
    }
}
