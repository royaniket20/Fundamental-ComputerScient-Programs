package com.aniket.multithreadingBasic.basics;

public class SyncronizationPerformance {

    public static void main(String[] args) throws InterruptedException {
        Efficient efficient = new Efficient();
        InEfficient inEfficient = new InEfficient();
        long start = System.currentTimeMillis();
        Thread th1 = new Thread(()->{
            try {
                efficient.call1();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        th1.start();
        Thread th2  = new Thread(()->{
            try {
                efficient.call2();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        th2.start();
        th1.join();
        th2.join();
        long end  = System.currentTimeMillis();
        System.out.println("Time taken Efficient - "+(end-start) +" ms");
        start = System.currentTimeMillis();
        th1 = new Thread(()->{
            try {
                inEfficient.call1();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        th1.start();
        th2 = new Thread(()->{
            try {
                inEfficient.call2();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        th2.start();
        th1.join();
        th2.join();
         end  = System.currentTimeMillis();
        System.out.println("Time taken InEfficient - "+(end-start) +" ms");
        System.out.println("End of Main Program ");

    }
}


class Efficient {
    Object lock1 = new Object();
    Object lock2 = new Object();

    public void call1() throws InterruptedException {
        synchronized (lock1){
            System.out.println("I am called from call1 ");
            Thread.sleep(3000);
            System.out.println("I am called  again from call1 ");
        }
    }

    public void call2() throws InterruptedException {
        synchronized (lock2){
            System.out.println("I am called from call2 ");
            Thread.sleep(3500);
            System.out.println("I am called  again from call2 ");
        }
    }
}

class InEfficient {


    public synchronized void  call1() throws InterruptedException {

            System.out.println("inEfficient I am called from call1 ");
            Thread.sleep(3000);
            System.out.println("inEfficient I am called  again from call1 ");

    }

    public synchronized void call2() throws InterruptedException {

            System.out.println("inEfficient I am called from call2 ");
            Thread.sleep(3500);
            System.out.println("inEfficient I am called  again from call2 ");

    }
}