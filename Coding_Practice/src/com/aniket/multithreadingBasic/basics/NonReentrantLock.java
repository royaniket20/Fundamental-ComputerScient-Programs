package com.aniket.multithreadingBasic.basics;

public class NonReentrantLock {
    public static void main(String[] args) {
        //This is the Reentrant Lock example using Syncronization
        Test test = new Test();
        Object mutex = new Object();
        new Thread(()->{
            test.call(mutex);
        }).start();
        new Thread(()->{
            test.call(mutex);
        }).start();
        //This is the NON Reentrant Lock example using Syncronization
        NotReentrantLock notReentrantLock = new NotReentrantLock();
        Test test2 = new Test();
        new Thread(()->{
            try {
                test2.fun(notReentrantLock);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).start();

    }
}

class NotReentrantLock {
    boolean isLocked;
    NotReentrantLock(){
        isLocked= false;
    }

    //Wait Until Current Lock is Unlocked by someone
    public void lock()  {
        while (this.isLocked){
            try {
                    wait(); // Go to waiting state
            } catch (Exception e) {

             //wILL GET mONITOR eXCEPTION If same thread call twice Lock
            }
        }
        this.isLocked = true; // Once unlocked One of them will come here as effect of Notify
    }

    public void unlock(){
        this.isLocked = false;
        notify(); //Notify any one of the waiting thread on this Obj
    }

}
class  Test {

    public void fun (NotReentrantLock mutex) throws InterruptedException {
        System.out.println(" NON REENTANT  Lock");
        mutex.lock();
            System.out.println("First Lock qcquired");
            mutex.lock();
                System.out.println("Second Lock qcquired ");
        mutex.unlock();
        mutex.unlock();


    }

    public void call (Object mutex){
        System.out.println("------ reentant Lock");
        synchronized (mutex){
            System.out.println("First Lock qcquired");
            synchronized (mutex){
                System.out.println("Second Lock qcquired ");
            }
        }

    }
}
