package com.aniket.multithreadingBasic.basics;

/**
 *  When Is It Thrown?
 * The IllegalMonitorStateException is related to multithreading programming in Java. If we have a monitor we want to synchronize on, this exception is thrown to indicate that a thread tried to wait or to notify other threads waiting on that monitor, without owning it. In simpler words, we’ll get this exception if we call one of the wait(), notify(), or notifyAll() methods of the Object class outside of a synchronized block.
 */
public class WaitNotifyNotifyAllExample {
    Object mutex = new Object();

    int position = 3;
    Runnable r1 = ()->{
        synchronized (mutex) {
            try {
                while (position!=1)
                {
                    System.out.println(Thread.currentThread()+"Now we will wait for Position");
                    mutex.wait();
                    System.out.println(Thread.currentThread()+"Now we will continue  for work ");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < 5; i++) {
                System.out.println("I am runnable 1");
            }
            if(position ==1 ){
                position = 0;
                //mutex.notify(); //Now only One waiting thread will Unblock
                mutex.notifyAll(); //Now All  waiting thread will Unblock
            }
        }
    };
    Runnable r2 = ()->{
        synchronized (mutex) {
            try {
                while (position!=2)
                {
                    System.out.println(Thread.currentThread()+"Now we will wait for Position");
                    mutex.wait();
                    System.out.println(Thread.currentThread()+"Now we will continue  for work ");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < 5; i++) {
                System.out.println("I am runnable 2");
            }
            if(position ==2 ){
                position = 1;
                mutex.notify(); //Now Thread 1 will be unlocked
            }
        }
    };

    Runnable r3 = ()->{
        synchronized (mutex) {

            for (int i = 0; i < 5; i++) {
                System.out.println("I am runnable 3");
            }
            if(position ==3 ){
                position = 2;
                mutex.notify(); //Now Thread 2 will be unlocked
                System.out.println(" Notify called by Th 3");
            }
        }
    };


    Runnable defaultData = ()->{
        synchronized (mutex) {
            try {
                while (position!=0)
                {
                    System.out.println(Thread.currentThread()+"Now we will wait for Position - DEFAULT");
                    mutex.wait();
                    System.out.println(Thread.currentThread()+"Now we will continue  for work - DEFAULT");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < 20; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("I am runnable defaultData"+ Thread.currentThread());
            }
        }
    };




    public void driver() throws InterruptedException {
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        Thread t4 = new Thread(defaultData);
        Thread t5 = new Thread(defaultData);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        System.out.println("Final Main thread ends ");
    }
    public static void main(String[] args) throws InterruptedException {
       new WaitNotifyNotifyAllExample().driver();
    }

}
