package com.aniket.multithreadingBasic.basics;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MissingSignalFixUsingSemaphore {

    public static void main(String[] args) throws InterruptedException {
        Semaphore reentrantLock = new Semaphore(0);


        Thread th1 = new Thread(() -> {
            try {
                reentrantLock.acquire();
                System.out.println("I will be waiting for Cond to fulfil ");
                System.out.println("I will be free Now "); //This will not get called - as Signal is Lost
                System.out.println("Available limits - "+reentrantLock.availablePermits());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        Thread th2 = new Thread(() -> {
                reentrantLock.release();
                System.out.println("I will be fulfilling the condition - available Now "+reentrantLock.availablePermits());
        });

        th2.start();
        th2.join();
       //Now Signal will Not be missed - as we have already released one ticket - So now available ticket is 1 which was 0 earler
        th1.start();
        th1.join();
    }
}
