package com.aniket.multithreadingBasic.basics;

public class LivelockExample {


    public static void main(String[] args) {

        Bus bus = new Bus();

        final Traveller tvlr1 = new Traveller("Arjun", bus);
        final Traveller tvlr2 = new Traveller("Shankar", bus);

        Thread t1 = new Thread(() -> {
            try {
                tvlr1.moveInsideTheBus(tvlr2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                tvlr2.moveInsideTheBus(tvlr1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();
    }

}





class Bus {

    public String personEnteringTheBus;

}

class Traveller {

    private String name;
    // Initial state of the traveller is blocked
    private boolean isBlocked = true;
    private Bus bus;

    public  void moveInsideTheBus(Traveller fellowTvlr) throws InterruptedException {
        // If the traveller feels blocked, he will make way for his fellow traveller
        if (isBlocked) {
            makeWayForFellowTraveller(fellowTvlr);
        }
        // If he is not blocked, his name would be the person to enter the bus
        else {
            bus.personEnteringTheBus = this.name;
            System.out.println(this.name + " moved inside.");
        }
    }

    private  void makeWayForFellowTraveller(Traveller fellowTvlr) throws InterruptedException {
        while (isBlocked) {
            bus.personEnteringTheBus = fellowTvlr.name;
            System.out.println(Thread.currentThread().getName() + ":" + this.name + " making way for " + fellowTvlr.name);
            Thread.sleep(1000);
        }
    }

    public Traveller(String name, Bus bus) {
        this.name = name;
        this.bus = bus;
    }

}
