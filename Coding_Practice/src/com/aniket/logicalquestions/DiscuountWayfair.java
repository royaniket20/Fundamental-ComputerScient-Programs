package com.aniket.logicalquestions;

import netscape.javascript.JSObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Coupons = [
 * {"CategoryName:Comforter Sets", "CouponName:Comforters Sale"},
 * {"CategoryName:Bedding", "CouponName:Savings on Bedding"},
 * {"CategoryName:Bed & Bath", "CouponName:Low price for Bed & Bath"}
 * ]
 *
 * Categories = [
 * {"CategoryName:Comforter Sets", "CategoryParentName:Bedding"},
 * {"CategoryName:Bedding", "CategoryParentName:Bed & Bath"},
 * {"CategoryName:Bed & Bath", "CategoryParentName:None"},
 * {"CategoryName:Soap Dispensers", "CategoryParentName:Bathroom Accessories"},
 * {"CategoryName:Bathroom Accessories", "CategoryParentName:Bed & Bath"},
 * {"CategoryName:Toy Organizers", "CategoryParentName:Baby And Kids"},
 * {"CategoryName:Baby And Kids", "CategoryParentName:None}
 * ]
 *
 * Requirements/Acceptance Criteria:
 *
 * Create a function that when passed a Category Name (as a String) will return Coupon Name (as a String)
 * Category structure is hierarchical. Categories without coupons inherit their parent's coupon.
 * No coupon should be returned if there are no coupons in the Category's hierarchy
 * For example: Toy Organizers receives no coupon because there is no coupon in the category hierarchy.
 * If a Category has a coupon it should not move up the hierarchy to find its Parent Category (or the Parent's Coupon)
 * For example: Comforter sets, should see the coupon for Comforter Sets and NOT Bedding
 * Beware of the following examples/edge cases and their expected behaviors:
 * Bathroom Accessories should receive the coupon for Bed & Bath because there are no coupons for Bathroom Accessories
 * Assumption: Product can only be associated with one category.
 * tests: input (CategoryName) => output (CouponName)
 * "Comforter Sets" => "Comforters Sale"
 * "Bedding" => "Savings on Bedding"
 * "Bathroom Accessories" => "Low price for Bed & Bath"
 * "Soap Dispensers" => "Low price for Bed & Bath"
 * "Toy Organizers" => null
 *
 *
 *
 *
 */
public class DiscuountWayfair {

    private Map<String , String>  couponsMap;
    private Map<String , String> categotyMap;
    public DiscuountWayfair(){
        this.couponsMap = new HashMap<>();
        this.categotyMap = new HashMap<>();

        this.couponsMap.put("Comforter Sets","Comforters Sale");
        this.couponsMap.put("Bedding","Savings on Bedding");
        this.couponsMap.put("Bed & Bath","Low price for Bed & Bath");

        this.categotyMap.put("Comforter Sets","Bedding");
        this.categotyMap.put("Bedding","Bed & Bath");
        this.categotyMap.put("Bed & Bath","None");
        this.categotyMap.put("Soap Dispensers","Bathroom Accessories");
        this.categotyMap.put("Bathroom Accessories","Bed & Bath");
        this.categotyMap.put("Toy Organizers","Baby And Kids");
        this.categotyMap.put("Baby And Kids","None");
    }

    public String getCouponForCategory(String category){
        if(couponsMap.containsKey(category)){
            return couponsMap.get(category);
        }else{
            //Find Parent
            while (categotyMap.containsKey(category)){
                String parent = categotyMap.get(category);
                if(couponsMap.containsKey(parent)){
                    return couponsMap.get(parent);
                }else{
                    category = parent;
                }
            }
            return null;
        }
    }

    public static void main(String[] args) {
        DiscuountWayfair discuountWayfair = new DiscuountWayfair();
        System.out.println("Comforter Sets => "+ discuountWayfair.getCouponForCategory("Comforter Sets"));
        System.out.println("Bedding => "+ discuountWayfair.getCouponForCategory("Bedding"));
        System.out.println("Bathroom Accessories => "+ discuountWayfair.getCouponForCategory("Bathroom Accessories"));
        System.out.println("Soap Dispensers => "+ discuountWayfair.getCouponForCategory("Soap Dispensers"));
        System.out.println("Toy Organizers => "+ discuountWayfair.getCouponForCategory("Toy Organizers"));
    }
}

