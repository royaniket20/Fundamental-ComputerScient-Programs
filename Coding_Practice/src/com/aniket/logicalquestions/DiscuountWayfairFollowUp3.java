package com.aniket.logicalquestions;

import java.util.*;

/**
 * Follow-up: 3
 * <p>
 * Problem Statement:
 * Now that we know what coupon to show to the user, let's make sure the user can apply the coupon by changing the selling price
 * of a Product.
 * Format (Category Name, Coupon Name, Date Modified, Discount)
 * <p>
 * Coupons = [
 * {"CategoryName:Comforter Sets", "CouponName:Comforters Sale", "DateModified:2020-01-01","Discount:10%"},
 * {"CategoryName:Comforter Sets", "CouponName:Cozy Comforter Coupon", "DateModified:2020-01-01","Discount:$15"},
 * {"CategoryName:Bedding", "CouponName:Best Bedding Bargains", "DateModified:2019-01-01","Discount:35%"},
 * {"CategoryName:Bedding", "CouponName:Savings on Bedding", "DateModified:2019-01-01","Discount:25%"},
 * {"CategoryName:Bed & Bath", "CouponName:Low price for Bed & Bath", "DateModified:2018-01-01","Discount:50%"},
 * {"CategoryName:Bed & Bath", "CouponName:Bed & Bath extravaganza", "DateModified:2019-01-01","Discount:75%"}
 * ]
 * <p>
 * categories = [
 * {"CategoryName:Comforter Sets", "CategoryParentName:Bedding"},
 * {"CategoryName:Bedding", "CategoryParentName:Bed & Bath"},
 * {"CategoryName:Bed & Bath", "CategoryParentName:None"},
 * {"CategoryName:Soap Dispensers", "CategoryParentName:Bathroom Accessories"},
 * {"CategoryName:Bathroom Accessories", "CategoryParentName:Bed & Bath"},
 * {"CategoryName:Toy Organizers", "CategoryParentName:Baby And Kids"},
 * {"CategoryName:Baby And Kids", "CategoryParentName:None"}
 * ]
 * <p>
 * products = [
 * <p>
 * {"ProductName:Cozy Comforter Sets","Price:100.00", "CategoryName:Comforter Sets"},
 * {"ProductName:All-in-one Bedding Set", "Price:50.00", "CategoryName:Bedding"},
 * {"ProductName:Infinite Soap Dispenser", "Price:500.00" ,"CategoryName:Bathroom Accessories"},
 * {"ProductName:Rainbow Toy Box","Price:257.00", "CategoryName:Baby And Kids"}
 * ]
 * <p>
 * Requirements/Acceptance Criteria:
 * • Function takes a String representing the Product Name
 * • Function Returns the discounted price (Product price minus the coupon discount) as a String
 * <p>
 * tests: inputs => output
 * <p>
 * "Cozy Comforter" => "90.00" OR "85.00"
 * "All-in-one Bedding Set" => "32.50" OR "37.50"
 * "Infinite Soap Dispenser" => "125.001
 * "Rainbow Tov Box" => "257.00"
 */
public class DiscuountWayfairFollowUp3 {

    private Map<String, PriorityQueue<Pair>> couponsMap;
    private Map<String, String> categotyMap;
    private Map<String, Integer> preComputedDiscount;

    private Map<String, Product> productMap;

    public DiscuountWayfairFollowUp3() {
        this.couponsMap = new HashMap<>();
        this.categotyMap = new HashMap<>();
        this.preComputedDiscount = new HashMap<>();
        this.productMap = new HashMap<>();

        Comparator<Pair> pairComparator = Comparator.comparing(pair -> pair.dateModifiDate, Comparator.reverseOrder());
        this.couponsMap.put("Comforter Sets", new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bedding", new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bed & Bath", new PriorityQueue<>(pairComparator));
        this.couponsMap.get("Comforter Sets").add(new Pair("Comforters Sale", "2020-01-01", 10));
        this.couponsMap.get("Comforter Sets").add(new Pair("Cozy Comforter Coupon", "2021-01-01", 15));
        this.couponsMap.get("Bedding").add(new Pair("Best Bedding Bargains", "2019-01-01", 35));
        this.couponsMap.get("Bedding").add(new Pair("Savings on Bedding", "2019-01-01", 25));
        this.couponsMap.get("Bed & Bath").add(new Pair("Low price for Bed & Bath", "2018-01-01", 50));
        this.couponsMap.get("Bed & Bath").add(new Pair("Bed & Bath extravaganza", "2019-01-01", 75));


        this.categotyMap.put("Comforter Sets", "Bedding");
        this.categotyMap.put("Bedding", "Bed & Bath");
        this.categotyMap.put("Bed & Bath", "None");
        this.categotyMap.put("Soap Dispensers", "Bathroom Accessories");
        this.categotyMap.put("Bathroom Accessories", "Bed & Bath");
        this.categotyMap.put("Toy Organizers", "Baby And Kids");
        this.categotyMap.put("Baby And Kids", "None");

        this.productMap.put("Cozy Comforter Sets", new Product("Cozy Comforter Sets", 100.00, "Comforter Sets"));
        this.productMap.put("All-in-one Bedding Set", new Product("All-in-one Bedding Set", 50.0, "Bedding"));
        this.productMap.put("Infinite Soap Dispenser", new Product("Infinite Soap Dispenser", 500.00, "Bathroom Accessories"));
        this.productMap.put("Rainbow Toy Box", new Product("Rainbow Toy Box", 257.00, "Baby And Kids"));
    }

    public Integer getCouponForCategoryDiscount(String category) {
        if (couponsMap.containsKey(category)) {

            PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(category));
            while (!pairPriorityQueueTemp.isEmpty()) {
                if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                    return pairPriorityQueueTemp.peek().Discount;
                } else {
                    pairPriorityQueueTemp.poll();
                }
            }
            //Nothing Found
            return 0;

        } else {
            //Find Parent
            while (categotyMap.containsKey(category)) {
                String parent = categotyMap.get(category);

                if (couponsMap.containsKey(parent)) {

                    PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(parent));
                    while (!pairPriorityQueueTemp.isEmpty()) {
                        if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                            return pairPriorityQueueTemp.peek().Discount;
                        } else {
                            pairPriorityQueueTemp.poll();
                        }
                    }
                    //Nothing Found
                    return 0;

                } else {
                    category = parent;
                }
            }
            return 0;
        }
    }


    public Double findDiscountedPriceOfProduct(String productName) {
        Product product = this.productMap.get(productName);
        Integer disc = getCouponForCategoryDiscount(product.CategoryName);
        Double finalPrice = product.Price - product.Price * (new Double(disc) / 100);
        return finalPrice;
    }

    public static void main(String[] args) {

        DiscuountWayfairFollowUp3 discuountWayfair = new DiscuountWayfairFollowUp3();
        System.out.println("Cozy Comforter => " + discuountWayfair.findDiscountedPriceOfProduct("Cozy Comforter Sets"));
        System.out.println("All-in-one Bedding Set => " + discuountWayfair.findDiscountedPriceOfProduct("All-in-one Bedding Set"));
        System.out.println("Infinite Soap Dispenser => " + discuountWayfair.findDiscountedPriceOfProduct("Infinite Soap Dispenser"));
        System.out.println("Rainbow Toy Box => " + discuountWayfair.findDiscountedPriceOfProduct("Rainbow Toy Box"));
        /**
         * Bed & Bath => Bed & Bath extravaganza
         * Bedding => Best Bedding Bargains
         * Bathroom Accessories => Bed & Bath extravaganza
         * Comforter Sets => Cozy Comforter Coupon
         */
    }
}

class Product {
    public String ProductName;
    public Double Price;
    public String CategoryName;

    Product(String ProductName, Double Price, String CategoryName) {
        this.ProductName = ProductName;
        this.Price = Price;
        this.CategoryName = CategoryName;
    }

}


