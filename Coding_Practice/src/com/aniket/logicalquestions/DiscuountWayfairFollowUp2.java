package com.aniket.logicalquestions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 Follow-up: 2

 Problem Statement:
 The findBestCoupon function is being called billions of times per day while not being a core feature of the site. Can you make the
 function faster?
 Instructions

 Copy and Paste the solution from Part 2 as a starting point to work through this exercise.

 Requirements/Acceptance Criteria:
 • All Requirements from prior question
 • Code should still pass all of the same prior test cases
 • O(n) is not fast enough come up with a O(1) solution!
 *
 *
 *
 * 
 */
public class DiscuountWayfairFollowUp2 {

    private Map<String , PriorityQueue<Pair>>  couponsMap;
    private Map<String , String> categotyMap;
    private Map<String , String> preComputedCupon;
    public DiscuountWayfairFollowUp2(){
        this.couponsMap = new HashMap<>();
        this.categotyMap = new HashMap<>();
        this.preComputedCupon = new HashMap<>();
        Comparator<Pair> pairComparator = Comparator.comparing(pair -> pair.dateModifiDate , Comparator.reverseOrder());
        this.couponsMap.put("Comforter Sets",new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bedding",new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bed & Bath",new PriorityQueue<>(pairComparator));
        this.couponsMap.get("Comforter Sets").add(new Pair("Comforters Sale", "2020-01-01"));
        this.couponsMap.get("Comforter Sets").add(new Pair("Cozy Comforter Coupon", "2021-01-01"));
        this.couponsMap.get("Bedding").add(new Pair("Best Bedding Bargains", "2019-01-01"));
        this.couponsMap.get("Bedding").add(new Pair("Savings on Bedding", "2019-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Low price for Bed & Bath",  "2018-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Bed & Bath extravaganza",  "2019-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Big Savings for Bed & Bath", "2030-01-01"));

        this.categotyMap.put("Comforter Sets","Bedding");
        this.categotyMap.put("Bedding","Bed & Bath");
        this.categotyMap.put("Bed & Bath","None");
        this.categotyMap.put("Soap Dispensers","Bathroom Accessories");
        this.categotyMap.put("Bathroom Accessories","Bed & Bath");
        this.categotyMap.put("Toy Organizers","Baby And Kids");
        this.categotyMap.put("Baby And Kids","None");
    }

    public String getCouponForCategory(String category){
        if(couponsMap.containsKey(category)){

            PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(category));
            while (!pairPriorityQueueTemp.isEmpty()) {
                if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                    return pairPriorityQueueTemp.peek().CouponName;
                }else{
                    pairPriorityQueueTemp.poll();
                }
            }
            //Nothing Found
            return null;

        }else{
            //Find Parent
            while (categotyMap.containsKey(category)){
                String parent = categotyMap.get(category);

                if(couponsMap.containsKey(parent)){

                    PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(parent));
                    while (!pairPriorityQueueTemp.isEmpty()) {
                        if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                            return pairPriorityQueueTemp.peek().CouponName;
                        }else{
                            pairPriorityQueueTemp.poll();
                        }
                    }
                    //Nothing Found
                    return null;

                }else{
                    category = parent;
                }
            }
            return null;
        }
    }

    public String findBestCoupon(String category){
        if(this.preComputedCupon.containsKey(category)){
            return this.preComputedCupon.get(category);
        }else{
            String coupon = getCouponForCategory(category);
            this.preComputedCupon.put(category ,coupon) ;
            return  coupon;
        }

    }

    public static void main(String[] args) {

        DiscuountWayfairFollowUp2 discuountWayfair = new DiscuountWayfairFollowUp2();
        System.out.println("Bed & Bath => "+ discuountWayfair.findBestCoupon("Bed & Bath"));
        System.out.println("Bedding => "+ discuountWayfair.findBestCoupon("Bedding"));
        System.out.println("Bathroom Accessories => "+ discuountWayfair.findBestCoupon("Bathroom Accessories"));
        System.out.println("Comforter Sets => "+ discuountWayfair.findBestCoupon("Comforter Sets"));
        /**
         * Bed & Bath => Bed & Bath extravaganza
         * Bedding => Best Bedding Bargains
         * Bathroom Accessories => Bed & Bath extravaganza
         * Comforter Sets => Cozy Comforter Coupon
         */
    }
}


