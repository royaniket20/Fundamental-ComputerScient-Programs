package com.aniket.logicalquestions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 Follow-up: 1

 Problem Statement:
 The system has added a new piece of data to the coupon - "Date Modified". Use this when resolving any ties (when 1 Category
 has 2+ Coupons).

 coupons = [
 { "CategoryName:Comforter Sets", "CouponName:Comforters Sale", "DateModified:2020-01-01"},
 { "CategoryName:Comforter Sets", "CouponName:Cozy Comforter Coupon", "DateModified:2021-01-01" },
 { "CategoryName:Bedding", "CouponName:Best Bedding Bargains", "DateModified": "2019-01-01" },
 { "CategoryName:Bedding", "CouponName:Savings on Bedding", "DateModified:2019-01-01" },
 { "CategoryName:Bed & Bath", "CouponName:Low price for Bed & Bath", "DateModified": "2018-01-01" },
 { "CategoryName:Bed & Bath", "CouponName:Bed & Bath extravaganza", "DateModified": "2019-01-01" },
 { "CategoryName:Bed & Bath", "CouponName:Big Savings for Bed & Bath", "DateModified:2030-01-01" }
 ]
 categories = [
 {"CategoryName:Comforter Sets", "CategoryParentName:Bedding"},
 {"CategoryName:Bedding", "CategoryParentName:Bed & Bath"},
 {"CategoryName:Bed & Bath", "CategoryParentName":None},
 {"CategoryName:Soap Dispensers", "CategoryParentName:Bathroom Accessories"},
 {"CategoryName:Bathroom Accessories", "CategoryParentName:Bed & Bath"},
 {"CategoryName:Toy Organizers", "CategoryParentName:Baby And Kids"},
 {"CategoryName:Baby And Kids", "CategoryParentName:None"}
 ]

 Requirements/Acceptance Criteria:
 • Create a function that when passed a Category Name (as a String) will return one Coupon Name (as a String)
 • If a Category has more than 1 coupon the Coupon with the most recent DateModified should be returned
 • If a Coupon's DateModified is in the future, it should not be returned
 • Category structure is hierarchical. Categories without coupons inherit their parent's coupon.

 tests: input (CategoryName) => output (CouponName)
 "Bed & Bath" => "Bed & Bath extravaganza"
 "Bedding" => "Savings on Bedding" | "Best Bedding Bargains"
 "Bathroom Accessories" => "Bed & Bath extravaganza"
 "Comforter Sets" => "Comforters Sale" | "Cozy Comforter Coupon"
 *
 *
 *
 * 
 */
public class DiscuountWayfairFollowUp1 {

    private Map<String , PriorityQueue<Pair>>  couponsMap;
    private Map<String , String> categotyMap;
    public DiscuountWayfairFollowUp1(){
        this.couponsMap = new HashMap<>();
        this.categotyMap = new HashMap<>();
        Comparator<Pair> pairComparator = Comparator.comparing(pair -> pair.dateModifiDate , Comparator.reverseOrder());
        this.couponsMap.put("Comforter Sets",new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bedding",new PriorityQueue<>(pairComparator));
        this.couponsMap.put("Bed & Bath",new PriorityQueue<>(pairComparator));
        this.couponsMap.get("Comforter Sets").add(new Pair("Comforters Sale", "2020-01-01"));
        this.couponsMap.get("Comforter Sets").add(new Pair("Cozy Comforter Coupon", "2021-01-01"));
        this.couponsMap.get("Bedding").add(new Pair("Best Bedding Bargains", "2019-01-01"));
        this.couponsMap.get("Bedding").add(new Pair("Savings on Bedding", "2019-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Low price for Bed & Bath",  "2018-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Bed & Bath extravaganza",  "2019-01-01"));
        this.couponsMap.get("Bed & Bath").add(new Pair("Big Savings for Bed & Bath", "2030-01-01"));

        this.categotyMap.put("Comforter Sets","Bedding");
        this.categotyMap.put("Bedding","Bed & Bath");
        this.categotyMap.put("Bed & Bath","None");
        this.categotyMap.put("Soap Dispensers","Bathroom Accessories");
        this.categotyMap.put("Bathroom Accessories","Bed & Bath");
        this.categotyMap.put("Toy Organizers","Baby And Kids");
        this.categotyMap.put("Baby And Kids","None");
    }

    public String getCouponForCategory(String category){
        if(couponsMap.containsKey(category)){

            PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(category));
            while (!pairPriorityQueueTemp.isEmpty()) {
                if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                    return pairPriorityQueueTemp.peek().CouponName;
                }else{
                    pairPriorityQueueTemp.poll();
                }
            }
            //Nothing Found
            return null;

        }else{
            //Find Parent
            while (categotyMap.containsKey(category)){
                String parent = categotyMap.get(category);

                if(couponsMap.containsKey(parent)){

                    PriorityQueue<Pair> pairPriorityQueueTemp = new PriorityQueue<>(couponsMap.get(parent));
                    while (!pairPriorityQueueTemp.isEmpty()) {
                        if (pairPriorityQueueTemp.peek().dateModifiDate.before(new Date())) {
                            return pairPriorityQueueTemp.peek().CouponName;
                        }else{
                            pairPriorityQueueTemp.poll();
                        }
                    }
                    //Nothing Found
                    return null;

                }else{
                    category = parent;
                }
            }
            return null;
        }
    }

    public static void main(String[] args) {

        DiscuountWayfairFollowUp1 discuountWayfair = new DiscuountWayfairFollowUp1();
        System.out.println("Bed & Bath => "+ discuountWayfair.getCouponForCategory("Bed & Bath"));
        System.out.println("Bedding => "+ discuountWayfair.getCouponForCategory("Bedding"));
        System.out.println("Bathroom Accessories => "+ discuountWayfair.getCouponForCategory("Bathroom Accessories"));
        System.out.println("Comforter Sets => "+ discuountWayfair.getCouponForCategory("Comforter Sets"));
    }
}
class Pair {
    public String CouponName;
    public Date dateModifiDate;

    public Integer Discount;

    public  Pair(String couponName , String dateModifiedDateStr){
        this.CouponName = couponName;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.dateModifiDate = simpleDateFormat.parse(dateModifiedDateStr);
        } catch (ParseException e) {
            this.dateModifiDate = new Date();
        }
    }

    public  Pair(String couponName , String dateModifiedDateStr , Integer Discount){
        this.CouponName = couponName;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.dateModifiDate = simpleDateFormat.parse(dateModifiedDateStr);
        } catch (ParseException e) {
            this.dateModifiDate = new Date();
        }
        this.Discount = Discount;
    }

}

