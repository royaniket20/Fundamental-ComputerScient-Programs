package com.aniket.logicalquestions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LRUCacheImpementation {
    private Map<Integer , Node> cahce;
    private Integer size;

    private Node head;
    private Node tail;

    public Integer getValue(Integer key){
        if(cahce.containsKey(key)){
            Node node = cahce.get(key);

            node.prev.next = node.next;
            node.next.prev = node.prev;

            Node temp = head.next;
            head.next = node;
            node.prev = head;
            temp.prev = node;
            node.next=temp;
            System.out.println("["+node.Value+"] Value present on Cache for Key  - ["+key+"]" );
            return  node.Value;
        }else{
            System.out.println("No Value present on Cache for Key  - ["+key+"]" );
            return -1;
        }

    }
    public void putValue(Integer key , Integer value){
        if(cahce.containsKey(key)){
            Node node = cahce.get(key);
            node.Value = value;

            node.next.prev = node.prev;
            node.prev.next = node.next;

            node.next = head.next;
            node.prev = head;
            head.next.prev= node;
            head.next = node;
            System.out.println("Updating  Cache Entry - ["+node.key+","+node.Value+"]" );
            return;
        }
        if(size ==cahce.size()){
            //Full Cache - need to evict LRU
            Node temp = tail.prev;
            temp.prev.next = tail;
            tail.prev = temp.prev;
            System.out.println("Evicting Cache Entry - ["+temp.key+","+temp.Value+"]" );
            cahce.remove(temp.key);
            temp = null;
        }
                Node node = new Node(key,value , null,null);
                cahce.put(key,node);
                node.next = head.next;
                node.prev = head;
                head.next.prev= node;
                head.next = node;
                System.out.println("Putting  Cache Entry - ["+node.key+","+node.Value+"]" );


    }

    public LRUCacheImpementation(Integer size){
        this.cahce = new HashMap<>();
        this.size = size;
        this.head = new Node(0,0,null,null);
        this.tail = new Node(0,0,null,null);
        head.next = tail;
        tail.next = head;

    }

    public void printLL(){
        Node temp = head.next;
        System.out.print("head==");
        while (temp!=tail){
            System.out.print("==["+temp.key+","+temp.Value+"]==");
            temp = temp.next;
        }
        System.out.println("==tail");
    }
    public static void main(String[] args) {
        LRUCacheImpementation lruCacheImpementation = new LRUCacheImpementation(3);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.putValue(1,10);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.putValue(3,15);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.putValue(2,12);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.getValue(3);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.putValue(4,20);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.getValue(2);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
        lruCacheImpementation.putValue(4,25);
        System.out.println(lruCacheImpementation.cahce);
        lruCacheImpementation.printLL();
    }
}
class Node {
    public Integer key ;
    public Integer Value;
    public Node next;
    public Node prev;

    public Node(Integer key , Integer Value , Node next,Node prev) {
        this.key = key;
        this.Value = Value;
        this.next = next;
        this.prev = prev;
    }

    @Override
    public String toString() {
        return "{" +
                "key=" + key +
                ", Value=" + Value +
                '}';
    }
}
