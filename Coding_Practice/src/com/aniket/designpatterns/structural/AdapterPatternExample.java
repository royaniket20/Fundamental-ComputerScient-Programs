package com.aniket.designpatterns.structural;

interface Bird{
	public void fly();
	public void makeSound();
	
}

class RealCrow implements Bird{

	@Override
	public void fly() {
		System.out.println("--Fly fly--");
	}

	@Override
	public void makeSound() {
		System.out.println("---KA KA---");
		
	}
	
}

interface Toy{
	public void quack();
}


class ToyCrow implements Toy{

	@Override
	public void quack() {
		System.out.println("**** quack quack*****");
		
	}
	
}

//Adapter pattern
class ToyAdapter implements Toy{

	private Bird bird;
	
	public ToyAdapter(Bird bird)
	{
		this.bird=bird;
	}
	
	@Override
	public void quack() {
	bird.makeSound();	
	}
	
}



public class AdapterPatternExample {
	
	
	public static void main(String[] args) {
		
	Toy toy = new ToyCrow();
	toy.quack();
	
	Bird bird = new RealCrow();
	bird.fly();
	bird.makeSound();
	
	//Now lets say I want to make Real Bird to quack
	//Need to create adapter
	ToyAdapter adapter = new ToyAdapter(bird);
	adapter.quack();
	
	}

}
