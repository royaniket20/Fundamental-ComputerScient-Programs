package com.aniket.designpatterns.structural;

interface CommandExecutor{
	public void executeCommand(String command);
}

class CommandExecutorImpl implements CommandExecutor
{
	@Override
	public void executeCommand(String command) {
		
		System.out.println("***Executed command****"+command);
		
	}
	
}

class CommandExecutorProxyImpl implements CommandExecutor
{

	private String username;
	private String password;
	private boolean isAdmin;
	private CommandExecutor commandExecutor;
	
	
	public CommandExecutorProxyImpl(String username, String password) {
		super();
		this.username = username;
		this.password = password;
		if(username.equals("admin") && password.equals("admin"))
		{
			isAdmin = true;
		}
		commandExecutor = new CommandExecutorImpl();
	}



	@Override
	public void executeCommand(String command) {
		
		if(command.contains("rm"))
		{
		if(isAdmin)
		{
			commandExecutor.executeCommand(command);
		}
		else
		{
			System.out.println("***not Executed command****"+command);
		}
			
		}
		else
		{
			commandExecutor.executeCommand(command);
		}
	}
	
}

public class ProxyPatternExample {

	public static void main(String[] args) {
		CommandExecutorProxyImpl commandExecutor = new CommandExecutorProxyImpl("admin", "admin");
		CommandExecutorProxyImpl commandExecutor2 = new CommandExecutorProxyImpl("user", "user");
		
		commandExecutor.executeCommand("ls -a");
		commandExecutor.executeCommand("rm -rf *");
		commandExecutor2.executeCommand("ls -a");
		commandExecutor2.executeCommand("rm -rf *");
	}
}
