package com.aniket.designpatterns.structural;

import java.sql.Connection;

interface oracleInterface
{
	public Connection getOracleConnection();
}
class OracleHelper implements oracleInterface{

	@Override
	public Connection getOracleConnection() {
		System.out.println("Returning oracle connection......");
		return null;
	}
	
}

interface mysqlInterface
{
	public Connection getmysqlConnection();
}

class MySqlHelper implements mysqlInterface
{

	@Override
	public Connection getmysqlConnection() {
		System.out.println("Returning mysql connection......");
		return null;
	}
	
}


class FacadeClass{
	public static enum DBTYPE{
		ORACLE,MYSQL;
	}
	
	public Connection getConnection(DBTYPE key)
	{
	System.out.println("***Calling facade*****");
	Connection conn = null;
		switch (key) {
		case ORACLE:
			oracleInterface oracleInterface = new OracleHelper();
			conn = oracleInterface.getOracleConnection();
			break;
		case MYSQL:
			mysqlInterface mysqlInterface = new MySqlHelper();
			conn = mysqlInterface.getmysqlConnection();
			break;
		}
		
		return conn;
		
	}
	
}
public class FacadePatternExample {

	public static void main(String[] args) {
		
		//without facade pattern
		oracleInterface interface1 = new OracleHelper();
		interface1.getOracleConnection();
		mysqlInterface interface2 = new MySqlHelper();
		interface2.getmysqlConnection();
		
		
		//with facade pattern
		FacadeClass class1 = new FacadeClass();
		class1.getConnection(FacadeClass.DBTYPE.MYSQL);
		class1.getConnection(FacadeClass.DBTYPE.ORACLE);
		
		
	}
}
