package com.aniket.designpatterns.structural;

abstract class Vehicle{
	public Workshop workshop1 ;
	public Workshop workshop2;
	
	public abstract void manufacture();
	
	public Vehicle(Workshop workshop1, Workshop workshop2) {
		super();
		this.workshop1 = workshop1;
		this.workshop2 = workshop2;
	}
	
}
class Bus extends Vehicle
{ 

	public Bus(Workshop workshop1, Workshop workshop2) {
		super(workshop1, workshop2);
	}

	@Override
	public void manufacture() {
		System.out.println("***Creating Bus-----");
		workshop1.work();
		workshop2.work();
		
	}
	
}
class Bike extends Vehicle
{

	public Bike(Workshop workshop1, Workshop workshop2) {
		super(workshop1, workshop2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void manufacture() {
		System.out.println("***Creating Bike-----");
		workshop1.work();
		workshop2.work();
		
	}
	
}
interface Workshop{
	
	public void work();
	
}

class Producer implements Workshop
{

	@Override
	public void work() {
		System.out.println("****Producing*****");
	}
	
}

class Assembler implements Workshop
{

	@Override
	public void work() {
		System.out.println("****Assembling*****");
	}
	
}




public class BridgePatternExample {

	public static void main(String[] args) {
		Workshop producer = new Producer();
		Workshop assembler = new Assembler();
		
		Vehicle bus = new Bus(producer, assembler);
		Vehicle bike = new Bike(producer, assembler);
		
		bus.manufacture();
		bike.manufacture();
		
	}
}
