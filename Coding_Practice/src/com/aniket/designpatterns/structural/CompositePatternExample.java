package com.aniket.designpatterns.structural;

import java.util.ArrayList;
import java.util.List;

interface Employee
{
	public void showEmpDetails();
}

//leaf nodes
class Developer implements Employee
{

	private String name;
	private int age;
	private String designation;
	
	
	
	public Developer(String name, int age, String designation) {
		super();
		this.name = name;
		this.age = age;
		this.designation = designation;
	}



	@Override
	public void showEmpDetails() {
		
		System.out.println("Developer : "+name+"   "+age+"   "+designation);
	}
	
}

//leaf nodes
class Manager implements Employee
{

	private String name;
	private int age;
	private String designation;
	
	
	
	public Manager(String name, int age, String designation) {
		super();
		this.name = name;
		this.age = age;
		this.designation = designation;
	}



	@Override
	public void showEmpDetails() {
		
		System.out.println("Manager : "+name+"   "+age+"   "+designation);
	}
	
}

//Composite nodes
class CompanyDirectory implements Employee
{

	private List<Employee> emps;
	
	
	
	public CompanyDirectory() {
		super();
		emps = new ArrayList<Employee>();
	}

	 public void addEmp(Employee emp) {
		 emps.add(emp);
		}


	@Override
	public void showEmpDetails() {
		emps.forEach(emp->{
		emp.showEmpDetails();
		});
		
	}
	
}

public class CompositePatternExample {

	public static void main(String[] args) {
		
		Developer dev1 = new Developer("Dev1", 10, "DEV1DES");
		Developer dev2 = new Developer("Dev2", 20, "DEV2DES");
		Developer dev3 = new Developer("Dev3", 30, "DEV3DES");
		Developer dev4 = new Developer("Dev4", 40, "DEV4DES");
		
		CompanyDirectory devDir = new CompanyDirectory();
		devDir.addEmp(dev1);
		devDir.addEmp(dev2);
		devDir.addEmp(dev3);
		devDir.addEmp(dev4);
		
		Manager man1 = new Manager("man1", 10, "man1DES");
		Manager man2 = new Manager("man2", 20, "man2DES");
		Manager man3 = new Manager("man3", 30, "man3DES");
		Manager man4 = new Manager("man4", 40, "man4DES");
		
		CompanyDirectory manDir = new CompanyDirectory();
		manDir.addEmp(man1);
		manDir.addEmp(man2);
		manDir.addEmp(man3);
		manDir.addEmp(man4);
		
		CompanyDirectory whole = new CompanyDirectory();
		whole.addEmp(manDir);
		whole.addEmp(devDir);
		
		System.out.println("***************************");
		whole.showEmpDetails();
		

	}

}
