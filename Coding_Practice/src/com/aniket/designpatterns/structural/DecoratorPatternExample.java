package com.aniket.designpatterns.structural;

abstract class Pizza{

public boolean istopping1;
public boolean istopping2;
public boolean istopping3;




public Pizza(boolean istopping1, boolean istopping2, boolean istopping3) {
	this.istopping1 = istopping1;
	this.istopping2 = istopping2;
	this.istopping3 = istopping3;
}




public int getPrice()
{
	int result = 0;
	if(istopping1)
		result+=100;
	
	if(istopping2)
		result+=50;
	
	if(istopping3)
		result+=25;
	
	return result;
}
	
}


class PizzaVariant1 extends Pizza{
	

	public PizzaVariant1(boolean istopping1, boolean istopping2, boolean istopping3) {
		super(istopping1, istopping2, istopping3);
	}

	public int getprice()
	{
		return super.getPrice()+111;
	}
}

class PizzaVariant2 extends Pizza{
	

	public PizzaVariant2(boolean istopping1, boolean istopping2, boolean istopping3) {
		super(istopping1, istopping2, istopping3);
	}

	public int getprice()
	{
		return super.getPrice()+222;
	}
}


 
 

public class DecoratorPatternExample {

	public static void main(String[] args) {
		PizzaVariant1 var1 = new PizzaVariant1(true, false, true);
		PizzaVariant2 var2 = new PizzaVariant2(false, false, true);
		
		System.out.println("****pizza var1----"+var1.getPrice());
		System.out.println("****pizza var2----"+var2.getPrice());
		
	}
}
