package com.aniket.designpatterns.behaviroual;


abstract class HouseBuilder
{
	public final void buildHouse()
	{
	  buildpart1();
	  buildpart2();
	  buildpart3();
	  buildpart4();
	  buildpart5();
	  buildpart6();
	  
	}

	private void buildpart6() {
	   System.out.println("***Do house warming***");
		
	}

	private void buildpart5() {
		 System.out.println("***Do house painting***");
		
	}

	protected abstract void buildpart4();

	protected abstract void buildpart3();

	protected abstract void buildpart2();

	protected abstract void buildpart1();
	
	
}

class WoodHouseBuilder extends HouseBuilder
{

	@Override
	protected void buildpart4() {
		System.out.println("wood house part 4");
		
	}

	@Override
	protected void buildpart3() {
		System.out.println("wood house part 3");
		
	}

	@Override
	protected void buildpart2() {
		System.out.println("wood house part 2");
		
	}

	@Override
	protected void buildpart1() {
		System.out.println("wood house part 1");
		
	}
	
}

class GlassHouseBuilder extends HouseBuilder
{

	@Override
	protected void buildpart4() {
		System.out.println("glass house part 4");
		
	}

	@Override
	protected void buildpart3() {
		System.out.println("glass house part 3");
		
	}

	@Override
	protected void buildpart2() {
		System.out.println("glass house part 2");
		
	}

	@Override
	protected void buildpart1() {
		System.out.println("glass house part 1");
		
	}
	
}


public class TemplateMethodDesignPattern {

	
	public static void main(String[] args) {
		HouseBuilder builder = new GlassHouseBuilder();
		builder.buildHouse();
		
		HouseBuilder builder2 = new WoodHouseBuilder();
		builder2.buildHouse();
	}
}
