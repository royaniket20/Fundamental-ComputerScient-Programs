package com.aniket.designpatterns.behaviroual;

interface FileSystem{
	public void openFile();
	public void writeFile();
	public void closeFile();
}

class UnixFileSystem implements FileSystem
{

	@Override
	public void openFile() {
		System.out.println("Unix file open");
	}

	@Override
	public void writeFile() {
		System.out.println("Unix file write");
	}

	@Override
	public void closeFile() {
		System.out.println("Unix file close");
	}
	
}

class WindowsFileSystem implements FileSystem
{

	@Override
	public void openFile() {
		System.out.println("Windows file open");
	}

	@Override
	public void writeFile() {
		System.out.println("Windows file write");
	}

	@Override
	public void closeFile() {
		System.out.println("Windows file close");
	}
	
}

interface Command{
	public void execute();
}

class OpenFileCommand implements Command
{
	FileSystem fis;
	
	public OpenFileCommand(FileSystem fis) {
		super();
		this.fis = fis;
	}

	@Override
	public void execute() {
		fis.openFile();
	}
	
}

class WriteFileCommand implements Command
{
	FileSystem fis;
	
	public WriteFileCommand(FileSystem fis) {
		super();
		this.fis = fis;
	}

	@Override
	public void execute() {
		fis.writeFile();
	}
	
}

class CloseFileCommand implements Command
{
	FileSystem fis;
	
	public CloseFileCommand(FileSystem fis) {
		super();
		this.fis = fis;
	}

	@Override
	public void execute() {
		fis.closeFile();
	}
	
}

class FileCommandInvoker{
Command c;

public FileCommandInvoker(Command c) {
	super();
	this.c = c;
}

public void execute()
{
	c.execute();
}
	
}

class FileSystemUtil{

	public static FileSystem getDependentFileSystem()
	{
		 String osName = System.getProperty("os.name");
		 System.out.println("Underlying OS is:"+osName);
		 if(osName.contains("Windows")){
			 return new WindowsFileSystem();
		 }else{
			 return new UnixFileSystem();
		 }
	}
}

public class CommandPatternExample {

	public static void main(String[] args) {
		System.out.println("********************");
		FileSystem fis = FileSystemUtil.getDependentFileSystem();
		Command command = new OpenFileCommand(fis);
		FileCommandInvoker invoker = new FileCommandInvoker(command);
		invoker.execute();
		
		 command = new WriteFileCommand(fis);
		 invoker = new FileCommandInvoker(command);
		invoker.execute();
		
		 command = new CloseFileCommand(fis);
		 invoker = new FileCommandInvoker(command);
		invoker.execute();
				
	}
}
