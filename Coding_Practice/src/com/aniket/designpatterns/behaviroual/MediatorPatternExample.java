package com.aniket.designpatterns.behaviroual;

import java.util.ArrayList;
import java.util.List;

interface ChatMediator{
	
	public void addUser(User user);
	public void sendMessage(User user,String message);
}

interface User{
	
	public void sendMessage(String message);
	public void receiveMessage(String message);
}

class ChatMediatorImpl implements ChatMediator
{
	List<User> users;

	public ChatMediatorImpl() {
		super();
		this.users = new ArrayList<User>();
	}

	@Override
	public void addUser(User user) {
		users.add(user);
		
	}

	@Override
	public void sendMessage(User user, String message) {
		for(User u : users)
		{
			if(u!=user)
			{
				u.receiveMessage(message);
			}
		}
		
	}
	
}

class UserImpl implements User{

	private String name;
	private ChatMediator mediator;
	
	
	
	public UserImpl(String name, ChatMediator mediator) {
		super();
		this.name = name;
		this.mediator = mediator;
	}

	@Override
	public void sendMessage(String message) {
		System.out.println("Messgae sent by "+name+"----"+message);
		mediator.sendMessage(this, message);
		
	}

	@Override
	public void receiveMessage(String message) {
		System.out.println("Messgae received by "+name+"----"+message);
		
	}
	
}

public class MediatorPatternExample {

	
	public static void main(String[] args) {
	ChatMediator chatMediator = new ChatMediatorImpl();
	User user1 = new UserImpl("Aniket", chatMediator);
	User user2 = new UserImpl("Amit", chatMediator);
	User user3 = new UserImpl("Sriparna", chatMediator);
	User user4 = new UserImpl("Krishna", chatMediator);
	User user5 = new UserImpl("Tapan", chatMediator);
	chatMediator.addUser(user1);
	chatMediator.addUser(user2);
	chatMediator.addUser(user3);
	chatMediator.addUser(user4);
	chatMediator.addUser(user5);
	
	user1.sendMessage("Hii All");
	
	
	}
}
