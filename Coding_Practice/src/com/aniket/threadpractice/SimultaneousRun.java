package com.aniket.threadpractice;

class ThreadClass extends Thread
{
	private String name;
	private TragetClass obj;
	
	public ThreadClass(String name, TragetClass obj) {
		super();
		this.name = name;
		this.obj = obj;
	}




	@Override
	public void run() {
		if(name.equals("class"))
		{
			try {
				TragetClass.call();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				obj.call1();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

class TragetClass
{
	private static final Object Obj = new Object();
	public static synchronized void call() throws InterruptedException
	{
		System.out.println("Calling by thread----"+Thread.currentThread().getName());
		Thread.sleep(3000);
		System.out.println("This is a static sync methoid");
	}
	
	public static void callAgain()
	{
		synchronized (TragetClass.class) {
			System.out.println("This is a static sync methoid");
		}
	
	}
	
	public static void callYetAgain()
	{
		synchronized (Obj) {
			System.out.println("This is a static sync methoid");
		}
	
	}
	
	
	private  final Object Obj1 = new Object();
	public  synchronized void call1() throws InterruptedException
	{
		System.out.println("Calling by thread----"+Thread.currentThread().getName());
		Thread.sleep(3000);
		System.out.println("This is a non static sync methoid");
	}
	
	public  void callAgain1()
	{
		synchronized (this) {
			System.out.println("This is a non static sync methoid");
		}
	
	}
	
	public  void callYetAgain1()
	{
		synchronized (Obj1) {
			System.out.println("This is a non static sync methoid");
		}
	
	}
	
	
	
}

public class SimultaneousRun {

	public static void main(String[] args) {
		TragetClass obj1 = new TragetClass();
		TragetClass obj2 = new TragetClass();
		
		ThreadClass th1 = new ThreadClass("class", null);
		ThreadClass th2 = new ThreadClass("obj",obj1);
		ThreadClass th3 = new ThreadClass("obj",obj1); // this will wait
		
		ThreadClass th4 = new ThreadClass("obj",obj2);//this can run simultaneously as his is diff obj
		
		th1.start();
		th2.start();
		th3.start();
		th4.start();
	}
}
