package com.aniket.threadpractice;

import java.util.Random;


class ThreadA extends Thread{

	private Thread th;
	private String whomToJoin;
	private String msg;
	
	

	public ThreadA(Thread th, String whomToJoin,String msg) {
		super();
		this.th = th;
		this.msg=msg;
		this.whomToJoin = whomToJoin;
	}



	@Override
	public void run() {
		try {
			if(whomToJoin!=null)
			{
				//th.join();
			}
			Thread.sleep(new Random().nextInt(100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			System.out.print(msg+" ");
		
	}
	
}

public class SerialThreadPrint {


	
	
public static void main(String[] args) throws InterruptedException {
	
	Runnable r1 = ()->{
		
	
		
	};
	
Runnable r2 = ()->{
		
	try {
		
		Thread.sleep(new Random().nextInt(100));
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			System.out.print("Roy is ");
		
	};
	
Runnable r3 = ()->{
		
		
	try {
		
		Thread.sleep(new Random().nextInt(100));
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			System.out.println(" in Bangalore ");
		
	};
	
	
	
	Thread th1 = new Thread(r1);
	Thread th2 = new Thread(r2);
	Thread th3 = new Thread(r3);
	System.out.println("***main start****");
	th1.start();
	th1.join();
	th2.start();
	th2.join();
	th3.start();
	th3.join();
	System.out.println("***main ended****");
	
	// System.out.println("***main start****");
	// ThreadA th1 = new ThreadA(null, null, "ANIKET");
	// ThreadA th2 = new ThreadA(th1, "", "ROY IS ");
	// ThreadA th3 = new ThreadA(th2, "", "IN BANGALORE");
	// th1.start();
	// th2.start();
	// th3.start();
	// System.out.println("***main ended****");
	
	
}




	
	
}
