package com.aniket.threadpractice;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class WebCrawler implements   Runnable {

    final ExecutorService executorService;
    final Semaphore semaphore;
    final HtmlParser htmlParser;
    final AtomicInteger maxWordCount;
    final AtomicInteger totalWordCount;
    final Site site;
    final List<Future> allTaskList;

    public WebCrawler(ExecutorService executorService, Semaphore semaphore, HtmlParser htmlParser, AtomicInteger maxWordCount, AtomicInteger totalWordCount, Site site, List<Future> allTaskList) {
        this.executorService = executorService;
        this.semaphore = semaphore;
        this.htmlParser = htmlParser;
        this.maxWordCount = maxWordCount;
        this.totalWordCount = totalWordCount;
        this.site = site;
        this.allTaskList = allTaskList;
    }


    @Override
    public void run() {
        try {
            System.out.println("Now Parsing the Data for "+site+" START");
            this.semaphore.acquire();
            Integer wordCount = htmlParser.wordCount(this.site);
            this.totalWordCount.addAndGet(wordCount);
            this.maxWordCount.set(Math.max(maxWordCount.get(), wordCount));
            htmlParser.getChildUrls(this.site).stream().forEach(siteData ->{
                System.out.println("Now Scheduling Task for  "+siteData);
                Future<?> submit = this.executorService.submit(new WebCrawler(this.executorService, this.semaphore, this.htmlParser, this.maxWordCount, this.totalWordCount, siteData, this.allTaskList));
                this.allTaskList.add(submit);
            });
            this.semaphore.release();
            System.out.println("Now Parsing the Data for "+site+" END");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
