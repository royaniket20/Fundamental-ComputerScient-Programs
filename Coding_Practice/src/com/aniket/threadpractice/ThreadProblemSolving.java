package com.aniket.threadpractice;

import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author Aniket Roy
 *There are 4 threads t1,t2,t3,t4.Write the code so that t1,t2 will be executed first and only 
 *after they finish t3 and t4 will start executing.
 */

class ThreadAB extends Thread{
	CountDownLatch countDownLatch;
	String name;
	
	
	
	public ThreadAB(CountDownLatch countDownLatch, String name) {
		super();
		this.countDownLatch = countDownLatch;
		this.name = name;
	}

	public void run() {
		try {
			System.out.println("---Thread started execution - "+name);
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		countDownLatch.countDown();
		System.out.println("---Thread completed execution - "+name);
	};
	
}


class ThreadCD extends Thread{
	CountDownLatch countDownLatch;
	String name;
	
	
	
	public ThreadCD(CountDownLatch countDownLatch, String name) {
		super();
		this.countDownLatch = countDownLatch;
		this.name = name;
	}

	public void run() {
		try {
			countDownLatch.await();
			System.out.println("---Thread started execution - "+name);
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("---Thread completed execution - "+name);
	};
	
}

public class ThreadProblemSolving {

	public static void main(String[] args) {
		CountDownLatch countDownLatch = new CountDownLatch(2);
		ThreadAB th1 = new ThreadAB(countDownLatch, "T1");
		ThreadAB th2 = new ThreadAB(countDownLatch, "T2");
		ThreadCD th3 = new ThreadCD(countDownLatch, "T3");
		ThreadCD th4 = new ThreadCD(countDownLatch, "T4");
		th4.start();
		th3.start();
		th1.start();
		th2.start();
		

	}

}
