package com.aniket.threadpractice;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Multithreading question to create a web crawler. The WC should not create more than 100 threads at a time, It should only use max. 5 threads per host and use some synchronization to calculate and store the max. and total words in the web page.
 */
public class WebCrawlerDesign {
    public static final Integer MAX_THREAD_COUNT = 100;
    public static final Integer MAX_THREAD_PER_HOST = 5;
    public static final ExecutorService executorService = Executors.newFixedThreadPool(100);
    public static final List<Future> allTaskList = new ArrayList<>();
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        HtmlParser htmlParser = new HtmlParser(3);
        List<WebCrawler> suppliedUrls = Stream.of("www.google.com", "www.yahoo.com", "www.amazon.com").map(url -> {
                    Site site = new Site();
                    site.url = url;
                    site.depth = 0;
                    return site;
                }).map(data -> new WebCrawler(
                        executorService,
                        new Semaphore(MAX_THREAD_PER_HOST),
                        htmlParser,
                        new AtomicInteger(0),
                        new AtomicInteger(0),
                        data,
                        allTaskList
                )).
                collect(Collectors.toList());

        for (WebCrawler site : suppliedUrls) {
            allTaskList.add(executorService.submit(site));
        }
        Thread.sleep(2000);
        for (Future future : allTaskList) {
            future.get();
        }
        executorService.shutdown();
        while (!executorService.isTerminated()) ;
        for (WebCrawler webCrawler : suppliedUrls) {
            System.out.println(webCrawler.site.url + "===MAX===" + webCrawler.maxWordCount.get() + "===TOTAL===" + webCrawler.totalWordCount.get());
        }

    }
}

class HtmlParser {
    public final Integer MAX_DEPTH;

    HtmlParser(Integer maxDepth) {
        MAX_DEPTH = maxDepth;
    }

    public Integer wordCount(Site site) {
        Integer count = new Random().ints(100, 200).findAny().orElse(55);
        System.out.println("Visiting Url = " + site.url + " For word counting .. " + count);
        return count;
    }

    public List<Site> getChildUrls(Site site) {
        System.out.println("Visiting Site for Child Urls  - " + site);
        final List<Site> childUrls = new ArrayList<>();
        if (site.depth < MAX_DEPTH) {
            //Just sanitization
            if (!site.url.endsWith("/")) {
                site.url = site.url + "/";
            }

            int numberOfChilds = new Random().ints(3, 5).findAny().orElse(4);
            IntStream.range(1, numberOfChilds).forEach(data -> {
                Site siteData = new Site();
                siteData.url = site.url + UUID.randomUUID().toString().substring(0,8) + "/";
                siteData.depth = site.depth + 1;
                childUrls.add(siteData);
            });
        }
        System.out.println("Parsed  Children = " + childUrls);
        return childUrls;
    }
}

class Site {
    String url;
    Integer depth;

    @Override
    public String toString() {
        return "Site{" +
                "url='" + url + '\'' +
                ", depth=" + depth +
                '}';
    }
}
