package com.aniket.threadpractice;

import java.util.concurrent.atomic.AtomicInteger;

class EvenOdd extends Thread {
	AtomicInteger number;
	Object locker;
	String type;

	public EvenOdd(AtomicInteger number, Object locker, String type) {
		super();
		this.number = number;
		this.locker = locker;
		this.type = type;
	}

	@Override
	public void run() {
		try {
			
			while( number.get()<100) {
			synchronized (locker) {
				
				// for even numbers
				if ("EVEN".equals(type)) {
					while(number.get()%2!=0)
					{
						System.out.println("---Goinig to wait---"+Thread.currentThread().getName());
						locker.wait();
						System.out.println("---Waiking from wait---"+Thread.currentThread().getName());
					}
					System.out.println(	Thread.currentThread().getName()+"---"+number.getAndIncrement());
					locker.notify();
				    
				}
				// for odd numbers
				else if("ODD".equals(type)) {
					while(number.get()%2==0)
					{
						System.out.println("---Goinig to wait---"+Thread.currentThread().getName());
						locker.wait();
						System.out.println("---Waiking from wait---"+Thread.currentThread().getName());
					}
					System.out.println(	Thread.currentThread().getName()+"***"+number.getAndIncrement());
					locker.notify();
					
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			System.out.println("***end of program thread***"+Thread.currentThread().getName());
		}
	}
}

public class EvenOddAlternatePrinting {

	public static void main(String[] args) {
		System.out.println("***start main****");
		AtomicInteger number = new AtomicInteger(1);
		Object locker = new Object();
		EvenOdd even = new EvenOdd(number, locker,"EVEN");
		EvenOdd odd = new EvenOdd(number, locker, "ODD");

		
		even.start();
		odd.start();
		System.out.println("***end main****");

	}
}
