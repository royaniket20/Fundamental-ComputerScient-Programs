package com.aniket.threadpractice;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

public class SimultaneousFileReader {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		String filePath = "E:\\Music\\Latest";
		Path path = Paths.get(filePath);
		
		System.out.println("Target path = "+path.toString());
		
		WatchService service = FileSystems.getDefault().newWatchService();
		
		path.register(service, StandardWatchEventKinds.ENTRY_CREATE,StandardWatchEventKinds.ENTRY_DELETE,StandardWatchEventKinds.ENTRY_MODIFY);
		
		
		 
		while(true)
		{
			WatchKey key;
			key = service.take();
		    List<WatchEvent<?>> events = key.pollEvents();
		    events.forEach(k->{
		     System.out.println(k.kind().name()+"-----"+k.context());
		    });
		    key.reset();
		}
		
		 
	}
}
