package com.aniket.threadpractice;

import java.util.Map;

public class SyncronizedMapImplementation<K,V> {

	private Map<K,V> map;
	private Object mutex;
	public SyncronizedMapImplementation(Map<K, V> map) {
		super();
		this.map = map;
		mutex = this;
	}
	
	public void put(K key , V value)
	{
		synchronized (mutex) {
			map.put(key, value);
		}
	}
	public V get(K key)
	{
		synchronized (mutex) {
		return map.get(key);	
		}
	}
	
	
}
