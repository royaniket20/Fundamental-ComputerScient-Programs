package com.aniket.java;

 abstract class AbsctractClass {

	protected abstract void call();
	public abstract void call(int i);
	 abstract void call(boolean b);
	
	 private int i ;
	 int k;
	 public int j;
	 protected int l;
	
	public AbsctractClass() {
		//constructor can be there of abstract class.
	}

}
 
 public class MyAbsctractClass
 {
	public static void main(String[] args) {
		//AbsctractClass abs = new  AbsctractClass();//error
		AbsctractClass abs = new  AbsctractClass() {
			
			@Override
			void call(boolean b) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void call(int i) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			protected void call() {
				// TODO Auto-generated method stub
				
			}
			//good
		};
	}
 }

