package com.aniket.java;

public class ProcessingLargeFile {
    /**
     *https://www.baeldung.com/java-read-lines-large-file
     *
     *
     * 2. Reading in Memory
     * The standard way of reading the lines of the file is in memory – both Guava and Apache Commons IO provide a quick way to do just that:
     *
     * Files.readLines(new File(path), Charsets.UTF_8);
     * Copy
     * FileUtils.readLines(new File(path));
     * Copy
     * The problem with this approach is that all the file lines are kept in memory – which will quickly lead to OutOfMemoryError if the File is large enough.
     *
     *
     *
     * 3.5. Using Stream API
     * Similarly, we can use the Stream API to read and process the content of a file.
     *
     * Here, we’ll be using the Files class which provides the lines() method to return a stream of String elements:
     *
     * try (Stream<String> lines = java.nio.file.Files.lines(Paths.get(fileName))) {
     *     lines.forEach(line -> {
     *         // do something with each line
     *     });
     * }
     * Copy
     * Please note that the file is processed lazily, which means that only a portion of the content is stored in memory at a given time.
     */
}
