import java.util.HashMap;
import java.util.Map;

public class HashMapMutableKeyProblem {
  public static void main(String[] args) {
    Employee em1 = new Employee(11, "Aniket");
    Employee em2 = new Employee(11, "Aniket");
    Map<Employee , String> data = new HashMap<>();
    //Hashcode get called
    data.put(em1, "USER1");
    //Hashcode get called
    //Equals get called
    data.put(em2, "USER2");
    System.out.println(data);
    //{Employee [id=11, name=Aniket]=USER2}
    //Hashcode get called
    //Equals get called
    System.out.println(data.get(em2));
    //USER2
    //Now If we change the Id value --
    em2.id = 1111; //Now It will point ot Different Bucket 
    //So value cannot be found - as Its calcuating hashcode to different Bucket 
    // the real value belogs to different Bucket 

    //Hashcode get called
    System.out.println(data.get(em2));
    //null
    em2.id = 11;
 /**
  * Hashcode get called
Equals get called
  */
    System.out.println(data.get(em2));
    //USER2

    //Now In the case if Hashcode calculation is such that - change will not change hashcode then still this 
    //mutability will work 

    Employee em3 = new Employee(14, "Aniket");
    em3.isSpecial = true;
    data.put(em3, "USER14");
    em3.id = 15;
    System.out.println(data.get(em3)); 
    //null when special is false 
    //USER14 when special is true 

    /**
     * Hashcode Special get called
     * Hashcode Special get called
     * USER14
     */
  }
}


class Employee {
  int id;
  String name;
  Boolean isSpecial = false ;
  
  public Employee(int id, String name) {
    this.id = id;
    this.name = name;
  }
  @Override
  public String toString() {
    return "Employee [id=" + id + ", name=" + name + "]";
  }
  @Override
  public int hashCode() {
    if(isSpecial){
      System.out.println("Hashcode Special get called");
       return 100;
    }else{
      System.out.println("Hashcode get called");
      final int prime = 31;
      int result = 1;
      result = prime * result + id;
      return result;
    }
   
  }
  @Override
  public boolean equals(Object obj) {
    System.out.println("Equals get called");

    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Employee other = (Employee) obj;
    if (id != other.id)
      return false;
    return true;
  }
  
}
