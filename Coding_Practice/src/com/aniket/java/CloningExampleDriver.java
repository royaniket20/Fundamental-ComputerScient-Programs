package com.aniket.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class ChildData implements Cloneable{

	public String name = "aniket";
	public String title = "roy";
	@Override
	public String toString() {
		return "ChildData [name=" + name + ", title=" + title + "]";
	}
	public ChildData() {
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	
}

class ABC implements Cloneable
{
	public int a = 100;
	public Integer b  = new Integer(500);
	//public List<String> datas=Arrays.asList("Aniket","Roy","Sri");
	ArrayList<String> datas = new ArrayList<String>() {{
	    add("A");
	    add("B");
	    add("C");
	}};
	public ChildData child = new ChildData();
	
	@Override
	public String toString() {
		return "ABC [a=" + a + ", b=" + b + ", datas=" + datas + ", child=" + child + "]";
	}

//	@Override
//	protected Object clone() throws CloneNotSupportedException {
//		// TODO Auto-generated method stub
//		return super.clone();
//	}
	
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		ABC cloned = (ABC)super.clone();
		cloned.child = (ChildData) cloned.child.clone();
		cloned.datas = new ArrayList<String>(cloned.datas);
		return cloned;
		
	}
	
	
}
public class CloningExampleDriver {

	
	public static void main(String[] args) throws CloneNotSupportedException {
		ABC abc1 = new ABC();
		System.out.println("****"+abc1);
		ABC abc2 = (ABC) abc1.clone();
		System.out.println("****"+abc2);
		//changing in cloned object
		abc2.a=9000;
		abc2.b=-888;
		abc2.child.name="AMIT";
		
		
		System.out.println("-----"+abc1);
		System.out.println("-----"+abc2);
		
		System.out.println("now if we try to modify the list it shows unsupportedoperation error");
		//This is because not all List supports add operation
		abc1.datas.add("EXTRA");
		
		System.out.println("^^^^^^^"+abc1);
		System.out.println("^^^^^^^^"+abc2);
		
		
		//Now let's do deep copy and create abother 
		System.out.println("******************deep clopning***********");
		abc2.a=7777;
		abc2.b=777;
		abc2.child.name="RRRR";
		abc1.datas.add("RRR");
		
		System.out.println("~~~~~~~"+abc1);
		System.out.println("~~~~~~~"+abc2);
		
		
	}
}
