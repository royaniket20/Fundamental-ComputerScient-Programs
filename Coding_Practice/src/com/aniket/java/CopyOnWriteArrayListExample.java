package com.aniket.java;

import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListExample {
    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();

        // Adding elements
        list.add("Alice");
        list.add("Bob");
        list.add("Charlie");

        // Iterating using forEach
        System.out.println("Initial List: ");
        for (String name : list) {
            System.out.println(name);
        }
        System.out.println(list.toString());
        // Modifying list
        list.add("David");
        list.remove("Alice");

        System.out.println("After Modification: " + list);
        System.out.println(list.toString());

    }
}

