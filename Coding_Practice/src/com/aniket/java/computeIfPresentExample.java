package com.aniket.java;

import java.util.HashMap;
import java.util.function.BiFunction;

/**
 * If you want to apply a function on all the mappings based on it’s key and value, then compute method should be used. If there is no mapping and this method is used, value will be null for compute function.
 *
 * If the specified key is already associated with a value, the method replaces the old value with the result of the specified function.
 *
 * The syntax of the merge() method is:
 *
 * hashmap.merge(key, value, remappingFunction)
 * Here, hashmap is an object of the HashMap class.
 *
 * merge() Parameters
 * The merge() method takes 3 parameters:
 *
 * key - key with which the specified value is to be associated
 * value - value to be associated with key, if key is already associated with any value
 * remappingFunction - result to be associated with key if key is already associated with a value - It take Old Value , New Value and then return some Computed Value
 *
 *
 *Example 1: HashMap merge() to Insert New Entry
 * import java.util.HashMap;
 *
 * class Main {
 *   public static void main(String[] args) {
 *     // create an HashMap
 *     HashMap<String, Integer> prices = new HashMap<>();
 *
 *     // insert entries to the HashMap
 *     prices.put("Shoes", 200);
 *     prices.put("Bag", 300);
 *     prices.put("Pant", 150);
 *     System.out.println("HashMap: " + prices);
 *
 *     int returnedValue = prices.merge("Shirt", 100, (oldValue, newValue) -> oldValue + newValue);
 *     System.out.println("Price of Shirt: " + returnedValue);
 *
 *     // print updated HashMap
 *     System.out.println("Updated HashMap: " + prices);
 *
 *
 *
 *
 *
 * Example 2: HashMap merge() to Insert Entry with Duplicate Key
 *
 *  // create an HashMap
 *     HashMap<String, String> countries = new HashMap<>();
 *
 *     // insert entries to the HashMap
 *     countries.put("Washington", "America");
 *     countries.put("Canberra", "Australia");
 *     countries.put("Madrid", "Spain");
 *     System.out.println("HashMap: " + countries);
 *
 *     // merge mapping for key Washington
 *     String returnedValue = countries.merge("Washington", "USA", (oldValue, newValue) -> oldValue + "/" + newValue);
 *     System.out.println("Washington: " + returnedValue);
 *
 *     // print updated HashMap
 *     System.out.println("Updated HashMap: " + countries);
 *
 *
 *
 */
public class computeIfPresentExample {
    public static void main(String[] args) {
        // create an HashMap
        HashMap<String, Integer> prices = new HashMap<>();

        // insert entries to the HashMap
        prices.put("Shoes", 200);
        prices.put("Bag", 300);
        prices.put("Pant", 150);
        System.out.println("HashMap Initial : " + prices);
        BiFunction bf = new MyBiFunction1();
        // recompute the value of Shoes with 10% VAT
        int shoesPrice = prices.computeIfPresent("Shoes", bf);
        System.out.println("Price of Shoes after VAT: " + shoesPrice);

        // print updated HashMap
        System.out.println("Updated HashMap: " + prices);
        BiFunction bf2 = new MyBiFunction2();
        prices.computeIfPresent("Bag", bf2);
        System.out.println("Updated HashMap: " + prices); //Bag is vanished
        // TODO : VERY IMPORTANT - IF WE RETURN NULL FROM BIFUNCTION - MAPPING WILL BE REMOVED

    }
}
//By Function is required so that we can decide what to do with  Key and  value as both are presernt
class MyBiFunction1 implements BiFunction<String,Integer, Integer> {

    @Override
    public Integer apply(String t, Integer u) {
        return  u + 20;
    }

}

class MyBiFunction2 implements BiFunction<String,Integer, Integer> {

    @Override
    public Integer apply(String t, Integer u) {
          if(t.equals("Bag")) return null;
          else return u +30;
    }

}
