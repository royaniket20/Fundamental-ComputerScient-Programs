package com.aniket.java;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public class MultiTabWindowOperation {

	public static void inserTabs(String tab,HashMap<String, Long> tabs)
	{
		try {
			Thread.sleep(new Random().nextInt(100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tabs.put(tab, new Date().getTime());
		
	}
	
	public static void removeTabs(String tab,HashMap<String, Long> tabs)
	{
		try {
			Thread.sleep(new Random().nextInt(100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tabs.remove(tab);
		
	}
	public static String getLastVisitedTab(HashMap<String, Long> tabs)
	{
		String tab = "";
	  List<Entry<String, Long>> datas =   new ArrayList<>(tabs.entrySet());
	  Comparator<Entry<String, Long>> sorting = new Comparator<Map.Entry<String,Long>>() {
		
		@Override
		public int compare(Entry<String, Long> o1, Entry<String, Long> o2) {
			return o2.getValue().compareTo(o1.getValue());
		}
	};
	datas.sort(sorting);
	System.out.println(datas);
	
	return datas.get(0).getKey();
	}
	
	public static void main(String[] args) {
		Comparator<String> comp =(String o1, String o2)-> {
				// TODO Auto-generated method stub
				return 0;
			};
	
	HashMap<String, Long> tabs = new HashMap<>();
	inserTabs("TAB1", tabs);
	inserTabs("TAB2", tabs);
	inserTabs("TAB3", tabs);
	inserTabs("TAB1", tabs);
	inserTabs("TAB3", tabs);
	removeTabs("TAB3", tabs);
	System.out.println("******"+getLastVisitedTab(tabs));
	
	}
}
