package com.aniket.java;

class ThirdPartClass{
	private int anyInt = 100;
	private static int anyStaticInt = 200;
}
public class NestedInnerClassExample {
static int parentStaticVar = 90;

private int parentInstanceVar = 100;
public void parentInstanceMethod()
{
	System.out.println("I am parent Instance method");
}
public static void parentStaticMethod()
{
	System.out.println("I am parent Static method");
}

//Inner class or Non Static Nested class
private  class Inner{
		//static int innerStaticVar = 90; //static not allowed
		int innerInstanceVar = 900; //Allowed
	public void innerInstanceMethod()
		{
			System.out.println("Calling from Inner instance method ");
			System.out.println(innerInstanceVar);
			parentInstanceMethod(); // directly cal refer
			parentStaticMethod();//this can be referred directly
			System.out.println(parentStaticVar);
			System.out.println(parentStaticVar); //Even private Vars accessible
		}
//	static void innerStaticMethod() //Not allowed
//	{
//	}

	}

	//Nested  class or  Static Nested class
	private static class Nested{
		private static int nestedStaticVar = 90; //allowed
		private  int nestedInstanceVar = 999; //allowed

		public  void nestedInstanceMethod(){
			System.out.println("This is nested Instance method");
			System.out.println(nestedStaticVar);
			System.out.println(nestedInstanceVar);
			System.out.println(parentStaticVar);
			NestedInnerClassExample example = new NestedInnerClassExample();
			//System.out.println(parentInstanceVar); //Not allowed Directly - You need Obj reference of Parent Class
			System.out.println(example.parentInstanceVar);
		}

		public static void nestedStaticMethod(){
			System.out.println("This is nested Static  method");
			System.out.println(nestedStaticVar);
			//System.out.println(nestedInstanceVar); // Not allowed as Non static Vars cannot be referenced from static context
			System.out.println(parentStaticVar);
			NestedInnerClassExample example = new NestedInnerClassExample();
			//System.out.println(parentInstanceVar); //Not allowed Directly - You need Obj reference of Parent Class
			System.out.println(example.parentInstanceVar);
		}
	}

	//Driver class
	public static void main(String[] args) {
		NestedInnerClassExample nestedInnerClassExample = new NestedInnerClassExample(); //Object of Main parent Class
		Inner inner = nestedInnerClassExample.new Inner(); //Object of Non Static Inner class
		inner.innerInstanceMethod();
		Nested nested = new NestedInnerClassExample.Nested(); //Object of Static Nested class
		nested.nestedInstanceMethod();
		Nested.nestedStaticMethod();

	} 
	
}
