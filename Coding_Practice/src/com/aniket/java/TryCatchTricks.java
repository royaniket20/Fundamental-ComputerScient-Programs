package com.aniket.java;

import java.sql.SQLException;

public class TryCatchTricks {

	public TryCatchTricks() {
		// TODO Auto-generated constructor stub
	}
	
	public static int finallyReturn()
	{
		try {
			throw new Exception();
		} catch (Exception e) {
			return 6;
		}finally {
			return 7;
		}
	}
	
	public void runtimeMethod()
	{
		throw new RuntimeException();
	}
	
	public void Compiletile() throws Exception 
	{
		throw new SQLException();
	}
	
	public static void main(String[] args) {
		System.out.println(finallyReturn());
	}

}
