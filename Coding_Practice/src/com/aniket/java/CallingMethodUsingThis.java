package com.aniket.java;

public class CallingMethodUsingThis {

    public void call (String  data){
        System.out.println("calling Instance method overloaded ");
    }

    public void call (){
        System.out.println("calling Instance method ");
    }

    public static void callStatic(){
        System.out.println("Calling static Method");
    }

    public static void main(String[] args) {
         new CallingMethodUsingThis().instanceCaller();
        CallingMethodUsingThis.staticCaller();
    }

    public void instanceCaller(){
        this.call();;
        this.call("Aniket");
        callStatic();
    }
    public static void staticCaller(){
        callStatic();;
    }
}
