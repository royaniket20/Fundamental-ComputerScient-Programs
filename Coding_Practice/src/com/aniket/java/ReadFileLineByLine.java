package com.aniket.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFileLineByLine {

	public static void main(String[] args) throws IOException {
	String filePath = "C:\\Users\\royan\\Downloads\\Documents\\abcd.txt";
	FileReader fis = new FileReader(filePath);
	BufferedReader br = new BufferedReader(fis);
	String line = null;
//	while ((line = br.readLine())!=null) {
//		System.out.println(line.length());
//		String[] words = line.split(" ");
//		String[] data  = Stream.of(words).filter(s->s.trim().length()>0).map(k->k.trim()).toArray(String[]::new);
//		System.out.println(Arrays.toString(data));
//	}
	
	
	String[] words = "I AM ANIKET ROY HOW   ARE YOU".split(" ");
	String[] data  = Stream.of(words).filter(s->s.trim().length()>0).map(k->{
		return k.trim();}).toArray(String[]::new);
	
	
	System.out.println(Arrays.toString(data)+"-----"+data[1].length());
	
	}

	}

