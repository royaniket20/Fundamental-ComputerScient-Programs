package com.aniket.java;

class Local{
	int i = 90;
	static int j = 900;
	final int k = 9000;
	public static void m1()
	{
		System.out.println("***Parent called*****");
	}
	
	public final void m2()
	{
		
	}
	
	private static void m3()
	{
		
	}
	
	public Local getLocal()
	{
		System.out.println("---Returned local");
		return null;
	}
	
}
public class StaticAndFinalTestCases  extends Local{

	int i = 80;
	static int j = 800;
	final int k = 8000;
	
	
	//We can override parent method in child class and return a subtype but vise versa not allowed
	@Override
	public StaticAndFinalTestCases getLocal()
	{
		System.out.println("---Returned StaticAndFinalTestCases");
		return null;
	}
	
	
//	public  void m1()
//	{
//		//Parent static method cannot be overridden by instance method
//	}
	
//	public  void m2()
//	{
//		//Parent final method cannot be overridden  by instance method
//	}
	
	public  static void m3()
	{
		//this is ok 
	}
	public static void m1()
	{
		System.out.println("***Child called*****");
		//This is completely ok but it is just method hiding not overriding
	}
	
//	public final void m2()
//	{
//		//Parent final method cannot be overridden  final  instance  method
//	}
	
	public static void main(String[] args) {
		Local l = new StaticAndFinalTestCases();
		l.m1(); // as this is static method does not depend on which call instance is being referred to - It will call the caller static method
		System.out.println("-----"+l.i+"*****"+l.j+"------"+l.k); // vars depend on which object being referred
		StaticAndFinalTestCases l1 = new StaticAndFinalTestCases();
		l1.m1();
		System.out.println("-----"+l1.i+"*****"+l1.j+"------"+l1.k);
		
		l.getLocal();
		l1.getLocal();
		Local l0 = new Local();
		l0.getLocal();
		
	}

	
	
}
