package com.aniket.java;

import java.io.IOException;
import java.util.HashSet;

public class StringAmbuigity {

	public static void foo(Object o) {
		System.out.println("Object impl");
	}
	public static void foo(String s) {
		System.out.println("String impl");
	}
	
	public static void foo(StringBuffer i){
		System.out.println("StringBuffer impl");
	}
	public static void main(String[] args) {
		//foo(null);
		
		String s3 = "JournalDev";
		int start = 1;
		char end = 5;
		System.out.println(start + end);
		System.out.println(s3.substring(start, end));
		
		
		try {
			throw new IOException("Hello");
//		}catch(IOException | Exception e) { // this is error because Exception already Caught IO Exception
//			System.out.println(e.getMessage());
//		}
			
		}catch(IOException e) { // this is good because Exception hirarchy maintaineed
				System.out.println(e.getMessage());
			}
		catch (Exception e2) {
			// TODO: handle exception
		}
		
		
		HashSet shortSet = new HashSet();
		for (short i = 0; i < 100; i++) {
			shortSet.add(i);//autoboxed and short is added
			shortSet.remove(i - 1); // Trying to remove Integer although value is same Integer obj not in Set
		}
		System.out.println(shortSet.size()); // size will be 100
		
		
		
		long longWithL = 1000*60*60*24*365L;//long during evaluation
		long longWithoutL = 1000*60*60*24*365; // integer during evaluation  - so out of range - less value printed
		System.out.println(longWithL);
		System.out.println(longWithoutL);
		
		
		String a = "aniket";
		String b = "roy";
		String c = a.concat(b); // 
		System.out.println(c=="aniketroy");
		
	}
}
