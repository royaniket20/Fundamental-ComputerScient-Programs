package com.aniket.java;

/**interface AA{
  default Integer call()
  {
	  System.out.println("****AA Called");
	return null;
  }
}

interface BB{
	  default Integer call()
	  {
		  System.out.println("****BB Called");
		  return null;
	  }
	}
class CC {// this has more precedence
	public Integer call()
	  {
		  System.out.println("****CC class Called");
		  return 0;
	  }
}
public class InterfaceTestMine extends CC implements AA,BB{

	public static void main(String[] args) {
		InterfaceTestMine abc = new InterfaceTestMine();
		abc.call();

	}

}**/

interface AA{
	

	
	  default Integer call()
	  {
		  System.out.println("****AA Called");
		return null;
	  }
	}

	interface BB extends AA{
		//public void test();//same method signature diff ret type 
		  default Integer call()
		  {
			  System.out.println("****BB Called");
			  return null;
		  }
		}
	interface CC extends AA {
		//public int test(); //same method signature diff ret type 
		default Integer call() //Hiding the AA method
		  {
			  System.out.println("****CC class Called");
			  return 0;
		  }
	}
	public class InterfaceTestMine  implements CC,BB{ //dIAMOND PROBLEM

		
		public static void main(String[] args) {
			InterfaceTestMine abc = new InterfaceTestMine();
			abc.call();

		}

	@Override
	public Integer call() { //RESULUTION OF DIAMOND PROBLEM
		// TODO Auto-generated method stub
		return BB.super.call();
	}

//	@Override
//	public void test() {
//		//Now this will be a problem as we cannot override same signature method of two diff ret type 
//		// TODO Auto-generated method stub
//		
//	}

	}
