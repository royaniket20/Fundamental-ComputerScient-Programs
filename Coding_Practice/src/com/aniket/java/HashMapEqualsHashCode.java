import java.util.HashMap;
import java.util.Map;

public class HashMapEqualsHashCode {
  public static void main(String[] args) {
    Employee em1 = new Employee(11, "Aniket");
    Employee em2 = new Employee(11, "Aniket");
    Map<Employee , String> data = new HashMap<>();
    data.put(em1, "USER1");
    data.put(em2, "USER2");
    System.out.println(data);
    //When hashcode and Equals Not Implemented 
    //Output - {Employee [id=11, name=Aniket]=USER1, Employee [id=11, name=Aniket]=USER2}

    //Now Lets Implement hashcode Only 
    EmployeeHash em11 = new EmployeeHash(11, "Aniket");
    EmployeeHash em21 = new EmployeeHash(11, "Aniket");
    Map<EmployeeHash , String> data1 = new HashMap<>();
    data1.put(em11, "USER1");
    data1.put(em21, "USER2");
    System.out.println(data1);
    //Output - {Employee [id=11, name=Aniket]=USER1, Employee [id=11, name=Aniket]=USER2}
    //This is becaue Hashmap do not know it they are logically equals 
    
    //Have Both hashcode and equals Implemented 
    EmployeeHashAndEquals em12 = new EmployeeHashAndEquals(11, "Aniket");
    EmployeeHashAndEquals em22 = new EmployeeHashAndEquals(11, "Aniket");
    Map<EmployeeHashAndEquals , String> data2 = new HashMap<>();
    data2.put(em12, "USER1");
    data2.put(em22, "USER2");
    System.out.println(data2);
 //Now correct Output - {Employee [id=11, name=Aniket]=USER2}
  }
}

class EmployeeHashAndEquals {
  int id;
  String name;
  
  public EmployeeHashAndEquals(int id, String name) {
    this.id = id;
    this.name = name;
  }
  @Override
  public String toString() {
    return "Employee [id=" + id + ", name=" + name + "]";
  }
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
      EmployeeHashAndEquals other = (EmployeeHashAndEquals) obj;
    if (id != other.id)
      return false;
    return true;
  }
  
}

class EmployeeHash {
  int id;
  String name;
  
  public EmployeeHash(int id, String name) {
    this.id = id;
    this.name = name;
  }
  @Override
  public String toString() {
    return "Employee [id=" + id + ", name=" + name + "]";
  }
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }
}

class Employee {
  int id;
  String name;
  
  public Employee(int id, String name) {
    this.id = id;
    this.name = name;
  }
  @Override
  public String toString() {
    return "Employee [id=" + id + ", name=" + name + "]";
  }
  
}
