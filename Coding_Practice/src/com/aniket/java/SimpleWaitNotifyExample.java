package com.aniket.java;


//This will be our Monitor object
class Message
{
	private String msg;
	public Message(String msg)
	{
		this.setMsg(msg);
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	
}

class Waiter implements Runnable{

	private Message msg;
	public Waiter(Message msg)
	{
		this.msg = msg;
	}
	
	@Override
	public void run() {
		System.out.println("Thread started-----"+Thread.currentThread().getName());
		synchronized (msg) {
		try {
			System.out.println("Thread going to wait state-----"+Thread.currentThread().getName());
			msg.wait();
			System.out.println("Thread coming back from wait state-----"+Thread.currentThread().getName());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		System.out.println("Thread Ended-----"+Thread.currentThread().getName());
		
	}
	
}


class Notifier implements Runnable
{

	private Message msg;
	public Notifier(Message msg)
	{
		this.msg = msg;
	}
	@Override
	public void run() {
		System.out.println("Thread started-----"+Thread.currentThread().getName());
		synchronized (msg) {
		try {
			Thread.sleep(1000);
			System.out.println("Thread going to notify one/many  mutex thread -----"+Thread.currentThread().getName());
			//msg.notify();
			msg.notifyAll();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		System.out.println("Thread Ended-----"+Thread.currentThread().getName());
		
	}
	
}



public class SimpleWaitNotifyExample {

	public static void main(String[] args) {
		
	
 	Message msg = new Message("Aniket");
	new Thread(new Waiter(msg), "Waiter1").start();
	new Thread(new Waiter(msg), "Waiter2").start();
	new Thread(new Waiter(msg), "Waiter3").start();
	new Thread(new Waiter(msg), "Waiter4").start();
	new Thread(new Waiter(msg), "Waiter5").start();
	new Thread(new Waiter(msg), "Waiter6").start();
	new Thread(new Notifier(msg), "Notifier").start();
	
	}
}
