package com.aniket;


import java.util.Comparator;
import java.util.Queue;

/**
 * Stack
 *            Implement PUSH, POP, TOP in O(1)
 *            Implement getMin() in O(1)
 *
 */
public class Arc {
    public static void main(String[] args) {
        Comparator<Integer> integerComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        Stack<Integer ,Comparator<Integer>> comparatorStack = new Stack<>(10,integerComparator);
        comparatorStack.push(5);
        comparatorStack.push(7);
        comparatorStack.push(1);
        comparatorStack.push(3);
        comparatorStack.push(6);
        comparatorStack.push(9);
        comparatorStack.push(-22);
        comparatorStack.push(100);
        comparatorStack.pop();
        comparatorStack.pop();
        System.out.println(comparatorStack.top()+"====="+ comparatorStack.getMin());
    }



}

class Stack<T , U extends Comparator<T>> {
    final Object att[];
    private T minVal;
    private int top = 0;
    private T topVal = null;

    private Comparator<T> customCompare;
    public Stack(int size , Comparator<T> customCompare ) {
        this.att = new Object [size];
        this.customCompare = customCompare;

    }


    //Push
    public void push(T t){
        att[top] = t;
        if(topVal == null){
            minVal = t;
        }
        else if(customCompare.compare(t,topVal)==0){
            minVal = t;
        } else if(customCompare.compare(t,topVal)>0){
            minVal = topVal;
        }else {
            minVal = t;
        }
        top++;
        topVal = t;
    }

    //Pop
    public T pop(){
         if(topVal == minVal){
           //calculate new Min
             for (int i = 0; i < top-1; i++) {
                 if(customCompare.compare(minVal,(T)att[i])>0){
                     minVal = (T)att[i];
                 }
             }
         }
        T data =  (T)att[top];
         top--;
         topVal = (T)att[top];
         return data;
    }
    //Top
    public T top(){
       return (T)att[top-1];
    }

    //GetMin

    public T getMin(){
       return minVal;
    }
}
