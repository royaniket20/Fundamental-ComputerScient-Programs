package com.aniket.problemsolving;

import java.util.HashMap;
import java.util.Map;

public class ApacheLog {

  /**
   * Given a log file, return IP address(es) which accesses the site most often.
   */

  public static String findTopIpaddress(String[] lines) {
	  
	  HashMap<String,Integer> data = new HashMap<>();
	  
	  
	  for(int i = 0 ; i<lines.length;i++)
	  {
		  String curr = lines[i];
		  curr = curr.substring(0,curr.indexOf(" - log entry"));
		  if(data.containsKey(curr))
		  {
			  data.put(curr, data.get(curr)+1);
		  }
		  else
		  {
			  data.put(curr,1);
		  }
	  }
	  String result = "";
	  int count = 0 ;
	  for(Map.Entry<String,Integer> entry : data.entrySet())
	  {
		int currCount = entry.getValue();
		if(currCount>count)
		{
			result = entry.getKey();
		}
		
	  }
	  
	  
    // Your code goes here
    return result;
  }
  
     
 
  
  public static void main(String[] args) {
    
String lines[] = new String[] {
        "10.0.0.1 - log entry 1 11",
        "10.0.0.1 - log entry 2 213",
        "10.0.0.2 - log entry 133132" };
    String result = findTopIpaddress(lines);
    
    if (result.equals("10.0.0.1")) {
      System.out.println("Test passed");
      
    } else {
      System.out.println("Test failed");
      
    }  

  }
  
}