package com.aniket.problemsolving;

import java.util.Arrays;

public class CountLengthOfCycle {

  /**
   *
   * You have an integer array.
   * Starting from arr[startIndex], follow each element to the index it points to. 
   * Continue to do this until you find a cycle. 
   * Return the length of the cycle. If no cycle is found return -1
   *
  */
  public static int countLengthOfCycle( int[] arr, int startIndex ) {
	int result = -1;
	  if(arr.length==0)
		  return result;
	  
	  int initialIndex = startIndex;
	  int cycleLenCount = 0;
	 
	  int Counterarr[] = new int[arr.length];
	  System.out.println(Arrays.toString(arr)+"-----"+Arrays.toString(Counterarr));
	  while(Counterarr[initialIndex]<=2)
	  {
		 
		  Counterarr[initialIndex] =Counterarr[initialIndex]+1;
		  initialIndex = arr[initialIndex];
	  }
	  System.out.println(Arrays.toString(arr)+"-----"+Arrays.toString(Counterarr));

	  for(int y = 0 ;y<Counterarr.length;y++)
	  {
		  if(Counterarr[y]==3)
		  {
			  cycleLenCount++;
		  }
	  }
	  result = cycleLenCount;
	  
	
  
  return result;
  }


  public static void main( String[] args ) {

 boolean testsPassed = true;
  
  testsPassed &= countLengthOfCycle(new int[]{1, 0}, 0) == 2;
  testsPassed &= countLengthOfCycle(new int[]{1, 2, 0}, 0) == 3;
  testsPassed &= countLengthOfCycle(new int[]{1, 3, 0, 4, 1}, 0) == 3;
  
  if(testsPassed) {
    System.out.println( "Test passed." );
    //return true;
  } else {
    System.out.println( "Test failed." );
    //return false;
  }


  }
}