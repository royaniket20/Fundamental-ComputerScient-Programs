package com.aniket.problemsolving;

public class SquareRoot {
  /*
  *   double squareRoot( double x )
  *
  */

  public static double squareRoot( double x )
  {
	  System.out.println("----called----"+x);
	int precision = 5;
	int start = 0 ; 
	int end = (int)x;
	int mid = 0;
	int input = (int) x;
	double result = 0;
	while(start<=end)
	{
	
		mid = (start+end)/2;
		System.out.println("---start--"+start+"----mid---"+mid+"---end---"+end);
		
		//perfect square
		if(mid*mid==input)
		{
			result = mid;
			break;
		}
		
		if(mid*mid>input)
		{
			end = mid-1;
			
		}
		else
		{
			start = mid+1;
			result = mid;//at least integral part is covering
		}
		
		
		
	}
	System.out.println("----result---"+result);
	
	//now let's do the fractioanl part
	double increment = 0.1;
	for(int i = 0 ; i<precision;i++)
	{
		while(result*result<=x)
		{
			result = result+increment;
		}
		result = result-increment;
		increment = increment/10;
	}
	
	System.out.println("----result---"+result);
	
    return result;
  }

  public static void main( String args[])
  {
    double[] inputs = {2, 4, 100};
    double[] expected_values = { 1.41421, 2, 10 };
    double threshold = 0.001;
    for(int i=0; i < inputs.length; i++)
    {
      if( Math.abs(squareRoot(inputs[i])-expected_values[i])>threshold )
      {
        System.out.printf( "Test failed for %f, expected=%f, actual=%f\n", inputs[i], expected_values[i], squareRoot(inputs[i]) );
      }
    }
    System.out.println( "All tests passed");
  }
}