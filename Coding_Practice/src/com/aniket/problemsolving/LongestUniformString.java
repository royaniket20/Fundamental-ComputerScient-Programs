
package com.aniket.problemsolving;
/**
 *  
 *  e.g.
 *      for the input: "abbbccda" the longest uniform substring is "bbb" (which starts at index 1 and is 3 characters long).
 */

import java.util.*;

public class LongestUniformString {

  private static final Map<String, int[]> testCases = new HashMap<String, int[]>();

  static int[] longestUniformSubstring(String input){
    int longestStart = -1;
    int longestLength = 0;
    
    if(input.length()>0) {
    int initialIndexOfresult = 0;
    char initialChar = input.charAt(initialIndexOfresult);
    int resultLen = 1;
    
    for(int i = 1 ; i<input.length();i++)
    {
    	char curr = input.charAt(i);
    	

    	if(curr==initialChar)
    	{
    		resultLen++;
    		
    	}
    	else
    	{
    		//reset things
    		  initialIndexOfresult = i;
    		  initialChar = input.charAt(i);
    		  resultLen = 1;
    		
    	}
    	
    	if(resultLen>longestLength)
    	{
    		longestLength = resultLen;
    		longestStart = initialIndexOfresult;
    		
    	}
    }
    
    System.out.println(longestLength+"------"+longestStart);
    }
    // your code goes here
    return new int[]{ longestStart, longestLength };
  }

  public static void main(String[] args) {
    testCases.put("", new int[]{-1, 0});
    testCases.put("10000111", new int[]{1, 4});
    testCases.put("aabbbbbCdAA", new int[]{2, 5});
    testCases.put("aabbbbbCdddRRRRRRRRRRRRRAA", new int[]{11, 13});
    

    boolean pass = true;
    for(Map.Entry<String,int[]> testCase : testCases.entrySet()){
      int[] result = longestUniformSubstring(testCase.getKey());
      pass = pass && (Arrays.equals(result, testCase.getValue()));
    }
    if(pass){
      System.out.println("Pass!");
    } else {
      System.out.println("Failed! ");
    }
  }
}