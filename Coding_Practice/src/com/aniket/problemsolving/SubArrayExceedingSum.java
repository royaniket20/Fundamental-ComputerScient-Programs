package com.aniket.problemsolving;

public class SubArrayExceedingSum
{
  public static int subArrayExceedsSum(int arr[], int target )
  {
	  System.out.println("****called here*******");
	  int result = Integer.MAX_VALUE;
	  int minimumLength = 0;
	  int sum = 0 ;
	  for(int i = 0 ; i<arr.length;i++)
	  {
		  sum = arr[i];
		  minimumLength = 1;
		  System.out.println(target+"----sum----"+sum);
		  for(int j = i+1;j<arr.length;j++)
		  {
			  if(sum>target)
			  {
				  break;
				 
			  }
			  else
			  {
				  sum = sum + arr[j];
				  minimumLength++;
			  }
			  System.out.println(target+"***sum***"+sum);
		  }
		  if(minimumLength<result && sum>target)
		  {
			  result = minimumLength;
		  }
		  System.out.println(result+"----min----"+minimumLength);
		  
	  }
	  if(result==Integer.MAX_VALUE)
	  {
		  result = -1;
	  }
  return result;
  }

  /**
   * Execution entry point.
  */
  public static void main(String[] args)
  {
   boolean result = true; 
  int[] arr = { 1, 2, 3, 4 };
  result = result && subArrayExceedsSum( arr, 6 ) == 2;
 result = result && subArrayExceedsSum( arr, 12 ) == -1;

  if( result )
  {
    System.out.println("All tests pass\n");
  }
  else
  {
    System.out.println("There are test failures\n");
  }
  }
};