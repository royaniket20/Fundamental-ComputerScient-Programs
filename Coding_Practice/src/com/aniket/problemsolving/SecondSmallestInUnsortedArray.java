package com.aniket.problemsolving;

public class SecondSmallestInUnsortedArray {

	public static void main(String[] args) {
		int arr[] = { 999, 13, 18, 19, 2, 22, 19, 81, 43, 18, 71, 11, 9 };
		System.out.println("Second smallest = " + findSecondSmallest(arr));

	}

	// -1 is default answer
	static int findSecondSmallest(int arr[]) {
		int result = -1;
		if (arr.length == 0 || arr.length == 1) {
			return result;
		} else {
			int firstSmall = arr[0];
			int secondSmall = arr[0];

			for (int i = 0; i < arr.length; i++) {
				if (firstSmall > arr[i]) {
					firstSmall = arr[i];
				}

			}
			System.out.println("fisrt one - " + firstSmall);
			int diff = Integer.MAX_VALUE;
			for (int i = 0; i < arr.length; i++) {
				if (firstSmall == arr[i])
					continue;

				if (Math.abs(arr[i] - firstSmall) < diff) {
					secondSmall = arr[i];
					diff = Math.abs(secondSmall - firstSmall);
				}

			}
			result = secondSmall;

		}
		return result;
	}
}
