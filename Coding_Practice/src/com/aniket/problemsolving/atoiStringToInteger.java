package com.aniket.problemsolving;

public class atoiStringToInteger
{
  
   // Takes a string str and returns the int value represented by
    // the string. 
	//For example, atoi("42") returns 42.
   
  public static int atoi(String str)
  {
  int result = 0;
  int strLength = str.length();
  int miunus = '-';
  int plus = '+';
  int zero = '0';
  int nine = '9';
  int intermediate = 0;
  String whichSign = "";
  System.out.println("***range is----"+miunus+"----"+plus+"----"+zero+"----to---"+nine);
 //input validation
  
  for(int i = 0; i<strLength; i++)
  {
    switch (str.charAt(i)) {
	case '-':
		whichSign = "NEGATIVE";
		break;
    case '+':
    	whichSign = "POSITIVE";
		break;

	default:
		//check for invalid numbers
		if(str.charAt(i)<zero || str.charAt(i)>nine)
		result = -1;
		if(str.indexOf('-')>0 || str.indexOf('+')>0 )
		result = -1;
		break;
	}
  }
  if(result==-1)
  	return -1;
  
  //end of input validation
  System.out.println("***Real Code****");
  for(int i = strLength-1,k=0; i >=0; i--,k++)
  {
    if(!(str.charAt(i)=='-')&& !(str.charAt(i)=='+'))
    {

        int currentChar = str.charAt(i)-zero;
    	intermediate =(int) (intermediate+(currentChar*Math.pow(10,k ))); 	
    }
 else {
    	if(whichSign.equals("NEGATIVE"))
		{
    		
			result =intermediate*-1;
		}
    }
    System.out.println("***res---"+intermediate);
    
  }

  System.out.println("--final result****"+result);
  return result;
  };

  public static boolean pass()
  {
  boolean result = true;
  result = result && (-1234 == atoi("-1234"));
  result = result && (1 != atoi("2"));

  return result;
  };

  public static void main(String[] args)
  {
  if(pass())
  {
    System.out.println("Pass");
  }
  else
  {
    System.out.println("Some fail");
  }
  }
}