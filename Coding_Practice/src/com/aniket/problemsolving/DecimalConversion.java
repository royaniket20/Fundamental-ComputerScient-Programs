package com.aniket.problemsolving;

import java.util.*;

public class DecimalConversion {
	/**
	 * Return the fraction output in the following way Examples: If after decimal,
	 * repeating numbers are there in the output . eg. 1/3=0.333333333, this should
	 * be represented as 0.(3) 6/11=0.54545454, this should be represented as 0.(54)
	 * fractionRepresentation(1,2)=0.5 fractionRepresentation(1,3)=0.(3)
	 * fractionRepresentation(6,11)=0.(54)
	 */
	public static String fractionRepresentation(int num, int den) {
		String result = "";
		
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int data = num/den;
		result =  result+data+".";
		
		int reminder= num%den;
		int fraction = 0;
		while(reminder!=0)
		{
			reminder = reminder*10;
			fraction = reminder/den;
			if(map.size()>0 && map.containsKey(fraction))
			{
				result = result.substring(0,result.lastIndexOf(fraction+""))+"("+result.substring(result.lastIndexOf(fraction+""))+")";
				break;
			}
			else
			{
				map.put(fraction,result.length());
				result = result+fraction;
				
				
			}
			reminder = reminder%den;
			
		}
		System.out.println("----result-----"+result);
		
		
		return result;
	}

	public static void main(String args[]) {
		// float f=6/11f;
		// System.out.println(f);
		System.out.println(fractionRepresentation(1, 2) + " " + fractionRepresentation(1, 3) + " "
				+ fractionRepresentation(6, 11) + "  "+ fractionRepresentation(1, 75) +"  "+ fractionRepresentation(4, 7));

		if (fractionRepresentation(1, 2).equals("0.5") && fractionRepresentation(6, 11).equals("0.(54)")
				&& fractionRepresentation(1, 3).equals("0.(3)")) {
			System.out.println("All passed");
		} else {
			System.out.println("Failed");
		}

	}
}