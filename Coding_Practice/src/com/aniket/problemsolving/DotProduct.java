package com.aniket.problemsolving;

public class DotProduct {

  /**
   *
   * Given two arrays of integers, returns the dot product of the arrays
   */

  public static int dotProduct( int[] array1, int[] array2 ) {
   
	  if(array1.length!=array2.length)
		  return -1;
	  int result = 0;
	  for(int i = 0 ; i<array1.length;i++)
	  {
		  int part1 =array1[i];
		  int part2 = array2[i];
		  result = result+part1*part2;  
	  }
    return result;
  }
 
  public static void main( String[] args ) {
   int[] array1 = { 1, 2 };
    int[] array2 = { 2, 3 };
    int result = dotProduct( array1, array2 );

    if( result == 8 ) {
      System.out.println( "Passed." );
     
    } else {
      System.out.println( "Failed." );
      
    }
  }
}