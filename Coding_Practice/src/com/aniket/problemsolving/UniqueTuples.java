package com.aniket.problemsolving;

 import java.util.Arrays;
import java.util.HashSet;

public class UniqueTuples {

  public static HashSet<String> uniqueTuples( String input, int len ) {
    // your code
	
	  input = input.toLowerCase();
	  char[] resultArr = new char[input.length()];
	  int countArr[] = new int[26];
	  char[] charArr = new char[26];
	  
	  for(int i = 0 ; i<input.length();i++)
	  {
		  char currentChar = input.charAt(i);
		  int currentIndexToStore = currentChar-'a';
		  countArr[currentIndexToStore]++;
		  charArr[currentIndexToStore] = currentChar;
	  }
	  
	  System.out.println("*****"+Arrays.toString(charArr));
	  System.out.println("*****"+Arrays.toString(countArr));
	
	 //permuteUtils(charArr,countArr,resultArr,0);
	 // comuteUtils(charArr,countArr,resultArr,0,0);
	  
    HashSet<String> result = new HashSet<String>();
    comuteUtilsModified(len,result,charArr, countArr, resultArr,0,0);
    //result.add( "aa" );
    //result.add( "ab" );
    System.out.println("-----"+result);
    return result;
  }

  private static void permuteUtils(char[] charArr, int[] countArr, char[] resultArr, int level) {
	
	  if(level==resultArr.length)
	  {
		  System.out.println("--"+Arrays.toString(resultArr));
		  return;
	  }
	  
	  for(int i = 0 ; i<charArr.length;i++)
	  {
		  if(countArr[i]==0)
		  {
			  continue;
		  }
		  
		  countArr[i]--;
		  resultArr[level] =charArr[i];
		  permuteUtils(charArr, countArr, resultArr, level+1);
		 
		  countArr[i]++;
		  
		  
	  }
	
}

  private static void comuteUtils( char[] charArr, int[] countArr, char[] resultArr, int level, int currentPos) {
	  System.out.println("-combination---"+Arrays.toString(resultArr));
	  for(int i = currentPos;i<charArr.length;i++)
	  {
		  if(countArr[i]==0)
		  {
			  continue;
		  }
		  countArr[i]--;
		  resultArr[level] = charArr[i];
		  comuteUtils(charArr, countArr, resultArr, level+1, i);
		  countArr[i]++;
		  resultArr[level]=Character.MIN_VALUE;;
	  }
	
}
  private static void comuteUtilsModified(int len, HashSet<String> result, char[] charArr, int[] countArr, char[] resultArr, int level, int currentPos) {
	  System.out.println("-combination---"+Arrays.toString(resultArr));
	  String str = new String(resultArr);
	  if(str.trim().length()==len)
	  {
		  result.add(str.trim());
	  }
	  for(int i = currentPos;i<charArr.length;i++)
	  {
		  if(countArr[i]==0)
		  {
			  continue;
		  }
		  countArr[i]--;
		  resultArr[level] = charArr[i];
		  comuteUtilsModified(len, result, charArr, countArr, resultArr, level+1, i);
		  countArr[i]++;
		  resultArr[level]=Character.MIN_VALUE;;
	  }
	
}

public static void main( String[] args ) {
    String input = "aab";
    HashSet<String> result = uniqueTuples( input, 2 );
    if( result.contains( "aa" ) && result.contains( "ab" ) ) {
      System.out.println( "Test passed." );
     
    } else {
      System.out.println( "Test failed." );
      
    }
  }
}