package com.aniket.problemsolving;

import java.util.Arrays;

public class AddFraction {

  /**
   * Given two fractions passed in as int arrays,
   * returns the fraction which is result of adding the two input fractions.
   */
  public static int[] addFractions( int[] fraction1, int[] fraction2 ) {
	  int res[] = new int[2];
    int num1 = fraction1[0];
    int den1 = fraction1[1];
    
    int num2 = fraction2[0];
    int den2 = fraction2[1];
    
    //Do the 
    int den3 = LCM(den1,den2);
   
    res[1] = den3;
    res[0] = ((den3/den1)*num1)+((den3/den2)*num2);
    
    
    
    System.out.println("----result----"+Arrays.toString(res));
    return res;
  }
  
  public static int LCM(int i , int j)
  {
	int max = Math.max(i, j);
	int min = Math.min(i, j);
	
	for(int k = max;;k = k + max)
	{
		if(k%min==0)
			return k;
	}
  }

  
  public static void main( String[] args ) {
    int[] result = addFractions( new int[]{ 2, 3 }, new int[]{ 1, 2 } );

    if( result[ 0 ] == 7 && result[ 1 ] == 6 ) {
      System.out.println( "Test passed." );
     
    } else {
      System.out.println( "Test failed." );
     
    }
  }
}