package com.aniket.problemsolving;

import java.util.Arrays;

/* Problem Name is &&& Snowpack &&& PLEASE DO NOT REMOVE THIS LINE. */

/*
** Instructions to candidate.
**  1) Given an array of non-negative integers representing the elevations
**     from the vertical cross section of a range of hills, determine how
**     many units of snow could be captured between the hills. 
**
**     See the example array and elevation map below.
**                                 ___
**             ___                |   |        ___
**            |   |        ___    |   |___    |   |
**         ___|   |    ___|   |   |   |   |   |   |
**     ___|___|___|___|___|___|___|___|___|___|___|___
**     {0,  1,  3,  0,  1,  2,  0,  4,  2,  0,  3,  0}
**                                 ___
**             ___                |   |        ___
**            |   | *   *  _*_  * |   |_*_  * |   |
**         ___|   | *  _*_|   | * |   |   | * |   |
**     ___|___|___|_*_|___|___|_*_|___|___|_*_|___|___
**     {0,  1,  3,  0,  1,  2,  0,  4,  2,  0,  3,  0}
**
**     Solution: In this example 13 units of snow (*) could be captured.
**  
**  2) Consider adding some additional tests in doTestsPass().
**  3) Implement computeSnowpack() correctly.
*/



class Snowpack
{
  /*
  **  Find the amount of snow that could be captured.
  */
  public static Integer computeSnowpack(Integer[] arr)
  {
	  int leftMax[] = new int[arr.length];
	  int rightMax[] = new int[arr.length];
	  
	  int max = 0 ;
	  for(int i = 0 ; i<arr.length;i++)
	  {
		  if(arr[i]>max)
		  {
			  max = arr[i];
		  }
		  leftMax[i] = max;
	  }
	  
	  max = 0 ;
	  for(int i =arr.length-1 ; i>=0;i--)
	  {
		  if(arr[i]>max)
		  {
			  max = arr[i];
		  }
		  rightMax[i] = max;
	  }
	  
	  System.out.println("--left buildings---"+Arrays.toString(leftMax));
	  System.out.println("--right buildings---"+Arrays.toString(rightMax));
	  int result = 0 ;
	  for(int i = 0 ; i<arr.length;i++)
	  {
		int leftBuilding = leftMax[i];
		int rightBuilding = rightMax[i];
		int water = Math.min(leftBuilding, rightBuilding)-arr[i];
		result = result+water;
	  }
	  
	  
  // Todo: Implement compute Snowpack
  return result;
  }

  /*
  **  Returns true if the tests pass. Otherwise, returns false;
  */
  public static boolean doTestsPass()
  {
  boolean result = true;
  result &= computeSnowpack(new Integer[]{0,1,3,0,1,2,0,4,2,0,3,0}) == 13;
  
  return result;
  }

  /*
  **  Execution entry point.
  */
  public static void main(String[] args)
  {
  if(doTestsPass())
  {
    System.out.println("All tests pass");
  }
  else
  {
    System.out.println("Tests fail.");
  }
  }
}