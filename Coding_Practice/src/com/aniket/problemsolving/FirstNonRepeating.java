package com.aniket.problemsolving;

import java.util.Arrays;

public class FirstNonRepeating {

  /**
  * Finds the first character that does not repeat anywhere in the input string
  * Given "apple", the answer is "a"
  * Given "racecars", the answer is "e"
  **/        
  public static char findFirst(String input)
  {
	 
		char result = 0 ;
		int[] charArray = new int[256];
		
		for(int i = 0 ; i<input.length();i++)
			{
			   int thisChar = input.charAt(i); 
			   charArray[thisChar]++;
			   
			}
		for(int i = 0 ; i<input.length();i++)
		{
		   int thisChar = input.charAt(i); 
		   System.out.println("***thisChar***"+(char)thisChar+"----"+charArray[thisChar]);
		   if(charArray[thisChar]==1)
		   {
			   result =(char) thisChar; 
			   break;
		   }
		   
		}
		System.out.println("----charArray----"+Arrays.toString(charArray));
		System.out.println("***Result***"+result);
    return result;
  }

  public static void main(String args[])
  {

    String[] inputs = {"apple","racecars", "ababdc"};
    char[] outputs = {'a', 'e', 'd' };

    boolean result = true;
    for(int i = 0; i < inputs.length; i++ )
    {
      result = result && findFirst(inputs[i]) == outputs[i];
      if(!result)
        System.out.println("Test failed for: " + inputs[i]);
      else
        System.out.println("Test passed for: " + inputs[i]);
    }
  }

}
