package com.aniket.problemsolving;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GroupAnagramsTogether {
	 static String input = "cat dog tac sat tas god dog";
	  
	  static void setOfAnagrams(String inputString){ 
	    
	   String arr[] = inputString.split(" ");
	  HashMap<String, List<String>> anagramMap = new HashMap<>();
	  for(int i = 0 ;i<arr.length;i++)
	  {
	    String data = arr[i];
	   char arrC[] = data.toCharArray();
	   Arrays.sort(arrC);
	   String key = new String(arrC);
	   if(anagramMap.containsKey(key))
	   {
		   List<String> WordSet = anagramMap.get(key);  
		   WordSet.add(data);
	   }
	   else
	   {
		   List<String> WordSet = new ArrayList<>();
		   WordSet.add(data);
		   anagramMap.put(key, WordSet);
		   
	   }
	   
	  }
	  
	  System.out.println("***final output -----"+anagramMap);
	    
	    
	  }
	  
	  
	  public static void main(String[] args) {
	    
	    String input = "cat dog tac sat tas god dog";
	    setOfAnagrams(input);
	    
	  }
}
