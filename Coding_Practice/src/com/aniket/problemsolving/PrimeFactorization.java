package com.aniket.problemsolving;

import java.util.ArrayList;
import java.util.Arrays;

public class PrimeFactorization
{
	/**
	   * Return an array containing prime numbers whose product is x
	   * Examples:
	   * primeFactorization( 6 ) == [2,3]
	   * primeFactorization( 5 ) == [5]
	   */
  public static ArrayList<Integer> primeFactorization(int x)
  {
	  ArrayList<Integer> results = new ArrayList<Integer>();
	  
	  //clear out evens first
	  while(x%2==0)
	  {
		  results.add(2);
		  x = x/2;
	  }
	  
	  for(int i = 3 ; i<=Math.sqrt(x);i=i+2)
	  {
		  while(x%i==0)
		  {
			  results.add(i);
			  x = x/i;  
		  }
	  }
	  
	  if(x>2)
	  {
		  results.add(x);
	  }
	  
	  return results;
  }

public static void main(String args[])
  {
	
	System.out.println(primeFactorization(6) + " " + primeFactorization(5)+"  "+primeFactorization(315)); 
	if(primeFactorization(6).equals(Arrays.asList(2,3))
			&& 
			primeFactorization(5).equals(Arrays.asList(5))
			&& 
					primeFactorization(315).equals(Arrays.asList(3,3,5,7))
			) {
		System.out.println("All passed");
	}else {
		System.out.println("Failed");
	}
  
  }
}