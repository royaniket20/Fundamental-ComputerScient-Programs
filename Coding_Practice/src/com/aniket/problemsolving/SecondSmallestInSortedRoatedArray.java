package com.aniket.problemsolving;

import java.util.Arrays;

public class SecondSmallestInSortedRoatedArray {

	public static void main(String[] args) {
		int arr[] = { 5, 6, 7, 8, 9, -8, -3, -2, -1, 0, 1, 2, 3, 4 };
		System.out.println("Second smallest in sorted rotated = " + findSecondSmallest(arr));

	}

	// -1 is default answer
	static int findSecondSmallest(int arr[]) {
		int result = -1;
		if (arr.length == 0 || arr.length == 1) {
			return result;
		} else {
			// find the index of pivot element
			int index = 0;
			int firstElement = arr[index];
			for (int k = 0; k < arr.length; k++) {
				if (arr[k] >= firstElement) {
					firstElement = arr[k];
				} else {
					index = k;
					break;
				}
			}
			System.out.println("---Pivot position - " + index);
			// now rotate the array clock wise or anti clock wise based on position of pivot
			// for now just do normal rotation

			if (index != 0) {
				rotateArr(arr, index);
			} else {
				System.out.println("***Arr is already non rotated***");
			}
			System.out.println("****main arr content****" + Arrays.toString(arr));
			result = arr[1];
		}
		return result;
	}

	private static void rotateArr(int[] arr, int index) {

		int tempArr[] = new int[arr.length - index];
		System.out.println("---temp arr size ---" + tempArr.length);
		int k = 0;
		for (int i = index; i < arr.length; i++) {
			tempArr[k++] = arr[i];
		}
		System.out.println("****temp arr content****" + Arrays.toString(tempArr));
		int l = 1;
		for (k = index - 1; k >= 0; k--) {
			arr[arr.length - l++] = arr[k];
		}
		for (k = 0; k < tempArr.length; k++) {
			arr[k] = tempArr[k];
		}
	}
}
