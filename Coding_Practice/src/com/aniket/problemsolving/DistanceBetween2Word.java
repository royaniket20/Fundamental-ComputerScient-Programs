package com.aniket.problemsolving;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class DistanceBetween2Word {

  
   // Input two words returns the shortest distance between their two midpoints in number of characters
   // Words can appear multiple times in any order and should be case insensitive.
   
   // E.g. for the document="Example we just made up"
   //   shortestDistance( document, "we", "just" ) == 4
   
	//default answer is -1
  public static double shortestDistance(String document, String word1, String word2) {
    double shortest = document.length();

    if(document.toUpperCase().indexOf(word1.toUpperCase())==-1 || document.toUpperCase().indexOf(word2.toUpperCase())==-1 )
    {
    	return -1d;
    }
    
    if(word1.toUpperCase().equals(word2.toUpperCase()))
    {
        return 0d;
    }
    
    int wordLen1 = word1.length();
    int wordLen2 = word2.length();
    Map<String,Set<Integer>> dataSet = new HashMap<>();
    dataSet.put(word1, new TreeSet<Integer>());
    dataSet.put(word2, new TreeSet<Integer>());
    
    int pos1 = 0,pos2 = 0;
    while(pos1<=document.length()-1 || pos2<=document.length()-1)
    {
    	int word1Index =document.toUpperCase().indexOf(word1.toUpperCase(), pos1) ;
    	int word2Index =document.toUpperCase().indexOf(word2.toUpperCase(), pos2) ;
    	if(word1Index!=-1)
    	{
    		dataSet.get(word1).add(word1Index)	;
    	}
    	if(word2Index!=-1)
    	{
    		dataSet.get(word2).add(word2Index)	;
    	}
    	pos1 = word1Index+wordLen1;
    	pos2 = word2Index+wordLen2;
    	if(word1Index==-1 && word2Index==-1)
    	{
    		break;
    	}
    	
    }
   
  System.out.println("****"+dataSet);
  TreeMap<Integer, String> finalSet = new TreeMap<>();
  for(Map.Entry<String,Set<Integer>> entry :dataSet.entrySet() )
  {
	  Set<Integer> pos = entry.getValue();
	  String word = entry.getKey();
	  for(int i:pos)
	  {
		  finalSet.put(i, word);    
	  }
  }
  
  System.out.println("****"+finalSet);
  int minDistance =Integer.MAX_VALUE;
  String currWord ="";
  int currPos = -1;
  int Firstcount = 0;
  for(Map.Entry<Integer, String> entry :finalSet.entrySet() )
  {
	 
	  
	  Integer pos = entry.getKey();
	  String word = entry.getValue();
	  if(Firstcount==0)
	  {
		  Firstcount++;
		  currPos = pos;
		  currWord =word; 
		  continue;
	  }
	  
	  if(word.equalsIgnoreCase(currWord))
	  {
		  currPos = pos;
		  continue;
	  }
	  else
	  {
		currWord = word; 
		if(Math.abs(pos-currPos)<minDistance)
		  {
			  minDistance = Math.abs(pos-currPos);
			 
		  }
		 currPos = pos;
		
	  }
       
	 
	  
		  
	 
  }
  
  minDistance = minDistance-wordLen1/2+wordLen2/2;
  System.out.println("----minDistance----"+minDistance);
 
  shortest = minDistance;
    return shortest;
  }

  public static boolean pass() {
    return  shortestDistance(document, "and", "graphic") == 10d &&
        shortestDistance(document, "transfer", "it") == 14d &&
        shortestDistance(document, "Design", "filler" ) == 25d ;
  }

  public static void main(String[] args) {
    if (pass()) {
      System.out.println("Pass");
    } else {
      System.out.println("Some Fail");
    }
  }

  private static final String document;
  static{
    StringBuffer sb = new StringBuffer();
    sb.append("In publishing and this is and hue graphic design, lorem ipsum is a filler text commonly used to demonstrate the graphic elements");
    sb.append(" lorem ipsum text has been used in typesetting since the 1960s or earlier, when it was popularized by advertisements");
    sb.append(" for Letraset transfer sheets. It was introduced to the Information Age in the mid-1980s by Aldus Corporation, which");

    document = sb.toString();
  }
}