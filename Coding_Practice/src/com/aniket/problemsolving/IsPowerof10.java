package com.aniket.problemsolving;

public class IsPowerof10
{
  /**
   * Returns true if x is a power-of-10. 
   */
  public static boolean isPowerOf10(int x)
  {
	  boolean res = false;
 while(x>1)
 {
	 if(x%10==0)
	 {
		 System.out.println(x);
		 x = x/10;
	 }
	 else
	 {
		break;
	 }
 }
 System.out.println(x);
if(x==1)
{
	res = true;
}
  return res;
  }

  public static boolean doTestsPass()
  {
  int[] isPowerList = {100000,10000,100,100000000,1000};
  int[] isNotPowerList = {3,103,10005,1000105,11000};

  for(int i : isPowerList)
  {
    if(!isPowerOf10(i))
    {
    System.out.println("Test failed for: " + i);
    return false;
    }
  }

  for(int i : isNotPowerList)
  {
    if(isPowerOf10(i))
    {
    System.out.println("Test failed for: " + i);
    return false;
    }
  }

  System.out.println("All tested passed");
  return true;
  };


  public static void main(String args[])
  {
  doTestsPass();
  }
}