package com.aniket.problemsolving;

import java.util.Arrays;

// find the median of the two sorted arrays.
 // ex. {1, 3} and {2} is 2


public class MedianTwoSortedArrays
{

  public static double logic(int[] A, int[] B) {
int res[] = new int[A.length+B.length];
//int iterator = Math.max(A.length,B.length);
int index = 0;
int Aindex = 0;
int Bindex = 0;
double result = -1;
System.out.println("******");
while(index<=res.length)
{
	System.out.println("----A---"+Aindex+"----B----"+Bindex);
	System.out.println("----arr---"+Arrays.toString(res));
	if(Aindex==A.length && Bindex==B.length)
	{
		break;
	}
	
	if(Aindex==A.length)
	{
		res[index++] =B[Bindex++]; 
		continue;
	}
	if(Bindex==B.length)
	{
		res[index++] =A[Aindex++]; 
		continue;	
	}
	
	if(A[Aindex]<B[Bindex])
	{
		res[index++] =A[Aindex++]; 
	}
	else if (A[Aindex]>B[Bindex])
	{
		res[index++] =B[Bindex++]; 
	}
	else
	{
		res[index++] =A[Aindex++];
		res[index++] =B[Bindex++]; 
	}
	
}

System.out.println("----final arr---"+Arrays.toString(res));

if(res.length%2==0)
{
	result  = (res[res.length/2-1]+res[res.length/2])/2.0;
}
else
{
	result  = (res[res.length/2]);
}

System.out.println("----result---"+result);	  
  return result;
  //your code
  }

  public static boolean pass()
  {
  boolean result = true;
  result = result && logic(new int[]{1, 3}, new int[]{2, 4}) == 2.5;
  result = result && logic(new int[]{1, 3,3,5,7,8,10,12,15,19,21}, new int[]{2, 4,6,9,11,13,14,16,17,18,20}) == 10.5;
  
  return result;
  };

  public static void main(String[] args)
  {
  if(pass())
  {
    System.out.println("pass");
  }
  else
  {
    System.out.println("some failures");
  }
  }
}