package com.aniket.problemsolving;

// Given a a string of letters and a dictionary, the function longestWord should
//     find the longest word or words in the dictionary that can be made from the letters
//     Input: letters = "oet", dictionary = {"to","toe","toes"}
//     Output: {"toe"}

import java.util.*;

class DictionarySet {
  private String[] entries;

  
  
  /**
 * @return the entries
 */
public String[] getEntries() {
	return entries;
}


public DictionarySet(String[] entries) {
    this.entries = entries;
  }

  public boolean contains(String word) {
    return Arrays.asList(entries).contains(word);
  }
}

public class Dictionary {
  public static Set<String> longestWord(String letters, DictionarySet dict) {
    Set<String> result = new HashSet<String>();
    ArrayList<String> tempResults = new ArrayList<>(); 
    int maxSize = 0;
    //suppose all letters are small
    int charCount[] = new int[26];
    int tempArrayCopy[] = new int[26];
    
    for(int i = 0 ; i<letters.length();i++)
    {
    	int index = letters.charAt(i)-'a';
    	charCount[index]++;
    }
    
    
    for(String currentStr: dict.getEntries())
    {
    	System.arraycopy(charCount, 0, tempArrayCopy, 0, 26);
    	System.out.println(Arrays.toString(tempArrayCopy)+"--initial---"+currentStr);
    	int length = currentStr.length();
    	for(int i = 0 ; i<currentStr.length();i++)
    	{
    		char curr = currentStr.charAt(i);
    		int index = curr-'a';

    		if(tempArrayCopy[index]>0)
    		{
    			tempArrayCopy[index]--;	
    			length--;
    		}
    		else
    		{
    			break;	
    		}
    		if(length==0)
    		{
    			if(currentStr.length()>maxSize)
    			{
    				maxSize = currentStr.length();
    			}
    			tempResults.add(currentStr);
    			System.out.println("--found---"+currentStr);
    		}
    			
    	}
    	
    	
    }
    
    for(String str : tempResults)
    {
    	if(str.length()==maxSize)
    		result.add(str);
    }
    return result;
  }


  public static boolean pass() {
    DictionarySet dict = new DictionarySet(new String[]{"to", "toe", "toes", "doe", "dog", "god", "dogs", "banana"});
    boolean r = new HashSet<String>(Arrays.asList("toe")).equals(longestWord("toe", dict));
    
     dict = new DictionarySet(new String[]{"ale", "apple", "monkey", "plea"});
     r = r &&  new HashSet<String>(Arrays.asList("apple")).equals(longestWord("abpcplea", dict));
    
    dict = new DictionarySet(new String[]{"pintu", "geeksfor", "geeksgeeks", "forgeek"});
    r = r &&   new HashSet<String>(Arrays.asList("geeksgeeks")).equals(longestWord("geeksforgeeks", dict));
    return r;
  }

  public static void main(String[] args) {
    if(pass()) {
      System.out.println("Pass");
    } else {
      System.err.println("Fails");
    }
  }
}