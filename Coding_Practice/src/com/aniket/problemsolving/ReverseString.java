package com.aniket.problemsolving;

public class ReverseString{
  /**
   * public static String reverseStr( String str )
   * Example: reverseStr(str) where str is "abcd" returns "dcba".
   */
  public static String reverseStr( String str ){
    if(str==null)
    	return null;
    else if(str.length()==0)
    {
    	return str;
    }
    else
    {
    	StringBuilder builder = new StringBuilder();
    	for(int k =str.length()-1;k>=0;k--)
    	{
    		char curr = str.charAt(k);
    		builder.append(curr);
    		
    	}
    	return builder.toString();
    }
  };

  
  public static void main(String[] args){
  
  String testString;
    String solution;
    boolean result = true;

    result = result && reverseStr("abcd").equals("dcba");
  
  if(result){
    System.out.println("All tests pass");
  }
  else{
    System.out.println("There are test failures");
  }

   }
}