package com.aniket.problemsolving;

import java.util.*;

/**
 * Pangram FInder
 *
 * The sentence "The quick brown fox jumps over the lazy dog" contains every
 * single letter in the alphabet. Such sentences are called pangrams. Write a
 * function findMissingLetters, which takes a String `sentence`, and returns all
 * the letters it is missing
 *
 */
public class Panagram {

	private static class PanagramDetector {
		private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

		public String findMissingLetters(String sentence) {
			sentence = sentence.toLowerCase();
			if (sentence == null)
				return ALPHABET;
			else if (sentence != null && sentence.trim().length() == 0)
				return ALPHABET;
			else {
				boolean[] data = new boolean[26];
				System.out.println("*****" + Arrays.toString(data));

				for (int i = 0; i < sentence.length(); i++) {
					int currCharacter = sentence.charAt(i) - 'a';
					if (currCharacter >= 26 || currCharacter < 0) {
						continue;
					}
					data[currCharacter] = true;
				}
				System.out.println("*****" + Arrays.toString(data));
				StringBuilder builder = new StringBuilder();

				for (int i = 0; i < data.length; i++) {
					if (data[i] == false) {
						builder.append((char) (i + (int) 'a'));
					}
				}

				System.out.println("***final result----" + builder.toString());
				return builder.toString();

			}

		}

	}

	public static void main(String[] args) {
		PanagramDetector pd = new PanagramDetector();
		boolean success = true;

		success = success && "".equals(pd.findMissingLetters("the quick brown fox jumps over the lazy dog"));
		success = success && "abcdefghijklmnopqrstuvwxyz".equals(pd.findMissingLetters(""));

		if (success) {
			System.out.println("Pass ");
		} else {
			System.out.println("Failed");
		}
	}
}