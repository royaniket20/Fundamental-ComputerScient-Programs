package com.aniket.problemsolving;

import java.util.*;
import java.util.Map.Entry;

//  Find the best average grade.
//  Given a list of student test scores
//  Each student may have more than one test score in the list.


class BestAverageGrade
{
	
  public static Integer bestAvgGrade(String[][] scores)
  {
    // write your code goes here
	  System.out.println("---scores---"+scores.length);
	  HashMap<String,List<Integer>> map = new HashMap<>();
	 
	  for(int i = 0 ;i<scores.length;i++)
	  {
		  if(map.containsKey(scores[i][0]))
		  {
			  List<Integer> data = map.get(scores[i][0]);
			  int realData = Integer.parseInt(scores[i][1]);
			  data.add(realData);
			  
			  if(data.size()==1)
			  {
				  data.add(realData);  
			  }
			  else
			  {
				  data.add(realData+data.get(data.size()-2));  
			  }
			  
			  map.put(scores[i][0],data);
		  }
		  else
		  {
			  List<Integer> data =new ArrayList<>();
			  int realData = Integer.parseInt(scores[i][1]);
			  data.add(realData);
			  
			  if(data.size()==1)
			  {
				  data.add(realData);  
			  }
			  else
			  {
				  data.add(realData+data.get(data.size()-2));  
			  }
			  
			  map.put(scores[i][0],data);
		  }
	  }
	  System.out.println("---final map----"+map);
	  int result = -1;
	  for(Entry<String,List<Integer>> entry : map.entrySet())
	  {
		List<Integer> vals  = entry.getValue();
		int score = vals.get(vals.size()-1)/(vals.size()/2);
		if(result<score)
		{
			result = score;
		}
		
		
	  }
	  
    return result;
  }

  public static boolean pass()
  {
    String[][] s1 = { { "Rohan", "84" },
               { "Sachin", "102" },
               { "Ishan", "55" },
               { "Sachin", "18" } };

    return bestAvgGrade(s1) == 84;
  }

  public static void main(String[] args)
  {
    if(pass())
    {
      System.out.println("Pass");
    }
    else
    {
      System.out.println("Some Fail");
    }
  }
}


