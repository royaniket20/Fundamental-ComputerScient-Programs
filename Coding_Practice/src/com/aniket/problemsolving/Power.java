package com.aniket.problemsolving;
/**
 *  Implement power(base,exp) correctly. Assume exp is an integer.
 *
 */

public class Power {

  /* Given base and integer exponent, compute value of base raised to the power of exponent.
   * Can you implement a solution faster than O(exp)?
   */
  public static double power(double base, int exp) {
//	if(exp==0)
//		{
//		//System.out.println(base+"----"+exp+"--ret - 1");
//		return 1;
//		}
//	else
//	{
//		if(exp%2==0)
//		{
//			double val =  power(base,exp/2)*power(base,exp/2);
//			//System.out.println(base+"----"+exp+"--ret--"+val);
//			return val;
//		}
//		else
//		{
//			double val =   base*power(base,exp/2)*power(base,exp/2);
//			//System.out.println(base+"----"+exp+"--ret--"+val);
//			return val;
//		}
//	}
	  
	  
	  //better solution
	  double temp;
	  if(exp==0)
			{
			System.out.println(base+"####"+exp+"--ret= 1");
			return 1;
			}
		else
		{
			temp =power(base,exp/2);
			if(exp%2==0)
			{
				double val = temp*temp;
				System.out.println(base+"~~~~"+exp+"--ret--"+val);
				return val;
			}
			else
			{
				if(exp>0)
				{
					double val =   base*temp*temp;
					System.out.println(base+"----"+exp+"--ret--"+val);
					return val;
				}
				else
				{
					double val =   (temp*temp)/base;
					System.out.println(base+"****"+exp+"--ret--"+val);
					return val;
				}
				
				
			}
		}
  }

  /* returns true if all tests pass, false otherwise */
  public static boolean doTestsPass() {
    boolean doTestsPass = true;
	double result = power(2,-8);
	System.out.println("--actual result---"+result);
	System.out.println("--system result---"+Math.pow(2, -8));
	doTestsPass = Math.pow(2, -8) == result;
	
    return  doTestsPass;
  }

  public static void main( String[] args ) {
    if (doTestsPass())
      System.out.println("All Tests Pass");
    else
      System.out.println("There are test failures");
  }
 }