package com.aniket.problemsolving;

/*
 * Implement a run length encoding function.
 * For a string input the function returns output encoded as follows:
 *
 * "a"     -> "a1"
 * "aa"    -> "a2"
 * "aabbb" -> "a2b3"
 */
public class RunLengthEncoding {

  public static String rle(String input) {
  int count = 0 ;
  String res = "";
  char currChar = Character.MIN_VALUE;
 if(input.length()>0)
 {
	 currChar = input.charAt(0);
 }
 StringBuilder builder = new StringBuilder();
for(int i = 0 ;i<input.length();i++)
{
	char cur = input.charAt(i);
	if(currChar!=cur)
	{
		builder.append(currChar);
		builder.append(count);
		currChar = cur;
		count = 0;
	}
	
	
	count++;
}
builder.append(currChar);
builder.append(count);
System.out.println(builder.toString()); 	  
    return builder.toString();
  }


 public static void main(String[] args)  {
	 rle("");
	 rle("a");
	 
	  if("".equals(rle("")) && 
			  "a1".equals(rle("a")) && 
			  "w4a3d1e1x6".equals(rle("wwwwaaadexxxxxx")) && 
			  "a3".equals(rle("aaa"))){
		  System.out.println("Passed");
	  }else {
		  System.out.println("Failed");
	  }
  }
}