package com.aniket.problemsolving;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//Implement double-ended (deque) queue that stores string

 class DequeImplementation {

	 private List<String> queue;
	 public DequeImplementation()
	 {
		queue = new ArrayList<String>();
	
	 }
	 
	 public void addInFront(String element)
	 {
		 System.out.println("--adding element to front----"+element);
		 queue.add(element);
		 System.out.println("----result----"+queue);
	 }
	 
	 public void addInRear(String element)
	 {
		 System.out.println("--adding element to rear----"+element);
		 queue.add(0,element);
		 System.out.println("----result----"+queue);
	 }
	 
	 public void removeFromFront()
	 {
		 System.out.println("--removing element from front----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return;
		 }
		 
		 queue.remove(queue.size()-1);
		 
		 System.out.println("----result----"+queue); 
	 }
	 
	 public void removeFromRear()
	 {
		 System.out.println("--removing element from rear----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return;
		 }
		 
		 queue.remove(0);
		 
		 System.out.println("----result----"+queue); 
	 }
	 
	 public String peekFront()
	 {
		 System.out.println("--peeking element from front----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return null;
		 }
		 
		 String res = queue.get(queue.size()-1);
		 
		 System.out.println("----result----"+queue);  
		 return res;
	 }
	 
	 public String peekRear()
	 {
		 System.out.println("--peeking element from rear----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return null;
		 }
		 
		 String res = queue.get(0);
		 
		 System.out.println("----result----"+queue);  
		 return res;
	 }
	 
	 public String pollFront()
	 {
		 System.out.println("--pooling element from front----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return null;
		 }
		 
		 String res = queue.remove(queue.size()-1);
		 
		 System.out.println("----result----"+queue);  
		 return res;
	 }
	 
	 public String pollRear()
	 {
		 System.out.println("--pooling element from rear----");
		 if(queue.size()==0)
		 {
			 System.out.println("---queue is underflowed----");
			 return null;
		 }
		 
		 String res = queue.remove(0);
		 
		 System.out.println("----result----"+queue);  
		 return res;
	 }
	 
	 
	 
	 
	 
	
}
 
 public class Deque{
	 
	 public static void main(String[] args) {
		 java.util.Deque<String> data= new LinkedList<String>();
		 
		 DequeImplementation deque = new DequeImplementation();
		 deque.addInFront("Aniket");
		 deque.addInFront("Amit");
		 deque.addInFront("Ashoke");
		 deque.addInRear("Rimi");
		 deque.addInRear("Sriparna");
		 
		 deque.removeFromFront();
		 deque.removeFromRear();
		 
		 System.out.println("----"+deque.peekFront());
		 System.out.println("----"+deque.peekRear());
		 
		 System.out.println("----"+deque.pollFront());
		 System.out.println("----"+deque.pollRear());
		 
		 deque.removeFromFront();
		 //now queue is empty
		 System.out.println("**************************************");
		 deque.removeFromFront();
		 deque.removeFromRear();
		 System.out.println("----"+deque.peekFront());
		 System.out.println("----"+deque.peekRear());
		 
		 System.out.println("----"+deque.pollFront());
		 System.out.println("----"+deque.pollRear());
		 

	}
 }