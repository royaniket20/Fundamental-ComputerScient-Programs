package com.aniket.problemsolving;

import java.util.HashSet;
import java.util.Set;

/*
Question: 
Combine ingredients in a specific order, any of which may be repeated

As an example, consider the following  
(A,B,C,D) in 11 steps: A, B, A, B, C, A, B, A, B, C, E 

Encode the string above using only 6 characters: A,B,*,C,*,E

Implement function that takes as input an un-encoded potion and returns the 
minimum number of characters required to encode

*/

public class MagicPotion {
	private static int minimalSteps(String ingredients) {
		int result = 0;
		char lastPrinted = ' ';
		if (ingredients == null) {
			return result;
		} else if (ingredients != null && ingredients.trim().length() == 0) {
			return result;
		} else {
			Set<Character> checker = new HashSet<>();
			StringBuilder builder = new StringBuilder();
			for(int k =0;k<ingredients.length();k++)
			{
				if(checker.contains(ingredients.charAt(k))){
					
					if(lastPrinted=='*')
					{
						continue;
					}
					else
					{
						lastPrinted = '*';
						builder.append(lastPrinted);
					}
				}
				else
				{
					lastPrinted =ingredients.charAt(k);
					checker.add(lastPrinted);
					builder.append(lastPrinted);
				}
			}
			System.out.println("*****----"+builder.toString());
		return builder.toString().length();	
		}
	}

	public static void main(String[] args) {

		if (minimalSteps("ABCDABCE") == 6 && minimalSteps("ABCABCE") == 5 &&  minimalSteps("ABABCABABCE") == 6) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
	}
}