package com.aniket;

import java.util.*;

public class GraphProblem {

    public static void main(String[] args) {
       int arr[][] =  {{1, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 7}, {4, 5}, {4, 8}, {8, 9}, {10,2}};
        Map<Integer , List<Integer>> map = new HashMap<>();
       for (int i = 0 ; i<arr.length ; i++){
           Integer parent = arr[i][0];
           Integer child = arr[i][1];
           if(map.containsKey(child)){
               List<Integer> integers = map.get(child);
               integers.add(parent);
           }else{
               map.put(child, new ArrayList<>());
               List<Integer> integers = map.get(child);
               integers.add(parent);
           }
       }
        System.out.println(map);
        List<Integer> maxList = new ArrayList<>();
        maxList.add(0);
        int input = 6;
      findAncestor(input,map,0 , maxList);
        System.out.println(input + "===>" + maxList);

        maxList.clear();
        maxList.add(0);
         input = 7;
        findAncestor(input,map,0 , maxList);
        System.out.println(input + "===>" + maxList);
        maxList.clear();
        maxList.add(0);
         input = 8;
        findAncestor(input,map,0 , maxList);
        System.out.println(input + "===>" + maxList);
    }

    public static Integer findAncestor(int node , Map<Integer , List<Integer>> map  , int initAncestor , List<Integer> maxList ){
        System.out.println("calling for node - "+node + " || initAncestor = "+initAncestor + " || AncestorMax = "+maxList);
        if(!map.containsKey(node)){
            return initAncestor;
        }else{
            int maxAncestor = 0;
            for (Integer parent : map.get(node)){
                int ancestor = findAncestor(parent,map ,initAncestor +1,maxList );
                if(ancestor>maxList.get(0)){
                    maxList.clear();
                    maxList.add(parent);
                    maxAncestor = ancestor;
                    System.out.println("Current result = "+ maxList + " || Current Max Ancestor - "+ maxAncestor);
                }

            }
            return maxAncestor;
        }
    }
}
