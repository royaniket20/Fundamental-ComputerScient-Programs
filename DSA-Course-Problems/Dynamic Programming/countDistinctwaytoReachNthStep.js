/**
 * 
You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 */
//MEMORIZATION APPROACH
let climbingStare = function (n, arr) {
  console.log(`Populated DP Array - ${arr}`);
  if (n == 1) {
    //Only One way you can Do that is Jump 1 step
    arr[n] = n; //Memorizing previous Solution
    return arr[n];
  }
  if (n == 2) {
    //Only Two  way you can Do that is  (1+1) , 2
    arr[n] = n; //Memorizing previous Solution
    return arr[n];
  }
  if (arr[n] > -1) {
    //If data precomputed return from there itself
    return arr[n];
  } else {
    //Compute if its Not computed yet
    let leftPath = climbingStare(n - 1, arr); //Jump 1 Step
    let rightPath = climbingStare(n - 2, arr); //Jump 2 Step
    arr[n] = leftPath + rightPath; //All possible way to Do this
    return arr[n];
  }
};

let arr = new Array(10 + 1).fill(-1);
let res = climbingStare(10, arr);
console.log(`Final Populated DP Array - ${arr}`);
console.log(`Result = ${res}`);
