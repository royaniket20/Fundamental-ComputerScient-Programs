/**
 * If you sell or buy a stock on a certain day
 * Given array - Per day stock closing price
 * Find max profit   - if you buy on a suitable date and sell on a suitable Date
 * You have to Buy the stock first on earlier days then only you can sell on a later date
 * **** You can buy - sell pair as mnany time you can wish
 * **** You cannot keep on buying before sellng items
 * **** So when you buy then sell then only you can buy again and then sell
 * Definately you shoud not buy and sell on same day because profit will be 0
 * Diff - Profit =Need to be maximum
 */
/**
 * DP approach
 * Here lot of ways to do Buy and sell - we have to try all the ways and then we have to get best possible solution
 * You have to write recurrion on DP
 * wE HAVE TO EXPRESS EVERYTHING IN index format
 * also we need to keep track if I am allowed to Buy or Not  - its the case if there is a pending sell
 * for sell we have two Option - either sell it or Not sell it
 * Keep track of the profit
 *
 * So here the Logic on Buy decision is -
 *  Either I will buy on that date [if no pending sell is there]
 *  Or I wikk skip buying that date
 * So here Logic on sell decision is -
 *  Either I will sell on that Date
 *  Or I will not sell on that date
 *
 * Alos remeber on profit track = Buy means (-x) - Money spent | Sell means (+x) - Money received
 */
let findmaxprofit = function (prices, index, isbuyAllowed) {
  //Bse case - Whe you have tired every days possible
  if (prices.length === index) {
    return 0;
  }
  let profit = 0;
  //If Buy is allowed
  if (isbuyAllowed === true) {
    //either today I will Buy
    let ifIBuy = -prices[index] + findmaxprofit(prices, index + 1, false);
    //Or Today I will Not Buy
    let ifNotBuy = 0 + findmaxprofit(prices, index + 1, true);
    profit = Math.max(ifIBuy, ifNotBuy);
  } else {
    //If Buy is Not allowed - means only sell allowed
    //Either I will sell
    let ifSell = prices[index] + findmaxprofit(prices, index + 1, true);
    //Or I will not sell
    let ifNotSell = 0 + findmaxprofit(prices, index + 1, false);
    profit = Math.max(ifSell, ifNotSell);
  }
  return profit;
};
let prices = [7, 1, 5, 3, 6, 4];
let result = findmaxprofit(prices, 0, true);
console.log(`Max profit is - ${result}`);
