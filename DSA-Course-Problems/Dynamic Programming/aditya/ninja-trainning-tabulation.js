/**
 * 
 * Ninja is planing this ‘N’ days-long training schedule. Each day, he can perform any one of these three activities. (Running, Fighting Practice or Learning New Moves). Each activity has some merit points on each day. As Ninja has to improve all his skills, he can’t do the same activity in two consecutive days. Can you help Ninja find out the maximum merit points Ninja can earn?

You are given a 2D array of size N*3 ‘POINTS’ with the points corresponding to each day and activity. Your task is to calculate the maximum number of merit points that Ninja can earn.

For Example
If the given ‘POINTS’ array is [[1,2,5], [3 ,1 ,1] ,[3,3,3] ],the answer will be 11 as 5 + 3 + 3.

 * 
 * @param {n} 
 * @param {*} points 
 * @returns 
 */

function ninjaTraining(n, points) {
  // Write your code here.
  //Here Rows will Represent the days (have one extra column )
  let days = n;
  //Columns will represent the task Variation (have one extra column )
  let tasks = points[0].length + 1;
  let dp = prepareDPArray(days, tasks);

  //This is the base case - when we are at first day
  // as per the rule each index we need to calculate whats best task we can choose excluding that Index
  //also as this is the first day - previous profit is 0 for all cases 
  // For each Index we will exclude that Index and find max profit by choosing between  other 2
  dp[0][0] = 0 + Math.max(points[0][1], points[0][2]);
  dp[0][1] = 0 + Math.max(points[0][0], points[0][2]);
  dp[0][2] = 0 + Math.max(points[0][0], points[0][1]);
  //This case - we know this Index is Out of Bound - Hence we can choose max from all 3
  //This Column is special - which will keep track of Result
  dp[0][3] = Math.max(points[0][0], points[0][1], points[0][2]);

  printDP(dp);
  //Now calculate for Rest of the elements
  for (let day = 1; day < days; day++) {
    for (let lastTask = 0; lastTask < tasks; lastTask++) {
      let maxProfit = 0;
      //calculate the max profit for this cell
      //Iterate throgh all the task excluding the One already tried in previous Round
      for (let currTask = 0; currTask < points[day].length; currTask++) {
        //Excluding the trainning which is already tried
        if (currTask != lastTask) {
          //profit for choosing the task
          let currentProfit = points[day][currTask];
          //If we choose the profit we have to choose max profit from last day when this task was Not done
          //Thats exactly How we have calculated the base case initialization
          let lastProfit = dp[day - 1][currTask];
          //Keep checking if thats what max possible
          maxProfit = Math.max(maxProfit, currentProfit + lastProfit);
        }
      }
      //Update max Profit for this cell
      dp[day][lastTask] = maxProfit;
    }
  }
  console.log(`--------------------------------`);
  printDP(dp);

  return dp[days - 1][tasks - 1];
}

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill(null);
  for (let i = 0; i < dp.length; i++) {
    dp[i] = new Array(cols).fill("-");
  }
  return dp;
}

//Printing DP Grid
function printDP(dp) {
  for (let i = 0; i < dp.length; i++) {
    console.log(dp[i].join("\t"));
  }
}

let merits = [
  [1, 2, 5],
  [3, 1, 1],
  [3, 3, 3],
];

let points = ninjaTraining(merits.length, merits);
console.log(`Max merit Possible is - ${points}`);

merits = [
  [73, 57, 31],
  [61, 30, 34],
  [87, 64, 41],
  [9, 69, 12],
  [33, 22, 62],
  [8, 15, 59],
  [74, 74, 51],
  [72, 5, 62],
  [23, 72, 93],
];

points = ninjaTraining(merits.length, merits);
console.log(`Max merit Possible is - ${points}`);
