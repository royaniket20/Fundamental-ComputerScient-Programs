function ninjaTraining() {
  //Lets say days rang from 0 to n-1
  let currentDay = 0;
  let currentIndex = points[0].length; //an Invalid Index  - means need to take every element
  printDP(memo);
  let result = resursion(currentDay, currentIndex);
  printDP(memo);
  return result;
}

function resursion(currentDay, lastIndex) {
  console.log(
    `Calling with Last Index = ${lastIndex} , Current Day = ${currentDay}`
  );
  if (memo[currentDay][lastIndex] > -1) {
    console.log(
      `Memorization is Found  memo[${currentDay}][${lastIndex}] = ${memo[currentDay][lastIndex]}`
    );
    return memo[currentDay][lastIndex];
  }

  if (currentDay == points.length) {
    console.log(`Base Condition is reached - Return FROM HERE`);
    return 0;
  }

  let currentTasks = points[currentDay];
  let maxi = 0;
  for (let taskIndex = 0; taskIndex < currentTasks.length; taskIndex++) {
    //Only do for the Paths whch are Not same task
    if (lastIndex != taskIndex) {
      let merit =
        currentTasks[taskIndex] + resursion(currentDay + 1, taskIndex);
      maxi = Math.max(maxi, merit);
    }
  }
  memo[currentDay][lastIndex] = maxi;
  return maxi;
}

//Printing DP Grid
function printDP(dp) {
  for (let i = 0; i < dp.length; i++) {
    console.log(dp[i].join("\t"));
  }
}

let points = [
  [1, 2, 5],
  [3, 1, 1],
  [3, 3, 3],
];

//Here meorization depends on Which day and Which task Index choosen
let memo = new Array(points.length + 1).fill(null);
for (let i = 0; i < memo.length; i++) {
  memo[i] = new Array(points[0].length + 1).fill(-1);
}

let result = ninjaTraining();
console.log(`Max Profit Possible is - ${result}`);

points = [
  [73, 57, 31],
  [61, 30, 34],
  [87, 64, 41],
  [9, 69, 12],
  [33, 22, 62],
  [8, 15, 59],
  [74, 74, 51],
  [72, 5, 62],
  [23, 72, 93],
];

//Here meorization depends on Which day and Which task Index choosen
memo = new Array(points.length + 1).fill(null);
for (let i = 0; i < memo.length; i++) {
  memo[i] = new Array(points[0].length + 1).fill(-1);
}

// result = ninjaTraining();
// console.log(`Max Profit Possible is - ${result}`);
