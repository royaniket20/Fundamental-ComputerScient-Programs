let arr = [1, 1, 2, 3];
let diff = 1;

function countOfSubsetWithGivenTargetSum(elements, target) {
  console.log(`Given Elements - ${elements}`);
  console.log(`target Sum  - ${target}`);

  let dp = prepareDPArray(elements.length + 1, target + 1);
  console.log(`DP matrix is Initalized `);
  printDP(dp);
  //Now we Compute For All the Remaing Cells
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      //Check If Currenat array element withing current Sum Target or Not
      let currentTarget = inner; //Columns represent target Sum
      let currentWeight = elements[outer - 1]; //Row represent the items //Outer-1 as Array is Zero Based Index
      let finalDecision = 0;
      if (currentWeight <= currentTarget) {
        //has capacity
        //We have tow Option Choose Item and Not to Choose Item  - Current Decision + Previous decison
        let itemChoosen = dp[outer - 1][inner - currentWeight];
        // - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];

        finalDecision = finalDecision + itemChoosen + itemNotChoosen;
      } else {
        //No capacity - We cannot Choose Item - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];
        finalDecision = finalDecision + itemNotChoosen;
      }
      dp[outer][inner] = finalDecision;
      //printDP(dp);
    }
  }
  console.log(`Now the DP is filled with Result`);
  printDP(dp);
  return dp[dp.length - 1][dp[0].length - 1];
}

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Now as we know if Sum 0 NEED TO BE ACHIEVED [1st column ] , we can always take empty subset
  //So First column will aways be 1 - for all cells in frst column

  //Now we know if no element is available in array i.e array length is 0 - then we can never achieve
  //target Sum >0 -- So all Values in 1st Row will be 0 .. Expect the First One

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = 1;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = 0;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function countOfSubsetPairWithGivenDiff(arr, diff) {
  let sum = arr.reduce((a, b) => a + b, 0);
  console.log(`Given array - ${arr} , diff - ${diff} , sum - ${sum}`);
  //Now we know we have Two Know Formula - lets say one subset is s1 and other subset is s2
  // s1 + s2 = sum || s1 - s2 = diff
  //So by this formula
  //s1 = [sum+diff]/2 - mixing above 2 formula
  //That means we just need to count how many subset with this value as target sum - thats it
  //We know for sure there must be some s2 for each such s1 so that s1 - s2 becomes diff

  let target = Math.floor((sum + diff) / 2);
  let result = countOfSubsetWithGivenTargetSum(arr, target);
  console.log(`SubSet pair with given diff  Possible = ${result}`);
}

countOfSubsetPairWithGivenDiff(arr, diff);
