//IMPORTANT POINT
//Tabulation  is a Buttom Up  - Hence start from Lowers  value and then come to Higer Value
//There is No Recursion at all
function knapsackUnboundedTabulation(values, weights, capacity) {
  let cols = capacity + 1;
  let rows = weights.length + 1;
  let dp = prepareDPArray(rows, cols);
  console.log(`After DP array is initialized -----`);
  printDP(dp);
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let currentCapacity = inner;
      let currentWeight = weights[outer - 1];
      let currentProfit = values[outer - 1];
      let profit = 0;
      if (currentWeight <= currentCapacity) {
        //let itemTaken = currentProfit + dp[outer - 1][inner - currentWeight];
        let itemTaken = currentProfit + dp[outer][inner - currentWeight];
        let itemNotTaken = dp[outer - 1][inner];
        profit = Math.max(itemTaken, itemNotTaken);
      } else {
        profit = dp[outer - 1][inner];
      }
      dp[outer][inner] = profit;
    }
  }
  console.log(` After DP Table is populated`);
  printDP(dp);
  return dp[dp.length - 1][dp[0].length - 1];
}
console.log("--------------------------------------");

//Lets create a Global DP matrix for Memorization
//Accordoing to the Rule - changing elements are capacity and index - they define the matrix size
//P.S - Profit is comething we are calculating - Not to consider that

let weights = [5, 10, 20];
let values = [7, 2, 4];
let capacity = 15;

let maxProfitDP = knapsackUnboundedTabulation(values, weights, capacity);
console.log(`Max Profit Coming UNBOUNDED is - ${maxProfitDP}`);

console.log("=======================================================");

weights = [4, 17];
values = [6, 12];
capacity = 3;

maxProfitDP = knapsackUnboundedTabulation(values, weights, capacity);
console.log(`Max Profit Coming UNBOUNDED is - ${maxProfitDP}`);

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Row is representing the capacity  0 --> n
  //Colun is represening value/ weight array length

  //For the first Row as there  is No profit array present as profit array length is 0
  // whatever is the capacity profit is 0
  //for the first column - as the capacity is 0 - nothing can be choosen - hence profit is also 0

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = 0;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = 0;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}
