//IMPORTANT POINT 
//Memorization is a Top Down Approach - Hence start from Higher value and then come down to Base case  - vvi
function knapsack01Memorizaton(values , weights  ,currentIdex, capacity){
    // Keep track of Profit so that we can get the maxz profit 
    console.log(`Calling for - [Index][Capacity] - [${currentIdex}][${capacity}] `);
     
    //If all Values traversed or Capacity is Fulfilled 
    if(currentIdex == -1 || capacity == 0 ){
      //Reached end - Need to return 
      return 0;
    }

    //DP check 
    if(dp[currentIdex][capacity]!= -1)
    {
      console.log(`Returning Memorized Value `);
      return dp[currentIdex][capacity];
    }

    //Only continue if Bag can hold it 
    //Call resursion only if Not memorized 
     if(capacity >= weights[currentIdex]){
      //Selecting the element 
      let resultOfSelectingElement =  values[currentIdex] +  knapsack01Memorizaton(values , weights , currentIdex -1   , capacity - weights[currentIdex]);
      //Not selecting the element 
      let resultOfNotSelectingElement = knapsack01Memorizaton(values , weights , currentIdex -1  ,   capacity);
      let result =   Math.max(resultOfNotSelectingElement , resultOfSelectingElement);
      //Memorization of Result  
      dp[currentIdex][capacity] = result;
      return result;
    }else {
       //Choose to No take the Item as capacity overloaded 
      let result = knapsack01Memorizaton(values , weights , currentIdex-1 ,   capacity) 
      //Memorization of Data 
      dp[currentIdex][capacity] = result;
      return result;
     }

   
}

//Lets create a Global DP matrix for Memorization 
//Accordoing to the Rule - changing elements are capacity and index - they define the matrix size  
//P.S - Profit is comething we are calculating - Not to consider that 


let weights = [1,3,4,5]
let values = [1,4,5,7]
let capacity = 7;
let currentIdex = weights.length>0? weights.length: -1;



let dp = new Array(values.length+1);
for (let index = 0; index < dp.length; index++) {
  dp[index] = new Array(capacity+1).fill(-1);
}


console.log(`Before DP memorization `);

for (let outer = 0; outer < dp.length; outer++) {
      console.log(dp[outer].join('\t'));
}


 let maxProfitDP  = knapsack01Memorizaton(values , weights , currentIdex,capacity )
console.log(`Max Profit Coming is - ${maxProfitDP}`);

console.log(`After  DP memorization `);

for (let outer = 0; outer < dp.length; outer++) {
      console.log(dp[outer].join('\t'));
}

console.log('==========================================================================');





 weights = [4,5,1]
 values = [1,2,3]
 capacity = 4;
 currentIdex = weights.length>0? weights.length: -1;



 dp = new Array(values.length+1);
for (let index = 0; index < dp.length; index++) {
  dp[index] = new Array(capacity+1).fill(-1);
}


console.log(`Before DP memorization `);

for (let outer = 0; outer < dp.length; outer++) {
      console.log(dp[outer].join('\t'));
}


  maxProfitDP  = knapsack01Memorizaton(values , weights , currentIdex,capacity )
console.log(`Max Profit Coming is - ${maxProfitDP}`);

console.log(`After  DP memorization `);

for (let outer = 0; outer < dp.length; outer++) {
      console.log(dp[outer].join('\t'));
}