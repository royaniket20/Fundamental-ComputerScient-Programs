let prices = [1, 5, 8, 9, 10, 17, 17, 20];
let cuts = [1, 2, 3, 4, 5, 6, 7, 8];

let rod = 8;

function maximiseProfit(rod, prices, cuts) {
  let cols = rod + 1;
  let rows = cuts.length + 1;

  //Create DP Array for tabulation
  let dp = createDPArray(rows, cols);
  console.log(`Inital DP Matrix `);
  printDP(dp);

  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let currentRodCapacity = inner;
      let currentProfit = prices[outer - 1]; // 0 based Index
      let currentCut = cuts[outer - 1]; //0 based index
      let profit = 0;
      if (currentCut <= currentRodCapacity) {
        //We hav capacity to Cut  - we are going to take same component
        //Row is not previous Row rather its same Row  outer-1 == knapsack 0-1 || outer == unbounder napsack
        let itemTaken = currentProfit + dp[outer][inner - currentCut];
        let itemNotTaken = dp[outer - 1][inner];
        profit = Math.max(itemTaken, itemNotTaken);
      } else {
        //We do not have capacity to Cut
        profit = dp[outer - 1][inner];
      }
      dp[outer][inner] = profit;
    }
  }
  console.log(`----------- Post Tabulation Prepared ----------`);

  printDP(dp);
  return dp[dp.length - 1][dp[0].length - 1];
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function createDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Here whatever the length of the rod - if profit is 0 no profit possible
  //So FirstRow will be 0
  for (let index = 0; index < dp[0].length; index++) {
    dp[0][index] = 0;
  }
  //For the first column - as Rod length is 0 so Nothing to Cut - No profit possible
  for (let index = 0; index < dp.length; index++) {
    dp[index][0] = 0;
  }
  return dp;
}

let result = maximiseProfit(rod, prices, cuts);
console.log(`Max Profit possible is - ${result}`);
