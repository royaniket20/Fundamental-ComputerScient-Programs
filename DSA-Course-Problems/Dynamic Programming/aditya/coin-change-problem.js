let coins = [1, 2, 3];
let target = 5;

function coinChange(coins, target) {
  let rows = coins.length + 1;
  let cols = target + 1;
  let dp = prepareDPArray(rows, cols);
  printDP(dp);
  console.log(`Calculation DP entries =-------------`);
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let currentTarget = inner;
      let currentCoin = coins[outer - 1];
      let count = null;
      console.log(`Current Coint - ${currentCoin} , Target - ${currentTarget}`);

      if (currentCoin <= currentTarget) {
        let notTake = dp[outer - 1][inner];
        let take = dp[outer][inner - currentCoin];
        count = take + notTake; //No of Ways is always add All ways
      } else {
        // Out of capacity - Not taking coin
        count = dp[outer - 1][inner];
      }
      dp[outer][inner] = count;
      printDP(dp);
    }
  }

  //printDP(dp);
  return dp[rows - 1][cols - 1];
}

let result = coinChange(coins, target);
console.log(`Maximum way possible is - ${result}`);

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //So First column will aways be 1  we can take empty set to preapre 0 sum target

  //We cannot form any Sum >0 when coins not available hence
  //First row will be 0  except left cell

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = 1;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = 0;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}
