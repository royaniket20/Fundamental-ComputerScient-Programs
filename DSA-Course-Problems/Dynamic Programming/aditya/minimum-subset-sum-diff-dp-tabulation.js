let arr = [1, 6, 11, 5];

//Find out for the Given Sum - Subset is Possible or Not

// Here we will take  array as Items and sum as Capacity

function subsetSum(elements, target) {
  let dp = prepareDPArray(elements.length + 1, target + 1);
  console.log(`DP matrix is Initalized `);
  printDP(dp);
  //Now we Compute For All the Remaing Cells
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      //Check If Currenat array element withing current Sum Target or Not
      let currentTarget = inner; //Columns represent target Sum
      let currentWeight = elements[outer - 1]; //Row represent the items - outer-1 beacause array is ero based
      let finalDecision = false;
      if (currentWeight <= currentTarget) {
        //has capacity
        //We have tow Option Choose Item and Not to Choose Item  - Current Decision + Previous decison
        let itemChoosen = true && dp[outer - 1][inner - currentWeight];
        // - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];

        finalDecision = finalDecision || itemChoosen || itemNotChoosen;
      } else {
        //No capacity - We cannot Choose Item - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];
        finalDecision = finalDecision || itemNotChoosen;
      }
      dp[outer][inner] = finalDecision;
      //printDP(dp);
    }
  }
  console.log(`Now the DP is filled with Result`);
  printDP(dp);
  return dp;
}

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Now as we know if Sum 0 NEED TO BE ACHIEVED [1st column ] , we can always take empty subset
  //So First column will aways be True - as its always Possible

  //Now we know if no element is available in array i.e array length is 0 - then we can never achieve
  //target Sum >0 -- So all Values in 1st Row will be False .. Expect the First One

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = true;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = false;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function minimumSubsetSumDiff(arr) {
  console.log(`Given array is - ${arr}`);
  //Now we know the Two subset sums s1 and s2 will be in some range
  //The range will be in between 0 and sum of all elements
  let sum = arr.reduce((a, b) => a + b, 0);
  console.log(
    `Both the Sums are in range of ${0}[Empty Set]----> ${sum}[Full Set]`
  );

  //We can also say that Sum1 + Sum2 = sum
  //that means if we always do such that Sum1 <sum2 then
  //sum2 - sum1 - we need to make sure this will be minimum
  //then can be reduced to  [sum-sum1]-sum1 need to be minimum
  // means sum- 2sum1 need to be minimum

  //To make sure Sum1 is always below Sum2 - we need to make sure all the elements of Sum1 Subset need to be
  //such that every possible sum1 need to be < sum/2

  //Then we just need to find what are those possible subset sums available to us from 0 -- sum/2 target sum
  //Then for each such s1 value we just need to check minimum value of sum-2s1

  let sumUpperBoundary = Math.floor(sum / 2); //We will consider it target Sum
  let populatedDp = subsetSum(arr, sumUpperBoundary);
  let possibleSum1s = populatedDp[populatedDp.length - 1];

  console.log(`Possible Sums [Boolean =true] - ${possibleSum1s}`);
  possibleSum1s = possibleSum1s
    .map((val, index) => (val ? index : -1))
    .filter((item) => item >= 0);
  console.log(`Possible Sums [Values] - ${possibleSum1s}`);
  //Now we need to find what the minimum value of sum- 2s1
  let minimum = Number.MAX_SAFE_INTEGER;
  for (let index = 0; index < possibleSum1s.length; index++) {
    const element = possibleSum1s[index];
    minimum = Math.min(minimum, sum - 2 * element);
    console.log(`Curent Minumum - ${minimum}`);
  }
  return minimum;
}

let result = minimumSubsetSumDiff(arr);
console.log(`Minimum Subset Sum Diff is - ${result}`);
