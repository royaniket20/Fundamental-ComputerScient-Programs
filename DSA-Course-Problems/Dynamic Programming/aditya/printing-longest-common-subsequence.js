/**
 * Print Longest common subsequence
 */

let x = `acbcf`;
let y = `abcdaf`;

function createTabulationStructure(xLen, yLen) {
  let arr = new Array(xLen).fill(0);
  for (let index = 0; index < arr.length; index++) {
    arr[index] = new Array(yLen).fill(0);
  }
  //First row will be 0 - As other string is empty
  //First colun will be 0 - as Other String is Emtpy
  for (let index = 0; index < arr.length; index++) {
    arr[index][0] = 0;
  }
  for (let index = 0; index < arr[0].length; index++) {
    arr[0][index] = 0;
  }
  printDP(arr);
  return arr;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function findlongestCommoonSubsequence(x, y) {
  let resultData = [];
  console.log(`Calling for X : ${x} | Y - ${y}`);
  let dp = createTabulationStructure(x.length + 1, y.length + 1);
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let result = null;
      if (x[outer - 1] === y[inner - 1]) {
        //X , Y have 0 based Index
        //When Both char matches
        result = 1 + dp[outer - 1][inner - 1]; //Get the previous result when reducing Both String
      } else {
        //When Both char Not matching
        let reduingXOnly = dp[outer - 1][inner];
        let reduingYOnly = dp[outer][inner - 1];
        result = Math.max(reduingXOnly, reduingYOnly);
      }

      dp[outer][inner] = result;
    }
  }

  console.log(`After final calculation ---------`);
  printDP(dp);
  console.log(
    `Now we need to Do the calculation from dp[x.length][y.length] Node`
  );
  let rowIndex = x.length;
  let colIndex = y.length;
  console.log(`Start Journey from [${rowIndex}][${colIndex}]`);
  //Very Important Point - It may happen that
  /**
   * 1. Both Index become 0 at same time
   * 2. One of them become Zero - In this case for For the Non zero One you need to add remaing chars n --> 0
   */
  while (rowIndex > 0 && colIndex > 0) {
    console.log(`Continue Journey from [${rowIndex}][${colIndex}]`);
    if (x[rowIndex - 1] === y[colIndex - 1]) {
      console.log(
        `character matching traverse to [${rowIndex - 1}][${colIndex - 1}]`
      );
      resultData.unshift(x[rowIndex - 1]);
      rowIndex = rowIndex - 1;
      colIndex = colIndex - 1;
    } else {
      console.log(`Character Not matching --`);
      if (dp[rowIndex][colIndex - 1] > dp[rowIndex - 1][colIndex]) {
        console.log(`traverse to [${rowIndex}][${colIndex - 1}]`);
        colIndex = colIndex - 1;
      } else {
        console.log(`traverse to [${rowIndex - 1}][${colIndex}]`);
        rowIndex = rowIndex - 1;
      }
    }
  }
  console.log(`Current Value of xIndex - ${rowIndex} & yIndex - ${colIndex} , CurrentVal - ${resultData.join("")} `);

  //For the Remaining character 
  while (rowIndex>=0) {
    resultData.unshift(x[rowIndex - 1]);
    rowIndex--;
  }

  while (colIndex>=0) {
    resultData.unshift(y[colIndex - 1]);
    colIndex--;
  }

  return resultData.join("");
}

let result = findlongestCommoonSubsequence(x, y);

console.log(`Longest common Subsequence  - ${result}`);
