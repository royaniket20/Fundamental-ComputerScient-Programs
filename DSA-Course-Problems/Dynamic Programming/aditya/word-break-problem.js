/**
 * 139. Word Break
Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a space-separated sequence of one or more dictionary words.

Note that the same word in the dictionary may be reused multiple times in the segmentation.

 

Example 1:

Input: s = "leetcode", wordDict = ["leet","code"]
Output: true
Explanation: Return true because "leetcode" can be segmented as "leet code".
Example 2:

Input: s = "applepenapple", wordDict = ["apple","pen"]
Output: true
Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
Note that you are allowed to reuse a dictionary word.
Example 3:

Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
Output: false
 

Constraints:

1 <= s.length <= 300
1 <= wordDict.length <= 1000
1 <= wordDict[i].length <= 20
s and wordDict[i] consist of only lowercase English letters.
All the strings of wordDict are unique.
 */

function wordBreakProblemn(wordDict, word) {
  //Here word is the capacity so will go along the Column
  //Here wordDict is the Value/weight - It will go along the Rows
  let rows = wordDict.length + 1;
  let cols = word.length + 1;
  let dp = prepareDPArray(rows, cols);
  console.log(`DP Prepared  before calculation --------------`);
  printDP(dp);
  console.log(`----------------------------------------------`);
  let dicTerms = [];
  for (let outer = 1; outer < dp.length; outer++) {
    dicTerms.push(wordDict[outer - 1]);
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let currentWordToFormValue = word.substr(0, inner);
      let currentWordToFormLength = currentWordToFormValue.length;
      let currentDicWordValue = wordDict[outer - 1];
      let currentDicWordLength = currentDicWordValue.length;
      console.log(`${dicTerms}|${currentWordToFormValue}`);
      let isPossible = null;
      if (currentDicWordLength <= currentWordToFormLength) {
        //It is possble to match as main workd is more larger than Dic term
        let notTaking = dp[outer - 1][inner];
        //Here the Idea is Upto Current term of Dic check for all words 
        //Wichever is Matching use that for calculation 
        let mnatchedWordPortion = checkDic(dicTerms, currentWordToFormValue);
        //Replace with  dp[outer-1][inner - mnatchedWordPortion.length]; - If 0-1 Knapsack 
        let taking =
          mnatchedWordPortion.length > 0 &&
          dp[outer][inner - mnatchedWordPortion.length];
        isPossible = notTaking || taking;
        console.log(
          `In Capacity - Taking - ${taking}| Not Taking - ${notTaking} ,Final - ${isPossible}`
        );
      } else {
        //Not possible to match - We will Mark It false
        isPossible = dp[outer - 1][inner];
        console.log(`Out Capacity - Not Taking Final- ${isPossible}`);
      }
      dp[outer][inner] = isPossible;
      printDP(dp);
      console.log(`---------------------------------`);
    }
  }
}

// let word = "applepenapple";
// let wordDict = ["apple", "pen"];
// let result = wordBreakProblemn(wordDict, word);

let word = "catsandog";
let wordDict = ["cats", "dog", "sand", "and", "cat"];
let result = wordBreakProblemn(wordDict, word);

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //So First column will aways be true as it is always possible to create empty  str from
  //any Dic

  //Now we know if No word available in Dic then
  //We cannot form any word >0
  //First row will be false  except left cell

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = true;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = false;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function checkDic(dicTerms, currentWordToFormValue) {
  let result = "";
  for (let index = 0; index < dicTerms.length; index++) {
    const element = dicTerms[index];
    if (currentWordToFormValue.endsWith(element)) {
      result = element;
    }
  }
  return result;
}
