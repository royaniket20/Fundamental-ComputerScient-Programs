let coins = [1, 2, 5];
let sum = 11;
//Problem statement - find minimum no of coins required to get target sum
minimumNoOfCoins(coins, sum);

coins = [2];
sum = 3;
//Problem statement - find minimum no of coins required to get target sum
minimumNoOfCoins(coins, sum);
function minimumNoOfCoins(coins, sum) {
  let rows = coins.length + 1;
  let cols = sum + 1;
  let dp = createDPMatrix(rows, cols);
  //Here one Important Logic we need to do is suppose you have 3 as only coin but
  //You have to achieve target Sum 4 - then We can fill with Infinity - as Its never possible to achieve
  //This is a special case where we need to fill the 2nd Row also
  console.log(`Basic Init is Done ----------------`);

  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let capacity = inner;
      let currentCoin = coins[outer - 1];
      let result = null;
      if (currentCoin <= capacity) {
        //In Capacity
        // index % currentCoin == 0 ? index / coins[0] : 999;
        let coinTake = 1 + dp[outer][inner - currentCoin];
        let coinNotTake = dp[outer - 1][inner];
        result = Math.min(coinTake, coinNotTake); //We have to achieve Minimum
      } else {
        //Not in capacity
        result = dp[outer - 1][inner];
      }
      dp[outer][inner] = result;
    }
  }
  printDP(dp);
}

function createDPMatrix(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Now for the First column It will be always 0 - because to cretae Target Sum 0
  //We need Minimum No Coins  - for all cells except Top One
  //VERY VERY IMPORTANT ----
  //First Row will be All INFINITY  - because when you have No coins available
  //We need to select Infinity coins then may be I will be able to reach Sum > 0
  //All cells  will be Infinity

  for (let index = 0; index < dp[0].length; index++) {
    dp[0][index] = 999; // Number.MAX_SAFE_INTEGER-1; //Number.MAX_SAFE_INTEGER shoud not be used - because we will add 1 later - which can do Overflow
  }
  for (let index = 1; index < dp.length; index++) {
    dp[index][0] = 0;
  }
  printDP(dp);
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}
