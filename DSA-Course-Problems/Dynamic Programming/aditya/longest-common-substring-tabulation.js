let x = `acbtdgrt`;
let y = `abtrtg`;

function createTabulationStructure(xLen, yLen) {
  let arr = new Array(xLen).fill("-");
  for (let index = 0; index < arr.length; index++) {
    arr[index] = new Array(yLen).fill("-");
  }
  //First row will be 0 - As other string is empty
  //First colun will be 0 - as Other String is Emtpy
  for (let index = 0; index < arr.length; index++) {
    arr[index][0] = 0;
  }
  for (let index = 0; index < arr[0].length; index++) {
    arr[0][index] = 0;
  }
  printDP(arr);
  return arr;
}

function printDP(arr) {
  for (let outer = 0; outer < arr.length; outer++) {
    console.log(arr[outer].join("\t"));
  }
}

function findLongestCommonSubStringLength(x, y) {
  let dp = createTabulationStructure(x.length + 1, y.length + 1);
  let max = 0;
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let result = null;
      if (x[outer - 1] === y[inner - 1]) {
        //Zero based Index
        //When Both char matches
        //We only Take element where Both element was reduced - because we need continuity of characters
        result = 1 + dp[outer - 1][inner - 1]; //Get the previous result when reducing Both String
      } else {
        //When Both char Not matching - Start a fresh - as Its substring
        result = 0;
      }
      max = Math.max(max, result);
      dp[outer][inner] = result;
    }
  }
  console.log(`After final calculation ---------`);
  printDP(dp);
  return max;
}

let result = findLongestCommonSubStringLength(x, y);

console.log(`Longest common Subsequence Length - ${result}`);
