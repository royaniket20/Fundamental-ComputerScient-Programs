let x = `ddqwerj`;
let y = `dhhhdhhhj`;
let memo = createTabulationStructure(x.length + 1, y.length + 1);

function createTabulationStructure(xLen, yLen) {
  let arr = new Array(xLen).fill(0);
  for (let index = 0; index < arr.length; index++) {
    arr[index] = new Array(yLen).fill(0);
  }
  //First row will be 0 - As other string is empty
  //First colun will be 0 - as Other String is Emtpy
  for (let index = 0; index < arr.length; index++) {
    arr[index][0] = 0;
  }
  for (let index = 0; index < arr[0].length; index++) {
    arr[0][index] = 0;
  }
  printDP(arr);
  return arr;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

function findLongestCommonSubsequenceLength(x, y) {
  console.log(`Calling for X : ${x} | Y - ${y}`);
  let dp = createTabulationStructure(x.length + 1, y.length + 1);
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      let result = null;
      if (x[outer - 1] === y[inner - 1]) {
        //X , Y have 0 based Index
        //When Both char matches
        result = 1 + dp[outer - 1][inner - 1]; //Get the previous result when reducing Both String
      } else {
        //When Both char Not matching
        let reduingXOnly = dp[outer - 1][inner];
        let reduingYOnly = dp[outer][inner - 1];
        result = Math.max(reduingXOnly, reduingYOnly);
      }

      dp[outer][inner] = result;
    }
  }
  console.log(`After final calculation ---------`);
  printDP(dp);
  return dp[x.length][y.length];
}

let result = findLongestCommonSubsequenceLength(x, y);

console.log(`Longest common Subsequence Length - ${result}`);
