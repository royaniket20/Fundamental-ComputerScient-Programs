let arr = [1, 5, 11, 5];

//Find out for the Given Sum - Subset is Possible or Not

// Here we will take  array as Items and sum as Capacity

function equalSumSubsetPartition(elements) {
  console.log(`Given Elements - ${elements}`);
  let target = elements.reduce((a, b) => a + b, 0);
  console.log(`Sum of all elements - ${target}`);
  if (target % 2 != 0) {
    //as the Result is Odd
    return false;
  }
  target = target / 2;
  //We Just need to find of a subset possible with target sum thats it - automatically that means
  //There is an equal sub other subset
  let dp = prepareDPArray(elements.length + 1, target + 1);
  console.log(`DP matrix is Initalized `);
  printDP(dp);
  //Now we Compute For All the Remaing Cells
  for (let outer = 1; outer < dp.length; outer++) {
    for (let inner = 1; inner < dp[outer].length; inner++) {
      //Check If Currenat array element withing current Sum Target or Not
      let currentTarget = inner; //Columns represent target Sum
      let currentWeight = elements[outer - 1]; //Row represent the items //outer-1 because array is Zero indexed
      let finalDecision = false;
      if (currentWeight <= currentTarget) {
        //has capacity
        //We have tow Option Choose Item and Not to Choose Item  - Current Decision + Previous decison
        let itemChoosen = dp[outer - 1][inner - currentWeight];
        // - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];

        finalDecision = finalDecision || itemChoosen || itemNotChoosen;
      } else {
        //No capacity - We cannot Choose Item - fallback to decision when Item was Not present - Previou Row same column
        let itemNotChoosen = dp[outer - 1][inner];
        finalDecision = finalDecision || itemNotChoosen;
      }
      dp[outer][inner] = finalDecision;
      //printDP(dp);
    }
  }
  console.log(`Now the DP is filled with Result`);
  printDP(dp);
  return dp[dp.length - 1][dp[0].length - 1];
}

function prepareDPArray(rows, cols) {
  let dp = new Array(rows).fill("-");
  for (let index = 0; index < dp.length; index++) {
    dp[index] = new Array(cols).fill("-");
  }
  //Now as we know if Sum 0 NEED TO BE ACHIEVED [1st column ] , we can always take empty subset
  //So First column will aways be True - as its always Possible

  //Now we know if no element is available in array i.e array length is 0 - then we can never achieve
  //target Sum >0 -- So all Values in 1st Row will be False .. Expect the First One

  for (let outer = 0; outer < dp.length; outer++) {
    dp[outer][0] = true;
  }
  for (let outer = 1; outer < dp[0].length; outer++) {
    dp[0][outer] = false;
  }
  return dp;
}

function printDP(dp) {
  for (let outer = 0; outer < dp.length; outer++) {
    console.log(dp[outer].join("\t"));
  }
}

let result = equalSumSubsetPartition(arr);

console.log(`Equal sum SubSet Partition is Possible = ${result}`);
