let x = `abcdgh`;
let y = `abedfh`;

function findLongestCommonSubsequenceLength(x, y) {
  console.log(`Calling for X : ${x} | Y - ${y}`);

  //Base case
  if (x.length === 0 || y.length === 0) {
    return 0;
  }
  //Choice diagram
  if (x[x.length - 1] === y[y.length - 1]) {
    //both char are matching
    return (
      1 +
      findLongestCommonSubsequenceLength(
        x.substr(0, x.length - 1),
        y.substr(0, y.length - 1)
      )
    );
  } else {
    //Both char not matchin
    //Taking x reduction
    let xReduce = findLongestCommonSubsequenceLength(
      x.substr(0, x.length - 1),
      y
    );
    //Taking Y reduction
    let yReduce = findLongestCommonSubsequenceLength(
      x,
      y.substr(0, y.length - 1)
    );
    return Math.max(xReduce, yReduce);
  }
}

let result = findLongestCommonSubsequenceLength(x, y);

console.log(`Longest common Subsequence Length - ${result}`);
