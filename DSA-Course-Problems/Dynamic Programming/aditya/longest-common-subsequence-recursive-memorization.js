let x = `abcdgh`;
let y = `abedfh`;
let memo = createMemoStructure(x.length + 1, y.length + 1);

function createMemoStructure(xLen, yLen) {
  let arr = new Array(xLen).fill(-1);
  for (let index = 0; index < arr.length; index++) {
    arr[index] = new Array(yLen).fill(-1);
  }
  return arr;
}
function findLongestCommonSubsequenceLength(x, y) {
  console.log(`Calling for X : ${x} | Y - ${y}`);
  if(memo[x.length][y.length] !== -1){
    console.log(`Found a memorized result ---`);
    return memo[x.length][y.length] ;
  }
  //Base case
  if (x.length === 0 || y.length === 0) {
    return 0;
  }
  //Choice diagram
  if (x[x.length - 1] === y[y.length - 1]) {
    //both char are matching
    let result =
      1 +
      findLongestCommonSubsequenceLength(
        x.substr(0, x.length - 1),
        y.substr(0, y.length - 1)
      );
    //Memorize the result
    memo[x.length][y.length] = result;
    return result;
  } else {
    //Both char not matchin
    //Taking x reduction
    let xReduce = findLongestCommonSubsequenceLength(
      x.substr(0, x.length - 1),
      y
    );
    //Taking Y reduction
    let yReduce = findLongestCommonSubsequenceLength(
      x,
      y.substr(0, y.length - 1)
    );
    let result = Math.max(xReduce, yReduce);
    //Memorize the result
    memo[x.length][y.length] = result;
    return result;
  }
}

let result = findLongestCommonSubsequenceLength(x, y);

console.log(`Longest common Subsequence Length - ${result}`);
