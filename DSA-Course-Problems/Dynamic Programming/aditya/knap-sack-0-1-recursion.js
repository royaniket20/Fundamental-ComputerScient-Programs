

let weights = [1,3,4,5]
let values = [1,4,5,7]
let capacity = 7;
let maxProfit = 0 ;
let currentIdex = 0;

function knapsack01Normal(values , weights , currentIdex   ,maxProfit ,   capacity){
  // Keep track of Profit so that we can get the maxz profit 
  console.log(`Calling for - [Index][Capacity] - [${currentIdex}][${capacity}] `);
     
  if(currentIdex == values.length){
    //Reached end - Need to return 
    //console.log(`Reached End of Tree - Need to return `);
    return maxProfit;
  }

  //Only continue if Bag can hold it 
  //Call resursion only if Not memorized 
   if(capacity >= weights[currentIdex]){
    //Lets choose Item
    let resultOfSelectingElement =  knapsack01Normal(values , weights , currentIdex +1   ,maxProfit + values[currentIdex] ,   capacity - weights[currentIdex]);
    //Lets Not choose Item 
    let resultOfNotSelectingElement = knapsack01Normal(values , weights , currentIdex +1   ,maxProfit ,   capacity);
    //Return Max profit 
    let result =  Math.max(resultOfNotSelectingElement , resultOfSelectingElement);
    return result;
  }else {
    //console.log(`Item is beyond Capacity - Skipping It `);
     //Choose to No take the Item as Its beyond capacity
    let result = knapsack01Normal(values , weights , currentIdex+1   ,maxProfit ,   capacity) 
    return result;
   }

 
}



let maxProfitDP  = knapsack01Normal(values , weights , currentIdex , maxProfit,capacity )
console.log(`Max Profit Coming is - ${maxProfitDP}`);

