/**
 * Given you have a Number N - Find out How many structurally Unique BinarY tREE Possible
 *
 * Example suppose n= 1
 *
 * that menas its One Node - Only 1 Binary tree Possible
 *
 * n = 2
 *
 * Now you have two Option make 1 as Root or 2 as Root and Rest is Leaf Node
 * So total Possibility is 2 - Here
 *
 * n = 3 -- Now we can make 1 as Root , 2 as Root , 3 As Root
 *
 * if 1 as Root -  you have Two Right child -- they can be permuted in 2 ways [W2e just discussed n=2 --> 2 ways possible ]
 * if 2 is Root - you havw 1 Right child and 1 Left child --each of them permuted in 1 way [We just discussed n =1 --> 1 way Possible]
 * if 3 is Root = you have Two Left  child -- they can be permuted in 2 ways [W2e just discussed n=2 --> 2 ways possible ]
 * total possiblie uniques = 2+1+2 = 5
 *
 * if n = 4
 * if n = 1 --> [3 nodes at right, 0 node at left  -->[5 ways]*[No way i.e 1 way] == 5 way
 * if n =2 --> 1 node at left , 2 node at right --> [1 way]*[2 way ] == 2 way possible
 * n = 3 --> 2 node at left , 1 node at right  --> [2 ways]*[1 way] = 2 way possible
 * n = 4  --> 3 node left , 0 node right [5 ways][1 way] == 5 ways
 * Total Unique - 5 + 2 + 2 + 5 = 14
 */

let uniqueBinaryTreeCOunt = function (n) {
  let arr = new Array(n + 1).fill(1);
  arr[0] = 1;
  arr[1] = 1;
  arr[2] = 2;
  if(n<=2){
    console.log(`Result is ---- ${arr[n]}`);
  return;
  }
  for (let index = 3; index <= n; index++) {
    console.log(`Calculating for n =================== ${index}`);
    //We will make each element as Root 1 time
    let sum = 0;
    for (let root = 1; root <= index; root++) {
      console.log(`Now Root is - ${root}`);
      console.log(
        `Left elements - ${root - 1} | Right elements - ${index - root}`
      );
       let leftpossible = arr[root-1];
       let rightpossible = arr[index - root];
       sum = sum + (leftpossible*rightpossible);
    }
    arr[index] = sum;
    console.log(` Solution for n =============== ${arr[index]}`);
  }
};

uniqueBinaryTreeCOunt(5);
