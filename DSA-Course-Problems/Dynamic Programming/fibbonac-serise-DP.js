//Fibonacci Nth element on DP way
//MEMORIZATION APPROACH
let fibonacci = function (n, arr) {
  console.log(`Populated DP Array - ${arr}`);
  if (n == 0 || n == 1) {
    arr[n] = n; //Memorizing previous Solution
    return n;
  }
  if (arr[n] > -1) {
    //If data precomputed return from there itself
    return arr[n];
  } else {
    //Compute if its Not computed yet
    arr[n] = fibonacci(n - 1, arr) + fibonacci(n - 2, arr); //Memorizing previous Solution
    return arr[n];
  }
};

let arr = new Array(8 + 1).fill(-1);
let res = fibonacci(8, arr);
console.log(`Final Populated DP Array - ${arr}`);
console.log(`Result = ${res}`);

//tabulation approach
let fibonacciTablulation = function (n, arr) {
  arr[0] = 0;
  arr[1] = 1;
  for (let index = 2; index <= n; index++) {
    arr[index] = arr[index - 1] + arr[index - 2];
  }
  //Tablulation Now completed -Get result
  console.log(`Populated DP Array - ${arr}`);
  return arr[n];
};

arr = new Array(8 + 1).fill(-1);
res = fibonacciTablulation(8, arr);
console.log(`Final Populated DP Array - ${arr}`);
console.log(`Result = ${res}`);
