//A frog can Jump 1 step or 2 step
//Enery Lost is Height between 2 Steps
//Find most efficient Jump path to reach destination

// Taken Memorization Approach
let frogJump = function (n, heights) {
  let arr = new Array(n + 1).fill(-1);
  return frogJumpDP(n, heights, arr);
};

function frogJumpDP(n, heights, arr) {
  if (n == 1) {
    arr[n] = 0;
    return 0; // No energey wasted
  } else if (n == 2) {
    arr[n] = Math.abs(heights[1 - 1] - heights[2 - 1]); //He can only Jup from 1st step to 2nd Step thats It
    return arr[n];
  }
  if (arr[n] > -1) {
    //precomputation is Done already - return result
    return arr[n];
  } else {
    let leftResult =
      Math.abs(heights[n - 1 - 1] - heights[n - 1]) +
      frogJumpDP(n - 1, heights, arr); //Going from N-1 th  Step to Nth Step
    let rightResult =
      Math.abs(heights[n - 2 - 1] - heights[n - 1]) +
      frogJumpDP(n - 2, heights, arr); //Going from N-2 th  Step to Nth Step
    arr[n] = Math.min(leftResult, rightResult);
    return arr[n];
  }
}

console.log(`Efficient Frog Jum - ${frogJump(4, [10, 20, 30, 10])}`);
console.log(`Efficient Frog Jum - ${frogJump(3, [10, 50, 10])}`);

console.log(`Efficient Frog Jum - ${frogJump(8, [7, 4, 4, 2, 6, 6, 3, 4])}`);
console.log(`Efficient Frog Jum - ${frogJump(6, [4, 8, 3, 10, 4, 4])}`);

// Taken Tabulation Approach
let frogJumpTabulation = function (n, heights) {
  let arr = new Array(n + 1).fill(-1);
  return frogJumpDPTab(n, heights, arr);
};

function frogJumpDPTab(n, heights, arr) {
  //Fixed entries in Tabulation
  arr[1] = 0;
  arr[2] = Math.abs(heights[1 - 1] - heights[2 - 1]); //He can only Jup from 1st step to 2nd Step thats It
  //Rest of the netries in Tabulation
  for (let index = 3; index <= n; index++) {
    let leftResult =
      arr[index - 1] + Math.abs(heights[index - 1 - 1] - heights[index - 1]); //Going from N-1 th  Step to Nth Step
    let rightResult =
      arr[index - 2] + Math.abs(heights[index - 2 - 1] - heights[index - 1]); //Going from N-2 th  Step to Nth Step
    arr[index] = Math.min(leftResult, rightResult);
  }
  return arr[n];
}
console.log("-------------------------------");
console.log(`Efficient Frog Jum - ${frogJumpTabulation(4, [10, 20, 30, 10])}`);
console.log(`Efficient Frog Jum - ${frogJumpTabulation(3, [10, 50, 10])}`);

console.log(
  `Efficient Frog Jum - ${frogJumpTabulation(8, [7, 4, 4, 2, 6, 6, 3, 4])}`
);
console.log(
  `Efficient Frog Jum - ${frogJumpTabulation(6, [4, 8, 3, 10, 4, 4])}`
);
