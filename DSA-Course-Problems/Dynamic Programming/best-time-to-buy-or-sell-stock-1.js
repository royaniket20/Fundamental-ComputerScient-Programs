/**
 * If you sell or buy a stock on a certain day
 * Given array - Per day stock closing price
 * Find max profit   - if you buy on a suitable date and sell on a suitable Date
 * You have to Buy the stock first on earlier days then only you can sell on a later date
 * you can do buy one time and then sell one time
 * Definately you shoud not buy and sell on same day because profit will be 0
 * Diff - Profit =Need to be maximum
 */
/**
 * DP approach
 * if you are selling a stock on ith date - then you will try to Buy the stock on kth date which have minimum value from sell date value
 *Formally - if you sell on ith date buy on a min value date from 0 -> (i-1)th days
 */
let findmaxprofit = function (arr) {
  console.log(`Given stock price array - ${arr}`);
  //For the first day of trading - suppose you buy on same date and sell on same date
  //So profit is 0 as of Now - And Minimum is also whatever is the first day price
  let profit = 0;
  let min = arr[0];
  //Now idea is for every other daty try to sale it when buy on min date also Update Min value and keep track of max profit
  for (let index = 1; index < arr.length; index++) {
    let cost = arr[index] - min;
    profit = Math.max(profit, cost);
    min = Math.min(min, arr[index]);
  }
  console.log(`Max profit generated - ${profit}`);
};
findmaxprofit([2, 100, 150, 120]);
