import LinkedList from "./ReusableLinkedList.js";

let sampleList = new LinkedList(100);
sampleList.addElementAttEnd(200);
sampleList.addElementAttEnd(300);
sampleList.addElementAttEnd(400);
sampleList.addElementAttEnd(500);
sampleList.addElementAttEnd(600);
sampleList.printLinkedList();

//Reverse a Linked List Recursively
console.log(`Given Head - ${JSON.stringify(sampleList.head)}`);

function recursiveLLReversal(head, currentPointer) {
  // console.log(
  //   `Calling for value - ${currentPointer.value} | current Head - ${head.value}`
  // );
  //Base condition
  if (currentPointer.next === null) {
    console.log(`Base condition reached - reassigned head `);
    currentPointer.next = head;
    head = currentPointer;
    return head;
  }
  //Actual Logic
  let next = currentPointer.next;
  if (currentPointer == head) {
    currentPointer.next = null;
  } else {
    currentPointer.next = head;
    head = currentPointer;
  }
  //Recursive call
  return recursiveLLReversal(head, next);
}
let currentPointer = sampleList.head;
let head = sampleList.head;
head = recursiveLLReversal(head, currentPointer);
console.log(`Current head = ${JSON.stringify(head)}`);
sampleList.head =  head;
sampleList.printLinkedList();
