/**
 * {"head":{"value":10,"next":{"value":5,"next":{"value":16,"next":null}}}}
 */
/**
 * Node is the tructire element on Linkedlist
 */
 class Node {
  value ; //Store actual Value
  next; //Point to the Next Node if Present 
  constructor(value) {
    this.value = value;
    this.next = null;
  }

  printNode() {
    return ` ${this.value} --> ${this.next != null ? this.next.value : "null"}`;
  }
}
export default  class LinkedList {
  head;
  tail;
  length;
  //Constructor is used when First time tyou create LinkedList
  constructor(value) {
    let node = new Node(value);
    //When First Value enetred Both Head and Tail Point to same Node
    this.head = node;
    this.tail = node;
    this.length = 1;
  }

    //append More elements at end of LinkedList
    addElementAttEnd(value) {
      //Create New Node
      let node = new Node(value);
      //Add To the End o Tail
      this.tail.next = node;
      //Update Tail Position
      this.tail = node;
      //Update Value Count
      this.length++;
      return this;
    }
   //append More elements at start of LinkedList
    appendAtStart(value) {
      //Create New Node
      let node = new Node(value);
      //Attache Node at Current Head
      node.next = this.head;
      //Update Head Pointer
      this.head = node;
      //Update Value Count
      this.length++;
      return this;
    }

    
  //Delete from Head Position One Node
  removeFromHead() {
    if (this.length == 0) {
      console.log("Nothing is there o delete");
    }
    //Taking Element to be deleted
    let temp = this.head;
    //Updating Head to Next element
    this.head = this.head.next;
    //Freeing th memory
    delete temp.next;
    temp = null; //Let it get garbage collected
    //Updating Value Count
    this.length--;
    return this;
  }

  //Removing Element from TAIL position of LL 
  removeFromTail() {
    if (this.length == 0) {
      console.log("Nothing is there o delete");
    }
    //Traver Till One item Before Tail and then Do deletion
    let current = this.head;
    while (current.next.next != null) {
      current = current.next;
    }
    //Break the Chain from last element
    current.next = null;
    //Update the Tail Pointer 
    this.tail = current;
    //Update Value Count 
    this.length--;
    return this;
  }

  //Printing all elements of Linked List
  printLinkedList() {
    let arr = [];
    let current = this.head;
    while (current.next != null) {
      arr.push(current.value);
      arr.push("-->");
      current = current.next;
    }
    arr.push(current.value); //Last elemebnt
    console.log(
      `Printed Linked List with Size - ${this.length} Linked List [ ${arr.join(
        " "
      )} ]`
    );
    return this;
  }

  //Searching an Element on Linked List
  searchLinkedList(element) {
    let current = this.head;
    let isFound = false;
    while (current.next != null) {
      if(element === current.value){
        console.log(`Element - ${element} present on Linked List`);
        isFound = true;
        break;
      }
      current = current.next;
    }
   //Check Last elemebnt
   if(!isFound && element === current.value){
    console.log(`Element - ${element} present on Linked List`);
  }
    return this;
  }



//Revese a Single LL 
  reverseSingleLinkedList() {
    if (this.length == 1) {
      console.log(`Only One element - Nothing to Reverse`);
      return this;
    } else {
      let first = this.head;
      let last = this.tail;
      let second = first.next;
      while (second != null) {
        let third = second.next;
        //Do the Link Reversal
        second.next = first;
        first = second;
        second = third;
      }
      this.head.next = null; //Put Head at Null - as this is Now end Node
      this.tail = this.head; //Resetting the Tail Position
      this.head = last; //Resetting Head to this Position
    }

    return this;
  }


  //Append in Middle Position
  appendAtPositionNInMiddle(value, position) {
    if (position > this.length + 1 || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.appendAtStart(value);
    } else if (position === this.length + 1) {
      return this.addElementAttEnd(value);
    } else {
      position--; //Go till prev Node where we need to Insert
      let node = new Node(value);
      let current = this.head;
      let index = 1;
      while (index != position) {
        index++;
        current = current.next;
      }
      //Now I have reached to the Insertion Position
      node.next = current.next;
      current.next = node;
      this.length++;
      return this;
    }
  }

  //Delete from a Position In Between 
  deleteAtPositionNInMiddleByPosition(position) {
    if (position > this.length || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.removeFromHead();
    } else if (position === this.length) {
      return this.removeFromTail();
    } else {
      position--; //Go till prev Node where we need to Insert
      let current = this.head;
      let index = 1;
      while (index != position) {
        index++;
        current = current.next;
      }
      //Now I have reached to the Deletion Position
      let temp = current.next;
      current.next = current.next.next;
      delete temp.next;
      this.length--;
      return this;
    }
  }
}

let createdLinkedList = new LinkedList(100);
createdLinkedList
  .addElementAttEnd(200)
  .addElementAttEnd(300)
  .addElementAttEnd(400)
  .printLinkedList();

createdLinkedList
  .appendAtStart(199)
  .appendAtStart(188)
  .appendAtStart(177)
  .printLinkedList();

createdLinkedList
  .appendAtPositionNInMiddle(44, 2)
  .printLinkedList()
  .appendAtPositionNInMiddle(11, 7)
  .printLinkedList()
  .appendAtPositionNInMiddle(10, createdLinkedList.length + 1)
  .printLinkedList()
  .appendAtPositionNInMiddle(9, createdLinkedList.length)
  .printLinkedList()
  .appendAtPositionNInMiddle(6, 1)
  .appendAtPositionNInMiddle(3, 1)
  .appendAtPositionNInMiddle(1, 1)
  .printLinkedList();

createdLinkedList
  .removeFromHead()
  .removeFromHead()
  .removeFromHead()
  .printLinkedList();

createdLinkedList.removeFromTail().removeFromTail().printLinkedList();

createdLinkedList
  .deleteAtPositionNInMiddleByPosition(2)
  .deleteAtPositionNInMiddleByPosition(6)
  .printLinkedList();

createdLinkedList.reverseSingleLinkedList().printLinkedList();

createdLinkedList.reverseSingleLinkedList().printLinkedList();

//Below one gives EError
//createdLinkedList.appendAtPositionNInMiddle(77, 77);

console.log(`Head Config Now : ${createdLinkedList.head.printNode()}`);
console.log(`Tail Config Now : ${createdLinkedList.tail.printNode()}`);
