import LinkedList from "./ReusableLinkedList.js";

let sampleList = new LinkedList(100);
sampleList.addElementAttEnd(200);
sampleList.addElementAttEnd(300);
sampleList.addElementAttEnd(400);
sampleList.addElementAttEnd(500);
sampleList.addElementAttEnd(600);
//sampleList.addElementAttEnd(700);
sampleList.printLinkedList();

//Reverse a Linked List
console.log(`Given Head - ${JSON.stringify(sampleList.head)}`);

/**
 * If the List is Even find Middle 2
 * If the List is Odd - Find the Middle 1 Element
 *
 * Approach : General Sol --- Find the Cout of the Nodes and then Find Mid Point and then Go till that Point and Find the Middle Elemebnt
 */
//Simple approach -------
let temp = sampleList.head;
let nodeCount = 0;
while (temp != null) {
  nodeCount++;
  temp = temp.next;
}
console.log(`Total Count of elements - ${nodeCount}`);
let targetNode = 0;
let result = [];
if (nodeCount % 2 == 0) {
  //Two Middle Nodes
  targetNode = Math.floor(nodeCount / 2);
  console.log(`Target Nodes [${targetNode} , ${targetNode + 1}]`);

  //Finding Nodes
  let temp = sampleList.head;
  let current = 1;
  while (current < targetNode) {
    temp = temp.next;
    current++;
  }
  result.push(temp.value);
  result.push(temp.next.value);
} else {
  //One Middle Node
  targetNode = Math.floor(nodeCount / 2) + 1;
  console.log(`Target Nodes [${targetNode}]`);

  //Finding Nodes
  let temp = sampleList.head;
  let current = 1;
  while (current < targetNode) {
    temp = temp.next;
    current++;
  }
  result.push(temp.value);
}
console.log(`Final Result -[ ${result}]`);

/******* Good approach - Tortoise Method  
Both Tortoise Will start from First Node 
1 Tortoise Will move 1 Hop while Onther One will move 2 Hop at a time 
Then wehn Fast one will reach the End of Link List Slow One will be Middle of the Linked List 
**/

console.log(` TORTOISE METHOD EFFICIENT APPROACH `);

result = [];
let SLOW = sampleList.head;
let FAST = sampleList.head;
let count = 1;
let tempForEvenLength = SLOW;
while (FAST != null && FAST.next != null) {
  //Important Condition to Ensure we can Hop 2 jumps
  tempForEvenLength = SLOW; //jUST IN CASE WE HAVE AN eVEN lENGTH lINKED lIST
  SLOW = SLOW.next; //1 jump
  FAST = FAST.next; //First Jump
  count++;
  FAST = FAST.next; //Second Jump
  if (FAST) {
    //iF THE fAST TORTOISE HAVE CROSSED THE LINKEDLIST AND POINTING TO NULL THAT WILL NOT BE CONTRIBUTED TO LENGTH
    count++;
  }
}
console.log(`Total Node Count - ${count}`);
if (count % 2 == 0) {
  result.push(tempForEvenLength.value);
  result.push(SLOW.value);
} else {
  result.push(SLOW.value);
}
console.log(`Final Result -[ ${result}]`);
