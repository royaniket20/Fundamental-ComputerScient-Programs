import LinkedList from "./ReusableLinkedList.js";

let sampleList = new LinkedList(100);
sampleList.addElementAttEnd(200);
sampleList.addElementAttEnd(300);
sampleList.addElementAttEnd(400);
sampleList.addElementAttEnd(500);
sampleList.addElementAttEnd(600);
sampleList.printLinkedList();

//Reverse a Linked List
console.log(`Given Head - ${JSON.stringify(sampleList.head)}`);
let first = sampleList.head;
let second = sampleList.head.next;
let newHead = null;
while (second != null) {
  let temp = second.next;
  second.next = first; // Reversing Link
  newHead = second; //Updating Head Ppointer 
  first = second; //Shifting Pointers 
  second = temp; //Shifting Pointer 
}
sampleList.head.next = null; //Making Old head Point to Null 
sampleList.head = newHead; //Upding new Head 
sampleList.printLinkedList();
