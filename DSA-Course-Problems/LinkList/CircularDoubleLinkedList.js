/**
 * {"head":{"value":10,"next":{"value":5,"next":{"value":16,"next":null}}}}
 */

class Node {
  value;
  next;
  prev;
  constructor(value) {
    console.log(`Adding Node : ${value}`);
    this.value = value;
    this.next = null;
    this.prev = null;
  }
  printNode() {
    return `${this.prev != null ? this.prev.value : "null"} <--- ${
      this.value
    } --> ${this.next != null ? this.next.value : "null"}`;
  }
}
class LinkedList {
  head;
  tail;
  length;
  //Constructor is used when First time tyou create LinkedList
  constructor(value) {
    let node = new Node(value);
    // this.head = {
    //   value: value,
    //   next: null,
    // };
    this.head = node;
    this.tail = this.head;
    this.tail.next = this.head;
    this.head.prev = this.tail;
    this.length = 1;
  }

  reverseDoubleLinkedList() {
    if (this.length == 1) {
      console.log(`Only One element - Nothing to Reverse`);
      return this;
    } else {
      let headTemp = this.head;
      let tailTemp = this.tail;

      let first = this.head;
      let second = first.next;
      while (second != this.head) {
        let third = second.next;
        console.log(
          `First : ${first != null ? first.value : "null"} ||Second : ${
            second != null ? second.value : "null"
          } || Third : ${third != null ? third.value : "null"}`
        );
        //Do the Link Reversal
        second.next = first;
        second.prev = third;
        first = second;
        second = third;
      }
      this.head.prev = this.head.next;
      this.head.next = this.tail;
      this.tail = headTemp; //Resetting the Tail Position
      this.head = tailTemp; //Resetting Head to this Position
      console.log(`Head Node Configuration Finally : ${this.head.printNode()}`);
      console.log(`Tail Node Configuration Finally : ${this.tail.printNode()}`);
    }

    return this;
  }

  removeFromHead() {
    if (this.length == 0) {
      throw new Error("Nothing is there To delete");
    }
    let temp = this.head;
    this.head = this.head.next;
    this.head.prev = this.tail;
    this.tail.next = this.head;
    delete temp.next;
    delete temp.prev;
    console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);
    temp = null; //Let it get garbage collected
    this.length--;
    return this;
  }

  removeFromTail() {
    if (this.length == 0) {
      throw new Error("Nothing is there o delete");
    }
    let temp = this.tail;
    this.tail = this.tail.prev;
    this.tail.next = this.head;
    this.head.prev = this.tail;
    delete temp.next;
    delete temp.prev;
    console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);
    temp = null; //Let it get garbage collected
    this.length--;
    return this;
  }

  //append More elements at E   nd of LinkedList
  addElementAttEnd(value) {
    // let node = {
    //   value: value,
    //   next: null,
    // };
    let node = new Node(value);
    node.prev = this.tail;
    this.tail.next = node;
    this.tail = node;
    this.head.prev = node;
    node.next = this.head;
    this.length++;
    return this;
  }

  appendAtStart(value) {
    // let node = {
    //   value: value,
    //   next: null,
    // };
    let node = new Node(value);
    node.next = this.head;
    this.head.prev = node;
    this.head = node;
    node.prev = this.tail;
    this.tail.next = node;
    this.length++;
    return this;
  }

  printLinkedListForward() {
    let arr = [];
    let current = this.head;
    while (current != this.tail) {
      arr.push(current.value);
      arr.push("-->");
      current = current.next;
    }
    arr.push(current.value); //Last elemebnt
    console.log(
      `Printed Linked List with Size - ${
        this.length
      } Linked List FORWARD [ ${arr.join(" ")} ]`
    );
    return this;
  }

  printLinkedListBackword() {
    let arr = [];
    let current = this.tail;
    while (current != this.head) {
      arr.push(current.value);
      arr.push("-->");
      current = current.prev;
    }
    arr.push(current.value); //First elemebnt
    console.log(
      `Printed Linked List with Size - ${
        this.length
      } Linked List BACKWORD [ ${arr.join(" ")} ]`
    );
    return this;
  }

  appendAtPositionNInMiddle(value, position) {
    if (position > this.length + 1 || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.appendAtStart(value);
    } else if (position === this.length + 1) {
      return this.addElementAttEnd(value);
    } else {
      position--; //Go till prev Node where we need to Insert
      // let node = {
      //   value: value,
      //   next: null,
      // };
      let node = new Node(value);
      let current = this.head;
      let index = 1;
      while (index != position) {
        index++;
        current = current.next;
      }
      //Now I have reached to the Insertion Position
      node.next = current.next;
      current.next.prev = node;
      node.prev = current;
      current.next = node;
      this.length++;
      return this;
    }
  }

  deleteAtPositionNInMiddleByPosition(position) {
    if (position > this.length || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.removeFromHead();
    } else if (position === this.length) {
      return this.removeFromTail();
    } else {
      //If Position is Skewed - I can traver either from Head or From Tail Side
      position--; //Go till prev Node where we need to Insert
      let current = null;
      let temp = null;
      if (position < Math.floor(this.length / 2)) {
        console.log(`Start at the Head Position `);
        current = this.head;
        let index = 1;
        while (index != position) {
          index++;
          current = current.next;
        }
        temp = current.next;
        console.log(
          `Adjacent Node to the Targeted Node : ${current.printNode()}`
        );
        console.log(`This Node will be deleted : ${temp.printNode()}`);
        current.next = temp.next;
        temp.next.prev = current;
        delete temp.next;
        delete temp.prev;
        console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);
        temp = null;
      } else {
        // console.log(`Start at the Tail Position `);
        position = this.length - position + 1;
        current = this.tail;
        let index = 1;
        while (index != position) {
          index++;
          current = current.prev;
        }
        temp = current.next;
        console.log(
          `Adjacent Node to the Targeted Node : ${current.printNode()}`
        );
        console.log(`This Node will be deleted : ${temp.printNode()}`);
        current.next = temp.next;
        temp.next.prev = current;
        delete temp.next;
        delete temp.prev;
        console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);
        temp = null;
      }
      this.length--;
      return this;
    }
  }
}

let createdLinkedList = new LinkedList(100);
console.log(`ADD DATA AT THE END OF DOUBLE LINKED LIST`);
createdLinkedList
  .addElementAttEnd(200)
  .addElementAttEnd(300)
  .addElementAttEnd(400)
  .printLinkedListForward()
  .printLinkedListBackword();

console.log(`ADD DATA AT THE START OF DOUBLE LINKED LIST`);
createdLinkedList
  .appendAtStart(199)
  .appendAtStart(198)
  .appendAtStart(197)
  .printLinkedListForward()
  .printLinkedListBackword();

console.log(`ADD DATA AT THE MIDDLE OF DOUBLE LINKED LIST`);
createdLinkedList
  .appendAtPositionNInMiddle(44, 2)
  .appendAtPositionNInMiddle(33, 1)
  .appendAtPositionNInMiddle(55, createdLinkedList.length + 1)
  .printLinkedListForward()
  .printLinkedListBackword();

console.log(`REMOVE  DATA AT THE HEAD OF DOUBLE LINKED LIST`);

createdLinkedList
  .removeFromHead()
  .removeFromHead()
  .printLinkedListForward()
  .printLinkedListBackword();

console.log(`REMOVE  DATA AT THE TAIL OF DOUBLE LINKED LIST`);

createdLinkedList
  .removeFromTail()
  .removeFromTail()
  .printLinkedListForward()
  .printLinkedListBackword();

console.log(`REMOVE  DATA AT THE MIDDLE OF DOUBLE LINKED LIST`);

createdLinkedList
  .deleteAtPositionNInMiddleByPosition(2)
  .printLinkedListForward()
  .printLinkedListBackword();

createdLinkedList
  .deleteAtPositionNInMiddleByPosition(4)
  .printLinkedListForward()
  .printLinkedListBackword();
console.log("REVERSE THE CIRCULAR LINKED LIST");
createdLinkedList
  .printLinkedListForward()
  .reverseDoubleLinkedList()
  .printLinkedListForward()
  .reverseDoubleLinkedList()
  .printLinkedListForward();

console.log(`Head Config Now : ${createdLinkedList.head.printNode()}`);
console.log(`Tail Config Now : ${createdLinkedList.tail.printNode()}`);
