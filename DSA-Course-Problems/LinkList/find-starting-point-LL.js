//Given the head of a linked list, return the node where the cycle begins. If there is no cycle, return null.
/**
 * So here first intution is Just detect the cycle using slow and fast tortoise.
 * Once you found the meeting Point means - cycle is detected .
 * Now again start a third pointer from head of ll
 * Now if we move slow and third pointer - they are boubnd to meet at the cycle starting point - there is a mathematical reason for this 
 * Suppose when the cycle is detected at that point 
 * SLOW is positioned at  L2 duistance from cycle start point 
 * suppose head to cycle start point is L1
 * suppose length of cycle is c
 * so it may happen that fast have traversed couple of cycle then matched eventually to slow pointer 
 * so L1 + L2 = total distance covered by slow pointer
 *  cn + (L1+L2) = total distance covered by fast pointer 
 * 
 * But we know fast pointer goes double that slow pointer 
 * so 
 * 2(L1 + L2) = cn + (L1+L2)
 *  L1 + L2 = cn = C [n does not matter ]
 * So L1 = L2-C;
 * so when we start third pointer and the slow pointer we can gurantee that 
 * they will meet at cycle start point because 
 * third pointer and slow pointer move at same speed 
 * and third pointer will traverse L1 distance by then slow pointer will must traverse L2-C distance which are actually same distance
 * 
 * 
 
 */





