import LinkedList from "./ReusableLinkedList.js";

let sampleList = new LinkedList(100);
sampleList.addElementAttEnd(200);
sampleList.addElementAttEnd(300);
sampleList.addElementAttEnd(400);
sampleList.addElementAttEnd(500);
sampleList.addElementAttEnd(600);
//sampleList.addElementAttEnd(700);
sampleList.printLinkedList();

//Reverse a Linked List
sampleList.tail.next = sampleList.head.next.next; //Making a Cirsular Linked List
//console.log(`Given Head - ${JSON.stringify(sampleList.head)}`);

/*
Find of there is Cycle in the Linked List or Not 

Naive Approach is Take Hash of the Each Node and Store in Set  - If that Value again appear then Its present 

Efficient Approach is take a Slow Pointer and a Fast pointer --> when again Fast Pointer Meet Slow Pointer cycle detected - If both Never cross - No cycle present 
*/

let set = new Set();
let head = sampleList.head;
set.add(head);
let isCycle = false;
while (head != null) {
  head = head.next;
  if (!set.has(head)) {
    set.add(head);
  } else {
    isCycle = true;
    break;
  }
}
if (isCycle) {
  console.log(`Cycle Detected in LL --`);
} else {
  console.log(`No Cycle Detected in LL --`);
}

//------------------- Efficient Approach
isCycle = false;
let FAST = sampleList.head;
let SLOW = sampleList.head;

while (FAST != null && FAST.next != null) {
  SLOW = SLOW.next;
  FAST = FAST.next;
  FAST = FAST.next;
  if (SLOW === FAST) {
    isCycle = true;
    break;
  }
}

if (isCycle) {
  console.log(`Cycle Detected in LL --`);
} else {
  console.log(`No Cycle Detected in LL --`);
}
