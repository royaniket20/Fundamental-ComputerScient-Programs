/**
 * apple-->grapes --> pears
 *
 * apples
 * 8888 -> grapes
 *         9999 ->pears
 *                7777 -->null
 */
"use strict";
let obj1 = { name: "Aniket" };

//This is a pointer
let obj2 = obj1;

//obj1 & obj2 Point to same Location

console.log(obj1);

console.log(obj2);
obj1.age = 23;
console.log(obj2);
delete obj1.age;
console.log(obj2);
delete obj1.name;
console.log(obj2);

//************************************** */

/**
 * Lets create 10-->5-->16
 */

let myLinkedList = {
  head: {
    value: 10,
    next: {
      value: 5,
      next: {
        value: 16,
        next: null,
      },
    },
  },
};

console.log(JSON.stringify(myLinkedList));
//{"head":{"value":10,"next":{"value":5,"next":{"value":16,"next":null}}}}