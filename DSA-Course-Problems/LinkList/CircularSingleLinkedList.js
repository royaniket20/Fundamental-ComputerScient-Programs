/**
 * {"head":{"value":10,"next":{"value":5,"next":{"value":16,"next":null}}}}
 */

class Node {
  value;
  next;
  constructor(value) {
    this.value = value;
    this.next = null;
  }

  printNode() {
    return ` ${this.value} --> ${this.next != null ? this.next.value : "null"}`;
  }
}
class LinkedList {
  head;
  tail;
  length;
  //Constructor is used when First time tyou create LinkedList
  constructor(value) {
    let node = new Node(value);
    // this.head = {
    //   value: value,
    //   next: null,
    // };
    this.head = node;
    this.tail = this.head;
    this.length = 1;
    this.tail.next = this.head;
  }

  reverseSingleLinkedList() {
    if (this.length == 1) {
      console.log(`Only One element - Nothing to Reverse`);
      return this;
    } else {
      let tempHead = this.head;
      let tempTail = this.tail;

      let first = this.head;
      let second = first.next;
      while (second != this.head) {
        let third = second.next;
        console.log(
          `First : ${first != null ? first.value : "null"} ||Second : ${
            second != null ? second.value : "null"
          } || Third : ${third != null ? third.value : "null"}`
        );
        //Do the Link Reversal
        second.next = first;
        first = second;
        second = third;
      }
      this.head.next = this.tail; //Comleting the Circle
      this.head = tempTail;
      this.tail = tempHead;
    }

    return this;
  }

  removeFromHead() {
    if (this.length == 0) {
      throw new Error("Nothing is there o delete");
    }
    let temp = this.head;
    this.head = this.head.next;
    this.tail.next = this.head;
    delete temp.next;
    console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);
    temp = null; //Let it get garbage collected
    this.length--;
    return this;
  }

  removeFromTail() {
    if (this.length == 0) {
      throw new Error("Nothing is there o delete");
    }
    //Traver Till One item Before Tail and then Do deletion
    let current = this.head;
    while (current.next.next != this.head) {
      current = current.next;
    }
    console.log(`Reached to the 1 pos Before End - ${current.value}`);
    let temp = current.next;
    delete temp.next;
    console.log(`Before Delete - temp : ${JSON.stringify(temp)}`);

    current.next = this.head;
    this.tail = current;
    this.length--;
    return this;
  }

  //append More elements at E   nd of LinkedList
  addElementAttEnd(value) {
    // let node = {
    //   value: value,
    //   next: null,
    // };
    let node = new Node(value);
    this.tail.next = node;
    this.tail = node;
    node.next = this.head;
    this.length++;
    return this;
  }

  appendAtStart(value) {
    // let node = {
    //   value: value,
    //   next: null,
    // };
    let node = new Node(value);
    node.next = this.head;
    this.head = node;
    this.tail.next = node;
    this.length++;
    return this;
  }

  printLinkedList() {
    let arr = [];
    let current = this.head;
    while (current.next != this.head) {
      arr.push(current.value);
      arr.push("-->");
      current = current.next;
    }
    arr.push(current.value); //Last elemebnt
    console.log(
      `Printed Circular Linked List with Size - ${
        this.length
      } Linked List [ ${arr.join(" ")} ]`
    );
    return this;
  }

  printCircularLinkedListLooped(count) {
    let arr = [];
    let totalLoop = count;
    let current = this.head;
    while (count != null && count > 0) {
      arr.push(current.value);
      arr.push("-->");
      if (current === this.tail) {
        arr.push("|Circling|-->");
      }
      current = current.next;
      if (current.next === this.head) {
        count--;
      }
    }
    arr.push(current.value); //Last elemebnt
    console.log(
      `Printed Circular Linked List with Loop Count - ${totalLoop} Linked List [ ${arr.join(
        " "
      )} ]`
    );
    return this;
  }

  appendAtPositionNInMiddle(value, position) {
    if (position > this.length + 1 || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.appendAtStart(value);
    } else if (position === this.length + 1) {
      return this.addElementAttEnd(value);
    } else {
      position--; //Go till prev Node where we need to Insert
      // let node = {
      //   value: value,
      //   next: null,
      // };
      let node = new Node(value);
      let current = this.head;
      let index = 1;
      while (index != position) {
        index++;
        current = current.next;
      }
      //Now I have reached to the Insertion Position
      node.next = current.next;
      current.next = node;
      this.length++;
      return this;
    }
  }

  deleteAtPositionNInMiddleByPosition(position) {
    if (position > this.length || position < 1) {
      throw new Error("Position is Outside Bound");
    }
    if (position === 1) {
      return this.removeFromHead();
    } else if (position === this.length) {
      return this.removeFromTail();
    } else {
      position--; //Go till prev Node where we need to Insert
      let current = this.head;
      let index = 1;
      while (index != position) {
        index++;
        current = current.next;
      }
      //Now I have reached to the Deletion Position
      let temp = current.next;
      current.next = current.next.next;
      console.log(
        `Reached to the 1 pos Before End  - ${JSON.stringify(current.value)}`
      );
      delete temp.next;
      console.log(`This Node will be deleted - ${JSON.stringify(temp)}`);
      this.length--;
      return this;
    }
  }
}

let createdLinkedList = new LinkedList(100);
createdLinkedList.printLinkedList();

createdLinkedList
  .addElementAttEnd(200)
  .addElementAttEnd(300)
  .addElementAttEnd(400)
  .printLinkedList();

createdLinkedList
  .appendAtStart(99)
  .appendAtStart(88)
  .appendAtStart(77)
  .printLinkedList();

createdLinkedList
  .appendAtPositionNInMiddle(44, 2)
  .appendAtPositionNInMiddle(11, 5)
  .printLinkedList()
  .appendAtPositionNInMiddle(10, 1)
  .printLinkedList()
  .appendAtPositionNInMiddle(9, createdLinkedList.length + 1)
  .printLinkedList()
  .appendAtPositionNInMiddle(6, createdLinkedList.length)
  .printLinkedList()
  .appendAtPositionNInMiddle(3, 1)
  .printLinkedList();

createdLinkedList.removeFromHead().removeFromHead().printLinkedList();

createdLinkedList.removeFromTail().removeFromTail().printLinkedList();

createdLinkedList
  .deleteAtPositionNInMiddleByPosition(3)
  .printLinkedList()
  .deleteAtPositionNInMiddleByPosition(7)
  .printLinkedList();

createdLinkedList.printCircularLinkedListLooped(2);

createdLinkedList.reverseSingleLinkedList().printLinkedList();

console.log(`Head Config Now : ${createdLinkedList.head.printNode()}`);
console.log(`Tail Config Now : ${createdLinkedList.tail.printNode()}`);

createdLinkedList.reverseSingleLinkedList().printLinkedList();

console.log(`Head Config Now : ${createdLinkedList.head.printNode()}`);
console.log(`Tail Config Now : ${createdLinkedList.tail.printNode()}`);
