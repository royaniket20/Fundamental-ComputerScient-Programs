/**
 * 

1
01
101
0101
10101
010101
1010101
01010101
101010101

*/

function pattern11(rows){
    for (let index = 0; index <rows; index++) {
        let startCount = index+1;
        let num = index%2;
        while(startCount>0){
            num  = num === 0 ?1 : 0;
            process.stdout.write(""+num);
            startCount--;
        }
       console.log(); 
    }
}

pattern11(9);