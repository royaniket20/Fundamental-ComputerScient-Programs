/**
 * 
// For each Row - The space count goes from Rowcount toward 0 
//For each Row Start count Increae in This Formula - (n*2) -1 // 1,3,5,7,9
     *     
    ***    
   *****   
  *******  
 ********* 


*/

export function pattern7(){
    for (let index = 0; index <5; index++) {
        //Calculate Start COunt 
        let starCount = (index+1)*2  -1;
        //Printing Space
       for (let inner = 5 - index-1; inner >0 ; inner--) {
        process.stdout.write(" "); 
       }
       //Printing Star
       while(starCount>0){
        process.stdout.write("*");
        starCount--;
       }
       //Printing Space
       for (let inner = 5 - index-1; inner >0 ; inner--) {
        process.stdout.write(" "); 
       }
       console.log(); 
    }
}
console.log(`Up Arrow Pattern - `);
pattern7();