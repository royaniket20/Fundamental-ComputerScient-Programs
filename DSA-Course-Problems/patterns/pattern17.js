/**
    A    
   ABA
  ABCBA
 ABCDCBA
ABCDEDCBA
*/

function pattern17(rows) {
  for (let index = 0; index < rows; index++) {
    let charStart = "A".charCodeAt(0) - 1;
    let charBreakpoint = rows;
    //console.log(`Requivalen of 'A' is ${charStart}`);
    //console.log(`Character Equivalen of ${charStart} is ${String.fromCharCode(charStart)}`)
    for (let indexInner = 0; indexInner < 2 * rows - 1; indexInner++) {
      let left = rows - index - 2;
      let right = rows + index;
      //console.log(left, indexInner, right);

      // Printing Initial Spaces
      if (indexInner <= left) {
        process.stdout.write(" ");
      }
      //Printing Ending Spaces
      else if (indexInner >= right) {
        process.stdout.write(" ");
      } else {
        //Print the chars
        if (indexInner >= charBreakpoint) {
          charStart--;
          process.stdout.write("" + String.fromCharCode(charStart));
        } else {
          charStart++;
          process.stdout.write("" + String.fromCharCode(charStart));
        }
      }
    }
    console.log();
  }
}

pattern17(5);
