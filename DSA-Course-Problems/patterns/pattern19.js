/**
* * * * * * * * * *
* * * *     * * * *
* * *         * * *
* *             * *
*                 *
*                 *
* *             * *
* * *         * * *
* * * *     * * * *
* * * * * * * * * *

*/

function pattern19(rows) {
  console.log(`Printing Pattern`);
  for (let index = 0; index < rows; index++) {
    let spaceCount = index*2;
    //Printig Left star 
    for (let index1 = 0; index1 < rows-index; index1++) {
      process.stdout.write("* ");
    }
     //Printig Space
     for (let index3 = 0; index3 < spaceCount; index3++) {
      process.stdout.write("  ");
    }
   //Printig Right star 
   for (let index2 = 0; index2 < rows-index; index2++) {
    process.stdout.write("* ");
  }
    console.log();
  }
  //Printing Second Half
  for (let index = 0; index < rows; index++) {
    let spaceCount = ((rows-1)*2) - (index*2);
    //Printig Left star 
    for (let index1 = 0; index1 < index+1; index1++) {
      process.stdout.write("* ");
    }
     //Printig Space
     for (let index3 = 0; index3 < spaceCount; index3++) {
      process.stdout.write("  ");
    }
   //Printig Right star 
   for (let index2 = 0; index2 < index+1; index2++) {
    process.stdout.write("* ");
  }
    console.log();
  }
}

pattern19(5);
