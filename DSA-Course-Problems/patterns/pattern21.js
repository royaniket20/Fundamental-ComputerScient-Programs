/**
* * * * *
*       *
*       *
*       *
* * * * *

*/

function pattern21(rows) {
  console.log(`Printing Pattern`);

  //Printing SQURE 
  for (let index = 0; index < rows; index++) {
     
    for (let index1 = 0; index1 < rows; index1++) {
      if(index === 0 || index === (rows-1)){
        //Print Full Lines 
        process.stdout.write("* ");
      }else {
        //Print with Spaces 
        if(index1 === 0  || index1 === (rows-1)){
          process.stdout.write("* ");
        }else{
          process.stdout.write("  ");
        }
      }
   
    }
    console.log();
  }
}

pattern21(5);
