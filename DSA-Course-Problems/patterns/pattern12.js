/**
 * 

1              1
12            21
123          321
1234        4321
12345      54321
123456    654321
1234567  7654321
1234567887654321

*/

function pattern12(rows) {
  for (let index = 0; index < rows; index++) {
    let breakpoint = rows;
    for (let indexInner = 0; indexInner < rows * 2; indexInner++) {
      if (indexInner > index && indexInner < rows * 2 - index - 1) {
        //Space Printing
        process.stdout.write(" ");
      } else {
        //Number er printing
        let num = indexInner + 1;
        //Number Decrement Case 
        if (indexInner >= breakpoint) {
          num = 2 * rows - indexInner;
          process.stdout.write("" + num);
        } else {
          process.stdout.write("" + num);
        }
      }
    }
    console.log();
  }
}

pattern12(8);
