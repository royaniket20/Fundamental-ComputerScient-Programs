/**
 * 
// For each Row - The space count goes from   0 toward Rowcount
*********
 ******* 
  *****  
   ***   
    *    



*/

export function pattern8(){
    for (let index = 5; index >0; index--) {
        //Calculate Start COunt 
        let starCount = (index)*2  -1;
        //Printing Space
       for (let inner = 5 - index; inner >0 ; inner--) {
        process.stdout.write(" "); 
       }
       //Printing Star
       while(starCount>0){
        process.stdout.write("*");
        starCount--;
       }
       //Printing Space
       for (let inner = 5 - index; inner >0 ; inner--) {
        process.stdout.write(" "); 
       }
       console.log(); 
    }
}
console.log(`Down Arrow Pattern - `);
pattern8();