/**
 * 

*
**
***
****
*****
******
****
***
**
*

*/

function pattern10(rowCount) {
  let breakPoint = Math.floor(rowCount / 2);
  console.log(`Breakpoint = ${breakPoint}`);
  for (let index = 0; index < rowCount; index++) {
    let startCount = index + 1;
    if (index >= breakPoint) {
      startCount = rowCount - index;
    }
    //console.log(startCount);
    while (startCount > 0) {
      process.stdout.write("*");
      startCount--;
    }
    console.log();
  }
}

pattern10(9);
