/**
 A B C D E
 A B C D
 A B C
 A B
 A

*/

function pattern15(rows) {
  for (let index = rows; index > 0; index--) {
    let charStart = "A".charCodeAt(0);
    //console.log(`Requivalen of 'A' is ${charStart}`);
    //console.log(`Character Equivalen of ${charStart} is ${String.fromCharCode(charStart)}`)
    for (let indexInner = 0; indexInner < index; indexInner++) {
      process.stdout.write(" " + String.fromCharCode(charStart++));
    }
    console.log();
  }
}

pattern15(5);
