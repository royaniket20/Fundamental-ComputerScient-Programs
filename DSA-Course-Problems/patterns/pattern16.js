/**
 A
 B B
 C C C
 D D D D
 E E E E E

*/

function pattern16(rows) {
  let charStart = 'A'.charCodeAt(0);
  //console.log(`Requivalen of 'A' is ${charStart}`);
  //console.log(`Character Equivalen of ${charStart} is ${String.fromCharCode(charStart)}`)
  for (let index = 0; index < rows; index++) {
 
    for (let indexInner = 0; indexInner < index + 1; indexInner++) {
      process.stdout.write(" " + String.fromCharCode(charStart));
    }
    console.log();
    charStart++;
  }
}

pattern16(5);
