/**
 A
 A B
 A B C
 A B C D
 A B C D E


*/

function pattern14(rows) {

  for (let index = 0; index < rows; index++) {
    let charStart = 'A'.charCodeAt(0);
    //console.log(`Requivalen of 'A' is ${charStart}`);
    //console.log(`Character Equivalen of ${charStart} is ${String.fromCharCode(charStart)}`)
    for (let indexInner = 0; indexInner < index + 1; indexInner++) {
      process.stdout.write(" " + String.fromCharCode(charStart++));
    }
    console.log();
  }
}

pattern14(5);
