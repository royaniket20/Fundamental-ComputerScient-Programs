/**
 4 4 4 4 4 4 4
 4 3 3 3 3 3 4
 4 3 2 2 2 3 4
 4 3 2 1 2 3 4
 4 3 2 2 2 3 4
 4 3 3 3 3 3 4
 4 4 4 4 4 4 4

*/

function pattern22(num) {
  console.log(`Printing Pattern`);

  //Printing SQURE 
  let rows = 2*num-1;
  for (let index = 0; index < rows; index++) {
    for (let index1 = 0; index1 < rows; index1++) {
         //Logic is here - For each element Find the Top , Buttom , Left , Right DFistance 
         //Then Just substract the Min distance from that Pos element 
         //Consider this as 2d array 
         let up = index;
         let down = rows-index-1;
         let left = index1;
         let right = rows-index1-1;
         let min = Math.min(up,down,left,right);
        process.stdout.write(" "+(num-min));
    }
    console.log();
  }
}

pattern22(4);
