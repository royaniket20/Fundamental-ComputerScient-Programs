/**
 1
 2 3
 4 5 6
 7 8 9 10
 11 12 13 14 15

*/

function pattern13(rows) {
  let startCount = 1;
  for (let index = 0; index < rows; index++) {
    for (let indexInner = 0; indexInner <index+1; indexInner++) {
      process.stdout.write(" " + startCount++);
    }
    console.log();
  }
}

pattern13(5);
