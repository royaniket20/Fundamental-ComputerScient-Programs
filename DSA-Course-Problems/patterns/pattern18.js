/**
 E
 D E
 C D E
 B C D E
 A B C D E

*/

function pattern18(rows) {
  for (let index = 0; index < rows; index++) {
    let charStart = "A".charCodeAt(0) + rows - 1;
    charStart = charStart - index;
    for (let indexInner = 0; indexInner < index + 1; indexInner++) {
      process.stdout.write(" " + String.fromCharCode(charStart));
      charStart++;
    }
    console.log();
  }
}

pattern18(5);
