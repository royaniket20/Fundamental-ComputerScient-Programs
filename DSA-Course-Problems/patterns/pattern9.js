/**
 * 
Diamond Pattern - 
    *    
   ***   
  *****  
 ******* 
*********
*********
 ******* 
  *****  
   ***   
    * 

*/
import {pattern7} from "./pattern7.js"
import {pattern8} from "./pattern8.js"
function pattern9(){
    pattern7();
    pattern8();
}
console.log(`Diamond Pattern - `);
pattern9();