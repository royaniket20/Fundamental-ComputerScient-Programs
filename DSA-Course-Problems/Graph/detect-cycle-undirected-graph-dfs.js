/**
 * cycle - start from a node - if you are able to come back to same Node
 * It has Cycle
 */

/**
 * Intution - Use BFS - breadth First search
 * Use adjaceny List
 * Now We know BFS will traverse - But How to detect Cycle Here
 * We know BFS is traversal techniqueu -
 * in BFS we will Only go to immediate neighbours ignoring the visited Ones
 *
 * IMPORTANT - if BFS Have a cyycle - Someday in Future Two Paths will cross on same Node - Starting from a Intial Node
 * This is when We have to say that It is cyclic graph
 *
 * It is Just a Modified BFS -
 * Here in Modified QUEUE you Put two elements
 * What you are Visiting as well as from where you are Visiting It
 * --Now in case of Cycle - you will come to one point wehre you have a visited Node
 * But that is Not from where you come from [Not your parent ]
 * --This means Cycle
 *
 */

/**
 *
 *  / 2 - 5 \
 * 1          7
 *  \ 3 - 6 /
 *
 * Here for 5 it is visitng 7 and that time store Its parent as 5
 * wHEN LATER 6 COMES FROM visiting it sees 7 is already visited - But Its parent is someone else Not me
 * Hence there is a cycle
 *
 * You ma need to code to Multiple disconnected components
 */
let input = [
  [7, 7],
  [1, 2],
  [2, 5],
  [5, 7],
  [7, 6],
  [6, 3],
  [3, 1],
  [3, 4],
];

let adj = convertInputToAdjList(input);
detectCycleUsingDFS(adj);

console.log(" //////////////////// ANOTHER EXAMPLE ///////////////////////");

input = [
  [7, 6],
  [1, 2],
  [2, 5],
  [5, 7],
  [6, 3],
  [3, 1],
  [3, 4],
];

adj = convertInputToAdjList(input);
detectCycleUsingDFS(adj);

function convertInputToAdjList(input) {
  let adj = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adj.length; index++) {
    adj[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adj[node1].push(node2);
    adj[node2].push(node1);
  }
  printAdjList(adj);
  return adj;
}

function printAdjList(adj) {
  for (let index = 1; index < adj.length; index++) {
    console.log(`[${index}] ---> {${adj[index]}}`);
  }
}

function detectCycleUsingDFS(adj) {
  let initNode = 1;
  let visitorArray = new Array(adj.length).fill(false);
  let isCycleDetected = new Array(adj.length).fill(false);
  deapthFirstSearchSpecial(
    adj,
    initNode,
    initNode,
    visitorArray,
    isCycleDetected
  );

  let  isCycle = false;
  for (let index = 1; index < isCycleDetected.length; index++) {
    if (isCycleDetected[index] == true) {
      isCycle = true;
      break;
    }
  }
  if(isCycle){
    console.log(`CYCLE IS PRESENT IN GRAPH`);
  }else{
    console.log(`CYCLE IS NOT PRESENT IN GRAPH`);

  }
}

function deapthFirstSearchSpecial(
  graphAAdjancyList,
  initNode,
  parentNode,
  visitorArray,
  isCycleDetected
) {
  console.log(`current Node ${initNode} | Parent Node - ${parentNode}`);
  console.log(`Visitor array state - ${visitorArray}`);
  visitorArray[initNode] = true;
  console.log(`Visiting Element - ${initNode}`);
  let adjacents = graphAAdjancyList[initNode];
  console.log(`Neighbours - ${adjacents}`);
  for (let index = 0; index < adjacents.length; index++) {
    const node = adjacents[index];
    console.log(`processing adjacent node - ${node}`);
    if (visitorArray[node] == false) {
      console.log(`RECUSRSIVE CALL  for Node - ${node}`);
      deapthFirstSearchSpecial(
        graphAAdjancyList,
        node,
        initNode,
        visitorArray,
        isCycleDetected
      );
    } else {
      if (node === parentNode) {
        console.log(`Node ${node} is already Visited`);
      } else {
        console.log(`CYCLE DETECTED AT NODE - ${node}`);
        isCycleDetected[node] = true;
      }
    }
  }
}
