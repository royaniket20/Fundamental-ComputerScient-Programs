// 0--1    3
//    |    |
//    2    4

//Notes on Ipad on BFS
let input = [
  [5, 3],
  [0, 1],
  [1, 2],
  [3, 4],
];
let printAdjancyListForUndirectedGrpah = function (input) {
  let adjancyList = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adjancyList.length; index++) {
    adjancyList[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adjancyList[node1].push(node2);
    adjancyList[node2].push(node1);
  }
  console.log(`Final Adjaency List represting the grpah - `);
  printMatrix(adjancyList);
  return adjancyList;
};
function deapthFirstSearch(graphAAdjancyList, initNode , visitorArray) {
  if(visitorArray == null){
    console.log(`Visitor array Initalized `);
     visitorArray = new Array(graphAAdjancyList.length).fill(false);
  }
  //Intial Setup For Levle 0 node
  visitorArray[initNode] = true;
  console.log(`Visiting Element - ${initNode}`);
  let adjacents = graphAAdjancyList[initNode]
  for (let index = 0; index < adjacents.length; index++) {
    const node  = adjacents[index];
    if(visitorArray[node] == false){
      deapthFirstSearch(graphAAdjancyList, node , visitorArray) 
    }else {
      console.log(`Node ${node} is already Visited`);
    }
  }
}
console.log(`Visitor array Initalized `);
let graphAAdjancyList = printAdjancyListForUndirectedGrpah(input);
let visitorArray = new Array(graphAAdjancyList.length).fill(false);
let  result = 0;
//Because this a a 0 based Node System 
for (let index = 0; index < visitorArray.length-1; index++) {
  if(visitorArray[index] == false){
    result++;
    deapthFirstSearch(graphAAdjancyList, index , visitorArray) 
  }
}
console.log("No of Connected Component - ",result);

//deapthFirstSearch(printAdjancyListForUndirectedGrpah(input), 1);
console.log("*****************");
input = [
  [9, 8],
  [1, 2],
  [1, 3],
  [2, 4],
  [4, 3],
  [5, 6],
  [5, 7],
  [6, 7],
  [8, 9],
];
console.log(`Visitor array Initalized `);
 graphAAdjancyList = printAdjancyListForUndirectedGrpah(input);
 visitorArray = new Array(graphAAdjancyList.length).fill(false);
  result = 0;
//Because this a 1 based Node System 
for (let index = 1; index < visitorArray.length; index++) {
  if(visitorArray[index] == false){
    result++;
    deapthFirstSearch(graphAAdjancyList, index , visitorArray) 
  }
}
console.log("No of Connected Component - ",result);




function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    process.stdout.write(`[${[index]}]-->`);
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}


//Time complexity - O(n + 2e)