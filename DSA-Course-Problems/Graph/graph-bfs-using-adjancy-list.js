//  1 --- 2---
//  |     |   \
//  |     |     5
//  3 --- 4-- /

//Notes on Ipad on BFS
let input = [
  [5, 6],
  [1, 2],
  [2, 5],
  [5, 4],
  [4, 2],
  [4, 3],
  [1, 3],
];
let printAdjancyListForUndirectedGrpah = function (input) {
  let adjancyList = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adjancyList.length; index++) {
    adjancyList[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adjancyList[node1].push(node2);
    adjancyList[node2].push(node1);
  }
  console.log(`Final Adjaency List represting the grpah - `);
  printMatrix(adjancyList);
  return adjancyList;
};
function breadthFirstSearch(graphAAdjancyList, initNode) {
  let visitorArray = new Array(graphAAdjancyList.length).fill(false);
  let bfsqueue = [];
  //Intial Setup For Levle 0 node
  visitorArray[initNode] = true;
  bfsqueue.unshift(initNode);

  while (bfsqueue.length>0) {
    initNode = bfsqueue.pop();
    console.log(`Visiting Element - ${initNode}`);
    console.log(`Finding Neighbours of ${initNode}`);
    console.log(`neighbours= ${graphAAdjancyList[initNode]}`);
    console.log(
      `Check the neighbour is already Visited or Not then move them to queue or else skip`
    );
    for (let index = 0; index < graphAAdjancyList[initNode].length; index++) {
      const element = graphAAdjancyList[initNode][index];
      if (visitorArray[element]) {
        console.log(`Node ${element} is already Visited`);
      } else {
        bfsqueue.unshift(element);
        visitorArray[element] = true;
      }
    }
    console.log(`bfs queue current state - ${bfsqueue}`);
  }
}
breadthFirstSearch(printAdjancyListForUndirectedGrpah(input), 1);
console.log("*****************");
breadthFirstSearch(printAdjancyListForUndirectedGrpah(input), 5);

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    process.stdout.write(`[${[index]}]-->`);
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}
//Time complexity - O(n + 2e)