//  1 --- 2---
//  |     |   \
//  |     |     5
//  3 --- 4-- /

/**
 * Input will be given as Array of Pairs 
 * Fist element of array is [V ,E ]-- V ==- Node , E == Edge Count 
 * 
 * [5 , 6] === Vertex , eDGE 
Rest below are edges --
* [1,2]
 * [2,5]
 * [5,4]
 * [4,2]
 * [4,3]
 * [1,3]
 * Here if the graph is Undirected - then we shoud assume [2,5] means [5,2]
 * That means if there is an edge between 2 --> 5 then there is an Edge from 5 --> 2
 * We need to create a List  using Vertex Size or [Vertex+1] - in case 1 based Index
 * Then based on Edge input Just keep the Neoughbour Edges in the Corresponding vertex as List in any order 
 * 1 --> {2,3}
 * 2 --> {1,5,4}
 * 3 --> {4,1}
 * 4 --> {5,2,3}
 * 5 --> {2 ,4}
 * tHE TIME O(N)
 * space = O(e * v)
 */
let input = [
  [5, 6],
  [1, 2],
  [2, 5],
  [5, 4],
  [4, 2],
  [4, 3],
  [1, 3],
];

let printAdjancyListForUndirectedGrpah = function (input) {
  console.log(`Given Input - ${input}`);
  console.log(
    `Given Vertex Count - ${input[0][0]} | Given Edge - ${input[0][1]}`
  );
  let adjancyList = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adjancyList.length; index++) {
    adjancyList[index] = new Array();
  }
  printMatrix(adjancyList);
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    //In case of Weighted Graph = 
    /**
     * let weight = edge[2]; //This will come from Input 
     */
    //Because it is a undirected graph a -> b and b-> a both need to be marked when any one of them given as Input
    adjancyList[node1].push(node2); 
    //In case of weighted Graph we need to use Pair 
     /**
      * let pair = {edge : node2 , weight : weight}
      *  adjancyList[node1].push(pair); 
      */
    adjancyList[node2].push(node1); //This will Not be required in Case of Directed graph 
  }
  console.log(`Final Adjaency List represting the grpah - `);
  printMatrix(adjancyList);
};

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    process.stdout.write(`[${[index]}]-->`);
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}
printAdjancyListForUndirectedGrpah(input);
