let islandMatrix = [
  [0, 1, 1, 0],
  [0, 1, 1, 0],
  [0, 0, 1, 0],
  [0, 0, 0, 0],
  [1, 1, 0, 1],
];

function findNoOfIslands(islandMatrix) {
  let visitedMatrix = new Array(islandMatrix.length).fill(null);
  for (let index = 0; index < visitedMatrix.length; index++) {
    visitedMatrix[index] = new Array(islandMatrix[index].length).fill(false);
  }
  let nboOfIsland = 0 ;
  console.log("Visited Matrix ------");
  printMatrix(visitedMatrix);
  console.log("Original Island Matrix ------");
  printMatrix(islandMatrix);

  for (let index = 0; index < islandMatrix.length; index++) {
    for (let inner = 0; inner < islandMatrix[index].length; inner++) {
      const element = islandMatrix[index][inner];
      if( !visitedMatrix[index][inner]) {
          if(islandMatrix[index][inner] == 1){
            console.log(` Got One New Island ----- islandMatrix[${index}][${inner}]`  );
            nboOfIsland++;
            bfs(islandMatrix, visitedMatrix,index, inner);
          }else {
            visitedMatrix[index][inner] = true; //Just wate 
          }
      }
    }
  }
  console.log("Updated  Visited Matrix ------");
  printMatrix(visitedMatrix);
  console.log("NO OF ISLAND FOUND - ", nboOfIsland);
  
}

function bfs(islandMatrix, visitedMatrix, row, col) {
  //Inital setup fo BFS
  let queue = [];
  visitedMatrix[row][col] = true;
  queue.unshift([row, col]);

  while (queue.length > 0) {
    let arr = queue.pop();
    const row = arr[0];
    const col = arr[1];
    console.log(
      `Visiting islandMatrix[${row}][${col}] : ${islandMatrix[row][col]}`
    );
    findConnections(row, col, queue, islandMatrix, visitedMatrix);
    console.log(`Adjacent Unvisited Elements - ${queue.join("|")}`);
    //Visiting adjacent elements
    for (let index = 0; index < queue.length; index++) {
      const row = queue[index][0];
      const col = queue[index][1];
      visitedMatrix[row][col] = true;
    }
  }
}

function findConnections(row, col, queue, islandMatrix, visitedMatrix) {
  let rowBound = islandMatrix.length;
  let colBound = islandMatrix[0].length;
  //left val
  if (
    row < rowBound &&
    col - 1 < colBound &&
    col - 1 > -1 &&
    islandMatrix[row][col - 1] == 1 &&
    visitedMatrix[row][col - 1] == false
  ) {
    queue.unshift([row, col - 1]);
  }
  //right val
  if (
    row < rowBound &&
    col + 1 < colBound &&
    islandMatrix[row][col + 1] == 1 &&
    visitedMatrix[row][col + 1] == false
  ) {
    queue.unshift([row, col + 1]);
  }
  //up val
  if (
    row - 1 < rowBound &&
    row - 1 > -1 &&
    col < colBound &&
    islandMatrix[row - 1][col] == 1 &&
    visitedMatrix[row - 1][col] == false
  ) {
    queue.unshift([row - 1, col]);
  }
  //down val
  if (
    row + 1 < rowBound &&
    col < colBound &&
    islandMatrix[row + 1][col] == 1 &&
    visitedMatrix[row + 1][col] == false
  ) {
    queue.unshift([row + 1, col]);
  }
  //left up corner
  if (
    row - 1 < rowBound &&
    row - 1 > -1 &&
    col - 1 < colBound &&
    col - 1 > -1 &&
    islandMatrix[row - 1][col - 1] == 1 &&
    visitedMatrix[row - 1][col - 1] == false
  ) {
    queue.unshift([row - 1, col - 1]);
  }
  //right up corner
  if (
    row - 1 < rowBound &&
    row - 1 > -1 &&
    col + 1 < colBound &&
    islandMatrix[row - 1][col + 1] == 1 &&
    visitedMatrix[row - 1][col + 1] == false
  ) {
    queue.unshift([row - 1, col + 1]);
  }
  //left down corner
  if (
    row + 1 < rowBound &&
    col - 1 < colBound &&
    col - 1 > -1 &&
    islandMatrix[row + 1][col - 1] == 1 &&
    visitedMatrix[row + 1][col - 1] == false
  ) {
    queue.unshift([row + 1, col - 1]);
  }
  //right down corner
  if (
    row + 1 < rowBound &&
    col + 1 < colBound &&
    islandMatrix[row + 1][col + 1] == 1 &&
    visitedMatrix[row + 1][col + 1] == false
  ) {
    queue.unshift([row + 1, col + 1]);
  }
}

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}

findNoOfIslands(islandMatrix);
