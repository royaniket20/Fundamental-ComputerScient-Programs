// 0--1    3
//    |    |
//    2    4

//Notes on Ipad on BFS
let input = [
  [5, 3],
  [0, 1],
  [1, 2],
  [3, 4],
];
let printAdjancyListForUndirectedGrpah = function (input) {
  let adjancyList = new Array(input[0][0]).fill(null);
  for (let index = 0; index < adjancyList.length; index++) {
    adjancyList[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adjancyList[node1].push(node2);
    adjancyList[node2].push(node1);
  }
  console.log(`Final Adjaency List represting the grpah - `);
  printMatrix(adjancyList);
  return adjancyList;
};

function bfs(graphAAdjancyList, visitorArray, initNode) {
  let queue = [];
  visitorArray[initNode] = true; //Node is Now Visited
  queue.unshift(initNode);
  while (queue.length > 0) {
    let element = queue.pop();
    let adjacentNodes = graphAAdjancyList[element];
    for (let index = 0; index < adjacentNodes.length; index++) {
      const element = adjacentNodes[index];
      if (visitorArray[element] != true) {
        visitorArray[element] = true;
        queue.unshift(element);
      }
    }
  }
}
function noOfConnectedComponents(graphAAdjancyList) {
  let result = 0;
  let visitorArray = new Array(graphAAdjancyList.length).fill(false);
  for (let index = 1; index < visitorArray.length; index++) {
    if (visitorArray[index] == false) {
      result++;
      bfs(graphAAdjancyList, visitorArray, index);
    }
  }
  console.log(`Number of Connected Components - ${result}`);
}
noOfConnectedComponents(printAdjancyListForUndirectedGrpah(input));
input = [
  [10, 8],
  [1, 2],
  [1, 3],
  [2, 4],
  [4, 3],
  [5, 6],
  [5, 7],
  [6, 7],
  [8, 9],
];
noOfConnectedComponents(printAdjancyListForUndirectedGrpah(input));

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    process.stdout.write(`[${[index]}]-->`);
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}
