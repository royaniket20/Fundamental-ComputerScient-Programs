/**
 * cycle - start from a node - if you are able to come back to same Node
 * It has Cycle
 */

/**
 * Intution - Use BFS - breadth First search
 * Use adjaceny List
 * Now We know BFS will traverse - But How to detect Cycle Here
 * We know BFS is traversal techniqueu -
 * in BFS we will Only go to immediate neighbours ignoring the visited Ones
 *
 * IMPORTANT - if BFS Have a cyycle - Someday in Future Two Paths will cross on same Node - Starting from a Intial Node
 * This is when We have to say that It is cyclic graph
 *
 * It is Just a Modified BFS -
 * Here in Modified QUEUE you Put two elements
 * What you are Visiting as well as from where you are Visiting It
 * --Now in case of Cycle - you will come to one point wehre you have a visited Node
 * But that is Not from where you come from [Not your parent ]
 * --This means Cycle
 *
 */

/**
 *
 *  / 2 - 5 \
 * 1          7
 *  \ 3 - 6 /
 *
 * Here for 5 it is visitng 7 and that time store Its parent as 5
 * wHEN LATER 6 COMES FROM visiting it sees 7 is already visited - But Its parent is someone else Not me
 * Hence there is a cycle
 *
 * You ma need to code to Multiple disconnected components
 */
let input = [
  [7, 7],
  [1, 2],
  [2, 5],
  [5, 7],
  [7, 6],
  [6, 3],
  [3, 1],
  [3, 4],
];

let adj = convertInputToAdjList(input);
detectCycleUsingBFS(adj);

console.log(" //////////////////// ANOTHER EXAMPLE ///////////////////////");

input = [
  [7, 6],
  [1, 2],
  [2, 5],
  [5, 7],
  [6, 3],
  [3, 1],
  [3, 4],
];

adj = convertInputToAdjList(input);
detectCycleUsingBFS(adj);

function convertInputToAdjList(input) {
  let adj = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adj.length; index++) {
    adj[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adj[node1].push(node2);
    adj[node2].push(node1);
  }
  printAdjList(adj);
  return adj;
}

function printAdjList(adj) {
  for (let index = 1; index < adj.length; index++) {
    console.log(`[${index}] ---> {${adj[index]}}`);
  }
}

function detectCycleUsingBFS(adj) {
  let initNode = 1;
  breadthFirstSearchSpecial(adj, initNode);
}

function breadthFirstSearchSpecial(graphAAdjancyList, initNode) {
  let visitorArray = new Array(graphAAdjancyList.length).fill(false);
  //This will store immediate parent also from where It got visited
  let immediateParent = new Array(graphAAdjancyList.length).fill(-1);
  let bfsqueue = []; // This will store Node for bfs
  //Intial Setup For Levle 0 node
  visitorArray[initNode] = true;
  let initParent = initNode;
  immediateParent[initNode] = initParent;
  bfsqueue.unshift(initNode);
  let isCycle = false;
  while (bfsqueue.length > 0) {
    initNode = bfsqueue.pop();
    console.log(`Visiting Element - ${initNode}`);
    console.log(`Finding Neighbours of ${initNode}`);
    console.log(`neighbours= ${graphAAdjancyList[initNode]}`);
    console.log(
      `Check the neighbour is already Visited or Not then move them to queue or else skip`
    );
    for (let index = 0; index < graphAAdjancyList[initNode].length; index++) {
      const element = graphAAdjancyList[initNode][index];
      if (visitorArray[element]) {
        if (element == immediateParent[initNode]) {
          //This Node is visited by current avelrsal path only
          console.log(`Node ${element} is already Visited`);
        } else {
          console.log(` CYCLE IS DETECTED AT NODE - ${element}`);
          isCycle = true;
          break;
        }
      } else {
        console.log(`adding ${element} as visited in system`);
        bfsqueue.unshift(element);
        visitorArray[element] = true;
        immediateParent[element] = initNode;
      }
    }
    console.log(`bfs queue current state - ${bfsqueue}`);
    console.log(`visitor arry state  - ${visitorArray}`);
    console.log(`immediate parent  array state - ${immediateParent}`);

    if (isCycle) {
      break;
    }
  }
  console.log("End of BFS - Is Cycle Detected - " + isCycle);
}
