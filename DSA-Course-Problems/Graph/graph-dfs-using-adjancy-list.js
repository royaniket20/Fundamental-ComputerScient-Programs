//  1 --- 2---
//  |     |   \
//  |     |     5
//  3 --- 4-- /

//Notes on Ipad on BFS
let input = [
  [5, 6],
  [1, 2],
  [2, 5],
  [5, 4],
  [4, 2],
  [4, 3],
  [1, 3],
];
let printAdjancyListForUndirectedGrpah = function (input) {
  let adjancyList = new Array(input[0][0] + 1).fill(null);
  for (let index = 0; index < adjancyList.length; index++) {
    adjancyList[index] = new Array();
  }
  for (let index = 1; index < input.length; index++) {
    let edge = input[index];
    let node1 = edge[0];
    let node2 = edge[1];
    adjancyList[node1].push(node2);
    adjancyList[node2].push(node1);
  }
  console.log(`Final Adjaency List represting the grpah - `);
  printMatrix(adjancyList);
  return adjancyList;
};
function deapthFirstSearch(graphAAdjancyList, initNode , visitorArray) {
  if(visitorArray == null){
    console.log(`Visitor array Initalized `);
     visitorArray = new Array(graphAAdjancyList.length).fill(false);
  }
  //Intial Setup For Levle 0 node
  visitorArray[initNode] = true;
  console.log(`Visiting Element - ${initNode}`);
  let adjacents = graphAAdjancyList[initNode]
  for (let index = 0; index < adjacents.length; index++) {
    const node  = adjacents[index];
    if(visitorArray[node] == false){
      deapthFirstSearch(graphAAdjancyList, node , visitorArray) 
    }else {
      console.log(`Node ${node} is already Visited`);
    }
  }
}
deapthFirstSearch(printAdjancyListForUndirectedGrpah(input), 1);
console.log("*****************");
input = [
  [8, 8],
  [1, 2],
  [1,3],
  [2, 5],
  [2,6],
  [3, 4],
  [3,7],
  [4, 8],
  [8,7]
];
deapthFirstSearch(printAdjancyListForUndirectedGrpah(input), 1);

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    process.stdout.write(`[${[index]}]-->`);
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}


//Time complexity - O(n + 2e)