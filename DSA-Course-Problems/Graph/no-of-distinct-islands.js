/**Number of Distinct Islands
Difficulty: MediumAccuracy: 62.29%Submissions: 90K+Points: 4
Given a boolean 2D matrix grid of size n * m. You have to find the number of distinct islands where a group of connected 1s (horizontally or vertically) forms an island. Two islands are considered to be distinct if and only if one island is not equal to another (not rotated or reflected).

Example 1:

Input:
grid[][] = {{1, 1, 0, 0, 0},
            {1, 1, 0, 0, 0},
            {0, 0, 0, 1, 1},
            {0, 0, 0, 1, 1}}
Output:
1
Explanation:
grid[][] = {{1, 1, 0, 0, 0}, 
            {1, 1, 0, 0, 0}, 
            {0, 0, 0, 1, 1}, 
            {0, 0, 0, 1, 1}}
Same colored islands are equal.
We have 2 equal islands, so we 
have only 1 distinct island.

Example 2:

Input:
grid[][] = {{1, 1, 0, 1, 1},
            {1, 0, 0, 0, 0},
            {0, 0, 0, 0, 1},
            {1, 1, 0, 1, 1}}
Output:
3
Explanation:
grid[][] = {{1, 1, 0, 1, 1}, 
            {1, 0, 0, 0, 0}, 
            {0, 0, 0, 0, 1}, 
            {1, 1, 0, 1, 1}}
Same colored islands are equal.
We have 4 islands, but 2 of them
are equal, So we have 3 distinct islands.

 */
/**
 * Intution - store structure in Set and count set length 
 * How we can store the Structe to calculatr they are actual Identical 
 *            0  1  2  3  4
 *        0  {1, 1, 0, 1, 1}
          1  {1, 0, 0, 0, 0} 
          2  {0, 0, 0, 1, 1} 
          3  {1, 1, 0, 1, 0}

Here above visually we can see that  [0][0]--[0][1]
                                       |
                                     [1][0]  
Here above visually we can see that  [2][3]--[2][4]
                                       |
                                     [3][3]  
Above two are Idential - But due to the  th                                     
 */