/**
Given a matrix where each cell in the matrix can have values 0, 1 or 2 which has the following meaning:
0 : Empty cell
1 : Cells have fresh oranges
2 : Cells have rotten oranges

We have to determine what is the earliest time after which all the oranges are rotten. A rotten orange at index [i, j] can rot other fresh orange at indexes [i-1, j], [i+1, j], [i, j-1], [i,j+1] (up, down, left and right) in unit time.

Note: Your task is to return the minimum time to rot all the fresh oranges. If not possible returns -1.

Examples:

Input: mat[][] = 
[[0, 1, 2], 
[0, 1, 2], 
[2, 1, 1]]
Output: 1
Explanation: Oranges at positions (0,2), (1,2), (2,0) will rot oranges at (0,1), (1,1), (2,2) and (2,1) in unit time.
Input: mat[][] = [[2, 2, 0, 1]]
Output: -1
Explanation: Oranges at (0,0) and (0,1) can't rot orange at (0,3).
Input: mat[][] = 
[[2, 2, 2], 
[0, 2, 0]]
Output: 0
Explanation: There is no fresh orange.
Constraints:
1 ≤ mat.size() ≤ 500
1 ≤ mat[0].size() ≤ 500
mat[i][j] = {0, 1, 2} 
Intution - 
We have to find the minimum time to rotten all tomato 
Find out all the starting points and Push ti queue for BFS with time 0 
next Just do bfs for each element in queue with One time Up at each Iteration 
Eventually track the time and look if al orages rotten 

 */

let orangeFormation = [
  [0, 1, 2],
  [0, 1, 2],
  [2, 1, 1],
];

orangeFormation = [[2, 2, 0, 1]];

orangeFormation = [
  [2, 2, 2],
  [0, 2, 0],
];

orangeFormation = [
  [0, 1, 2],
  [0, 1, 1],
  [2, 1, 1],
];
//Its a simple traversal of the elements
//And Rot the elements as we traverse

// Yoyu need to do Rot in Minimum time -
//BFS - Will be used as We simultanepously - so less time will be needed
//If we use DFS - It go uni Direction and will take Much more time

/**
 * Use a 2d Visited array - initally make mark on Already rotten tomato
 * Also in Queu Start Puting [row][col] , (time)
 * Then Just use Queue and Visited array Matrix to Solve
 */

function rottenTomato(orangeFormation) {
  let visitedArray = prepareVisitedArray(orangeFormation);
  console.log(`Tomato Array --------`);
  printMatrix(orangeFormation);
  console.log(`Visited Array -------`);
  printMatrix(visitedArray);
  let queue = [];
  for (let outer = 0; outer < orangeFormation.length; outer++) {
    for (let inner = 0; inner < orangeFormation[outer].length; inner++) {
      const element = orangeFormation[outer][inner];
      if (element == 2) {
        console.log(`Got one rotten Tomato at [${outer}][${inner}]`);
        queue.unshift([outer, inner, 0]); //Starting Point so No cost
        visitedArray[outer][inner] = true;
      }
    }
  }
  let total = bfs(queue, visitedArray, orangeFormation); //Call for BFS
  printMatrix(visitedArray);

  printMatrix(orangeFormation);
  //CHECK IF SOMETHING LEFT BEHIND
  for (let outer = 0; outer < orangeFormation.length; outer++) {
    for (let inner = 0; inner < orangeFormation[outer].length; inner++) {
      const element = orangeFormation[outer][inner];
      if (element == 1 && visitedArray[outer][inner] == false) {
        total = -1;
        break;
      }
    }
    if (total == -1) {
      break; //Break outer Loop
    }
  }

  console.log(" FINAL COST TO ROT TOMATOS - " + total);
}

function bfs(queue, visitedArray, orangeFormation) {
  let total = 0;
  //Now Only Visit the Non Rotten Tomato Positions

  while (queue.length > 0) {
    let element = queue.pop();
    let outer = element[0];
    let inner = element[1];
    let cost = element[2];
    total = Math.max(total, cost);
    console.log(`Visiting Positions adjacent to [${outer}][${inner}]`);
    positionsToVisit(
      outer,
      inner,
      queue,
      visitedArray,
      orangeFormation,
      cost + 1
    ); //Cost increased for next level
    queue.forEach((item) => {
      console.log(
        `Rotting - tomato at [${item[0]}][${item[1]}] with Cost ${item[2]}`
      );
      visitedArray[item[0]][item[1]] = true;
    });
  }
  return total;
}
function positionsToVisit(
  row,
  col,
  queue,
  visitedArray,
  orangeFormation,
  cost
) {
  let rowLowerBound = 0;
  let rowUpperBound = visitedArray.length - 1;

  let colLowerBound = 0;
  let colUpperBound = visitedArray[0].length - 1;

  //for Up
  if (
    row - 1 >= rowLowerBound &&
    row - 1 <= rowUpperBound &&
    visitedArray[row - 1][col] == false &&
    orangeFormation[row - 1][col] == 1
  ) {
    queue.unshift([row - 1, col, cost]);
  }

  //For Down
  if (
    row + 1 >= rowLowerBound &&
    row + 1 <= rowUpperBound &&
    visitedArray[row + 1][col] == false &&
    orangeFormation[row + 1][col] == 1
  ) {
    queue.unshift([row + 1, col, cost]);
  }
  //For Left
  if (
    col - 1 >= colLowerBound &&
    col - 1 <= colUpperBound &&
    visitedArray[row][col - 1] == false &&
    orangeFormation[row][col - 1] == 1
  ) {
    queue.unshift([row, col - 1, cost]); //1 - cost of rotting this tomato
  }
  //For Right

  if (
    col + 1 >= colLowerBound &&
    col + 1 <= colUpperBound &&
    visitedArray[row][col + 1] == false &&
    orangeFormation[row][col + 1] == 1
  ) {
    queue.unshift([row, col + 1, cost]);
  }
}

function printMatrix(matrix) {
  console.log();
  for (let index = 0; index < matrix.length; index++) {
    console.log(matrix[index].join(", "));
  }
}

function prepareVisitedArray(data) {
  let matrix = new Array(data.length).fill(null);
  for (let index = 0; index < matrix.length; index++) {
    matrix[index] = new Array(data[index].length).fill(false);
  }
  return matrix;
}

rottenTomato(orangeFormation);
