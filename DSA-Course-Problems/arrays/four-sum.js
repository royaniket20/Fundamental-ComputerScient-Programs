/**
 * Better Solution - We are using hashing  - to Find that Quadropolet is
 * Have one Loop i from 0-> n-1
 * Another Loop will Start from j = i+1 -> n-1
 * Another Loop will Start from k = j+1 -> n-1
 * So Inner Loop We have access to Three elements - arr[i] , arr[j] i.e arr[i+1] , arr[k] i.e arr[i+2]
 * Now Find out whats the elelemnt Required to Fulfil target   next = target - (nums[i] + nums[j] + nums[k])
 * check if its available in set - then tuplet formed else Put on Set and Move on to next iteration
 * @param {*} nums
 * @param {*} target
 * @returns
 */

var fourSum = function (nums, target) {
  let result = new Set();
  for (let h = 0; h < nums.length; h++) {
    for (let i = h + 1; i < nums.length; i++) {
      let set = new Set();
      for (let j = i + 1; j < nums.length; j++) {
        console.log(
          `Calculatng Fourth element for arr[${h}] , arr[${i}] , arr[${j}]`
        );
        let next = target - (nums[h] + nums[i] + nums[j]);
        if (set.has(next)) {
          let arr = [nums[h], nums[i], nums[j], next].sort((a, b) => a - b);
          result.add(arr.join(","));
        } else {
          set.add(nums[j]);
        }
      }
    }
  }
  let finalResult = [];
  result.forEach((item) => {
    finalResult.push(item.split(","));
  });
  return finalResult;
};

process.stdout.write(`Four Sum Digits - \n`);
console.log(fourSum([1, 0, -1, 0, -2, 2], 0));

process.stdout.write(`Four Sum Digits - \n`);
console.log(fourSum([2, 2, 2, 2, 2], 8));

process.stdout.write(`Four Sum Digits - \n`);
console.log(fourSum([1, 0, -1, 0, -2, 2], 0));

process.stdout.write(`Four Sum Digits - \n`);
console.log(fourSum([4, 3, 3, 4, 4, 2, 1, 2, 1, 1], 9));
