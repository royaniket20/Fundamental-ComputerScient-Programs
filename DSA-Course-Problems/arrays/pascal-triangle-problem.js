/* Program to generate Pascal's Triangle


Problem Statement: This problem has 3 variations. They are stated below:

Variation 1: Given row number r and column number c. Print the element at position (r, c) in Pascal’s triangle.

Variation 2: Given the row number n. Print the n-th row of Pascal’s triangle.

Variation 3: Given the number of rows n. Print the first n rows of Pascal’s triangle.

In Pascal’s triangle, each number is the sum of the two numbers directly above it as shown in the figure below:

Examples
Example 1:
Input Format:
 N = 5, r = 5, c = 3
Result:
 6 (for variation 1)
1 4 6 4 1 (for variation 2)

       1 
      1 1 
     1 2 1 
    1 3 3 1 
   1 4 6 4 1    (for variation 3)
 */
//Variation 1: Given row number r and column number c. Print the element at position (r, c) in Pascal’s triangle.

let factorial = function (num) {
  if (num === 0 || num === 1) return 1;
  return num * factorial(num - 1);
};

let function_variation1 = function (r, c) {
  console.log(`Given Row - ${r} || Given Column - ${c}`);
  console.log(`Finding the Element at R ,C is just using  [row-1]C[col-1]`);
  //nCr = n!/(r! * (n-r)!)
  let result =
    factorial(r - 1) / (factorial(c - 1) * factorial(r - 1 - (c - 1)));

  console.log(`Value is - ${result}`);
};

//Here the idea is Not retedly Calculate Factorial

/**
 * 8C3 == (8.7.6.5.4.3.2.1)/[(3.2.1) * (5*4*3*2.1)] === (8.7.6)/(3.2.1)
 * 4C2 == (4.3.2.1)/[(2.1)*(2.1)] == (4.3)/(2.1)
 *
 * hERE A PATTERN EMARGES TO CALCULATE nCr = the pattern is
 * Multiply the Numbers from N , n-1 ... Upto R terms
 * Multiply the Numbers from R ... upto 1
 * Then devide first devided by second
 */

let function_variation1_advanced = function (r, c ,logData) {
  if(logData) {
  console.log(`Given Row - ${r} || Given Column - ${c}`);
  console.log(`Finding the Element at R ,C is just using  [row-1]C[col-1]`);
  }
  //nCr = n!/(r! * (n-r)!)
  r = r - 1;
  c = c - 1;
  let numerator = 1;
  for (let index = 1; index <= c; index++) {
    numerator = numerator * r;
    r--;
  }
  let denominator = 1;
  for (let index = 1; index <= c; index++) {
    denominator = denominator * c;
    c--;
  }
  let result = numerator / denominator;
 if(logData)
  console.log(`Value is - ${result}`);
  return result;
};

function_variation1(5, 3);

function_variation1_advanced(5, 3 , true);

/////////////////////////////////////////////////////////////////////////////////
//Variation 2: Given the row number n. Print the n-th row of Pascal’s triangle.
/**
 * Here first we see the observation  - 
 * 
           1 
          1 1 
         1 2 1 
        1 3 3 1 
       1 4 6 4 1

We can see FIRST ROW HAVE 1 ELEMENT , 2ND rOW HAVE 2 ELEMENT AND SO ON 
so we can say nth row will have n elements 
for for each element  at x row and y column it is tru that the value will be (x-1)C(y-1);
Now we can use a For Loop to for Calculating the Row for given Input 
 */
let function_variation2 = function (n, logData) {
  if (logData) 
   console.log(`Need to Print ${n}th Row `);

  let result = [];
  let col = 1;
  let row = n;
  for (let index = 0; index < n; index++) {
    const element = function_variation1_advanced(row, col ,false);
    result.push(element);
    col++;
  }
  if (logData)
    console.log(`Result - ${result}`);
  return result;
};

function_variation2(5, true);

/**
 * Variation 3: Given the number of rows n. Print the first n rows of Pascal’s triangle.
 */
let function_variation3 = function (n) {
  let result = [];

  for (let index = 0; index < n; index++) {
    let row = index + 1;
    const elements = function_variation2(row);
    result.push(elements);
  }
  console.log(`Printing the pascal triangle `);
  for (let index = 0; index < result.length; index++) {
    const elements = result[index];
    for (let index = 0; index < elements.length; index++) {
      const element = elements[index];
      process.stdout.write(element + `\t`);
    }
    console.log();
  }
};

function_variation3(10);
