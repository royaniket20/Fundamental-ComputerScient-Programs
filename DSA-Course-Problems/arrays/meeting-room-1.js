/**
 * Description
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.

Only $39.9 for the "Twitter Comment System Project Practice" within a limited time of 7 days!

WeChat Notes Twitter for more information（WeChat ID jiuzhang104）


(0,8),(8,10) is not conflict at 8

Example
Example1

Input: intervals = [(0,30),(5,10),(15,20)]
Output: false
Explanation: 
(0,30), (5,10) and (0,30),(15,20) will conflict
Example2

Input: intervals = [(5,8),(9,15)]
Output: true
Explanation: 
Two times will not conflict 
 */

//[(0,30),(5,10),(15,20)]

//[(5,8),(9,15)]

let isconflict = function meetingRoomConflict(intervals) {
  intervals.sort((a, b) => {
    return a.start - b.start;
  });
  let result = false;
  console.log(`Given intervals - ${JSON.stringify(intervals)}`);
  for (let index = 0; index < intervals.length - 1; index++) {
    let current = intervals[index];
    let next = intervals[index + 1];
    if (current.end > next.start) {
      result = true;
      break;
    }
  }
  if (result) {
    console.log(`There is a conflict !!!`);
  } else {
    console.log(`There is a No conflict !!!`);
  }
};

let intervals = [];
let interval1 = { start: 0, end: 30 };
intervals.push(interval1);
interval1 = { start: 15, end: 20 };
intervals.push(interval1);
interval1 = { start: 5, end: 10 };
intervals.push(interval1);
isconflict(intervals);
console.log(`*-*************************`);
intervals = [];
interval1 = { start: 9, end: 15 };
intervals.push(interval1);
interval1 = { start: 5, end: 8 };
intervals.push(interval1);
isconflict(intervals);
