/**
 * Set Matrix Zeros - N*M cross matrix of Binary
 * for zero sell  go to  column and rown and mark everythig as Zero
 *
 * Here catch is if on Row Column you make them Zro your Orginal matrix will be messed Up
 * So instead of Zero we shoud mark them as Other than Zero  - If its Not Zero
 * Keep the Original Zeros intact
 *
 * But it will have very high Time complexty - Traversing array - n*n
 * Now for each time we Get a 0 - wE makr Entrie Rw and entire column - [n+n]
 * So time complexty - (n*n)[n+n] + (n*n) - because frst we used -1 then again traverse matrix to maskerk as 0 Finally
 *
 * VERY BAD TIME COMPLEXT - N^3
 *
 * whet we can eliminate is Doing makring real time - rathar then when firt time we are traversing matrix keep track
 * which Row and Coln 0 is appearing - anyway that means everyone on that Row and Colmn will be Zero - No need to mark them immediately
 * later we can mark them on retraverse  --Now its n2
 *
 *
 */

let matrix = [
  [1, 1, 1, 1, 1, 1],
  [1, 1, 0, 1, 1, 1],
  [1, 1, 1, 1, 1, 1],
  [1, 0, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1],
];
function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(element<0 ? `[${element}]` :  `[ ${element}]`);
    }
    console.log();
  }
}

let rowMark = function (row, matrix, num) {
  for (let inner = 0; inner < matrix[row].length; inner++) {
    matrix[row][inner] = matrix[row][inner] ===0?0: num;
  }
};

let colMark = function (col, matrix, num) {
  for (let index = 0; index < matrix.length; index++) {
    matrix[index][col] = matrix[index][col]===0?0: num;
  }
};


let setMatrixBruteForce = function (matrix) {
  printMatrix(matrix);
  console.log("*******************");
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      if(element === 0){
        rowMark(index, matrix, -1);
        colMark(inner, matrix, -1);
      }
     
    }
   
  }
  printMatrix(matrix);
  console.log("*******************");
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      if(element === -1){
        matrix[index][inner] = 0;
      }
     
    }
   
  }
  printMatrix(matrix);
};
//setMatrixBruteForce(matrix);

//Now we will do the better Solution -- 
let betterSolution = function (matrix){
let rowArr = new Array(matrix.length).fill(-1);
let colArr = new Array(matrix[0].length).fill(-1);
console.log(`row tracker - ${rowArr} || col tracker - ${colArr} `);
printMatrix(matrix);
for (let index = 0; index < matrix.length; index++) {
  for (let inner = 0; inner < matrix[index].length; inner++) {
    const element = matrix[index][inner];
    if(element === 0){
      rowArr[index]= 0;
      colArr[inner]= 0;
    }
  }
}
console.log(`row tracker - ${rowArr} || col tracker - ${colArr} `);

//Here idea is if aelement is part of row or olumn which is marked  It will be marked as 0 
for (let index = 0; index < matrix.length; index++) {
  for (let inner = 0; inner < matrix[index].length; inner++) {
    if(rowArr[index] === 0 || colArr[inner] === 0 ){
      matrix[index][inner] = 0;
    }
  }
}
printMatrix(matrix);


}

betterSolution(matrix);
