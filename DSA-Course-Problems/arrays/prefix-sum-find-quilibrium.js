//Equlibrum is when for a element -- left sum of all elements === right sum of all elements
//If the elelemt is Corner element the other side consider as 0
//Naive Solution

let method0 = function findEqulibrium(arr) {
  let leftSum = 0;
  let rightSum = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < i; j++) {
      leftSum = leftSum + arr[j];
    }
    for (let k = i + 1; k < arr.length; k++) {
      rightSum = rightSum + arr[k];
    }
    if (leftSum === rightSum) {
      console.log(`The arr Item == ${arr[i]} is in Equlibrium`);
    }
    leftSum = 0;
    rightSum = 0;
  }
};

//Efficient Solution with Prefix Sum Using the Aux space - O(N) Time and Space
//Here we calculate prefix Sum before hand - then use that to check if an Element is Qulibriam or Not 
//
let method1 = function findEqulibriumEfficient(arr) {
  let prefixArray = [];
  let currentSum = 0;
  let leftSum = 0;
  let rightSum = 0;

  for (let i = 0; i < arr.length; i++) {
    currentSum = currentSum + arr[i];
    prefixArray.push(currentSum);
  }

  arr.forEach((item, index) => {
    leftSum = index === 0 ? 0 : prefixArray[index - 1];

    rightSum =
      index === arr.length - 1
        ? 0
        : prefixArray[arr.length-1] - prefixArray[index];

    console.log(
      `For Query : Arr[${index}] == ${item} the Left Sum [${leftSum}] : Right Sum ${rightSum} :  is Equlibrim = ${
        leftSum === rightSum
      }`
    );
    leftSum = 0;
    rightSum = 0;
  });
};

let arr1 = [3,4,8,-9,20,6];
let arr2 = [2,-2,4];
let arr3 = [4,2,-2];


method0(arr1);
method1(arr1);

method0(arr2);
method1(arr2);

method0(arr3);
method1(arr3);

