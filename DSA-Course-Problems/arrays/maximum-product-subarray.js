/**
 * [2,3,-2,4]
 * Find out Maximum Product Subarray 
 * Approach - Observation Based Thought 
 * First observation - What if all positive elements - Answer is Multiplication of all Nums 
 * What if It has even negative - Then We take All Nums and Multiply them 
 * What if there is Odd Negatives - 
 * 
 * [3,2,-1,4,-6,3,-2,6] -- Now we cannot take all Numns - 3 negative will give negative value 
 * we can Ignore one Negative - we will devide the array on two subarray -
 * But we need to Pick the subarray with Max PRODUCT 
 * Now Alog ---- 
 * We shoud calculate Prefix Max  , Suffix Max  
 * Our answer will be either on Prefix Multiplication or Suffix Multiplication 
 * 
 * Special Case - what if it has 0 - iF SUBARRAY HAS 0 - then multiplcation will turn 0 
 * So based on 0 we shoud devided array in Multiple Subarray  - then solve them seperately 
 * Or a better approach - Whenever We encounter 0 - We assume we start fresh so Make prefix/Suffix Sum as 1 , keep track of Max 
 * [-2 , 3,4,-1,0,-2,3,1,4,0,4,6,-1,4]
 * 
 * 
 */
let maxProduct = function(arr){
  console.log(`Given array - ${arr}`);
let Max = Number.MIN_SAFE_INTEGER;
let prefix = 1;
let suffix = 1;
for (let index = 0; index < arr.length; index++) {
  const element = arr[index];
  if(element === 0){
    prefix =1;
  }else{
    prefix = prefix * element;
  }
  Max = Math.max(Max , prefix);

}

for (let index = arr.length-1; index >=0; index--) {
  const element = arr[index];
  if(element === 0){
    suffix =1;
  }else{
    suffix = suffix * element;
  }
  Max = Math.max(Max , suffix);
  
}
console.log(` Max Subarray Product = ${Max}`);
}

maxProduct([-2 , 3,4,-1,0,-2,3,1,4,0,4,6,-1,4])

maxProduct([2,3,-2,4])

maxProduct([3,2,-1,4,-6,3,-2,6])