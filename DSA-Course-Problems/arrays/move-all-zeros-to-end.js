/**
 * Create a temp array and Fill the non zero elelments at the beginning then 
 * fiull remaining length of the array with 0s
*/

let method1 = function moveZerostoEnd(arr) {
  let temp = [];
  console.log(`The Initial Array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] != 0) {
      temp.push(arr[i]); //Filling the temp array with non zero elements
    }
  }
 //Fill remaining elements with 0
  for (let i = 0; i < arr.length; i++) {
    if (i > temp.length - 1) {
      arr[i] = 0;
    } else {
      arr[i] = temp[i];
    }
  }

  console.log(`The Final Array - ${arr}`);
};

method1([0, 0, 0, 0, 0, 0, 0, 1]);
method1([1, 0, 2, 0, 0, 3, 4, 5, 0, 0, 9]);

let method2 = function moveZerostoEnd(arr) {
  //Here we will Not use the Additional Space
  /**
   * Here thinking is Just keep track of the Index of the non zero element from beginning.
   * Whenever next non zero element is found - Just put there and update non zero Index
   */
  let nonZeroIndex = 0;
  console.log(`The Initial Array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] != 0) {
      arr[nonZeroIndex++] = arr[i];
    }
  }
  //As all non zero are sided to Left - Just fill rest of the other with Zeros
  for (let i = nonZeroIndex; i < arr.length; i++) {
    arr[i] = 0;
  }

  console.log(`The Final Array - ${arr}`);
};

method2([0, 0, 0, 0, 0, 0, 0, 1]);
method2([1, 0, 2, 0, 0, 3, 4, 5, 0, 0, 9]);
