class MyArray {
  arr;
  length;
  currEmpty;
  constructor(size) {
    console.log(`Array got initialized`);
    this.arr = new Array(size);
    this.length = size;
    this.currEmpty = size;
  }
  //Add element in the array
  addElement(num) {
    if (this.currEmpty > 0) {
      this.currEmpty--;
      for (let index = 0; index < this.length; index++) {
        if (this.arr[index] === undefined) {
          this.arr[index] = num;
          break;
        }
      }
      console.log(`Added element - ${num}`);
    } else {
      throw new Error("Array is already full");
    }
  }

  //Remove lement from array and shif to left
  removeElement(indexPos) {
    if (indexPos < 0 || indexPos >= this.length) {
      throw new Error("Index is out of bound");
    }
    if (this.arr[indexPos] === undefined) {
      console.log("No item there to remove");
      return;
    }
    console.log(`${this.arr[indexPos]} element is removed !!`);
    arr[indexPos] = undefined;
    this.currEmpty++;
    for (let index = indexPos; index < this.length - 1; index++) {
      this.arr[index] = this.arr[index + 1]; // shifting elements
      this.arr[index + 1] = undefined;
    }
  }

  //Print array Fully

  printArray() {
    console.log(`Below are the array elements`);
    for (let index = 0; index < this.length; index++) {
      process.stdout.write(this.arr[index] + "-->");
    }
    console.log();
  }
}

let arr = new MyArray(5);

try {
  arr.addElement(5);
  arr.addElement(3);
  arr.addElement(4);
  arr.addElement(5);
  arr.addElement(6);
  arr.addElement(1);
} catch (err) {
  console.error(err.message);
}

try {
  arr.printArray();
  arr.removeElement(3);
  arr.printArray();
} catch (err) {
  console.error(err.message);
}
