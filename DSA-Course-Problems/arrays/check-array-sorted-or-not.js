let checkArraySortedOrNot = function checkArraySortedOrNot(arr) {
  if (arr.length === 1 || arr.length === 0) {
    return true;
  } else {
    for (let i = 0; i < arr.length - 1; i++) {
      //This means there is one such elelment which is larger than next element on array 
      if (arr[i] > arr[i + 1]) {
        return false;
      }
    }
    return true;
  }
};

let result = checkArraySortedOrNot([2, 7, 6, 1, 6, 9, 2, 9, 4]);

console.log(`Is the Array Sorted - ${result}`);

result = checkArraySortedOrNot([
  -3, -2, -1, 1, 2, 2, 3, 4, 5, 5, 5, 6, 7, 8, 8, 9,
]);

console.log(`Is the Array Sorted - ${result}`);
