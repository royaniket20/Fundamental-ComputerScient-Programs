/**
 * For a Given Ranges ind out which element occurs max number of time in all ranges 
 * Example left = [1,2,5,15]
 *        Right = [5,8,7,18]
 * 
 * Range 1 = [1,5] => 1,2,3,4,5
 * Range 2 = [2,8] => 2,3,4,5,6,7,8
 * Range 3 = [5,7] => 5,6,7
 * Range 4 = [15,18] => 15,16,17,18
 * 
 * Here 5 occurs max number of time i.e 3 times 
 */

/**
 * Naive approach - For each Range find out frequency of the items of that Range in a Map 
 * Now iterate the Whole map and then try to Find out which element hav Max frequency 
 * This will take M*N time complexity And  M*N space complexity
 */
let method1 = function findMaxElementinRange(left, right) {
  let map = new Map();
  for (let i = 0; i < left.length; i++) {
    let start = left[i];
    let end = right[i];
    for (let j = start; j <= end; j++) {
      map.set(j, !map.has(j) ? 1 : map.get(j) + 1);
    }
    console.log(`* * * R A N G E : ${start} TO ${end} * * * `);
    console.log(map);
  }
  let maxElement = undefined;
  let maxFrequency = Number.MIN_VALUE;
  map.forEach((value, key) => {
    if (value > maxFrequency) {
      maxFrequency = value;
      maxElement = key;
    }
  });

  console.log("FINAL MAX OCCURED ELEMENT IN ALL RANGES - " + maxElement);
};

//Here Time Complexity is O(M*N)
method1([1, 2, 5, 15], [5, 8, 7, 18]);

//If Max Range is Not very Big - We can Optimise the solution
/**
 * We will use prefix Sum arrpach for this 
 * Create an Array of 0 as value whose length is Max +1 of Right Array 
 * 
 * Then we will mark the Left array Position as 1 
 * Then will mark (Right Arr Pos + 1 ) as -1
 * Then we will Do a prefix Sum of the Array 
 * Find out what is the Max element 
 * 
 * Index odf the Max element = The Number ocuurs Most of the time 
 * Vaklue of the Max element = The number of times it occurs 
 * 
 * Here the algo works because when we do prefix Sum - from left pos where first you mark as 1 
 * frequency keep on incresing due to prefix Sum effect 
 * 
 * [0,0,1,0,0] ==> [0,0,1,1,1]
 * 
 * But at the Same time we want to make sure that Post the End of range - The increnemtns are nullified - So Right Index +1 Pos is 
 * marked with -1 
 * 
 * [0,0,1,0,0,0,-1,0,0,0] ==> [0,0,1,1,1,1,0,0,0,0] //Nully fiy for rest of out of range items 
 * 
 * Time complexity will be M 
 * 
 */

let method2 = function findMaxElementinRangeOptimised(left, right) {
  console.log("LEFT RANGE = "+ left);
  console.log("RIGHT RANGE ="+ right);
  let maxLength = Number.MIN_VALUE;
  right.forEach((item) => {
    maxLength = Math.max(maxLength, item);
  });
  console.log("Array Length : " + (maxLength + 2)); //Because its a Zero Based Index so if Range is given 18 i.e that is for array Length = 19 but We need one extra 
  let arr = new Array(maxLength + 2).fill(0);
  console.log(`Initial Array - ${arr}`);
  for (let i = 0; i < left.length; i++) {
    arr[left[i]] = 1;
  }
  console.log(`Modified Array Post Left Pos Updation - ${arr}`);
  for (let i = 0; i < right.length; i++) {
      arr[right[i] + 1] = arr[right[i] + 1] - 1;
 
  }
  console.log(`Modified Array Post Right Pos Updation - ${arr}`);
  //Now Doing Prefix Sum
  for (let i = 1; i < arr.length; i++) {
    arr[i] = arr[i] + arr[i - 1];
  }
  console.log(`Prefix Summed Array - ${arr}`);
  let maxFrequency = Number.MIN_VALUE;
  let maxElement = -1;
  arr.forEach((value, index) => {
    maxFrequency = Math.max(maxFrequency, value);
    if (value === maxFrequency) {
      maxElement = index;
    }
  });

  console.log("FINAL MAX OCCURED ELEMENT IN ALL RANGES - " + maxElement + " | IT OCCURED - "+ arr[maxElement]);
};

method2([1, 2, 5, 15], [5, 8, 7, 18]);
