let findSecondLargestArray = function findSecondLargestArray(arr) {
    //Here we find the 2nd Largest using Two Travrsal of the array - although the time complexity is Linear
    //On first round - found the largest element - then on second round find the largest which is smaller than frst largest 
  if (arr.length === 1) {
    return arr[0];
  } else if (arr.length === 0) {
    return -1;
  } else {
    let result = Number.MIN_SAFE_INTEGER;
    let finalresult = Number.MIN_SAFE_INTEGER;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] > result) {
        result = arr[i];
      }
    }
    console.log(`First largest element is Now found - ${result}`);
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] > finalresult && arr[i] != result) {
        finalresult = arr[i];
      }
    }
    return finalresult;
  }
};

let result = findSecondLargestArray([2, 7, 6, 1, 6, 9, 2, 9, 4]);

console.log(`Second Largest element in the array - ${result}`);

let findSecondLargestArraySingleLoop = function findSecondLargestArraySingleLoop(arr) {
    //Here we find the 2nd Largest using Single Travrsal of the array - the time complexity is Linear
    /**
     * Here the idea is that - Whenever you get a new largest elelment the Current
     * largest will become your second largest element nd so on 
     */
  if (arr.length === 1) {
    return arr[0];
  } else if (arr.length === 0) {
    return -1;
  } else {
    let firstLargest = Number.MIN_SAFE_INTEGER;
    let secondLargest = Number.MIN_SAFE_INTEGER;
    console.log(`The Current First Largest - ${firstLargest} & The Second Largest - ${secondLargest}`);
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] > firstLargest) {
        secondLargest=firstLargest;
        firstLargest = arr[i];
        console.log(`The Current First Largest - ${firstLargest} & The Second Largest - ${secondLargest}`);
      }
    }

    return secondLargest;
  }
};

let resultNew = findSecondLargestArraySingleLoop([2, 7, 6, 1, 6, 9, 2, 9, 4]);

console.log(`Second Largest element in the array - ${result}`);
