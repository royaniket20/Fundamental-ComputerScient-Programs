let findLargestArray =function findLargestArray(arr){
   if(arr.length===1)
   {
       return arr[0];
   }else if(arr.length===0)
   {
       return -1;
   }else{
       let result = Number.MIN_SAFE_INTEGER;
       for(let i=0;i<arr.length;i++){
           if(arr[i]>result){
               result=arr[i];
           }
       }
       return result;
   }
}


let result = findLargestArray([2,7,6,1,8,6,9,2,9,4])

console.log(`Largest element in the array - ${result}`);