/**
 * Here we simple Start swapping the Positions fro, both end
 * When doing this withouth additional array 
 * else with additional array its Simple 
 */


let withArray = function reverseArrExtraSpace(arr){
    let result = [];

for(let  i = arr.length-1 ; i>=0 ; i--){
    result.push(arr[i]);
}

console.log(`Original Array : {}`,arr);
console.log(`Reversed Array : {}`,result);
}

withArray([1,2,3,4,5,6,7,8]);



let withoutArray = function reverseArrWithoutExtraSpace(arr){
  let start = 0;
  let end = arr.length-1;
  console.log(`Original Array **** : {}`,arr);
while(start<=end){
    temp = arr[start];
    arr[start]=arr[end];
    arr[end]=temp;
    start++;
    end--;
}


console.log(`Reversed Array **** : {}`,arr);
}

withoutArray([1,2,3,4,5,6,7,8]);
withoutArray([1,2,3]);