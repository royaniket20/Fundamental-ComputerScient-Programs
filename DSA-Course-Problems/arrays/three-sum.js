/**
 * Better Solution - We are using hashing  - to Find that Tuple is
 * Have one Loop i from 0-> n-1
 * Another Loop will Start from j = i+1 -> n-1
 * So Inner Loop We have access to Tow elements - arr[i] , arr[j] i.e arr[i+1]
 * Now Find out whats the elelemnt Required to Fulfil target   next = target - (nums[i] + nums[j])
 * check if its available in set - then tuplet formed else Put on Set and Move on to next iteration
 * @param {*} nums
 * @param {*} target
 * @returns
 */
var threeSum = function (nums, target) {
  let result = new Set();
  for (let i = 0; i < nums.length; i++) {
    let set = new Set();
    for (let j = i + 1; j < nums.length; j++) {
      console.log(`Calculatng Thrid element for arr[${i}] , arr[${j}]`);
      let next = target - [nums[i] + nums[j]];
      if (set.has(next)) {
        let arr = [nums[i], nums[j], next].sort((a, b) => a - b);
        result.add(arr.join(","));
      } else {
        set.add(nums[j]);
      }
    }
  }
  let finalResult = [];
  result.forEach((item) => {
    finalResult.push(item.split(","));
  });
  return finalResult;
};

console.log(threeSum([-1, 0, 1, 2, -1, -4], 0));
