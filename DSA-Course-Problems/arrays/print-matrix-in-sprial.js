let matrix = [
  [1, 2, 3, 5, 88],
  [4, 5, 6, 8, 99],
  [7, 8, 9, 0, 66],
  [0, 3, 4, 1, 77],
];

let matrix1 = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
  [0, 3, 4],
];

let matrix2 = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

/*
You have to print matrix in sprial Order '
Here we will start from top left --> top right --> buttom right --> buttom left --> carry On 
First we decided Four corners of the Matrix
Created a Method to Print Row or Column Only 
Then every time a RTow or Column is printed We just start excluding that Row or Column before proceeding Further 
This will create spiral Affect 
Now we can simply Count How may we are printing - then stop once all array element printed 

*/

let spiralPrint = function (matrix) {
  console.log(`Given Matrix is  - `);
  printMatrix(matrix);
  let topLeftRow = 0;
  let topLeftCol = 0;
  let topRightRow = 0;
  let topRightCol = matrix[0].length - 1;
  let buttomRightRow = matrix.length - 1;
  let buttomRightCol = matrix[matrix.length - 1].length - 1;
  let buttomLeftRow = matrix.length - 1;
  let buttomLeftCol = 0;
  let totalElement = matrix.length * matrix[0].length;
  console.log(`Top Left = [${topLeftRow}][${topLeftCol}]`);
  console.log(`Top Right = [${topRightRow}][${topRightCol}]`);
  console.log(`Buttom Right = [${buttomRightRow}][${buttomRightCol}]`);
  console.log(`Buttom Left = [${buttomLeftRow}][${buttomLeftCol}]`);
  console.log(`Total Element - ${totalElement}`);
  while (totalElement > 0) {
    printMatrixColOrRowWise(
      topLeftRow,
      topLeftCol,
      topRightRow,
      topRightCol,
      matrix
    );
    totalElement = totalElement - (Math.abs(topLeftCol - topRightCol) + 1);
    console.log(`Total element left - ${totalElement}`);
    if (totalElement <= 0) {
      break;
    }
    topRightRow++;
    topLeftRow++;

    printMatrixColOrRowWise(
      topRightRow,
      topRightCol,
      buttomRightRow,
      buttomRightCol,
      matrix
    );
    totalElement = totalElement - (Math.abs(topRightRow - buttomRightRow) + 1);
    console.log(`Total element left - ${totalElement}`);
    if (totalElement <= 0) {
      break;
    }
    buttomRightCol--;
    topRightCol--;

    printMatrixColOrRowWise(
      buttomRightRow,
      buttomRightCol,
      buttomLeftRow,
      buttomLeftCol,
      matrix
    );
    totalElement =
      totalElement - (Math.abs(buttomRightCol - buttomLeftCol) + 1);
    console.log(`Total element left - ${totalElement}`);
    if (totalElement <= 0) {
      break;
    }
    buttomLeftRow--;
    buttomRightRow--;

    printMatrixColOrRowWise(
      buttomLeftRow,
      buttomLeftCol,
      topLeftRow,
      topLeftCol,
      matrix
    );
    totalElement = totalElement - (Math.abs(topLeftRow - buttomLeftRow) + 1);
    console.log(`Total element left - ${totalElement}`);
    if (totalElement <= 0) {
      break;
    }
    topLeftCol++;
    buttomLeftCol++;
  }
};

function printMatrixColOrRowWise(rowStart, colStart, rowEnd, colEnd, matrix) {
  console.log(
    `Printing from [${rowStart}][${colStart}] ==> [${rowEnd}][${colEnd}]`
  );
  if (rowStart === rowEnd) {
    // console.log(`Its Row  printing ==> `);
    if (colStart > colEnd) {
      for (let inner = colStart; inner >= colEnd; inner--) {
        const element = matrix[rowStart][inner];
        process.stdout.write(`[${element}]`);
      }
    } else {
      for (let inner = colStart; inner <= colEnd; inner++) {
        const element = matrix[rowStart][inner];
        process.stdout.write(`[${element}]`);
      }
    }
  } else if (colStart === colEnd) {
    //console.log(`Its Col  printing ==> `);
    if (rowStart > rowEnd) {
      for (let inner = rowStart; inner >= rowEnd; inner--) {
        const element = matrix[inner][colStart];
        process.stdout.write(`[${element}]`);
      }
    } else {
      for (let inner = rowStart; inner <= rowEnd; inner++) {
        const element = matrix[inner][colStart];
        process.stdout.write(`[${element}]`);
      }
    }
  }
  console.log();
}

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}

spiralPrint(matrix);
spiralPrint(matrix1);
spiralPrint(matrix2);
