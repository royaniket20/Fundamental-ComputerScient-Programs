/**
 * Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.

We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.

You must solve this problem without using the library's sort function.
 */

let sortArrayWith3elements = function sortArrayWith3elements(arr) {
  console.log(`Given array - ${arr}`);
  let start = 0;
  let end = arr.length - 1;
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    console.log(`arr[${index}] = ${element}`);
    //Push Zeros toward start
    if (element === 0) {
      while (arr[start] === 0) {
        start++; //Keep on sliding window so that already in place Zeros are considered
      }
      if (start < index) {
        //Only Do swap operation if its Not going to distrub the already sorted posrtion
        swap(arr, index, start);
        start++;
      }
    }

    //Push Twos toward end
    if (element === 2) {
      while (arr[end] === 2) {
        end--; //Keep on sliding window so that already in place Twos  are considered
      }
      if (index < end) {
        //Only Do swap operation if its Not going to distrub the already sorted posrtion
        swap(arr, index, end);
        end--;
      }
    }
    console.log(`Sorted Array - ${arr}`);
  }
  console.log(`Final Sorted Array - ${arr}`);
};

function swap(arr, i, j) {
  if (i > arr.length - 1 || j > arr.length - 1) {
    throw new Error("Out Of Bound for array ");
  }
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

let arr = [1, 0, 0, 1, 2, 0, 2, 0, 1, 2, 0];
sortArrayWith3elements(arr);

arr = [2, 0, 2, 1, 1, 0];
sortArrayWith3elements(arr);

arr = [2, 0, 1];
sortArrayWith3elements(arr);
