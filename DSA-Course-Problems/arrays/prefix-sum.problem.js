//Naive Solution
/**
 * Query for Sub array Sum calculation
 * Now general apprach is Get the start and end index and Find the Sum of elements between them
 * If M such query comes  Time comp,lexity can go M*N
 */
let method0 = function findsubarraySumNaive(arr, quiries) {
  let currentSum = 0;
  quiries.forEach((item, index) => {
    let left = item.left;
    let right = item.right;
    for (let i = left; i <= right; i++) {
      currentSum = currentSum + arr[i];
    }
    console.log(
      `For Query : ${index} the Array Element arr[${left}] : ${arr[left]} ---> arr[${right}] : ${arr[right]} : Sum is = ${currentSum}`
    );
    currentSum = 0;
  });
};

//Efficient Solution with Prefix Sum Using the Aux space - O(N) Time and Space
/**
 * hERE WHAT WE DO IS PRE CALCULATE A PREFIX SUM of the Given array
 * Now whwenever a Querty COme we just do substract of two prexis Sum vlaues based on Index
 * But it need additional 0(N ) space also
 */
let method1 = function findsubarraySum(arr, quiries) {
  let prefixArray = [];
  let currentSum = 0;

  for (let i = 0; i < arr.length; i++) {
    currentSum = currentSum + arr[i];
    prefixArray.push(currentSum);
  }
  quiries.forEach((item, index) => {
    let left = item.left;
    let right = item.right;
    let result =
      left === 0
        ? prefixArray[right]
        : prefixArray[right] - prefixArray[left - 1];
    console.log(
      `For Query : ${index} the Array Element arr[${left}] : ${arr[left]} ---> arr[${right}] : ${arr[right]} : Sum is = ${result}`
    );
  });
};

let arr = [];
let queries = [];
for (let i = 0; i < 100; i++) {
  arr.push(i + 1);
  let left = i;
  let right = i > 95 ? i : i + 3;
  let query = {
    left: left,
    right: right,
  };
  queries.push(query);
}
method0(arr, queries);
method1(arr, queries);
// arr.forEach((item , index)=>{
//     console.log( index +"====>"+item)
// })

// queries.forEach((item , index)=>{
//     console.log( index +"====>"+JSON.stringify(item))
// })
