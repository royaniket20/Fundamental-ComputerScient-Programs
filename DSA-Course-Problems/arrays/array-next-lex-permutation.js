/**
 * Problem Statement: Given an array Arr[] of integers, rearrange the numbers of the given array into the lexicographically next greater permutation of numbers.
 * Integer array with Repetation is given
 * [1,2,3] = 3! = 6 possible ways It can be rearranged
 *  ==== [1,2,3] [1,3,2] , [3,2,1] [3,1,2] [2,1,3] [2,3,1]
 *
 * Need to Find the Sorted Next permutation
 * 123<132 <213<231<312<321
 *
 * So If [132] is Given you have to Find [2,1,3]
 * If there is No next permutaton fall back to First Permutation
 * [3,2,1] ==> [1,2,3]
 */
/**
 * Observation ---
 * We are looking for longer prefix match
 * we have only one combination
 * if we match eveerything - Same array will be returned
 * if we leave only last element - there is No combi possible
 * we need totry out with larger combi
 * [2,1,5,4,3,0,0] - example array
 * [0] -- No new Data
 * [00] - No Nedw Data
 * [300] - No New Data because any combination of 300 [003,030,300] is smaller than or equal to 300
 * So keep Increasing permutation set eventually you will get a combination where next larger permutation is possible
 * [2][154300] --Here possibility is there
 * Now we want to get the next possible permutation
 * Find out the breakpoint where increasing order breaks [2][1][54300] - Swap breakpoint with right array larger lement traersing from right 
 * [2][3][54100] //Swapped  3 with 1 
 * [2][3][54100] //Sort last part 
 * [23][00145] // Result 
 *
 *
 *
 * Algo ---
 * if there is No dip - Reverse array and that's it - that means there is No next pewrmutation
 * we are failing back to First one
 *
 * nOW IN CASE BREAKPOINT PRESENT
 * --> go from n --> till brkpoint+1 and Find out Who is grear than Breakpoint - swap it and Break there
 * --> Still you have elementa n --> brkpoint +1 is Increasing Order
 * --> Just reverse and Append to Post brkpoint
 *
 */

let nextpermutation = function (arr) {
  console.log(`Given array - ${arr}`);
  //First find out peak element in the array from right to left
  //Here we know from end to start the Nums will be in increasing order wth a sudden Break in between
  //We need to catch that break point

  let breakpoint = -1;
  for (let index = arr.length - 2; index >= 0; index--) {
    if (arr[index] < arr[index + 1]) {
      //Here is the breakpoint
      breakpoint = index;
      break;
    }
  }
  console.log(`Calculated Breapoint - ${breakpoint}`);
  if (breakpoint !== -1) {
    console.log(`More calculation will be needing`);
    //Now again we will start from end of array till breakpoint-1 and find who is greater than breakpoint
    //As items to the right of breakpoint are sorted in asending order from end --> breakpoint - We will find the closet large eventually
    for (let index = arr.length - 1; index > breakpoint; index--) {
      const element = arr[index];
      if (element > arr[breakpoint]) {
        //We found our closest element which we can swap
        swap(arr, breakpoint, index);
        break;
      }
    }
    console.log(`Modified array - ${arr}`);
    //Now we need to Just reverse the element from breakpoint + 1 --> arr.length -1
    reverseArrWithoutExtraSpace(arr, breakpoint + 1, arr.length - 1);
    console.log(`next permutation = ${arr}`);
  } else {
    console.log(
      `There is No next Permutation - We need to Reverse the array and Return `
    );
    /****
     *  123<132 <213<231<312<321
     * So of 321 given to you next is nothing you need to retun 123 which is simple reverse
     */
    console.log(`next permutation = ${arr.reverse()}`);
  }
};

function swap(arr, source, dest) {
  let temp = arr[source];
  arr[source] = arr[dest];
  arr[dest] = temp;
}

function reverseArrWithoutExtraSpace(arr, start, end) {
  let temp;
  while (start <= end) {
    temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
}

nextpermutation([2, 1, 5, 4, 3, 0, 0]);
nextpermutation([2, 4, 1, 3]);
nextpermutation([3, 2, 1]);
