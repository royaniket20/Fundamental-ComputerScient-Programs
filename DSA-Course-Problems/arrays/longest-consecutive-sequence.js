/*
* Problem Statement: You are given an array of ‘N’ integers. You need to find the length of the longest sequence which contains the consecutive elements.
The array may have Random elements [102,4,100,1,101,3,2,1,1]
Longest consecutive sequence - 1,2,3,4 == length is 4 
* 
*/

let breuteforce = function (arr) {
  //Trying to find the sequence starting from that Point and scan whole array - Record the sequence length
  //Keep track of Max length
  let max = 0;
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    let length = 1;
    let next = element + 1;
    while (true) {
      if (findElementinArray(arr, next)) {
        next++;
        length++;
      } else {
        break;
      }
    }
    max = Math.max(max, length);
    console.log(`Max Consecutive Length till Now - ${max}`);
  }
  console.log(`Max Consecutive Length Final Now - ${max}`);
};

//breuteforce([102, 4, 100, 1, 101, 3, 2, 1, 1]);

function findElementinArray(arr, element) {
  let res = false;
  for (let index = 0; index < arr.length; index++) {
    if (element === arr[index]) {
      res = true;
      break;
    }
  }
  return res;
}
//TODO - Better solution and sorting Solution
//[100,102,100,101,101,4,2,3,3,2,1,1,1,2] - Use for sorting also
let sortingSolution = function sortAndFindLongestconsecutiveSequence(arr) {
  console.log(`Given array - ${arr}`);
  arr.sort((a, b) => a - b);
  console.log(`Sorted Array --- ${arr}`);
  let max = 1;
  let length = 1;
  let first = arr[0];
  let target = first + 1;
  for (let index = 1; index < arr.length; index++) {
    console.log(`Current Length - ${length} || Max till Now - ${max}`);
    const element = arr[index];
    if (element === arr[index - 1]) {
      continue;
    } else if (element === target) {
      length++;
      target++;
      max = Math.max(max, length);
    } else if (element !== target) {
      length = 1;
      target = element + 1;
    }
  }
  console.log(`Result Max Consecutive Sequence Length = ${max}`);
};


