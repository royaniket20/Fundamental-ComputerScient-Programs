/* Majority Element II

Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

 

Example 1:

Input: nums = [3,2,3]
Output: [3]
Example 2:

Input: nums = [1]
Output: [1]
Example 3:

Input: nums = [1,2]
Output: [1,2]
 

Constraints:

1 <= nums.length <= 5 * 104
-109 <= nums[i] <= 109
 

 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var majorityElement = function(nums) {
  let results = new Map();
  let result = new Set();
  for(let i = 0 ; i<nums.length ; i++){
      if(results.has(nums[i])){
         results.set(nums[i] ,results.get(nums[i])+ 1);
      }else{
        results.set(nums[i] , 1);
      }
        if(results.get(nums[i]) > Math.floor(nums.length/3)){
               result.add(nums[i]);
           }
  }
  return Array.from(result);
  
};
console.log("Result = "+ majorityElement([1,2]));
console.log("Result = "+ majorityElement([3,2,3]));
console.log("Result = "+ majorityElement([1]));

/**
 * Optimal Solution - No extra Space and O(N) tIME SOLUTION 
 * 
 * First of all we need to understand How many Max element can be present more than n/3 times - 
 * Whatever the size of the array it cannot be more than 2 For sure 
 * Suppose n = 9 n/3 = 3 
 * So if We need more than On majority element - then 2 element need to be preset atleast 4 times 
 * That means Only one other elment can be of 1 times - 4+4+1
 * 
 * So here we will solve the problem by running Moore Voting algo But for Tracking Two elements 
 * 
 * Algo ---
 * We will have Two Var to trck Two Possible Unique moajority candidate and Two vote tracker for corresponding them 
 * And then atlast we will manually verify if they pass the >N/3 Rule or Not 
 * 
 */

var majorityElement_optimal = function(nums) {
  let majority1 = Number.MAX_SAFE_INTEGER;
  let majority2 =  Number.MAX_SAFE_INTEGER;
  let voting1 = 0;
  let voting2 = 0;
  for(let i = 0 ; i<nums.length ; i++){
    if(voting1 === 0 && majority2 !== nums[i]){ //This 2nd condition ensure Not same candidate Stored on Both var 
      //Find out a fresh candidate with No voting ount till Now 
       voting1 = 1;
       majority1 = nums[i];
    }else if(voting2 === 0 && majority1 !== nums[i]){ //This 2nd condition ensure Not same candidate Stored on Both var 
      //Find out another  fresh candidate with No voting ount till Now 
       voting2 = 1;
       majority2 = nums[i];
    }else if (majority1 === nums[i]){
      voting1++; // Increse first candidate voting
    }else if (majority2 === nums[i]){
      voting2++; //Increase 2nd candidate voting
    }else{
      //None of the element match - Lets loose vote for both candidate 
      voting1 --;
      voting2 --;
    }
  }
  console.log(`Possible candidates - ${majority1} | ${majority2}`);
  let results = [];
  voting1 = voting2 = 0;
  //Do the real count of the candidates 
  for (let index = 0; index < nums.length; index++) {
    const element = nums[index];
    if(element === majority1){
      voting1++;
    }else if(element === majority2){
      voting2++;
    }
  }

  if(Math.floor(nums.length/3)<voting1){
    results.push(majority1);
  }
  if(Math.floor(nums.length/3)<voting2){
    results.push(majority2);
  }
  console.log(`Final Result - [${results}]`);
  
};

majorityElement_optimal([2,1,1,3,1,4,5,6]);