//Largest Even Odd Subarray -  Solution O(N)
/**
 * Here also we can use kadane's algo -- start from the 0th index of the array
 * Keep on checking the Even Odd combinations - If the sequence break start from that Point again -
 * Then Just print the Logest one
 */
let method1 = function largestEvenOffSubArray(arr) {
  let subArrStartIndex = 0;
  let subArrEndIndex = 0;
  let resultLength = Math.max(1, subArrEndIndex - subArrStartIndex + 1);
  let isEvenStart = arr[0] % 2 === 0 ? true : false;
  console.log("Formula Started  - Keep Going " + arr[0]);
  let nextCheck = null;
  if (isEvenStart) {
    nextCheck = "ODD";
  } else {
    nextCheck = "EVEN";
  }
  for (let i = 1; i < arr.length; i++) {
    if (nextCheck === "ODD") {
      if (arr[i] % 2 !== 0) {
        nextCheck = "EVEN";
        subArrEndIndex = i;
        resultLength = Math.max(
          resultLength,
          subArrEndIndex - subArrStartIndex + 1
        );
        console.log("Formula Approved  - Keep Going " + arr[i]);
      } else {
        console.log("Formula Broken - Start again from here " + arr[i]);
        //Lets preserve the Result
        console.log(
          resultLength +
            "---------------Got one Result ------------------" +
            arr.slice(subArrStartIndex, i)
        );
        subArrStartIndex = i;
        subArrEndIndex = i;
      }
    } else if (nextCheck === "EVEN") {
      if (arr[i] % 2 === 0) {
        nextCheck = "ODD";
        subArrEndIndex = i;
        resultLength = Math.max(
          resultLength,
          subArrEndIndex - subArrStartIndex + 1
        );
        console.log("Formula Approved  - Keep Going " + arr[i]);
      } else {
        console.log("Formula Broken - Start again from here " + arr[i]);
        //Lets preserve the Result
        console.log(
          resultLength +
            "---------------Got one Result ------------------" +
            arr.slice(subArrStartIndex, i)
        );
        subArrStartIndex = i;
        subArrEndIndex = i;
      }
    }
  }
  console.log(`Longest even Odd Subarray  ---> ${resultLength}`);
};
console.log("***********************************************");
method1([2, 2, 3, 4, 3, 3, 2, 5, 6, 7, 1, 1, 1, 5, 6, 7, 1, 1]);
console.log("***********************************************");
method1([1, 2, 3, 4, 3, 8, 1, 8, 7, 6, 7, 1, 1, 1, 5, 6, 7, 1, 1]);
