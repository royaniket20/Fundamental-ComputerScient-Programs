let method1 = function findMaxConsecutive1s(arr) {
  let result = 0;
  let maxConsecutiveOnes = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === 1) {
      result++;
      maxConsecutiveOnes = Math.max(maxConsecutiveOnes, result);
    } else {
      result = 0;
    }
  }

  console.log(
    `Max consecutive Ones in Arr : ${arr} is === ${maxConsecutiveOnes}`
  );
};

method1([1, 2, 3, 4, 3, 2, 5, 6, 7, 1, 1, 1, 5, 6, 7, 1, 1]);
