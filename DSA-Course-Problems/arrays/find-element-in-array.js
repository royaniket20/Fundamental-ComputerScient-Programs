let arr = [1, 3, 5, 6, 7, 2];

function findElementIndex(arr, num) {
  let result = -1;
  console.log(`Given Array - ${arr} , | Num to search - ${num}`);
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (element === num) {
      result = index;
      break;
    }
  }
  console.log(`Found element at Index = ${result}`);
}

findElementIndex(arr, 7);

findElementIndex(arr, 33);
