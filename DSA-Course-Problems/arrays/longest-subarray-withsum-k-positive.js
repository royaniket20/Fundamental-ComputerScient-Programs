let arr = [1, 2, 3, 1, 1, 1, 1, 4, 3, 2];
/**
 * Give the length of longest subarray with Sum K 
* We may try using Sliding window - we will have a Window with start and end at 0th index
We will try to make window size such that - we get the K sum -- Then keep track of largest window  

This will work perfectly - When you have Only Positive and Zero Values in Array 
*/

let findLongestSubArrayWithSumKPositive = function (arr, target) {
  console.log(`Given array - ${arr} | Target Sum - ${target}`);
  let maxLength = Number.MIN_SAFE_INTEGER;
  let start = 0;
  let end = 0;
  let sum = arr[start];
  while (end < arr.length) {
    if (sum < target) {
      end++;
      sum = sum + arr[end];
    } else if (sum > target) {
      sum = sum - arr[start];
      start++;
    } else {
      console.log(`Got One Entry - start [${start}] ==> end [${end}]`);
      maxLength = Math.max(maxLength, end - start + 1);
      end++;
      sum = sum + arr[end];
    }
  }
  console.log(`Given Max length with Subarray Sum K is - ${maxLength}`);
};
findLongestSubArrayWithSumKPositive(arr, 6);

//Generate all Sub array Combination and Keep track of Max length
let naiveSolution = function (arr, target) {
  console.log(`Given array Naive Sol  - ${arr} | Target Sum - ${target}`);
  let maxLength = Number.MIN_SAFE_INTEGER;
  for (let index = 0; index < arr.length; index++) {
    let sum = arr[index];
    for (let inner = index + 1; inner < arr.length; inner++) {
      sum = sum + arr[inner];
      console.log(
        `SubArray Sum for subArray starting with ${arr[index]} = ${sum}`
      );
      if (sum === target) {
        console.log(`Got One Entry - start [${index}] ==> end [${inner}]`);
        maxLength = Math.max(maxLength, inner - index + 1);
      }
    }
  }
  console.log(`Given Max length with Subarray Sum K is - ${maxLength}`);
};

naiveSolution(arr, 6);
