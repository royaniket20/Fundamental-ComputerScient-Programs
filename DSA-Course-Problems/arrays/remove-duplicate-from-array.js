var removeDuplicatesNormal = function (nums) {
  let arr = [];
  if (nums.length === 0) return 0;
  arr.push(nums[0]);
  let currentUniqueIndex = 0;
  for (let i = 1; i < nums.length; i++) {
    console.log(arr[currentUniqueIndex] + "-----" + nums[i]);

    if (arr[currentUniqueIndex] != nums[i]) {
      arr[++currentUniqueIndex] = nums[i];
    }
  }
  for (let k = 0; k < arr.length; k++) {
    nums[k] = arr[k];
  }

  console.log(`The data - ${nums} --- ${arr.length}`);
  return nums.slice(0, arr.length);
};

console.log(
  `The Result - :${removeDuplicatesNormal([
    1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 4, 5, 5,
  ])}`
);

/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicatesEfficient = function (nums) {
  //Idea is Just simply take a track of Unique elements Index and
  //hener find next unique ellement put there and update index

  if (nums.length === 0) return 0;
  //First element always unique
  let currentUniqueIndex = 0;
  for (let i = 1; i < nums.length; i++) {
    if (nums[currentUniqueIndex] != nums[i]) {
      nums[++currentUniqueIndex] = nums[i];
    }
  }

  return nums.slice(0, currentUniqueIndex + 1);
};

console.log(
  `The Result - :${removeDuplicatesEfficient([
    1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 4, 5, 5,
  ])}`
);
