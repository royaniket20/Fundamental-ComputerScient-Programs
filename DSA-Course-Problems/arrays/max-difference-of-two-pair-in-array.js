//Find the two paid ai and aj such that i<j and difference is Max

//Naive Approach
//Here we keep track of the Max diff - And do for All possible Combinations
let method1 = function findMaxDifferenceInarray(arr) {
  let result = Number.MIN_VALUE;
  let pair = [];
  for (let i = 0; i < arr.length; i++) {
    for (let j = i; j < arr.length; j++) {
      if (arr[j] - arr[i] > result) {
        result = arr[j] - arr[i];
        pair[0] = i;
        pair[1] = j;
      }
    }
  }

  console.log(`The Pair is arr[${pair[0]}] & arr[${pair[1]}]`);
};
method1([
  1, -3, 555, 88, 9, 77, 88, -999, 44, 3, 2, 1, 777, 1999, -2000, -10000,
]);
//Advance Approach - Here we do a Simple thing - We just assume that Every Num is Max Number nad Keep Track of Minimum Element
let method2 = function findMaxDifferenceInarrayEfficient(arr) {
  let result = Number.MIN_VALUE;
  let pair = [];
  let max = arr[0];
  let min = arr[0];
  pair[0] = 0; //Min Index
  pair[1] = 0; //Max Index
  let tempMinIndex = -1;
  result = Math.max(max - min, result);
  console.log(`Initial Max - [${max}] | Initial Min - [${min}]`);
  for (let i = 1; i < arr.length; i++) {
    //First consider every Element as Max - V.V Imp point
    max = arr[i];
    //Keeping track of The Latest Min element
    if (arr[i] < min) {
      min = arr[i];
      tempMinIndex = i;
      console.log("Updated Min Index - " + tempMinIndex);
    }
    //Getting the Diff between Max and Min and then compare if its larger than last time Diff
    let tempDiff = max - min;
    //If we get a larger Diff - Then we will Upodate the Diff and Capture Max and Min Index for result
    if (tempDiff > result) {
      result = tempDiff;
      pair[1] = i; //Updating Max Index
      pair[0] = tempMinIndex; //Updated Min Index
      console.log("Updated Max Index - " + pair[1]);
      console.log(`Updated Max - [${max}] | Updated Min - [${min}]`);
    }
  }

  console.log(`The Pair is arr[${pair[0]}] & arr[${pair[1]}]`);
};

method2([
  1, -3, 555, 88, 9, 77, 88, -999, 44, 3, 2, 1, 777, 1999, -2000, -10000,
]);


