/**
 * Rotate Array By Counter Clock Wise By 1 Position
 */
let rotateArrayLeft1Time = function (arr) {
  console.log(`Initial Arr - ${arr}`);
  let leftMostItem = arr[0];
  for (let i = 1; i < arr.length; i++) {
    arr[i - 1] = arr[i];
  }
  arr[arr.length - 1] = leftMostItem;
  console.log(`Final Arr - ${arr}`);
  return arr;
};

rotateArrayLeft1Time([1, 2, 3, 4, 5, 6]);

/**
 *
 * Now Rotate an Array By D time
 * Always remeber if a array has size 4 - you rorate it 5 times - 
 * You are actually rorating 1 times , if you rotate 10 times you are actyually rotating 2 times 
 * 5%4 = 1
 * 10%4 = 2
 * Whenever Rotation  > arr size do Modulo to Find actual rotations 
 * Now easy way is prserve D elements 
 * Shift Remainig elements 
 * Put reserved d elements at last
 */
let rotateArrayDTimesWithAuxSpace= function (arr, rotateTime) {
  console.log(`Initial Arr For Multiple Rotation - ${arr} | Rotation - ${rotateTime}`);
  if (rotateTime > arr.length) {
    rotateTime = rotateTime % arr.length; 
  }
  console.log(`Actual rotation - ${rotateTime}`);
  let aux = [];
   for (let index = 0; index < rotateTime; index++) {
    aux.push(arr[index]);
   }
   let auxIndex = -1;
   console.log(`Reserved Elements - [${aux}]`);
   for (let index = rotateTime; index < arr.length; index++) {
        arr[index-rotateTime] = arr[index];
        auxIndex = index-rotateTime+1;
   }
  console.log(`Array after partial shifting - ${arr} | Aux elements to be Filled from Pos - ${auxIndex}`);
   
  for (let index = auxIndex ,  auxStart = 0; auxStart < aux.length; index++,auxStart++) {
   arr[index] = aux[auxStart];
  }

  console.log(`Array after FINAL shifting - ${arr}`);
};

rotateArrayDTimesWithAuxSpace([1, 2, 3, 4, 5, 6], 16);

/**
 *
 * @param {*} arr
 * @param {*} rotateTime
 * Here Logic is A bit trick Remember
 * 1.ReverseFirst  D elements
 * 2.Reverse Remaining n-d elements
 * 3.Reverse all Array on N
 * yOU WILL GET ANSWER
 */

let trickSolutionUsingReverseArrNoAuxSpace = function (arr, rotateTime) {
  console.log(`Initial Arr For Multiple Rotation Trick Solution - ${arr}`);
  if (rotateTime >= arr.length) {
    rotateTime = rotateTime % arr.length; // If  YOU ROTATE A ARRAY OF 4 LENTH 5 TIMES MEANS YOU ARE ROTATING IT ONLY 1 TIME
  }
  console.log(`Reverse First ${rotateTime} Elements INITIAL- ${arr}`);
  let start = 0;
  let end = rotateTime - 1;
  while (start < end) {
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
  console.log(`Reverse First ${rotateTime} Elements FINAL- ${arr}`);

  console.log(
    `Reverse Remaining ${arr.length - rotateTime} Elements INITIAL- ${arr}`
  );
  start = rotateTime;
  end = arr.length - 1;
  while (start < end) {
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
  console.log(
    `Reverse Remaining ${arr.length - rotateTime} Elements FINAL- ${arr}`
  );

  console.log(`Reverse Whole Array ${arr.length} Elements INITIAL- ${arr}`);
  start = 0;
  end = arr.length - 1;
  while (start < end) {
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
  console.log(`Reverse Whole Array ${arr.length} Elements FINAL- ${arr}`);

  console.log(`Final Arr For Multiple Rotation Trick Solution- ${arr}`);
};

trickSolutionUsingReverseArrNoAuxSpace([1, 2, 3, 4, 5, 6], 8);
trickSolutionUsingReverseArrNoAuxSpace([1, 2, 3, 4, 5, 6], 16);