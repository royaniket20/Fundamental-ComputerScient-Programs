/***
 *
 * An element is called Majority Element in the Array if that element appear more than Math.floor(n/2) times in the array
 * Always consider only one Majority element
 * [8,3,4,8,8] =Here n = 5
 * Majority element is the One having more than 2 tIMES APPREANCE
 * so 8 is majority element
 */

/**
 *
 * First naive approach can be for each element find Out if this element
 * is a Mjority Element or Not - Once a Majority element found break it
 *
 */

let method1 = function findmajorityElementinArray(arr) {
  let arrayLength = arr.length;
  let majorityElementCount = Math.floor(arrayLength / 2) + 1;
  let result = -1;
  console.log(
    `The array : ${arr} and Majority Count = ${majorityElementCount}`
  );
  for (let i = 0; i < arr.length; i++) {
    let count = 0;
    for (let j = i; j < arr.length; j++) {
      if (arr[j] === arr[i]) {
        count++;
      }
    }
    if (count >= majorityElementCount) {
      result = arr[i];
      break;
    }
  }
  console.log("The Final Majority Element is - " + result);
};

method1([
  8, 4, 4, 4, 4, 4, 3, 4, 8, 8, 6, 7, 4, 3, 4, 6, 5, 4, 5, 7, 4, 4, 3, 4, 9, 3,
  4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4,
]);

//For Efficient Solution we will use Moore Voting Algo -
/**
 * First consider First  element is a Majority Element  - Now keep traversing from left to Right
 * Start with VOTE count is 1
 * When you get same element again - Vote Count is increased by 1
 * When you get diff element - vote count decreased by 1
 * If on one iNdex - Vote count becomes 0 , then Make that as New Majority element and set vote count as 1 - AND THEN CARRY ON
 * Every Diff element will Reduce the Count-Srtart fro,m beginnig - Conside it as Majority element
 *
 * Algo is find the candidate which can be Majority element
 * Then check if its satify the Majority element condition
 **/
let method2 = function findmajorityElementinArrayEfficient(arr) {
  let arrayLength = arr.length;
  let majorityElementCount = Math.floor(arrayLength / 2) + 1;
  let result = -1;
  let count = 1;
  let currentMajor = arr[0];
  console.log(
    `The array : ${arr} and Majority Count = ${majorityElementCount}`
  );
  console.log(
    `Initial Major - ${currentMajor} With Count = ${count} - Current Candidate - ${arr[0]}`
  );
  for (let i = 1; i < arr.length; i++) {
    if (currentMajor === arr[i]) {
      count++;
    } else {
      count--;
    }
    if (count === 0) {
      //Draw happened time to choose Next leader
      count = 1;
      currentMajor = arr[i];
    }
    console.log(
      `Current Major - ${currentMajor} With Count = ${count} - Current Candidate - ${arr[i]}`
    );
  }
  //Now just validate if this really is the Majority element or Not
  let repetation = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === currentMajor) {
      repetation++;
    }
  }
  if (repetation >= majorityElementCount) {
    result = currentMajor;
  }
  console.log("The Final Majority Element is - " + result);
};

method2([
  8, 4, 4, 4, 4, 4, 3, 4, 8, 8, 6, 7, 4, 3, 4, 6, 5, 4, 5, 7, 4, 4, 3, 4, 9, 3,
  4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4,
]);

method2([8, 4, 4, 4, 4, 3, 2, 4, 4, 4, 4]);
method2([8, 3, 4, 8, 8]);
