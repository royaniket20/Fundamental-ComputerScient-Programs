/**
 * A matrix is called valid matrix of nxn if and only if for each row and column we have all the elements from '1' TO 'n' one time EACH
 * 1<=n<=10
 * Here idea is For travering Each Row /Each Column - Have a Boolean Array with init value false.
 * If you encounter the element make it true - But if the element is already present - means Flag is already tru - So break and say that Matrix have Duplicates
 */
let checkValid = function (matrix) {
  let result = true;
  for (let outer = 0; outer < matrix.length; outer++) {
    let cWise = new Array(matrix.length + 1).fill(false);
    let rWise = new Array(matrix.length + 1).fill(false);
    for (let inner = 0; inner < matrix[outer].length; inner++) {
      let rowData = matrix[inner][outer];
      let columnData = matrix[outer][inner];
      if (cWise[columnData] === true || rWise[rowData] === true) {
        //duplicate case - so some number must be missing
        result = false;
        break;
      } else {
        cWise[columnData] = true;
        rWise[rowData] = true;
      }
    }
    // console.log(cWise, rWise);
    if (!result) {
      break;
    }
  }
  return result;
};

let matrix = [
  [1, 2, 3],
  [3, 1, 2],
  [2, 3, 1],
];
console.log(`Is valid Matrix - ${checkValid(matrix)}`);

matrix = [
  [1, 2, 3, 4],
  [4, 3, 1, 2],
  [3, 1, 4, 1],
  [2, 4, 2, 3],
];

console.log(`Is valid Matrix - ${checkValid(matrix)}`);
