//Given a square matrix - rotate in 90 deg clock woise
//Brute force solution is just create a new matrix and logically put elements in correct place
/**
  * One observation - Fir row goes to last column ... 2nd Row goes to 2nd last matrix et ,, 
  * 
  * [0][0] --> [0][3]
  * [0][1] --> [1][3]
  * .. so on
  * [1][0] --> [0][2]
  * [1][1] --> [1][2]
     i  j       k  l
 So we can observe that j is replicatd to k
 also we can see l = n-i;
     */

let matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}

let bruteForceSolUsingAdditionalMatrix = function (matrix) {
  let row = matrix.length;
  let col = matrix[0].length;
  let ans = new Array(row).fill(null);
  ans = ans.map(() => new Array(col).fill(0));
  printMatrix(ans);

  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      let destRow = inner;
      let destCol = matrix.length - 1 - index;
      console.log(
        `Mapping from matrix [${index}][${inner}] ===>ans  [${destRow}][${destCol}]`
      );
      ans[destRow][destCol] = element;
    }
  }

  printMatrix(ans);
};

bruteForceSolUsingAdditionalMatrix(matrix);
import transposeSquareMatrix from "./transpose-matrix.js";

/**
 * Brute force soluion is addition n^2 space complexity
 * first we need to transpose the Matrix - Row will become column
 * Then we will reverse everry Row
 * This come from below observation
 *
 * [1 2 3]      [7 4 1]
 * [4 5 6]  ==> [8 5 2]
 * [7 8 9]      [9 6 3]
 *
 * So we can see there after 90 deg rotate - Column become reverse Row
 * [1 4 7] ===> [7 4 1]
 * So first we need to transpose the matrix - that is make columns as rows
 * tyhen we need to reverse each row
 *
 */

function reverseArr(arr) {
  let start = 0;
  let end = arr.length - 1;
  while (start < end) {
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
}
let rotateUsingTranspose = function (matrix) {
  console.log(`Transpose in place first ---`);
  transposeSquareMatrix(matrix);
  console.log(`Nor reverse every Row of the matrix `);
  for (let index = 0; index < matrix.length; index++) {
    reverseArr(matrix[index]);
  }
  printMatrix(matrix);
};

rotateUsingTranspose(matrix);
