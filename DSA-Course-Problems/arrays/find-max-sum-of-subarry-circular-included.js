let method1 = function findMaxSubofSubArrayIncludingCircularOnes(arr) {
  let maxSum = 0;
  console.log(`Given array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    let tempSum = 0;
    for (let j = 0; j < arr.length; j++) {
      let index = (j + i) % arr.length; //Very Important Step to Traver Array in rotation
      console.log(`Sub Array Element -arr[${index}] ==> ${arr[index]}`);
      tempSum = tempSum + arr[index];
      maxSum = Math.max(maxSum, tempSum);
    }
    console.log(`Max Sum Till Now - ${maxSum}`);
  }
  console.log(`Final Maximum Sum - ${maxSum}`);
};

method1([10, 5, -5]);
console.log("------------------------------");
method1([2, 3, -8, 7, -1, 2, 3]);

//couple of Observation to Rememeber here
//If a Normal Sub Array Is Providing Max Sum <0 - There is Never to have a Possible Circular Sub Array Having More Grater Sum
//First Find Max Sum of  Normal Subarray Using kadane Algo
//Then Find the Max Sum of Circular Subarray Uasing Modified kadane Algo  - What is the Modification
//Here the Mod is Like - Max Sum of Circular Sub array = [ Sum of all ellement - minimum Sum of Normal Subarray] - very IMPORTANT
//But How to calculate Minimum Sum of a Normal Subarray - Find Max Sum of a inverted Array(-ve)
/**
 * Then Find the Max of them
 *
 */

console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

let findMaxSumOfSubArray = function findMaxSumOfSubArrayNormalOnly(arr) {
  let maxSumOfAllSubArrays = arr[0];
  let maxSumOfSubArrayEndingWithI = arr[0];
  // console.log(
  //   `For SubArray Ending with ${arr[0]} -  Max Ending - ${maxSumOfSubArrayEndingWithI} || Till Now Max Sum ${maxSumOfAllSubArrays}`
  // );
  for (let i = 1; i < arr.length; i++) {
    maxSumOfSubArrayEndingWithI = Math.max(
      maxSumOfSubArrayEndingWithI + arr[i],
      arr[i]
    );
    maxSumOfAllSubArrays = Math.max(
      maxSumOfAllSubArrays,
      maxSumOfSubArrayEndingWithI
    );
    // console.log(
    //   `For SubArray Ending with ${arr[i]} - Max Sum  - ${maxSumOfSubArrayEndingWithI} || Till Now Max Sum ${maxSumOfAllSubArrays}`
    // );
  }
  //  console.log("FINAL MAX SUM OF SUB ARRAY IS - " + maxSumOfAllSubArrays);
  return maxSumOfAllSubArrays;
};

let findMaxSumOfCircularSubArray = function findMaxSumOfCircularSubArrayOnly(
  arr
) {
  let sumoffAllElements = 0;
  for (let index = 0; index < arr.length; index++) {
    sumoffAllElements = sumoffAllElements + arr[index];
    arr[index] = -1 * arr[index];
  }
  //Inverting elements of the Array - Because Finding Max Sum of Circulr array only  Means === (sum of all elements - Min sum of Normal  subarray Only)
  ///Finding Max sum of inverted array means - finding Min sum of actual array .
  let minimumSumOfArray = findMaxSumOfSubArray(arr);
  console.log("Maximum  Sum - Inverted - " + minimumSumOfArray);
  minimumSumOfArray = minimumSumOfArray * -1; //Very Important Step
  console.log("Minimum Sum - Actual  - " + minimumSumOfArray);

  let result = sumoffAllElements - minimumSumOfArray;
   console.log("FINAL MAX SUM OF CIRCULAR ARRAY IS - " + result);
  return result;
};

let maxSumOfAllInclusiveSubArrays = function maxSumOfAllInclusiveSubArrays(
  arr
) {
  let maxSumOfNormalSubarrays = findMaxSumOfSubArray(arr);
  if (maxSumOfNormalSubarrays < 0) {
    console.log("Final Result - " + maxSumOfNormalSubarrays);
  } else {
    let maxSumofCircularSubArrays = findMaxSumOfCircularSubArray(arr);
    let result = Math.max(maxSumOfNormalSubarrays, maxSumofCircularSubArrays);
    console.log("Final Result - " + result);
  }
};

maxSumOfAllInclusiveSubArrays([2, 3, -8, 7, -1, 2, 3]);
