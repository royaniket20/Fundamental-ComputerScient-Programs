/**
 * In linear algebra, the transpose of a matrix is a flipped version of the original matrix, created by switching its rows and columns. The transpose of a matrix is often represented by Aᵀ, where "T" is a superscript of the given matrix. For example, if "A" is the given matrix, then the transpose of the matrix is represented by A' or AT. 
 Written By Aniket
 */
let matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

function printMatrix(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
    console.log();
  }
}

/**
 * How we do in Place transpose
 * In transpose matrix [if its Not squre - assume it squre ] - Diagonal from left upper to right lower will alwyas be intact
 * Rest of the elements will swap place --- that means a[i][j] --> a[j][i]
 *  But we only traverse the Upper half of this Matrix  else swappuing will have No meaning 
  [1, *, *],
  [4, 5, *],
  [7, 8, 9],
 * We will only traverse * marked area 
 * 
 */

let transposeSquareMatrix = function (matrix) {
  printMatrix(matrix);

  for (let index = 0; index < matrix.length-1; index++) {
    for (let inner = index+1; inner < matrix[index].length; inner++) {
        const element = matrix[index][inner];
        matrix[index][inner] = matrix[inner][index];
        matrix[inner][index] = element;
    }
  }
  console.log("Post transpose operation ----- ");
  printMatrix(matrix);
};

transposeSquareMatrix(matrix);

export default transposeSquareMatrix;
