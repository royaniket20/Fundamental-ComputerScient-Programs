let arr = [
  1, 2, -3, -1, 1, 1, 1, -4, 3, 2, 4, 3, -2, 1, 7, 6, 5, -4, 3, 2, -1, 7, 5, 4,
  -3, 2, -1, 1, 2, -3, 4, 6,
];
/**
 * Give the length of longest subarray with Sum K 
Here sliding window technique will Not work due to presence of negative Numbers 
So what we do is For each prefix Sum We store whats the Index at which this prefix Sum happens - As a map [K = Prefix Sum , V = Index where that occurs ] 
Now for each element we check we from somewhere at previous Point till Current Point is there the target Sum present - that will give you the Subarray which can give target Sum
  0 1 2 3 4 5 6 7 8
[ . . . . . . . . . ]
----X-----|----K---  
--------T-----------
here K is the target Sum - Now at 8th Position we can surely Say Its last index of a subarray with target Sum K -If and only if there is an Point behind the line 
T-K = X happned = > If so happened then it must be available in our Hashmap for referreing to   
This will work perfectly - When you have Positive , Nehative , Zero Values
*/

let findLongestSubArrayWithSumKAny = function (arr, target) {
  console.log(`Given array = ${arr} | Target Sum - ${target}`);
  let maxLength = Number.MIN_SAFE_INTEGER;
  let start = 0;
  let end = 0;
  let sum = 0;
  let map = new Map();
  map.set(sum , -1); //This is an Important Step - Because suppose you got a Sum which start from 0th Index - then as par the Logic 
  //We need to excude where (T-K ) happened - which never happend and happening first time - hence -1 is suitable 
  //Prefilling the prefix Sum Map with Index at which prefix Sum happened
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    sum = sum + element;
    //IMP - This need to be ensured that we do not make duplicate entry and Just override the Entry - This will sometime create problem
    //Specially ibn the case where we are habndling negative use cases

    //Special Case when having 0
    /**
     * [2,0,0,3] == Here if target is 3 It shoud Give an Subarra of [0,0,3]
     * But for the nature of hashmap -- Sum 2 Index will replaced by 0 then 1 then 2
     * So we will get result for target 3 as [3] Instead of [0,0,3]
     */

    if (!map.has(sum)) {
      map.set(sum, index);
    }
    // console.log(`Hydrated Map --->`);
    // console.log(map);

    let isSumPreviouslyHappend = sum - target;
    //console.log("Current Sum = "+ sum , "Sum to Find = " + isSumPreviouslyHappend);
    if (map.has(isSumPreviouslyHappend)) {
      console.log(
        `Got One Entry - start [${
          map.get(isSumPreviouslyHappend) + 1
        }] ==> end [${index}]`
      );
      maxLength = Math.max(
        maxLength,
        index - (map.get(isSumPreviouslyHappend) + 1) + 1
      );
    }
  }

  console.log(`Given Max length with Subarray Sum K is - ${maxLength}`);
};
findLongestSubArrayWithSumKAny(arr, 6);

let arrData = [2, 0, 0, 3];
findLongestSubArrayWithSumKAny(arrData, 3);


 arrData = [2, 0, 0, 3,-3,0,0,3];
findLongestSubArrayWithSumKAny(arrData, 3);

//This is a special case - Becuse this is why we shoud pre hydrate our porefix Sum map with an entry 
//[0,-1] === That means - if somewhere in future you get sum-k as 0 IS happening then we can 
//say that subarray start from 0th Index 
//
 arrData = [ 1,2];
findLongestSubArrayWithSumKAny(arrData, 3);
//Generate all Sub array Combination and Keep track of Max length
let naiveSolution = function (arr, target) {
  console.log(`Given array Naive Sol  = ${arr} | Target Sum - ${target}`);
  let maxLength = Number.MIN_SAFE_INTEGER;
  for (let index = 0; index < arr.length; index++) {
    let sum = arr[index];

    if (sum === target) {
      console.log(`Got One Entry - start [${index}] ==> end [${index}]`);
      maxLength = Math.max(maxLength, index - index + 1);
    }

    for (let inner = index + 1; inner < arr.length; inner++) {
      sum = sum + arr[inner];
      // console.log(
      //   `SubArray Sum for subArray starting with ${arr[index]} = ${sum}`
      // );
      if (sum === target) {
        console.log(`Got One Entry - start [${index}] ==> end [${inner}]`);
        maxLength = Math.max(maxLength, inner - index + 1);
      }
    }
  }
  console.log(`Given Max length with Subarray Sum K is - ${maxLength}`);
};

//naiveSolution(arr, 6);
