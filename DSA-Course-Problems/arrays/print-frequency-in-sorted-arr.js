/**
 * Take a Map .. Keep on adding the Dta and Its Count
 * Then Print the Map
 */
let frequency = function frequency(arr) {
  let resultMap = new Map();
  for (let i = 0; i < arr.length; i++) {
    if (resultMap.has(arr[i])) {
      resultMap.set(arr[i], resultMap.get(arr[i]) + 1);
    } else {
      resultMap.set(arr[i], 1);
    }
  }
  resultMap.forEach((key, value) => {
    console.log(`${value} =appreas=> ${key}`);
  });
};

frequency([1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5, 6]);
console.log("*********************");
frequency([1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5, 5]);
console.log("*********************");
frequency([1, 2, 3]);
console.log("*********************");
frequency([1, 1, 1]);
console.log("--------------Sperate soltuin--------------");
//Because the arrays is Sorted - we can keep tracking that If similar elements keep getting on consecutive space - Increase Count
//When we get Diff element make count 0 and start again with new element
//Here only cavet is you need to Keep printing the result as Number changes
let frequencyNoExtraSpace = function frequencyNoExtraSpace(arr) {
  let result = 1;
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] === arr[i - 1]) {
      result++;
    } else {
      console.log(`${arr[i - 1]} =appreas=> ${result}`);
      result = 1;
    }
  }
  console.log(`${arr[arr.length - 1]} =appreas=> ${result}`);
};

frequencyNoExtraSpace([
  1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5, 6,
]);
console.log("*********************");
frequencyNoExtraSpace([
  1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5, 5,
]);
console.log("*********************");
frequencyNoExtraSpace([1, 2, 3]);
console.log("*********************");
frequencyNoExtraSpace([1, 1, 1]);
