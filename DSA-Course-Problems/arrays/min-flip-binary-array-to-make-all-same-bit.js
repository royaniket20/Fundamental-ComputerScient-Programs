/**
 * This is a observation based Problem
 *
 * 00110101000110
 * Here First and Last Bit is Same
 * if we flip 0s [Which are the First ans last Bit ] then Total Group Flip = 5
 * if we Flip  1s then Total Group Flip = 4 [Choose this ]
 *
 * 1100110101000110001
 * Here First and Last Bit is Same
 * if we flip 1s [Which are the First ans last Bit ] then Total Group Flip = 6
 * if we Flip  0s then Total Group Flip = 5 [Choose this ]
 *
 *  * 110011010100011000
 * Here First and Last Bit is Different
 * if we flip 1s  then Total Group Flip = 5
 * if we Flip  0s then Total Group Flip = 5
 * Choose any
 *
 * So algo is is Both end Bits are Equal - Choose the 2nd Group
 * If both End Bits are same - Choose any group
 */

// General Solution - Just count the 0s Group and 1 s Group and Compart count

let method1 = function findMinimumFlip(arr) {
  let filpOneCount = 0;
  let flipZeroCount = 0;
  if (arr[0] === 0) {
    flipZeroCount++;
  } else {
    filpOneCount++;
  }
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] === arr[i - 1]) {
      continue;
    } else {
      if (arr[i] === 0) {
        flipZeroCount++;
      } else {
        filpOneCount++;
      }
    }
  }
  console.log(
    `The Zero Group - ${flipZeroCount} , The One Group - ${filpOneCount}`
  );
  console.log(
    `MinimumFlip Required - ${Math.min(flipZeroCount, filpOneCount)}`
  );
};

method1([0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0]);
method1([1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1]);
method1([1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0]);

//Efficient Sol based on Algo

let method2 = function findMinimumFlip(arr) {
  let groupCount = 0;
  let groupIdentifier = -1;
  if (arr[0] === arr[arr.length - 1]) {
    //Both End same Bits - Find the Second Group Count
    groupIdentifier = 1 - arr[0]; //So It will literally reverse the Bit
    console.log(`Identifier Bit - ${groupIdentifier}`);
  } else {
    //Both End Diff Bits - Just Find  any group count
    groupIdentifier = arr[0];
    console.log(`Identifier Bit - ${groupIdentifier}`);
  }

  if (groupIdentifier === arr[0]) {
    groupCount++;
  }
  let isGroupCounted = false;
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] === groupIdentifier) {
      isGroupCounted = true;
      continue;
    } else {
      if (isGroupCounted) {
        groupCount++;
        isGroupCounted = false;
      }
    }
  }

  console.log(`MinimumFlip Required - ${groupCount}`);
};

method2([0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0]);
method2([1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1]);
method2([1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0]);
