/**
 * Write a program to check if the array can be Devided into Subarrays with Equal Sum .e - each Subarray will have Same sum of all elements
 * Create A Prefix Sum Array First 
 * Then Find Out if Multiples can be Found or Not
 */

let method = function (arr) {
  let result = false;
  console.log(`The Given array = ${arr}`);
  //Make it a prefix Sum array
  for (let i = 1; i < arr.length; i++) {
    arr[i] = arr[i - 1] + arr[i];
  }
  console.log(`The Prefix Sum array = ${arr}`);
  //First find Out if ther is an element which can devide last element of the prefix Sum array
  // Very Important - Idea is Try to Find the elements which can devide last element  - Which will Give you the Index
  let index = -1;
  let subArrEndEndex = [];
  for (let i = 0; i < arr.length; i++) {
    if (index > -1 && arr[i] % arr[index] === 0) {
      subArrEndEndex.push(i);
    } else if (arr[arr.length - 1] % arr[i] === 0) {
      subArrEndEndex.push(i);
      index = i; //For First Number
    }
  }
  console.log(`Sub array Ending Indexes = ${subArrEndEndex}`);
  if (subArrEndEndex.length > 1) {
    result = true;
    console.log("Now Just check that the Indexes are Continious Multiple");
    let initialMultiple = 2;
    let initialElement = arr[subArrEndEndex[0]];
    for (let i = 1; i < subArrEndEndex.length; i++) {
      let currentElement = arr[subArrEndEndex[i]];
      if (currentElement === initialMultiple * initialElement) {
        initialMultiple++;
      } else {
        result = false;
        break;
      }
    }
  } else {
    result = false;
  }

  console.log(`Is Division to Subarray of equal Sum POssible - ${result}`);
};

method([5, 2, 6, 2, 1, 1, 4]);
method([5, 2, 5, 2, 1, 1, 5, 2, 1, 1, 1, 1, 1]);
