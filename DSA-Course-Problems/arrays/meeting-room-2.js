/**
Description
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), find the minimum number of conference rooms required.)

(0,8),(8,10) is not conflict at 8

Example
Example1

Input: intervals = [(0,30),(5,10),(15,20)]
Output: 2
Explanation:
We need two meeting rooms
room1: (0,30)
room2: (5,10),(15,20)
Example2

Input: intervals = [(2,7)]
Output: 1
Explanation: 
Only need one meeting room
 */

//[(0,30),(5,10),(15,20)]

//[(5,8),(9,15)]

let countMinMeetingRoom = function (intervals) {
  intervals.forEach((item) => {
    console.log(`Intervals - ${item.start}==> ${item.end}`);
  });

  let startArr = intervals.map((item) => item.start).sort((a, b) => a - b); //Getting all start time sorted
  let endArr = intervals.map((item) => item.end).sort((a, b) => a - b); //getting all end time sorted
  let count = 0;
  let max = 0;
  let startPointer = 0; //Upcoming meeting start time index - Value of which needed for understanding the Meeting Root
  let endPointer = 0; //Current meeting end  time index - Value of which needed for understanding the Meeting Root
  console.log(`Star time sorted - ${startArr}`);
  console.log(`End time sorted - ${endArr}`);
  while (startPointer < startArr.length) {
    //Do Until all meetings are not scheduled
    //Both have some value remains
    if (startArr[startPointer] < endArr[endPointer]) {
      // Means for this upcoming meeting  needed new Room as last meeting still Going
      count++;
      startPointer++; // Pointing to next meeting schedule to be started
    } else {
      //This means last meeting ended  exactly at same time or Much before - So release meeting Room
      count--;
      endPointer++; //Pointing to the next meeting ending Soon
    }
    max = Math.max(max, count);
  }
  console.log(`Total Conference Room Required - ${max}`);
};

let intervals = [];
let interval1 = { start: 0, end: 30 };
intervals.push(interval1);
interval1 = { start: 15, end: 20 };
intervals.push(interval1);
interval1 = { start: 5, end: 10 };
intervals.push(interval1);
interval1 = { start: 10, end: 15 };
intervals.push(interval1);
countMinMeetingRoom(intervals);
console.log(`*-*************************`);
intervals = [];
interval1 = { start: 9, end: 15 };
intervals.push(interval1);
interval1 = { start: 5, end: 8 };
intervals.push(interval1);
countMinMeetingRoom(intervals);
console.log(`*-*************************`);
intervals = [];
interval1 = { start: 1, end: 3 };
intervals.push(interval1);
interval1 = { start: 8, end: 10 };
intervals.push(interval1);
interval1 = { start: 7, end: 8 };
intervals.push(interval1);
interval1 = { start: 9, end: 15 };
intervals.push(interval1);
interval1 = { start: 2, end: 6 };
intervals.push(interval1);
countMinMeetingRoom(intervals);
