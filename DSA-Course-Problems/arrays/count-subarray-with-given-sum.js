/**
 * Problem Statement: Given an array of integers and an integer k, return the total number of subarrays whose sum equals k.

A subarray is a contiguous non-empty sequence of elements within an array.

Example 1:
Input Format:
 N = 4, array[] = {3, 1, 2, 4}, k = 6
Result:
 2
Explanation:
 The subarrays that sum up to 6 are [3, 1, 2] and [2, 4].
VERY IMPORTANT - YOU NEED TO COUNT ALL THE POSSIBLE SUBARRAYS 
[2,0,0,3] --iF TARGET IS 3 THEN THERE ARE 2 cOUNT [0 0 3 ] & [3]

So here Idea is that we can naively check all subarry permutation and keep counting 

Efficient Solution is Just like Longest Subarry with given target -- We will go for prefix Sum 
But there because we wnat longest so we just keep ignoring duplicate entries 
Here we will try to keep track How mnany times previously currentSum-target happens -- Both count we will take into consideration 
also Keep one entry 
 */
let countSubarrayWithGivenSum = function (arr, target) {
  console.log(`Given Array - ${arr} || Target Sum - ${target}`);
  let map = new Map();
  let result = 0;
  let prefixSum = 0;
  map.set(prefixSum, 1); // 0 is Init Value In case [1,1,1]- and Target 3 This will be needed
  //This we have added as Count as 1 - So that we can count when FIRST TIME it matches
  for (let index = 0; index < arr.length; index++) {
    console.log(`Prrepared Prefix Sum  Duplicate count -`);
    console.log(map);
    const element = arr[index];
    prefixSum = prefixSum + element;
    console.log(`Updated Preofix Sum - ${prefixSum}`);

    if (map.has(prefixSum)) {
      map.set(prefixSum, map.get(prefixSum) + 1);
    } else {
      map.set(prefixSum, 1);
    }
    //Now check till this Point is there a Pint from where till current Point target happened
    let prevSumTarget = prefixSum - target;
    if (map.has(prevSumTarget)) {
      result = result + map.get(prevSumTarget);
      console.log(`Count till Now - ${result}`);
    }
  }
};
countSubarrayWithGivenSum([3, 1, 2, 4], 6);
countSubarrayWithGivenSum([2, 0, 0, 3], 3);
countSubarrayWithGivenSum([1, 2, 3, -3, 1, 1, 1, 4, 2, -3], 3);
