/**
 * You are given an array of CPU tasks, each represented by letters A to Z, and a cooling time, n. Each cycle or interval allows the completion of one task. Tasks can be completed in any order, but there's a constraint: identical tasks must be separated by at least n intervals due to cooling time.

​Return the minimum number of intervals required to complete all tasks.

Example 1:

Input: tasks = ["A","A","A","B","B","B"], n = 2

Output: 8

Explanation: A possible sequence is: A -> B -> idle -> A -> B -> idle -> A -> B.

After completing task A, you must wait two cycles before doing A again. The same applies to task B. In the 3rd interval, neither A nor B can be done, so you idle. By the 4th cycle, you can do A again as 2 intervals have passed.

Example 2:

Input: tasks = ["A","C","A","B","D","B"], n = 1

Output: 6

Explanation: A possible sequence is: A -> B -> C -> D -> A -> B.

With a cooling interval of 1, you can repeat a task after just one other task.

Example 3:

Input: tasks = ["A","A","A", "B","B","B"], n = 3

Output: 10

Explanation: A possible sequence is: A -> B -> idle -> idle -> A -> B -> idle -> idle -> A -> B.

There are only two types of tasks, A and B, which need to be separated by 3 intervals. This leads to idling twice between repetitions of these tasks.

 *************************************************************

Intution - 
Here we know we have given tasks A-Z . with Multiple Occurance 
Also After every same task Next task can be taken after N unit cool down Period 
Also all task Take 1 unit tile only 

So we can do some observation -- 
[A,A,A,B,B,C,C]
First we will assume a wrost case - That means same task getting scheduled one after another Until it get exhausted .
By the rule of the scheduler - We have to give n unit coold down in Between 
But then CPU will take so much Long time 
So what we will do is Fill those Gaps with other tasks 

Algo --
First find out which task have Heighest Count 
Then we will Know that If we have scheduled them one after another - How many times we need to put n cooldown chunk 
[A,A,A,B,B,C,C] n = 2
-- Here A occurs Max time 
A - - A - - A 
   n     n
So if A appear X time Idel chunk will be X -1;   
Now we will try those gaps with other diff tasks to minimize idel time 
A B - A B - A
--->
A B C A B C A

So first we will get the Chunks and hence Chunk*n = Total idel time - then we will try to Minimize it by seeing other Tasks 

Suppose you have 2 Chunks and You have other task 3 .. then we can say each chunk 1 Unit time can be eaten by those tasks 
Hence 2 Unit of Idel time will be reduced 


Suppose you have 2 Chunks and You have other task 1 .. then we can say 1  chunk 1 Unit time can be eaten by those tasks 
Hence  1 Unit of Idel time will be reduced 

For every task - we minimize Idel time by Min of ( Chunk Count , Frq of that task) 
   */

var leastInterval = function (tasks, n) {
  console.log(`Given task - ${tasks} || idel time - ${n}`);
  let tasksArr = new Array(26).fill(0);
  for (let index = 0; index < tasks.length; index++) {
    const element = tasks[index];
    let idx = element.charCodeAt() - 'A'.charCodeAt();
    tasksArr[idx]++;
  }
  tasksArr.sort((a, b) => b - a);
  console.log(`Given task Frequency in Sorted Order  - ${tasksArr} `);
  //If Max req is X then Oviously Cooldown Chunks between then X-1
  let maxChunk = tasksArr[0] - 1;
  let maxIdleTime = maxChunk * n;
  for (let index = 1; index < tasksArr.length; index++) {
    maxIdleTime = maxIdleTime - Math.min(tasksArr[index], maxChunk);
  }
  console.log(`Max idle time - ${maxIdleTime}`);
  if (maxIdleTime >= 0) {
    console.log("Total time Taken = "+ maxIdleTime + tasks.length);
  } else {
    //Means All Idel Time wasted already - We can simpley Send task length
    console.log("Total time Taken = "+ tasks.length);
  }
};
leastInterval(["A", "A", "A", "B", "B", "B"], 2);
leastInterval(["A", "C", "A", "B", "D", "B"], 1);
leastInterval(["A", "A", "A", "B", "B", "B"], 3);
