/**
 *
 * Question is - Find the Amount of Tapped Water in the wall 
 * 
 * Here the idea is that - Only we need to Find at head of each Pillar how much water is tapped 
 * For any piller - amount of water = Pillar Height - Min[Left wall Height , Right Wall Height ]
 *
 * Naive Approach -
 * 
 *  For every Pillar - Find Out left max wall and Right Max Wall
 * 
 * Advance approach - For each Pillar - Find Left Max wall in one shot and thne Find Right Max wall in one shot 
 * Then traverse and for each Piller calculate Tapped wanter 
 * 
 */

let method1 = function tapRainWater(arr) {
  let leftwall = 0;
  let rightWall = 0;
  let maxWaterLogged = 0;
  for (let i = 0; i < arr.length; i++) {
    console.log(`Considering for the Wall - ${arr[i]}`);
    leftwall = arr[i];
    rightWall = arr[i];
    //Find Max Left wall for current Piller 
    for (let j = 0; j < i; j++) {
      leftwall = Math.max(leftwall, arr[j]);
    }
    //Find Max Righht wall for current Pillar 
    for (let k = arr.length - 1; k > i; k--) {
      rightWall = Math.max(rightWall, arr[k]);
    }
    console.log(
      `The Left Side Wall - ${leftwall} & Right Side Wall - ${rightWall}`
    );
    //calculate for that Piller How much water logged 
    let waterLogged = Math.min(leftwall, rightWall) - arr[i];
    console.log(`Water Logged at Current Pillar - ${waterLogged}`);
    maxWaterLogged = maxWaterLogged + waterLogged;
    console.log(`Total Water Logged Till Now - ${maxWaterLogged}`);
  }
};

method1([2, 0, 2]);
console.log(`***********************************`);
method1([3, 0, 1, 2, 5]);

//Efficient Solution
/**
 * 
 * @param {*} arr
 * Here Logic is Simple - For exvery Pillar we will pre compute the Left Wall
 * And the right wall Before hand
 *  
 */

let method2 = function tapRainWaterEfficient(arr) {
    let leftwallArr = [];
    let rightWallArr = [];
    let maxWaterLogged = 0;
    //Left Wall Calculation
    leftwallArr[0] = arr[0];//There is No Left wall for extrement Left Pos
    for (let i = 1; i < arr.length; i++) {

      let leftwall = Math.max(arr[i],leftwallArr[i-1]);
      leftwallArr[i]=leftwall;
    }
      //Right Wall Calculation
      rightWallArr[arr.length-1] = arr[arr.length-1];//There is No Right wall for extrement Right Pos
    for (let k = arr.length - 2; k >=0; k--) {
        let rightWall = Math.max(arr[k],rightWallArr[k+1]);
        rightWallArr[k]=rightWall;
      }
      for (let i = 0; i < arr.length; i++) {
        console.log(`Considering for the Wall - ${arr[i]}`);
        console.log(
            `The Left Side Wall - ${leftwallArr[i]} & Right Side Wall - ${rightWallArr[i]}`
          );
          let waterLogged = Math.min(leftwallArr[i], rightWallArr[i]) - arr[i];
    console.log(`Water Logged at Current Pillar - ${waterLogged}`);
    maxWaterLogged = maxWaterLogged + waterLogged;
    console.log(`Total Water Logged Till Now - ${maxWaterLogged}`); 
      }

  };
  console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
method2([2, 0, 2]);
console.log(`***********************************`);
method2([3, 0, 1, 2, 5]);
