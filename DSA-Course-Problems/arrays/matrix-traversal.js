//Matrrix can be traversed Row Wise or Colun Wise
//rOW mAJOR TRAVERSAL IS FASTER IN MANY LANGUAGE AS THERE MATRIX IS TIRED IN THAT WAY

let matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];
console.log("******* Row  major ********");
function rowMajor(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[index][inner];
      process.stdout.write(`[${element}]`);
    }
  }
}
rowMajor(matrix);
console.log("\n******* col major ********");
function colMajor(matrix) {
  for (let index = 0; index < matrix.length; index++) {
    for (let inner = 0; inner < matrix[index].length; inner++) {
      const element = matrix[inner][index];
      process.stdout.write(`[${element}]`);
    }
  }
}
colMajor(matrix);
