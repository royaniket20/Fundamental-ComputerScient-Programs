/**
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * @param {*} nums
 * @param {*} target
 * @returns
 * Here we can simply Use an Map <K,V>
 * in Value we have the Index of array elelment
 * in Key will be (target- curr element value)
 * Idea is if we Find an Elelemnt later which is matching any Key - then we will return
 * That element Index and Index stored in Vlaue part of the Map
 */
var twoSum = function (nums, target) {
  let map = new Map();

  let result = [];
  map.set(target - nums[0], 0);
  for (let i = 1; i < nums.length; i++) {
    if (map.has(nums[i])) {
      result.push(map.get(nums[i]));
      result.push(i);
      break;
    } else {
      map.set(target - nums[i], i);
    }
  }
  return result;
};
console.log(`Two Sum Indexes - ${twoSum([2, 7, 11, 15], 9)}`);
