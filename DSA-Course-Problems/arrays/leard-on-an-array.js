/**
 * Find out leaders of an array - A leader have all elements at Right side Smaller than It
 * If the array is sorted small to large - then Only last elelment is leader
 * In Case Array sorted large to Small - all elements are leader
 */

//For an Unsorted array 
/**
 * Simple Idea is For each element of array 
 * traverse the whole array and Find out which element is 
 * matching the leader selection criteria 
 * 
 */
let method1 = function findLeader(arr) {
  let result = [];
  let isElderFound = false;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] >= arr[i]) {
        isElderFound = true;
        break;
      }
    }
    if (!isElderFound) {
      result.push(arr[i]);
    }
    isElderFound = false;
  }
  console.log(`In the Array : ${arr} the Leaders are - ${result}`);
};
method1([5, 3, 4, 2, 1, 9]);
method1([6, 5, 4, 2, 3, 1]);

/**
 *
 * @param {*} arr
 * Here the Idea is Simple - We just use the Thinking that
 * From  Right to Left -Current element is greater that recent leader he is also a  leader
 * Here we will Not Traverse from Left rather traverse from Right 
 *
 */
let method2 = function findLeaderEfficient(arr) {
  let result = [];
  result.push(arr[arr.length - 1]); //No matter what Right element is always leader
  let currentleader = arr[arr.length - 1];
  for (let i = arr.length - 2; i >= 0; i--) {
    if (arr[i] > currentleader) {
      currentleader = arr[i];
      result.push(arr[i]);
    }
  }
  result.reverse(); //Just simple Reverse
  console.log(`In the Array : ${arr} the Leaders are Efficient - ${result}`);
};
method2([6, 5, 4, 2, 3, 1]);
