//This is a Sliding Window problem with HashMap 
//Here maintain a Map - where we maintain frequency of elements 
//Then slide the Window from Left to Right and Keep updating the frequency 

let method1 = function distinnctElementinArrayWindow(arr, window_size) {
  console.log(`The Given array - ${arr} & Window ize - ${window_size}`);
  let counterMap = new Map();
  let resultMap = new Map();
  let window = 1;
  for (let i = 0; i < window_size; i++) {
    counterMap.set(
      arr[i],
      counterMap.get(arr[i]) === undefined ? 1 : counterMap.get(arr[i]) + 1
    );
  }
  resultMap.set(window, counterMap.size);
  console.log(resultMap);

  for (let i = window_size; i < arr.length; i++) {
    let elementToBeAdded = arr[i];
    let elementToBeRemoved = arr[i - window_size];
    console.log(
      `Element added - arr[${i}] => ${elementToBeAdded}| Element Removed - arr[${
        i - window_size
      }] => ${elementToBeRemoved}`
    );
    counterMap.set(
      elementToBeAdded,
      counterMap.get(elementToBeAdded) === undefined
        ? 1
        : counterMap.get(elementToBeAdded) + 1
    );

    counterMap.set(elementToBeRemoved, counterMap.get(elementToBeRemoved) - 1);
    //If the element is Now counting as Zero - Just remove it 
    if (counterMap.get(elementToBeRemoved) === 0) {
      counterMap.delete(elementToBeRemoved);
    }

    resultMap.set(++window, counterMap.size);
    console.log(resultMap);
  }

  resultMap.forEach((value, key) => {
    console.log(`For Window ${key} -- Distinct Element Count ${value}`);
  });
};

method1([1, 3, 4, 6, 2, 4, 6, 2, 1, 4, 6, 7, 9, 2, 5, 9, 1, 5, 8], 4);
