/**
 * There will be an array with N elements with half positive Num and Half Negative Num - No Zeros 
 * You need to rearrange them by Sign in alternative position 
 * 
 *  + - + - + - .... However we need to keep mainitaining the order of Signed elements 
[3,1,-2,-5,2,-4] ==> [3,-2,1,-5,2,-4] -- Keeping Signed elements in same order - Just make sure they come at alternate  
*/
let arr = [3, 1, -2, -5, 2, -4];
let rearrangeElements = function (arr) {
  console.log(`Given array - ${arr}`);
  let ans = new Array(arr.length);
  let positiveIndex = 0;
  let negativeIndex = positiveIndex + 1;
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (element > 0 && positiveIndex < arr.length) {
      ans[positiveIndex] = element;
      positiveIndex = positiveIndex + 2;
    } else if (element < 0 && negativeIndex < arr.length) {
      ans[negativeIndex] = element;
      negativeIndex = negativeIndex + 2;
    }
  }
  console.log(`Modified array - ${ans}`);
};

rearrangeElements(arr);

/**
 *
 * Another Problem - Positive and Negative Nums are UnEqual - So we need to keep additionals at end of the array without altering the
 * order
 * Here is We will use normal solution rather than Optimal Solution as shown above
 */

let reaarangeElementsUnEqualPosNeg = function (arr) {
  console.log(`Given Array ---- ${arr}`);
  let positives = [];
  let negatives = [];
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (element > 0) {
      positives.push(element);
    } else if (element < 0) {
      negatives.push(element);
    }
  }
  console.log(`Positives - ${positives} || Negative - ${negatives}`);
  //Now we need to check How Much we need to rearrange alternatively
  if (positives.length > negatives.length) {
    //Positives are extra
    let pos = 0;
    let neg = pos + 1;

    for (let index = 0; index < negatives.length; index++) {
      const element = negatives[index];
      arr[pos] = positives[index];
      pos = pos + 2;
      arr[neg] = element;
      neg = neg + 2;
    }
    let currentIndex = negatives.length * 2;
    //For remaining Pos elements
    for (let index = negatives.length; index < positives.length; index++) {
      const element = positives[index];
      //console.log(`remainng Pos element - ${element}`);
      arr[currentIndex] = element;
      currentIndex++;
    }
  } else {

     //Negatives are extra
     let pos = 0;
     let neg = pos + 1;
 
     for (let index = 0; index < positives.length; index++) {
       const element = positives[index];
       arr[pos] = element;
       pos = pos + 2;
       arr[neg] = negatives[index];
       neg = neg + 2;
     }
     let currentIndex = positives.length * 2;
     //For remaining Neg elements
     for (let index = positives.length; index < negatives.length; index++) {
       const element = negatives[index];
       //console.log(`remainng Neg element - ${element}`);
       arr[currentIndex] = element;
       currentIndex++;
     }


  }

  console.log(`Result array - ${arr}`);

};

reaarangeElementsUnEqualPosNeg([-1, 2, 3, 4, -3, 1]);

reaarangeElementsUnEqualPosNeg([1, -2, -3, -4, 3, -1]);

reaarangeElementsUnEqualPosNeg([1, -2, -3, -4, 3,6, -1,9]);