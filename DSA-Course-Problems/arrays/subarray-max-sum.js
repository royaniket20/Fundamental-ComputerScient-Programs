/**
 * sub array --
 * [1,2,3] === Followings are the Sub array
 *
 * [1] , [1,2] , [1,2,3]
 * [2] , [2,3]
 * [3]
 */
//Here for each sub array we simply DO the Sum and check if its crossing Max
//N^2 Time Complexity
let method1 = function subarraySum(arr) {
  let max = Number.MIN_VALUE;
  for (let i = 0; i < arr.length; i++) {
    let tempSum = 0;
    console.log(`For all subarray starting from arr[${i}]`);
    for (let j = i; j < arr.length; j++) {
      tempSum = tempSum + arr[j];
      max = Math.max(max, tempSum);
      console.log(
        `Sum of the Sub Array  is : ${tempSum} || Max Till Now - ${max}`
      );
    }
  }
  console.log("Maximum Sum : " + max);
};

method1([1, 2, 3]);
method1([2, 3, -8, 7, -1, 2, 3]);

/**
 *WE WILL USE kadane'S ALGORITHM
 * Efficient Solution - Here we can do a trick that Max of Sub array [i-1]
 * can be used to find Max of Subarray [i]
 *
 * Suppose array is [1,2,3]
 * for 1 - you have 3 choice - take 1 ,1+2, 1+2+3
 * for 2 - you have 2 choice - take 2 ,2+3
 * for 3 - you have 1 choice - take 3
 * For every element we are tring to find out the Max sum of the subarray endin with it
 * When we traverse the array from left to right
 *
 * In a plane language when we proceed from start to end of the array - for each element we will decide if its going to increment the total Sum
 * or it is going to reduce the total sum . In case it reduce the total sum - We will discard previous elements and start from there again
 */

let method2 = function subarraySumEfficient(arr) {
  let max = arr[0]; //Lets initialize at First element
  let maxEnding = arr[0]; ////Lets initialize at First element
  for (let i = 1; i < arr.length; i++) {
    maxEnding = Math.max(maxEnding + arr[i], arr[i]);
    max = Math.max(max, maxEnding);
  }
  console.log("Maximum Sum : " + max);
};

method2([1, 2, 3]);
method2([2, 3, -8, 7, -1, 2, 3]);
