/**
 * let arr = [1,1,2,2,2,3,3,4,5]
 * arr = [1,2,3,4,_,_,_ ... So on ]
 */

let removeduplicate = function removeDuplicateWithAdditionalSpace(arr) {
  console.log(`Original Array - ${arr} and Size = ${arr.length}`);
  let set = new Set();
  for (let index = 0; index < arr.length; index++) {
    set.add(arr[index]);
  }
  console.log(`Unique elements Size - ${set.size}`);
  set.forEach((item) => {
    console.log(item);
  });
};

removeduplicate([1, 2, 3, 4, 5, 5, 5, 5, 8, 9, 9, 10, 11, 12, 12]);

/**
 * Here idea is Two Pointer approach
 * First we will start from pos 0 with Two pointers
 * Move one pointer 1 pos at a time until we reach a lelement where the tow points point to
 * diff value.
 * Then agai nbring the Frst pointer to this p[osition and go on .. ]
 */

let removeduplicateOptimal = function removeDuplicateOptimal(arr) {
  console.log(`Original Array - ${arr} and Size = ${arr.length}`);
  let pos1 = 0;
  let pos2 = 0;
  let count = 0;
  while (pos1 < arr.length && pos2 < arr.length) {
    if (arr[pos1] === arr[pos2]) {
      arr[pos1] = arr[pos2];
      pos2++;
    } else {
      pos1++;
      arr[pos1] = arr[pos2];
    }
  }
  console.log(`Unique elements Size - ${pos1 + 1}`);
  console.log(arr.slice(0,pos1+1));
};

removeduplicateOptimal([1, 2, 3, 4, 5, 5, 5, 5, 8, 9, 9, 10, 11, 12, 12]);
