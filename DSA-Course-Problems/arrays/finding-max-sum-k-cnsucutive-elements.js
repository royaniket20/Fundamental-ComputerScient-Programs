/**
 * Naive apprach is Create a nested for Loop
 * For each Element - Firnd the Sum of Next K elements
 * Keep track of Max Sum using a variable and eventuially give result
 *
 */

let method1 = function findmaxSumOfKConsecutiveItems(arr, k) {
  console.log(`Given array - ${arr} | Window Size - ${k}`);
  let maxResult = 0;
  if (arr.length < k) {
    console.log(
      "Max sum is simply the Sum of all elements as Window Size is Greater than Array Length"
    );
    let result = 0;
    for (let i = 0; i < arr.length; i++) {
      result = result + arr[i];
    }
    console.log(`The Maximum Sum is : ${result}`);
  } else {
    for (let i = 0; i < arr.length - k + 1; i++) {
      console.log(`Current Element is : ${arr[i]}`);
      let result = 0;
      for (let j = i, count = 0; count < k; count++, j++) {
        result = result + arr[j];
      }
      maxResult = Math.max(maxResult, result);
      console.log(`Till Now Max Result - ${maxResult}`);
    }
    console.log(`The Maximum Sum is : ${maxResult}`);
  }
};

method1([1, 4, 9, 2, 8, 3, -4, -5, 7, -2, 1, -9, 3, 9, 9, 9], 3);

method1([1, 1, 1, 1], 5);

//Efficient Solution - We can use Sliding Window Protocol
/**
 * Here we First create Two Points - One point to 0th and 1 point to Kth element  - Calcualte Sum of the Wondow element
 * Then Slide the window one by One - So deduct the left element and add the right element
 * Keep track of Max sum and print it
 *
 */
console.log("****************************************************");
let method2 = function findmaxSumOfKConsecutiveItemsEfficient(arr, k) {
  //edige case
  if (arr.length < k) {
    console.log(
      "Max sum is simply the Sum of all elements as Window Size is Greater than Array Length"
    );
    let result = 0;
    for (let i = 0; i < arr.length; i++) {
      result = result + arr[i];
    }
    console.log(`The Maximum Sum is : ${result}`);
  }

  let initialSum = 0;
  let maxResult = 0;
  for (let i = 0; i < k; i++) {
    initialSum = initialSum + arr[i];
  }
  console.log(`Initial Sum of the Window is - ${initialSum}`);
  maxResult = Math.max(maxResult, initialSum);

  for (let i = k; i < arr.length; i++) {
    console.log(`Current Element is : ${arr[i]}`);
    initialSum = initialSum + arr[i] - arr[i - k];
    maxResult = Math.max(maxResult, initialSum);
    console.log(`Till Now Max Result - ${maxResult}`);
  }

  console.log(`The Maximum Sum is : ${maxResult}`);
};

method2([1, 4, 9, 2, 8, 3, -4, -5, 7, -2, 1, -9, 3, 9, 9, 9], 3);
