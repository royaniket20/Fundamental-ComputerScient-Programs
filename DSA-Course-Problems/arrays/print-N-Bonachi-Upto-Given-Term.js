/**
 *N BONACHI NUMBER MEANS - next element is Sum of last N elements 
 Fibonachi = Next element is sum of last 2 elements 
 2 bonachi = Fibonachi 

 4 bonachi = Initial Nums  = 0,0,0,1
 5th Num = 0+0+0+1 = 1 

 Here First create a Sliding Window on N elements .
 Then Keep on adding sum of the Window to Next element and Shift the Left and Right Index one by One 
 */
let method1 = function n_bonachi_upto_m_terms(n_bonachi, kth_term) {
  let results = [];
  if (n_bonachi > kth_term) {
    for (let i = 0; i < kth_term; i++) {
      results.push(0);
    }
  } else {
    //Initial Setup
    for (let i = 0; i < n_bonachi - 1; i++) {
      results.push(0);
    }
    results.push(1); // Pushing nth Element element of N bonachi Window 
    console.log(`Initial N Bonachi Serise Filled - ${results}`);
    let initialWindowSum = 1; // By Default as from above 
    let leftIndex = -1; //Because we need to discard element outside the Range
    let rightIndex = results.length - 1;
    console.log(
      `Initial Left Index to Discard - ${leftIndex},Right Index to Add - ${rightIndex}`
    );
    while (results.length < kth_term) {
      results.push(initialWindowSum);
      rightIndex++;
      leftIndex++;
      console.log(
        `Next Left Index to Discard - ${leftIndex},Right Index to Add - ${rightIndex} , Array - [${results}]`
      );
      initialWindowSum = initialWindowSum + results[rightIndex];
      initialWindowSum = initialWindowSum - results[leftIndex];
    }
  }

  console.log(`FOR A GIVEN INPUT OF ${n_bonachi}-BONACHI SERISE - OF ${kth_term} TERMS : `);
  console.log(results);
};
method1(15, 3);
method1(3, 15);
