//Here Array is the Non negative array
/**
 * First we keep both Left and Right Index at 0th Poisition 
 * Now we keep on adding using Right Index until we cross the Sum 
 * If we cross Sum then keep on discarding from Left Index 
 * //Rule is When the Sum is More than Given One - Discard from left
  //When Some is Less than Given Sum Keep adding at Right
 */
let method1 = function findSubarraywithGivenSum(arr, sum) {
  let currentSum = 0;

  let leftIndex = -1;
  let rightIndex = -1;
  let elementchecked = 0;
  while (sum != currentSum) {
    console.log(
      `The Current Sum -- ${currentSum} -- Actual Given Sum - ${sum}`
    );
    if (
      rightIndex < arr.length &&
      leftIndex < arr.length &&
      leftIndex <= rightIndex
    ) {
      if (currentSum > sum) {
        currentSum = currentSum - arr[++leftIndex];
        console.log(`Sum at Left Side Discard - ${currentSum} `);
      } else if (currentSum < sum) {
        currentSum = currentSum + arr[++rightIndex];
        console.log(`Sum at Right Side Addition - ${currentSum} `);
      }
    }
  }
  if (currentSum === sum) {
    //Here we do ++leftIndex because the Current Left Index is already excluded
    console.log(
      `Final SubArray with Sum : ${sum} is --- arr[${++leftIndex}] ---> arr[${rightIndex}]`
    );
  } else {
    console.log(`No Such Subarray Found with Sum - ${sum}`);
  }
};

method1([2, 5, 4, 3, 6, 8, 9, 2, 3, 5, 7, 3, 3, 4], 19);
