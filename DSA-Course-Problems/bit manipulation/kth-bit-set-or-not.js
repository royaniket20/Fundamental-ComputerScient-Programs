//
let using_right_shift = function usingRightShift(num, pos) {
  //First we do right shift upto pos-1 time to get the pos bit at LSB .
  //Then and with 1 which is Just 00000000...0000...001

  let temp = num >> (pos - 1);
  console.log(`The shifted Value  ${temp.toString(2)}`);
  let res = temp & 1;
  console.log(`The result is : ${res.toString(2)} and Decimal is ---- ${res}`);
  if (res > 0) {
    console.log(
      `The Bit at  position ${pos} at Number ${num.toString(2)} is  set`
    );
  } else {
    console.log(
      `The Bit at  position ${pos} at Number ${num.toString(2)} is Not set`
    );
  }
};

using_right_shift(650254, 5);

//
let using_left_shift = function usingLeftShift(num, pos) {
  //First we do Left shift upto pos-1 time for value 1 to make pos bit 1 in  .
  //Then AND  with Original
  console.log(num.toString(2), pos);
  let temp = 1 << (pos - 1);
  console.log(`The shifted Value of 1 is :   ${temp.toString(2)}`);
  let res = num & temp;
  console.log(`The result is : ${res.toString(2)} and Decimal is ---- ${res}`);
  if (res > 0) {
    console.log(
      `The Bit at  position ${pos} at Number ${num.toString(2)} is  set`
    );
  } else {
    console.log(
      `The Bit at  position ${pos} at Number ${num.toString(2)} is Not set`
    );
  }
};

console.log("**************************************");
using_left_shift(650254, 11);
