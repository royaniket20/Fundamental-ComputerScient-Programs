let longestConsecutiveOnes = function (num) {
  console.log(`${num.toString(2).padStart(16, "0")}`);
  let max = Number.MIN_SAFE_INTEGER;
  let count = 0;
  while (num > 0) {
    if ((num & 1) === 1) {
      count++;
      num = num >>> 1;
      console.log(`${num.toString(2).padStart(16, "0")}`);
    } else {
      max = Math.max(max, count);
      console.log(`Recent Count = ${count} || Max Till Now ${max}`);
      //Zero has occured
      num = num >>> 1;
      console.log(`${num.toString(2).padStart(16, "0")}`);
      //Reset Coubnter
      count = 0;
    }
  }
};

longestConsecutiveOnes(222);
