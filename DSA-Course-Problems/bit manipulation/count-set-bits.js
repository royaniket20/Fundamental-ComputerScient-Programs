//
let using_right_shift = function countSetBits(num) {
  let result = 0;
  while (num > 0) {
    console.log(`The Number Value  ${num.toString(2)}`);
    let res = num & 1;
    if (res > 0) {
      console.log(`The Bit  is  set`);
      result++;
    }
    num = num >> 1;
  }
  console.log(`The Set Bit  Count at  Number ${num.toString(2)} is ${result}`);
};

using_right_shift(6589745);

// N & (N-1) = wILL set the Left Most 1 Bit - to 0 . So in that way continue till the Number become zero
let using_brain_karnighum_algo = function usingAlgo(num) {
  let result = 0;
  while (num != 0) {
    console.log(`The Number Value  ${num.toString(2)}`);
    num = num & (num - 1);
    result++;
  }
  console.log(`The Set Bit  Count at  Number ${num.toString(2)} is ${result}`);
};

using_brain_karnighum_algo(254865);
