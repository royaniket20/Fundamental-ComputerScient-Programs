let findOddtwoOut = function findOddtwoOut(nums) {
  console.log(`The array is -- ${nums}`);
  let xorResult = 0;
  nums.forEach((num) => {
    // console.log(num);
    xorResult = xorResult ^ num;
  });
  console.log(
    `The Xor result is --This is The odd Two Value XOR  -- ${xorResult} | ${xorResult
      .toString(2)
      .padStart(8, "0")}`
  );
  /**
Odd Number 1 -[3]: 00000011
Odd Number 2 -[7]: 00000111
Two Value XOR  4 | 00000100

Here we can See that From LSB [Right Side] First Set Bit is the Position where Both Odd Numbers have Diff Bit 
from LSB 3rd Bit is in this case ... There can be many other Bits set - But we can Just concentrate on First Set Bit 
from LSB Side 
[X & ~(X-1)] Will only keep the last SET BIT intact from LSB intace Rest all Will be unset .

[46] : 00101110
[==] : 00000010 [X & ~(X-1)]
[37] : 00100101
[==] : 00000001 [X & ~(X-1)]

So Idea is using the XOR result Post processing with above formula we get such a Number where only the Bit Set 
which have Diff bit value on Two Odds 

Now we can use this xorResult ==> Post processed with Formula to Use to seperage Those two In diff group 
*/
  xorResult = xorResult & ~(xorResult - 1);
  console.log(
    `After applying Formula - ${xorResult.toString(2).padStart(8, "0")}`
  );
  let group1 = [];
  let group2 = [];

  nums.forEach((num) => {
    let temp = num & xorResult;
    if (temp === 0) {
      console.log("Part of Group 1 - " + num); //This group holds all even NuMBERS
      group1.push(num);
    } else {
      console.log("Part of Group 2 - " + num); //tHIS GROUP hOLDS ALL ODD numbers
      group2.push(num);
    }
    console.log("******************");
  });
  console.log(`Group 1 - ${group1} || Group 2 - ${group2}`);
  xorResult = 0;
  group1.forEach((num) => {
    xorResult = xorResult ^ num;
  });
  console.log(`The first Odd Number Out is --- ${xorResult}`);
  xorResult = 0;
  group2.forEach((num) => {
    xorResult = xorResult ^ num;
  });
  console.log(`The Second Odd Number Out is --- ${xorResult}`);
};

findOddtwoOut([5, 5, 3, 6, 6, 9, 8, 5, 8, 5, 9, 16, 4, 4]);

console.log("#############################################");
console.log(`Odd Number 1 -[${3}]: ${(3).toString(2).padStart(8, "0")}`);
console.log(`Odd Number 2 -[${7}]: ${(7).toString(2).padStart(8, "0")}`);

findOddtwoOut([5, 5, 3, 7]);

console.log("#############################################");
//TEST THE fUNCTIONALITY OF  [x & ~(x-1)]
let nums = [46, 37];
nums.forEach((num) => {
  console.log(`[${num}] : ${num.toString(2).padStart(8, "0")}`);
  console.log(`[==] : ${(num & ~(num - 1)).toString(2).padStart(8, "0")}`);
});
