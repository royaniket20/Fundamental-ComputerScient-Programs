let findOddOneOut = function oddOneOut(nums) {
  console.log(`The array is -- ${nums}`);
  let xorResult = 0;
  nums.forEach((num) => {
    console.log(num);
    xorResult = xorResult ^ num;
  });
  console.log(`The Xor result is --This is The odd one  -- ${xorResult}`);
};

findOddOneOut([5, 5, 6, 6, 9, 8, 5, 8, 5, 9, 16, 4, 4]);

//Find out in 1 - N which Number is Missing in N-1 size array
/**
 *
 *  First do xor of all nums form 1->n == Suppose result is R
 * then when we do again xor with Given number and R -then All the available Numbers are cancleed
 * because X ^ X =0
 *
 */

let findMissingNum = function findMissingNum(nums) {
  console.log(`The array is -- ${nums}`);
  let xorResult = 0;
  nums.forEach((num) => {
    console.log(num);
    xorResult = xorResult ^ num;
  });
  let expectedArrySize = 1 + nums.length;
  console.log(`The array Length is -- ${expectedArrySize}`);
  for (let i = 1; i <= expectedArrySize; i++) {
    console.log("The number is --" + i);
    xorResult = xorResult ^ i;
  }
  console.log("The result is -- " + xorResult);
};

findMissingNum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15]);
