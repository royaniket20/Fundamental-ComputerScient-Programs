let decToBin = function (num) {
  console.log(`Given Number - ${num}`);
  let arr = new Array();
  while (num > 0) {
    arr.unshift(num & 1);
    num = num >> 1;
  }
  console.log(`Result Decimal to Binary ${arr.length > 0 ? arr.join("") : 0}`);
};

decToBin(0);
decToBin(1);
decToBin(123568);

console.log((123568).toString(2));
