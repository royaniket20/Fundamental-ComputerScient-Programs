console.log("****Binary Interative Solution******");

let decimalToBuinaryManual = function decimalToBinary(base, num) {
  console.log(`Binary of ${num} is ${num.toString(2)}`);
  console.log(`The Base value - ${base} and  Power Value - ${num}`);
  //Every Number can be represented like below
  //3^19 == 3^1 * 3^2 * 3^16
  /**
   * 19 - 10011 - calculate Power for only Non zero Positions
   */
  let result = 1;
  let arr = [];
  while (num > 0) {
    console.log(`Current Num - > ${num}`);
    if (num % 2 != 0) {
      arr.unshift(1);
      console.log("Bit Pushed -----" + 1 + " Calculation Needed ");
      //This is the point where we need to calculate the Power
      console.log(`Result Was -> ${result} | Base Was - ${base}`);
      result = result * base;
      console.log(`Result is -> ${result}`);
    } else {
      arr.unshift(0);
      console.log("Bit Pushed -----" + 0 + " Calculation Ignored");
      //This is the Point where we do not need to compute power
    }
    //bUT EVERY TIME WE MOVE FROM LSB TO MSB WE MUST KEEP MULTIPLY THE ACTUAL NUMBER WITH ITSELF - because of Squre effect
    //To elaborate More 
    /**
     * 3^19 == 3^1 * 3^2 * 3^16
     * == 3^(2^0) * 3^(2^1) * 3^(2^4)
     * === 3^1 * 9^1 * 81^0  * 6561^0 * 43046721^1
     * //So we can see every time we move from LSB -> MSB we make 3 --> 3*3 --> 9*9 --> 81*81 --> 6561*6561
     */
    //num = Math.floor(num / 2);
    //We could have Use Binary Ops also - Faster
    num = num >> 1;
    console.log(` Base Was - ${base}`);
    base = base * base;
    console.log(` Base Is - ${base}`);
  }
  console.log(`The Binary data ---> ${arr}`);
  console.log(`The Result is ${result}`);
};

decimalToBuinaryManual(3, 19);
