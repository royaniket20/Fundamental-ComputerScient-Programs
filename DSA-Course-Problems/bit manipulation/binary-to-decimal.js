let binToDecimal = function (str) {
  let arr = str.split("");
  console.log(`Given Binary = ${arr}`);
  let result = 0;
  for (let index = arr.length - 1, pow = 0; index >= 0; index--, pow++) {
    let element = arr[index];
    result = result + element * Math.pow(2, pow);
  }
  console.log(`Rwsult is - ${result}`);
};
//Formula is 100 = 1*(2^2) + 0*(2^1) + 0*(2^0)

binToDecimal((236).toString(2));

//.We can do using Left Shift Operator also

let binToDecimalWithoutPow = function (str) {
  let arr = str.split("");
  console.log(`Given Binary = ${arr}`);
  let result = 0;
  let pow = 1;
  for (let index = arr.length - 1; index >= 0; index--) {
    let element = arr[index];
    console.log(pow);
    result = result + element * pow;
    pow = pow << 1; //Every time Right Shift happens - Its Multplied by 2
  }
  console.log(`Result is - ${result}`);
};
//Formula is 100 = 1*(2^2) + 0*(2^1) + 0*(2^0)

binToDecimalWithoutPow((236).toString(2));
