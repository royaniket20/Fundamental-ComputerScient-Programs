let method1 = function is_power_of_2(num) {
  let result = true;

  while (num != 1) {
    if (num % 2 === 0) {
    } else {
      result = false;
      break;
    }
    num = Math.floor(num / 2);
  }
  console.log(`The number ${num} is Power of 2 --- ? ${result}`);
};

method1(255);
method1(16);

//Big O(1) Solution
//Rule is - A Number which is Power of 2 Have only 1 set Bit

let method2 = function is_power_of_2_brain_algo(num) {
  let result = true;
  let temp = num;
  console.log(`The number is  ${num.toString(2)}`);

  //As per brain also to unset the lsb side set bit
  num = num & (num - 1);

  if (num != 0) {
    result = false;
  }

  console.log(`The number ${temp} is Power of 2 --- ? ${result}`);
};

method2(548745);
method2(16);
