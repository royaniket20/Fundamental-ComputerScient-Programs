let powerSetGenerator = function powerSetGenerator(str) {
  console.log(`The String is -- ${str}`);
  let finalResultSet = [];
  let length = str.length;
  let loopLendth = Math.pow(2, length); //We can Do 1<<length also to calculate Power
  //If a String have 3 characters - then total 2^3 Elements possible in Power Set
  for (let i = 0; i < loopLendth; i++) {
    let binaryData = i.toString(2).padStart(length, "0");
    console.log(binaryData);
    let result = [...str];
    for (let pos in binaryData) {
      // console.log(`The Position is ----> ${pos}  --Value --- ${binaryData.charAt(pos)}  `);
      if (binaryData.charAt(pos) == 0) {
        //We could have Do this also  binaryData & (1<<pos)
        result[pos] = "";
      }
    }
    console.log(result, "===>", result.join(""));
    finalResultSet.push(result.join(""));
  }
  console.log(`The array is -- ${finalResultSet}`);
};

powerSetGenerator("ABC");
