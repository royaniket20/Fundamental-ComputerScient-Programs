let num = 9;
console.log(
  `Number ${num} -> Binary Rep =                           ${num
    .toString(2)
    .padStart(32, "0")}`
);

//  2 's Complement Representation of Negative Number (num>>>0)
num = -9;
console.log(
  `Number ${num} -> Binary Rep [This is 2's Complement] = ${(
    num >>> 0
  ).toString(2)}`
);
//In case of Positive Number n>>>0 Have No effect
num = 9;
console.log(
  `Number ${num} -> Binary Rep [This is 2's Complement]  = ${(num >>> 0)
    .toString(2)
    .padStart(32, "0")}`
);

let base = 2;
let exp = 31;

let result = Math.pow(base, exp);
console.log(`Power Result = ${result}`);
result = 1 << exp;
console.log(`Power Result = ${result}`);
console.log(`Power Result [2's Complement] = ${result >>> 0}`);
