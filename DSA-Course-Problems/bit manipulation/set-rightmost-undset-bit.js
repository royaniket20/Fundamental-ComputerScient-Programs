let flipRightMostUnsetBit = function (num) {
  console.log(`Given Number - ${num.toString(2).padStart(16, "0")}`);
  let pos = 0;
  let temp = num;
  while (num > 0) {
    if ((num & 1) === 0) {
      break;
    }
    num = num >>> 1;
    pos++;
  }
  temp = temp | (1 << pos);
  console.log(`Result Number -${temp.toString(2).padStart(16, "0")}`);
};

flipRightMostUnsetBit(357);
