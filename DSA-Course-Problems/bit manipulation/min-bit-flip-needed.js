let minBitFliNeeded = function (num1, num2) {
  //We can use XOR operation to check this
  //If for a Certain Position Bit is same XOR will be Zero
  let resultCount = 0;
  while (num1 > 0 || num2 > 0) {
    console.log(
      `${num1.toString(2).padStart(32, "0")} |  ${num2
        .toString(2)
        .padStart(32, "0")} `
    );
    let checkLsb = (num1 & 1) ^ (num2 & 1);
    if (checkLsb === 1) {
      resultCount++;
    }
    num1 = num1 >>> 1; //Want to Ignore Sign Bit issue - So Doing >>> instead of >>
    num2 = num2 >>> 1;
  }
  console.log(`Minimum ${resultCount} bits are required to Make A -> B`);
};

/**
 * Another Approach is Calculate
 * (A XOR B), since 0 XOR 1 and 1 XOR 0 is equal to 1. So calculating the number of set bits in A XOR B will give us the count of the number of unmatching bits in A and B, which needs to be flipped.
 */

let minBitFliNeededEfficient = function (num1, num2) {
  //We can use XOR operation to check this
  //If for a Certain Position Bit is same XOR will be Zero
  let resultCount = 0;
  let num = num1 ^ num2;
  while (num > 0) {
    console.log(`${num.toString(2).padStart(32, "0")}`);
    let checkLsb = num & 1;
    if (checkLsb === 1) {
      resultCount++;
    }
    num = num >>> 1; //Want to Ignore Sign Bit issue - So Doing >>> instead of >>
  }
  console.log(`Minimum ${resultCount} bits are required to Make A -> B`);
};
/****This Problem can even be more efficient using Brain Carnginhum Formula 
N & (N-1) = wILL set the Left Most 1 Bit - to 0 . So in that way continue till the Number become zero
**/

minBitFliNeeded(10, 20);
minBitFliNeededEfficient(10, 20);
minBitFliNeeded(20, 25);
minBitFliNeededEfficient(20, 25);
