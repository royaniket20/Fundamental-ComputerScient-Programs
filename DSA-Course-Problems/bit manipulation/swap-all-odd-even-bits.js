//Given an unsigned integer N. The task is to swap all odd bits with even bits.

let swapBits = function (num) {
  console.log(`Original Number      ${num.toString(2).padStart(8, "0")}`);
  //0xAAAAAAAA means 10101010101010101010101010101010 in binary.
  //we get all even bits of n.
  let ev = num & parseInt("1010101010101010", 2);
  console.log(`Even Position Bits - ${ev.toString(2).padStart(8, "0")}`);

  //0x55555555 means 01010101010101010101010101010101 in binary.
  //we get all odd bits of n.
  let od = num & parseInt("0101010101010101", 2);
  console.log(`Odd Position Bits  - ${od.toString(2).padStart(8, "0")}`);

  //right Shifting the odd bits obtained previously.
  ev >>= 1;
  console.log(`Even Position Bits Now ${ev.toString(2).padStart(8, "0")}`);
  //left Shifting the odd bits obtained previously.
  od <<= 1;
  console.log(`Odd Position Bits Now  ${od.toString(2).padStart(8, "0")}`);

  //doing bitwise OR of even and odd bits obtained and
  //returning the final result.
  let res = ev | od;
  console.log(`${res.toString(2).padStart(8, "0")}`);
};

swapBits(23);
