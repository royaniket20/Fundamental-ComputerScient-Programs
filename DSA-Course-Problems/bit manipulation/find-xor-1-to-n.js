   /**
    * Starting Number is 0 Because 0^ N = N     
Num : 1 | Binary : 00000001 | XOR : 00000001 -- If N is 1 then Anyway XOR is 1 [0^1 = 1]
Num : 2 | Binary : 00000010 | XOR : 00000011 -- N% 4 ==2 =tHEN xor IS  n+1   
Num : 3 | Binary : 00000011 | XOR : 00000000 -- N% 4 ==3 =tHEN xor IS 0 
Num : 4 | Binary : 00000100 | XOR : 00000100 -- We get Same NUMBER ON Multiple of 4 
Num : 5 | Binary : 00000101 | XOR : 00000001 -- N% 4 ==1 =tHEN xor IS 1 
Num : 6 | Binary : 00000110 | XOR : 00000111 -- N% 4 ==2 =tHEN xor IS  n+1  
Num : 7 | Binary : 00000111 | XOR : 00000000 --  N% 4 ==3 =tHEN xor IS 0 
Num : 8 | Binary : 00001000 | XOR : 00001000  -- We get Same NUMBER ON Multiple of 4 
Num : 9 | Binary : 00001001 | XOR : 00000001  -- N% 4 ==1 =tHEN xor IS 1 
    * 
    * We can see if we try to Find XOR from 0 -> 4 => Actually it will be 
    * 
    *  */ 


let findXor = function(n){
    let result = 0;
   for (let index = 1; index <= n; index++) {
    result = result ^ index;
    console.log(`Num : ${index} | Binary : ${index.toString(2).padStart(8,"0")} | XOR : ${result.toString(2).padStart(8,"0")}`);

   }
   console.log(`ResuLT xor IS - ${result}`);
}

export  function findXorEfficient(n){
   if(n ===0 ) return 0;
   if(n ===1) return 1;
   let mod = n%4;
   if(mod ===0)return n;
   else if(mod ===3) return 0;
   else if (mod === 2 ) return n+1;
 
}

findXor(22);

console.log(`ResuLT xor IS - ${findXorEfficient(22)}`);