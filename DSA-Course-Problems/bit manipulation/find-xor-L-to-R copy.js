   /**
    * Using find-xor-1-to-n.js Approach = > 
    * First Find the Xor for 1 -> (L-1)
    * Then Find XOR for  1 -> R
    * 
    * then xor the respective answers again to get the xor of the elements from the range [L, R].
    *  This is because every element from the range [1, L – 1] will get XORed twice in the result resulting in a 0 
    * which when XORed with the elements of the range [L, R] will give the result.
    *  */ 

import {findXorEfficient as findXorEfficient1ToN} from './find-xor-1-to-n.js'
let findXorEfficientInRange = function(L,R){
 //fINDING xor FROM 1-> L-1
 let result1 =findXorEfficient1ToN(L-1);
 //finding XOR from 1->R
 let result2 = findXorEfficient1ToN(R);
 //nOW xOR ING THE RESULTS WILL CANCLE OUT dUPLICATES I.E 1 -> L-1
 let result = result1 ^ result2;
 return  result;
}



console.log(`ResuLT xor IN RANGE - ${findXorEfficientInRange(11,22)}`);