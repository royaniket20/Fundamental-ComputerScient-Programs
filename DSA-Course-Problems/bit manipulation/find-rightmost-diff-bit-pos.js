let findRightMostDiffBitPosition = function (num1, num2) {
  console.log(`Num1 Binary = ${(num1 >>> 0).toString(2).padStart(32, "0")}`);
  console.log(`Num2 Binary = ${(num2 >>> 0).toString(2).padStart(32, "0")}`);
  let position = 1;
  while (num1 != 0 && num2 != 0) {
    if ((num1 & 1) != (num2 & 1)) {
      break;
    } else {
      num1 = num1 >>> 1;
      num2 = num2 >>> 1;
      position++;
    }
  }
  console.log(`Rightmost Different Bit Position is ${position}`);
};

findRightMostDiffBitPosition(11, 9);

findRightMostDiffBitPosition(-11, 9);
