let findMaxANDPairNaive = function (array) {
let max =Number.MIN_SAFE_INTEGER;
console.log(`Give array - [${arr}]`);
  for (let index = 0; index < array.length-1; index++) {
  for (let innerIndex =0 ; innerIndex < array.length; innerIndex++) {
    if(index !=innerIndex)
    {
      console.log(`Array Pair - ${array[index]} | ${array[innerIndex]}`);
      let res = array[index] & array[innerIndex];
      max = Math.max(max,res);
      console.log(`All Time Max - ${max} | Current Max - ${res}`);
    }
  }
  
}
console.log(`Final Result - ${max}`);

}



let findMaxANDPairEfficient = function (arr) {
  console.log(`Give array - [${arr}]`);
  //First calculate Max Bit Count Required -------------------------------
  let heighestVal = arr.reduce((a, b) => Math.max(a, b), -Infinity); //Finding Max Element in Array - O(N)
  let digitCount = Math.floor(Math.log2(heighestVal)) + 1; //Max Digits required to represent in Binary 

  let binArr = arr.map((item) => item.toString(2).padStart(digitCount, "0"));
  console.log(`Give array In Binary - [${binArr}]`);
  let max = 0;//000
  for (let i = digitCount; i >0; i--) {
    console.log(`Check for Set Bit at Pos : ${i}`);
    let positionalSetCount = 0;

    let checkNumber = 1<<(i-1); //This is the Number bit Set to check the Position -- 00010
    checkNumber = max | checkNumber ; //We need to keep tracking previous Resultes also -Suppose already we have res = 10100 /So filter will be 10110 [00010 | 10100]
    console.log(`Check Number - ${checkNumber.toString(2).padStart(digitCount,'0')}`); //Printing One time for Reference 


       for (let j = 0; j < arr.length; j++) {
        let res = arr[j] & checkNumber;
        if(res === checkNumber){ //Filter criteria Matching | 10101010 & {filter} 10100000 == 10100000{filter}
          positionalSetCount++;
        }
       }
       if(positionalSetCount>=2){
        console.log(`Matching Filters ----- ${positionalSetCount}`);
        max = max | checkNumber ; //Updating the Filter for Next iteration
       }else{
        console.log(`No Matching Filter - Will discard this !!`);
       }

  }
  console.log(`Result Finally  = ${max.toString(2).padStart(digitCount, "0")} | ${max}`);
 
}

let arr = [1, 2, 3, 4, 5];
findMaxANDPairEfficient(arr);
findMaxANDPairNaive(arr);

arr = [345,546,345,213,789,543,236,875,432,890,4326,7890,4321];
findMaxANDPairEfficient(arr);