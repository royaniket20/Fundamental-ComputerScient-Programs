/**
 *
 * @param {*} arr
 * @param {*} num
 * @returns
 * Here idea is that use Bimnary search - Find the Mid of the array
 * if Mid is the matching with target then check mid-1 also once - If that is also matching
 * then mid is not the first occurance - then search on left array
 * if mid-1 != target then Mid is your result
 * Rest all are convensional Binary search
 */

let method1 = function firstOccuranceOfNum(arr, num) {
  let start = 0;
  let end = arr.length - 1;
  let result = -1;

  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    if (arr[mid] === num) {
      //check if its first occurance
      if (mid - 1 >= 0) {
        // to ensure Out of Bound Protection
        if (arr[mid - 1] === num) {
          //Mid is Not the First element
          end = mid - 1;
        } else {
          result = mid;
          break;
        }
      } else {
        result = mid;
        break;
      }
    } else if (arr[mid] > num) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }

  console.log(`The Position is - ${result}`);
  return result;
};

method1([10, 20, 30, 30, 30, 30, 40, 50], 30);
method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 77, 77, 77, 77, 99, 111, 333, 555, 777,
    888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  77
);

method1(
  [
    2, 4, 6, 7, 8, 9, 11, 11, 11, 11, 11, 11, 33, 55, 77, 99, 111, 333, 555,
    777, 888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  1112
);
method1([30, 30, 30, 30, 30, 30, 30, 50, 50, 50, 50], 50);

let method2 = function firstOccuranceOfNum(arr, num) {
  let start = 0;
  let end = arr.length - 1;
  let result = recursion(num, arr, start, end);

  console.log(`The Position is Recursive - ${result}`);
};

let recursion = function recursiveCall(num, arr, start, end) {
  let mid = Math.floor((start + end) / 2);
  if (start > end) {
    return -1;
  }

  if (arr[mid] === num) {
    //check if its first occurance
    if (mid - 1 >= 0) {
      // to ensure Out of Bound Protection
      if (arr[mid - 1] === num) {
        //Mid is Not the First element
        end = mid - 1;
        return recursiveCall(num, arr, start, end);
      } else {
        return mid;
      }
    } else {
      return mid;
    }
  } else if (arr[mid] > num) {
    end = mid - 1;
    return recursiveCall(num, arr, start, end);
  } else {
    start = mid + 1;
    return recursiveCall(num, arr, start, end);
  }
};

method2([30, 30, 30, 30, 30, 30, 30, 35, 35, 35], 35);
method2([10, 20, 30, 30, 30, 30, 40, 50], 30);
method2(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 77, 77, 77, 77, 99, 111, 333, 555, 777,
    888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  77
);

method2(
  [
    2, 4, 6, 7, 8, 9, 11, 11, 11, 11, 11, 11, 33, 55, 77, 99, 111, 333, 555,
    777, 888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  1112
);

function findLeftIndex(arr, num) {
  return method1(arr, num);
}

//export default { findLeftIndex };
export default { findLeftIndex };
