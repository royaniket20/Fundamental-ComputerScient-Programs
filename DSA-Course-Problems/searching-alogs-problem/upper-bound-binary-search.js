/**
 * When you cannot Find the elelment is search space - then Lower Bound will gO bEYOND THE SEARCH sPACE
 * sO if No element is present eventually the Lower Bound Will shift to  [last arr Index + 1] == Length of the array
 * This trick will be used for problem solving
 */
/**
 * Find the smallest Index in Shorted array such that  the number  at that Index is greater than  Give nNumber
 *[2,3,6,7,8,8,11,11,11,12]
 Upper Bound index is 3 for 6 , 
  Upper Bound index is 11 for 12 , 
   Upper Bound index is 1 for 0 , 
 * Here one trick is Even if we cannot find the Lower Bound  then also my hypothetical ans will be (last index + 1) of the array 
 * i.e array length 
 */

let findTheSmallestIndex = function (arr, num) {
  console.log(`Given array - ${arr} | target Number - ${num}`);
  let low = 0;
  let high = arr.length - 1;
  let result = arr.length;
  while (low <= high) {
    let mid = Math.floor((low + high) / 2);
    console.log(
      `The Current Start : [${low}] & end : [${high}] -- Mid - ${mid} | Result - ${result}`
    );
    //There is no need to check for Equal comparison 
    if (arr[mid] > num) {
      //There may be posibility of having a More smallest Index than Mid as Lower Bound
      result = mid; //Storing the result and go further left  for better Smaller Index
      high = mid - 1;
    } else {
      //Here the Given Number is Bigger  than Mid Index Number  - Lower Bound will ve More towards Right where array Have Bigger Numbers
      //
      low = mid + 1; //No need to store result because we have Not reached anything still Finding a Index to start with
    }
  }
  console.log(
    `The Final  Index of Upper Bound  : Result = ${result}`
  );
};

let arr = [1, 3, 5, 7, 9];
console.log("***********************");
findTheSmallestIndex(arr, 2);
console.log("***********************");
findTheSmallestIndex(arr, 5);
console.log("***********************");
findTheSmallestIndex(arr, 10);
console.log("***********************");
findTheSmallestIndex(arr, 0);
arr = [1, 3, 5, 7, 7, 7, 7, 7, 9];
console.log("***********************");
findTheSmallestIndex(arr, 7);
console.log("**************************--------");
arr = [1, 2, 8, 10, 11, 12, 19];
findTheSmallestIndex(arr, 0);
