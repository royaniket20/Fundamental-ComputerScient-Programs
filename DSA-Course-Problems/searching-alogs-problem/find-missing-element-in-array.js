//All elements appreas exactly Ones and one elleennt is missing
//Rules = 0 is always present in the array
//Rule- Only one element is missing
//if max element is is x then all elements from 0 to max element is present
//Efficient Sol shoud be solved in O(N) Time , O(1) Aux space & not changing original array
/**
 * First approach  - here For each element check if there is a repeted element in rest of the array
 */
let arr = [1, 2, 5, 8, 10, 9, 6, 4, 7];
let method0 = function findtheMissingElement(arr) {
  // O(N^2) Solution

  for (let i = 0; i < arr.length; i++) {
    let result = -1;
    for (let j = 0; j < arr.length; j++) {
      if (i + 1 === arr[j]) {
        result++;
      }
    }
    if (result == -1) {
      console.log("method0 : The Missing Element is = " + (i + 1));
      break;
    }
  }
};

method0(arr);

//Here we can do hashing to Find the result  -Use  Array with F  - then mark the ones which are present
let method1 = function findtheMissingElement(arr) {
  // O(N) Solution with O(N) space
  let result = -1;
  let booleanArr = new Array(arr.length + 1); //Because there is Only Non Zero elements 1--> n
  booleanArr.fill(false);
  console.log(`the boolean array - ${booleanArr}`);
  for (let i = 0; i < arr.length; i++) {
    booleanArr[arr[i]] = true;
  }

  for (let i = 1; i < booleanArr.length; i++) {
    if (booleanArr[i] === false) {
      console.log("method1 : The Missing  Element is - " + i);
      break;
    }
  }
};

method1(arr);
