let arr = [1, 2, 4, 4, 5];

//Here one element is Missing and One element is repeting More than Once
/**
 *  Some formula - 1+2+3+4+.. Upto N term = [n(n+1)]/2
 *   Sum of sqare of elements = [n*(n+1)*(2n+1)]/6
 * @param {*} arr
 *
 * Idea is First Sum all elements of array
 * Thren Sum first N elements
 * Do subs
 * So we can see that -----
 * [1,2,3,4,4,6]  -  [1,2,3,4,5,6]
 * --- So at left Side 4 will remain - That is Duplicate element
 * --- At Right Side 5 will remain - That is the missing element
 *   X - Y = result
 * Same way Find Out X^2 - Y^2 = some reasult
 * Now use Normal Formula to Calculate Vlayue
 */
let methodMath = function (arr) {
  console.log(`Given array - ${arr}`);
  let sumOfArr = 0;
  let sumOfSquareArr = 0;
  let max = -1;
  for (let index = 0; index < arr.length; index++) {
    sumOfArr += arr[index];
    sumOfSquareArr += arr[index] * arr[index];
    if (max < arr[index]) {
      max = arr[index];
    }
  }
  console.log(`Sum of elements of array - ${sumOfArr}`);
  console.log(`Sum of Square of elements of array - ${sumOfSquareArr}`);
  console.log(`Max element is - ${max}`);
  let sumOfNElements = Math.floor((max * (max + 1)) / 2);
  let sumOfNElementsSquares = Math.floor((max * (max + 1) * (2 * max + 1)) / 6);
  console.log(`Sum of all elements Upto ${max} is = ${sumOfNElements}`);
  console.log(
    `Sum of all elements^2 Upto ${max} is = ${sumOfNElementsSquares}`
  );
  let DuplicateMinusMissing = sumOfArr - sumOfNElements;
  console.log(` X = Duplicate | Y = Missing`);
  console.log(`X -Y = ${DuplicateMinusMissing}`);
  let DuplicateSqareMinusMissingSqare = sumOfSquareArr - sumOfNElementsSquares;
  console.log(`X^2 -Y^2 = ${DuplicateSqareMinusMissingSqare}`);
  let DuplicatePlusMissing = Math.floor(
    DuplicateSqareMinusMissingSqare / DuplicateMinusMissing
  );
  let Duplicate = Math.floor(
    (DuplicateMinusMissing + DuplicatePlusMissing) / 2
  );
  let Missing = DuplicatePlusMissing - Duplicate;
  console.log(`Missing element - ${Missing}`);
  console.log(`Duplicate element - ${Duplicate}`);
};
methodMath(arr);
