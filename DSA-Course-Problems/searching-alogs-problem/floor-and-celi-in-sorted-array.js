/**
 * Defination of Floor - largrest Num in the array which is <=X 
 * Defination of Celi - Smallest Num in the Array which is  >=X 
 [10,20,30,40,50]
 for Ip - 55 --> answer is None for Celi  so -1 ;
  for Ip - 55 --> answer is 50 for Floor ;

 */
let findCeli = function (arr, num) {
  console.log(`Given array - ${arr} |  target Num = ${num} `);
  console.log(`Finding Celi Value ---- `);
  let low = 0;
  let high = arr.length - 1;
  let result = -1;
  while (low <= high) {
    let mid = Math.floor((low + high) / 2);
    if (arr[mid] === num) {
      result = arr[mid];
      break;
    } else if (arr[mid] > num) {
      // We may be able to Find more smaller Num which can be Celi
      //But atleast Keep the Result handy
      result = arr[mid];
      high = mid - 1;
    } else {
      low = mid + 1;
    }
  }
  console.log(`Computed Celi value - ${result}`);
};

let findFloor = function (arr, num) {
  console.log(`Given array - ${arr} |  target Num = ${num} `);
  console.log(`Finding Floor Value ---- `);
  let low = 0;
  let high = arr.length - 1;
  let result = -1;
  while (low <= high) {
    let mid = Math.floor((low + high) / 2);
    if (arr[mid] === num) {
      result = arr[mid];
      break;
    } else if (arr[mid] < num) {
      // We may be able to Find more Larger Num  which can be Floor 
      //But atleast Keep the Result handy
      low = mid + 1;
      result = arr[mid];
    } else {
      high = mid - 1;
     
    }
  }
  console.log(`Computed Floor value - ${result}`);
};
let arr = [10, 20, 30, 40, 50];
findCeli(arr, 25);
findCeli(arr, 19);
findCeli(arr, 5);
findCeli(arr, 500);
findFloor(arr, 25);
findFloor(arr, 19);
findFloor(arr, 5);
findFloor(arr, 500);
