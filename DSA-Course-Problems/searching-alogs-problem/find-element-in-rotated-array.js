let method = function findElementinRotatedArray(arr, num) {
  //Idea is Find the Pivot point - the point at which - left element is smaller than
  //pivot in assending sorted rotated array

  //Try to check if the given element is present in sorted range- do general binary search
  let start = 0;
  let end = arr.length - 1;
  let result = resursiveBinarySearch(arr, start, end, num);
  console.log(`The Element  ${num} Found - arr[${result}] in Array ${arr}`);
};

let resursiveBinarySearch = function binarysearch(arr, start, end, number) {
  //Base case
  if (start > end) {
    return -1;
  }

  //Calculating Mid
  let mid = Math.floor((start + end) / 2);
  if (arr[mid] === number) {
    return mid;
  }
  //Finding which Half is sorted
  else if (arr[mid] > arr[start]) {
    //Left half sorted
    //See if the element is in range
    if (number >= arr[start] && number < arr[mid]) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  } else {
    //Right half is e Sorted [Imp]
    //See if element is in range
    if (number > arr[mid] && number <= arr[end]) {
      start = mid + 1;
    } else {
      end = mid - 1;
    }
  }
  return binarysearch(arr, start, end, number);
};

method([10, 20, 30, 40, 50, 8, 9], 30); //Array rotated counter clock wise 2 times

method([8, 9, 10, 20, 30, 40, 50], 30); //Fully rotated - so original array

method([100, 500, 10, 20, 30, 40, 50], 40);

method([100, 500, 10, 20, 30, 40, 50], 500);

method([100, 500, 10, 20, 30, 40, 50], 999);
