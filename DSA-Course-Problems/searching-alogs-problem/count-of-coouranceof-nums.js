/**
 * 
Naive solution is just iterating the array 
However you can Do one thing is Find laat occurance and first occurance as shown on other problem and then 
Do index Minus 

Or what you can do find the First occurance and do Iteration from that Point toward end until you get the last occurance 

 */
let method0 = function (arr, num) {
  let result = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === num) {
      result++;
    }
  }
  console.log(`The total Count of ${num} is ${result}`);
};
//Here it is O(N) time complexity

method0([10, 10, 10, 20, 30], 10);

method0([10, 10, 10, 20, 30], 50);

let method1 = function (arr, num) {
  let result = 0;
  let start = 0;
  let end = arr.length - 1;
  let index = -1;
  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    if (arr[mid] === num) {
      index = mid;
      break;
    } else if (arr[mid] > num) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }
  if (index > -1) {
    start = end = index;
    while (start >= 0 && arr[start] === num) {
      result++;
      start--;
    }
    while (end < arr.length && arr[end] === num) {
      result++;
      end++;
    }
    // As start and end Started from same index - 1 count is coming extra
    result--;
  }

  console.log(`The total Count of ${num} is ${result}`);
};

//Here it is O(K  + Log n) = O(N) as in Wrost case K can be N i.e - All elements same  time complexity

method1([10, 10, 10, 20, 30], 10);

method1([10, 10, 10, 20, 30], 50);

method1([10, 10, 10, 10, 10], 10);

method1([], 10);

// import findRightIndex from "./last-occurance-of-num.js";
// import findLeftIndex from "./first-occurance-of-num.js";

import { findRightIndex } from "./last-occurance-of-num.js";
import { findLeftIndex } from "./first-occurance-of-num.js";

let method2 = function (arr, num) {
  let leftIndex = findLeftIndex(arr, num);
  let rightIndex = findRightIndex(arr, num);
  console.log(`The Left Most Index = ${leftIndex}`);
  console.log(`The Right Most Index = ${rightIndex}`);
  let result = 0;
  if (rightIndex >= 0 && leftIndex >= 0) {
    result = rightIndex - leftIndex + 1;
  }
  console.log(`The total Count of ${num} is ${result}`);
};

method2([10, 10, 10, 20, 30], 10);

method2([10, 10, 10, 20, 30], 50);

method2([10, 10, 10, 10, 10], 10);

method2([], 10);
