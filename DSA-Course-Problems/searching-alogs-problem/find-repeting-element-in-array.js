//All elements appreas exactly Ones and one elleennt can appear more than 1 time
//Rules = 0 is always present in the array
//Rule- only one elelment is repeting
//if max element is is x then all elements from 0 to max element is present
//Efficient Sol shoud be solved in O(N) Time , O(1) Aux space & not changing original array
/**
 * First approach  - here For each element check if there is a repeted element in rest of the array
 */
let arr = [1, 2, 5, 3, 8, 3, 10, 9, 6, 4, 7];
let method0 = function findtheRepetingElement(arr) {
  // O(N^2) Solution
  let result = -1;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        result = arr[i];
        break;
      }
    }
    if (result != -1) {
      break;
    }
  }
  console.log("method0 : The Duplicate Element is - " + result);
};

method0(arr);

// Here we use a Boolean array of same size of the Original array 
//Now because we know that Max element is always less than array Size because oit has repeted element 
//We will use array element as Index in Boolean array - Start flagging them - Once we see we are revising the Node again 
//We will sya this is a repeted element 
let method1 = function findtheRepetingElement(arr) {
  // O(N) Solution with O(N) space
  let result = -1;
  let booleanArr = new Array(arr.length);
  booleanArr.fill(false);
  console.log(`the boolean array - ${booleanArr}`);
  for (let i = 0; i < arr.length; i++) {
    if (booleanArr[arr[i]] === true) {
      result = arr[i];
      break;
    } else {
      booleanArr[arr[i]] = true;
    }
  }
  console.log("method1 : The Duplicate Element is - " + result);
};

method1(arr);

// Here we sort the array and then check if there is an element which is same as next  element - then We break and get the element
//But here Original array is changed  
let method2 = function findtheRepetingElement(arr) {
  // O(NLog N) Solution
  let result = -1;
  arr.sort((a, b) => a - b);
  console.log(`Sorted array - ${arr}`);
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] === arr[i + 1]) {
      result = arr[i];
      break;
    }
  }
  console.log("method2 : The Duplicate Element is - " + result);
};

method2(arr);

// Now this is the Most efficient Solution 
let method3 = function findtheRepetingElement(arr) {
  // O(N) Solution with No modification to array
  let result = -1;
  //Both will start from Same Position
  let fastBullet = arr[0];
  let slowBullet = arr[0];
  //Start the Run
  //Phase 1 -- fast bullet goes are double speed of slow bullet
  console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  do {
    fastBullet = arr[arr[fastBullet]];
    slowBullet = arr[slowBullet];
    console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  } while (fastBullet != slowBullet);
  //Phase 2 - Now Put faster bullet - as is , Put slower bullte to initial Position
  slowBullet = arr[0];
  //Now they will go a same speed
  console.log("Will go no spame speed now on ....");
  console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);

  while (fastBullet != slowBullet) {
    fastBullet = arr[fastBullet];
    slowBullet = arr[slowBullet];
    console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  }
  result = fastBullet;
  console.log("method3 : The Duplicate Element is - " + result);
};

arr = [1, 3, 2, 4, 6, 5, 7, 3];
method3(arr);

// Special case when the Array is having the minimum element as 0
//Here will will always add 1 to the bullet traversal  to prevent Infinite Loop - During Final Result -1 done 
let method4 = function findtheRepetingElement(arr) {
  // O(N) Solution with No modification to array
  let result = -1;
  //Both will start from Same Position
  let fastBullet = arr[0] + 1;
  let slowBullet = arr[0] + 1;
  //Start the Run
  //Phase 1 -- fast bullet goes are double speed of slow bullet
  console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  do {
    //On movement 1 setp extra added to prevent infinite Loop
    fastBullet = arr[arr[fastBullet] + 1] + 1;
    slowBullet = arr[slowBullet] + 1;
    console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  } while (fastBullet != slowBullet);
  //Phase 2 - Now Put faster bullet - as is , Put slower bullte to initial Position
  slowBullet = arr[0] + 1;
  //Now they will go a same speed
  console.log("Will go no spame speed now on ....");
  console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);

  while (fastBullet != slowBullet) {
    //On movement 1 setp extra added to prevent infinite Loop
    fastBullet = arr[fastBullet] + 1;
    slowBullet = arr[slowBullet] + 1;
    console.log(`First Bullet - ${fastBullet} || Slow Bullet : ${slowBullet}`);
  }
  result = fastBullet - 1;
  console.log("method3 : The Duplicate Element is - " + result);
};

arr = [1, 0, 3, 2, 4, 6, 5, 7, 3]; //hERE THE SMALLEST ELEMENT IS 0
method4(arr);
