/**
 *One Solution is Find first occur and laast occure and do Index Diff 
 2nd Solution - we know in Sorted array 1 will come at last only if its a binary array 
 Then count from last toward first 
  
 * @param {*} arr 
 * @param {*} num 
 */
let method0 = function (arr, num) {
  let result = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === num) {
      result = i;
      break;
      //because Once we got first element We know rest of them will be 1
    }
  }
  console.log(`The total Count of ${num} is ${arr.length - result}`);
};
//Here it is O(N) time complexity

method0([0, 0, 0, 0, 1, 1, 1], 1);

method0([1, 1, 1, 1, 1, 1, 1, 1, 1], 1);

// import findRightIndex from "./last-occurance-of-num.js";
// import findLeftIndex from "./first-occurance-of-num.js";

const left = require("./first-occurance-of-num.js");

let method2 = function (arr, num) {
  let leftIndex = left.findLeftIndex(arr, num);
  console.log(`The Left Most Index = ${leftIndex}`);
  let result = 0;
  if (leftIndex >= 0) {
    result = arr.length - leftIndex;
  }
  console.log(`The total Count of ${num} is ${result}`);
};

method2([0, 0, 0, 0, 1, 1, 1], 1);

method2([1, 1, 1, 1, 1, 1, 1, 1, 1], 1);

method2([], 1);

method2([0,0,0,0,0,0], 1);
