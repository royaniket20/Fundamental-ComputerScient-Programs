let method1 = function recursiveBinarySearch(arr, num) {
  let start = 0;
  let end = arr.length - 1;
  console.log(`The Current Start : [${start}] & end : [${end}]`);
  let result = resursiveFunction(arr, start, end, num);
  console.log(`The Result Found at Index : ${result}`);
};

let resursiveFunction = function searhBinary(arr, start, end, num) {
  let mid = Math.floor((start + end) / 2);
  console.log(
    `The Current Start : [${start}] & end : [${end}] -- Mid - ${mid}`
  );

  if (start > end) {
    return -1; // We ran out of search space - Recursion breaking
  }

  if (arr[mid] === num) {
    return mid;
  } else if (arr[mid] > num) {
    end = mid - 1;
    return searhBinary(arr, start, end, num);
  } else {
    start = mid + 1;
    return searhBinary(arr, start, end, num);
  }
};

method1([10, 20, 30, 40, 50], 30);
method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 99, 111, 333, 555, 777, 888, 999, 1122,
    1133, 1144, 1166, 1188,
  ],
  77
);

method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 99, 111, 333, 555, 777, 888, 999, 1122,
    1133, 1144, 1166, 1188,
  ],
  88
);

// Time Complexity is always Log N base 2
//Space Complexity will be Lon N base 2
