let method1 = function iterativeBinarySearch(arr, num) {
  let result = -1; // In case we do not Find the Result
  let start = 0;
  let end = arr.length - 1;
  console.log(`The Current Start : [${start}] & end : [${end}]`);
  while (start <= end) {
    // Wait until qwe exhausted the Search space
    mid = Math.floor((start + end) / 2); // Finding out the mid position
    console.log(
      `The Current Start : [${start}] & end : [${end}] -- Mid - ${mid}`
    );
    if (arr[mid] === num) {
      result = mid; // exact Number is Found
      break;
    } else if (arr[mid] > num) {
      end = mid - 1; // shifting  the End Point to shorten search space
    } else {
      start = mid + 1; // Shifting the Start Point to shorten the search space
    }
  }

  console.log(`The Result Found at Index : ${result}`);
};

method1([10, 20, 30, 40, 50], 30);
method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 99, 111, 333, 555, 777, 888, 999, 1122,
    1133, 1144, 1166, 1188,
  ],
  77
);

method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 99, 111, 333, 555, 777, 888, 999, 1122,
    1133, 1144, 1166, 1188,
  ],
  88
);

// Time Complexity is always Log N base 2
//Space complexity is 1 