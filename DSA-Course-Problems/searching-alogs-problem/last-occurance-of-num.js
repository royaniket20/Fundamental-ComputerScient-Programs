/**
 * 
 * Simple solution iterate from last toward start

 */
let method0 = function lastOccuranceOfNum(arr, num) {
  let result = -1;
  for (let i = arr.length - 1; i >= 0; i--) {
    if (arr[i] === num) {
      result = i;
      break;
    }
  }
  console.log(`The Position is Simple  - ${result}`);
};

method0([10, 20, 30, 30, 30, 30, 40, 50], 30);
method0(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 77, 77, 77, 77, 99, 111, 333, 555, 777,
    888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  77
);

method0(
  [
    2, 4, 6, 7, 8, 9, 11, 11, 11, 11, 11, 11, 33, 55, 77, 99, 111, 333, 555,
    777, 888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  1112
);
method0([30, 30, 30, 30, 30, 30, 30, 50, 50, 50, 50], 50);
/**
 * Here idea is Simple once you Find the Middle Element which is matching target
 *  just try to Check if mid+1 is matching the target or Not 
If that is the case then mid is not the last occurance - we shoud look further on the Right array  
Else mid is the last occurance 
*/
let method1 = function lastOccuranceOfNum(arr, num) {
  let start = 0;
  let end = arr.length - 1;
  let result = -1;

  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    if (arr[mid] === num) {
      //check if its last occurance
      if (mid + 1 < arr.length) {
        // to ensure Out of Bound Protection
        if (arr[mid + 1] === num) {
          //Mid is Not the Last element
          start = mid + 1;
        } else {
          result = mid;
          break;
        }
      } else {
        result = mid; //because its last element anyway
        break;
      }
    } else if (arr[mid] > num) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }

  console.log(`The Position is - ${result}`);
  return result;
};

method1([10, 20, 30, 30, 30, 30, 40, 50], 30);
method1(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 77, 77, 77, 77, 99, 111, 333, 555, 777,
    888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  77
);

method1(
  [
    2, 4, 6, 7, 8, 9, 11, 11, 11, 11, 11, 11, 33, 55, 77, 99, 111, 333, 555,
    777, 888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  1112
);
method1([30, 30, 30, 30, 30, 30, 30, 50, 50, 50, 50], 50);

let method2 = function lastOccuranceOfNum(arr, num) {
  let start = 0;
  let end = arr.length - 1;
  let result = recursion(num, arr, start, end);

  console.log(`The Position is Recursive - ${result}`);
};

let recursion = function recursiveCall(num, arr, start, end) {
  let mid = Math.floor((start + end) / 2);
  if (start > end) {
    return -1;
  }

  if (arr[mid] === num) {
    //check if its last occurance
    if (mid + 1 < arr.length) {
      // to ensure Out of Bound Protection
      if (arr[mid + 1] === num) {
        //Mid is Not the Last element
        start = mid + 1;
        return recursiveCall(num, arr, start, end);
      } else {
        return mid;
      }
    } else {
      return mid; //because its last element anyway
    }
  } else if (arr[mid] > num) {
    end = mid - 1;
    return recursiveCall(num, arr, start, end);
  } else {
    start = mid + 1;
    return recursiveCall(num, arr, start, end);
  }
};

method2([30, 30, 30, 30, 30, 30, 30, 35, 35, 35], 35);
method2([10, 20, 30, 30, 30, 30, 40, 50], 30);
method2(
  [
    2, 4, 6, 7, 8, 9, 11, 33, 55, 77, 77, 77, 77, 77, 99, 111, 333, 555, 777,
    888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  77
);

method2(
  [
    2, 4, 6, 7, 8, 9, 11, 11, 11, 11, 11, 11, 33, 55, 77, 99, 111, 333, 555,
    777, 888, 999, 1122, 1133, 1144, 1166, 1188,
  ],
  1112
);

function findRightIndex(arr, num) {
  return method1(arr, num);
}
export default { findRightIndex };
