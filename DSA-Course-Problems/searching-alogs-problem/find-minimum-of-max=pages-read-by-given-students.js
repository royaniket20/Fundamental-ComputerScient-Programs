//Book shoud be read in such a way that max pages read by one studen can be minimized

let method0 = function minimumBookAllocation(arr, studentCount) {
  let result = -1;
  let totalPages = arr.reduce(
    (previousValue, currentValue) => previousValue + currentValue
  );
  let maxPages = arr.reduce((previousValue, currentValue) =>
    Math.max(previousValue, currentValue)
  );
  console.log(
    `The Total Number of pages - ${totalPages} - Max page Count-- ${maxPages}`
  );

  console.log(`The  range is ${maxPages} ---> ${totalPages}`);
  let start = maxPages;
  let end = totalPages;

  while (start <= end) {
    let midPoint = Math.floor((start + end) / 2);
    console.log(`Mid Point - ${midPoint}`);
    let minStudentRequired = checkAllocation(arr, midPoint);
    console.log(`Minimum Student Required to Read - ${minStudentRequired}`);
    if (minStudentRequired <= studentCount) {
      //Here minimum Number of studets required to read the Max midPoint Books pages  is less than Equal to the given student
      //We have scope of imporvement - we may  ant to reduce Max limit of page per student to se how many student required
      end = midPoint - 1;
    } else {
      //Already more than Require students required to red the pages -Lets Increate per student Page counts - so see if less setudent
      //can accomodate the book read
      start = midPoint + 1;
    }
    console.log(start, midPoint, end);
    result = midPoint;
  }

  console.log(
    `THE MINIMUM NUMBER OF PAGES MAX A STUDENT CAN READ IS - ${result}`
  );
};

checkAllocation = function (arr, midPoint) {
  let Count = 1;
  let pageRead = 0;
  console.log(arr);
  for (let i = 0; i < arr.length; i++) {
    if (pageRead + arr[i] > midPoint) {
      pageRead = arr[i];
      Count++;
    } else {
      pageRead = pageRead + arr[i];
    }
    console.log(pageRead, midPoint, Count);
  }
  return Count;
};

let arr = [10, 20, 10, 30];
method0(arr, 2);

arr = [10, 5, 20];
method0(arr, 2);
