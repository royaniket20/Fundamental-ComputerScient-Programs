let method0 = function findSqrt(number, precision) {
  console.log(`Actual SQRT of ${number} is --- ${Math.sqrt(number)}`);
  //Simple iterative solution - we know that in case sqrt is a fraction then num >= sqrt*sqrt [where sqrt is a whole integer]

  //Finding the integer part
  let x = 0;
  while (x * x <= number) {
    x++;
  }
  //As it has increased one point
  x--;
  console.log(`Integer part of the Sqrt is - ${x} for Number - ${number}`);
  //for precision part do iterative approach for each decimal Point
  let initialPrecision = 0.1;
  let initialWholeNumber = x;
  for (let i = 1; i <= precision; i++) {
    while (initialWholeNumber * initialWholeNumber < number) {
      initialWholeNumber = initialWholeNumber + initialPrecision; // Calculating the ith Position Post decimal
    }
    //As it has ncreased .1 Point extra
    initialWholeNumber = initialWholeNumber - initialPrecision;
    initialPrecision = initialPrecision / 10; //Now moving to Next decimal Point calculation --> .1 --> .01 --> .001 so on
  }
  console.log(
    `Whole part of the Sqrt is - ${initialWholeNumber} for Number - ${number}`
  );
};

method0(35, 9);

let method1 = function findSqrt(number, precision) {
  //We know SQRT Number lies betwwen 0 and that Number
  let initial = 0;
  let final = number;
  let initialWholeNumber = -1;
  while (initial <= final) {
    let mid = Math.floor((initial + final) / 2);
    if (mid * mid === number) {
      initialWholeNumber = mid; //Exact Thing Found
      break;
    } else if (mid * mid < number) {
      initialWholeNumber = mid; //Atleast this is the Number for sure  - It can be a bit whole number  more or some fraction
      initial = mid + 1; // Shortening the range
    } else {
      final = mid - 1; //Shortening the range
    }
  }
  console.log(
    `METHOD 1 : Integer part of the Sqrt is - ${initialWholeNumber} for Number - ${number}`
  );
  let initialPrecision = 0.1;
  for (let i = 1; i <= precision; i++) {
    while (initialWholeNumber * initialWholeNumber < number) {
      initialWholeNumber = initialWholeNumber + initialPrecision;
    }
    initialWholeNumber = initialWholeNumber - initialPrecision;
    initialPrecision = initialPrecision / 10;
  }
  console.log(
    `Whole part of the Sqrt is - ${initialWholeNumber} for Number - ${number}`
  );
};

method1(35, 9);
