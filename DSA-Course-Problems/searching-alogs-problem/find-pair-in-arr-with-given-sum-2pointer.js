let method0 = function findPairinArray(arr, num) {
  let result = [];
  console.log(`the given array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    for (j = i + 1; j < arr.length; j++) {
      //  console.log(`Current Pair - ${arr[i]} : ${arr[j]}`);
      if (arr[i] + arr[j] === num) {
        result.push("[" + arr[i] + "," + arr[j] + "]");
      }
    }
  }
  console.log(`All available Pairs with Sum - ${num} - ${result}`);
};

method0([2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3], 16);

//Two Pinter approach - Only in case of sorted array
method1 = function twoPointerApproach(arr, num) {
  console.log(`the given array - ${arr}`);
  let result = [];
  let checker = new Set();
  let start = 0;
  let end = arr.length - 1;
  while (start < end) {
    if (arr[start] + arr[end] === num && !checker.has(start + "-" + end)) {
      result.push("[" + arr[start] + "," + arr[end] + "]");
      checker.add(start + "-" + end);
    } else if (arr[start] + arr[end] > num) {
      end--;
    } else {
      start++;
    }
  }
  console.log(`All available Pairs with Sum - ${num} - ${result}`);
};

method1(
  [2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3].sort((a, b) => a - b),
  16
);
//Hashing in case of Unsorted array
method2 = function hashMapApproach(arr, num) {
  console.log(`the given array - ${arr}`);
  let result = [];
  let calculator = new Map();
  calculator.set(arr[0], 0);
  for (let i = 1; i < arr.length; i++) {
    let elementToFind = num - arr[i];
    console.log(calculator, elementToFind, calculator.has(elementToFind));
    if (calculator.has(elementToFind)) {
      result.push(
        "[" + arr[i] + "," + arr[calculator.get(elementToFind)] + "]"
      );
    } else {
      calculator.set(arr[i], i);
    }
  }
  console.log(`All available Pairs with Sum - ${num} - ${result}`);
};

method2([2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3], 16);
