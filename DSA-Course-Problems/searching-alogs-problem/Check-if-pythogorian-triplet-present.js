findPair = function twoPointerApproachtoFindPair(arr, num) {
  //console.log(`the given array** - ${arr}`);
  let result = [];
  let checker = new Set();
  let start = 0;
  let end = arr.length - 1;
  while (start < end) {
    if (arr[start] + arr[end] === num && !checker.has(start + "-" + end)) {
      result.push(
        "[" +
          Math.sqrt(arr[start]) +
          "," +
          Math.sqrt(arr[end]) +
          "," +
          Math.sqrt(num) +
          "]"
      );
      result.push(
        "[" +
          Math.sqrt(arr[end]) +
          "," +
          Math.sqrt(arr[start]) +
          "," +
          Math.sqrt(num) +
          "]"
      );
      checker.add(start + "-" + end);
    } else if (arr[start] + arr[end] > num) {
      end--;
    } else {
      start++;
    }
  }
  //console.log(`All available Pairs with Sum - ${num} - ${result}`);
  return result;
};

let method0 = function findPythogorianTriplet(arr) {
  console.log(`the given array - ${arr}`);

  let checker = new Set();
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      for (let k = j + 1; k < arr.length; k++) {
        let x = Math.pow(arr[i], 2);
        let y = Math.pow(arr[j], 2);
        let z = Math.pow(arr[k], 2);
        if (x + y === z || y + z === x || x + z === y) {
          if (x + y === z) {
            checker.add(arr[i] + "-" + arr[j] + "-" + arr[k]);
            checker.add(arr[j] + "-" + arr[i] + "-" + arr[k]);
          } else if (y + z === x) {
            checker.add(arr[j] + "-" + arr[k] + "-" + arr[i]);
            checker.add(arr[k] + "-" + arr[j] + "-" + arr[i]);
          } else {
            checker.add(arr[i] + "-" + arr[k] + "-" + arr[j]);
            checker.add(arr[k] + "-" + arr[i] + "-" + arr[j]);
          }
        }
      }
    }
  }
  console.log(
    `All available Pythogorian Triplet NON EFFICIENT - ${Array.from(checker)}`
  );
};

method0([2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3]);
method0([
  2, 6, 5, 481, 3, 9, 8, 2, 5, 40, 7, 3, 8, 4, 3, 41, 31, 12, 37, 35, 480,
]);

method1 = function findPythogorianTripletEfficient(arr) {
  console.log(`the given array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i] * arr[i];
  }
  console.log(`the modified array - ${arr}`);
  let result = [];
  for (let i = arr.length - 1; i >= 0; i--) {
    let pair = findPair(arr.slice(0, i), arr[i]);
    if (pair.length > 0) {
      result.push(...pair);
    }
  }
  console.log(
    `All available Pythogorian Triplet - ${Array.from(new Set(result))}`
  );
};

method1([2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3].sort((a, b) => a - b));
method1(
  [
    2, 6, 5, 481, 3, 9, 8, 2, 5, 40, 7, 3, 8, 4, 3, 41, 31, 12, 37, 35, 480,
  ].sort((a, b) => a - b)
);
