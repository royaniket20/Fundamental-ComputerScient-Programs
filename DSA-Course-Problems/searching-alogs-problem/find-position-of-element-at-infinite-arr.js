/**
 * If the Given number Present in array then Freturn Index of the array or
 * If its Not poresent Find the Insertion Position of thart
 * [1,2,3,4] ==> given number is 2 --> THEN it need to be inserted on Index 0
 * This problem is simply Lower Bound Problem
 * We need to Just find the Lower Bound and that't It - that will be the Position where the Element need to be inserted
 *
 * @param {*} arr
 * @param {*} num
 */

let method = function findinInfiniteArray(arr, num) {
  //Lets make an Infinite array as Finite search space
  console.log(`Need to Find the Position og Given Number - ${num}`);
  /**
 * First here we start from first 2 Index 0,1 
 * Then increase the range Twice every time until we get a valid range 
VVI - You now get a good computable range  
*/
  let start = 0;
  let end = 1;
  while (num > arr[end]) {
    start = end;
    end = end * 2;
  }
  console.log(`The Range is --- ${start} ------ ${end}`);
  //Now do Binary search on this range - It's just finding the Lower Bound problem
  //as we know Lower Bound is Nothing but the Index which have value >= target value
  /**
   * [1,2,3,4] --> for Input 2 we want to Return Index as 1 || Result will Look like [1,2,2,3,4]
   * [1,2,3,4] --> for Input 0 we want to Return Index as 1 || Result will Look like [0,1,2,3,4]
   * [1, 3, 5, 7, 7, 7, 7, 7, 9] -->
   * For Input 6 we want answer as 3 as Index || [1,3,5,6,7,7,...]
   * For Input 10 we want answer as 9 as Index || [...7,9,10,...]
   *  ALL THESE CAN BE ACHIEVED USING LOWER BOUND CODE
   */
  let result = end + 1;
  while (start <= end) {
    let mid = Math.floor((start + end) / 2);
    console.log(
      `The Current Start : [${start}] & end : [${end}] -- Mid - ${mid} | Result - ${result}`
    );
    if (arr[mid] === num) {
      result = mid; // We found the exact match - But there may be repeted element - Store It and go further for Better result
      end = mid - 1; //Because we want to Find Smallest Index Possible
    } else if (arr[mid] > num) {
      //There may be posibility of having a More smallest Index than Mid as Lower Bound
      result = mid; //Storing the result and go further left  for better Smaller Index
      end = mid - 1;
    } else {
      //Here the Given Number is Bigger  than Mid Index Number  - Lower Bound will ve More towards Right where array Have Bigger Numbers
      //
      start = mid + 1; //No need to store result because we have Not reached anything still Finding a Index to start with
    }
  }
  console.log(" Final anser of Index = " + result);
};

let counter = 1000;
let arr = [];
let incrementer = 1;
for (let i = 1; i <= counter; i++) {
  if (i % 10 === 0) incrementer++;
  arr.push(i * incrementer);
}
console.log(`The Infinite array Size - ${arr.length}`);

method(arr, 0);

method(arr, 1);

method(arr, 2);

method(arr, 323);

method(arr, 327);
method(arr, 329);
method(arr, 330);
method(arr, 335);
