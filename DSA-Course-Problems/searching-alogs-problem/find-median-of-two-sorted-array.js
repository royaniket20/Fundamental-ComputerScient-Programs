//Simple Solution is Very easy -
/**
 * Here we first add Two array to a single array
 * Then sort the array
 * Find the Mid of the array anf then Just do Avarage of It = Median of an Array
 * If Array size is Even  - Median will be avarage of two Mid element
 *
 */
let method0 = function findMedianOfTwoSortedArrayEasy(arr1, arr2) {
  console.log(`Array 1 - ${arr1}`);
  console.log(`Array 2 - ${arr2}`);
  let arr3 = [...arr1, ...arr2].sort((a, b) => a - b);
  console.log(`Sorted Array - ${arr3}`);
  let result = -1;
  if (arr3.length % 2 === 0) {
    let mid1 = Math.floor(arr3.length / 2);
    let mid2 = mid1 - 1;
    result = (arr3[mid1] + arr3[mid2]) / 2;
  } else {
    let mid = Math.floor(arr3.length / 2);
    result = arr3[mid];
  }
  console.log(`Final Median of two sorted array ----- ${result}`);
};

/**
 * Here we are Now going to Use efficient Solution
 * @param {*} arr1
 * @param {*} arr2
 */
let method1 = function findMedianOfTwoSortedArrayEfficient(arr1, arr2) {
  //Here first find Out which is the smaller array
  //We do the Binary search on Smaller array Only
  let smallerArray = arr1.length > arr2.length ? arr2 : arr1;
  let largerArray = arr1.length > arr2.length ? arr1 : arr2;
  console.log(`Smaller Array  - ${smallerArray}`);
  console.log(`Larger Array  - ${largerArray}`);
  //Do Binary search on Smaller array
  let result = -1;
  let start = 0;
  let end = smallerArray.length; //VERY IMPORTANT HERE END IS NOT LENGTH-1 BUT IT IS LENGTH
  while (start <= end) {
    let smallerMid = Math.floor((start + end) / 2);
    //THSI IS AN IMPORTANT FORMULA TO FIND LARGER ARRAY MID
    let largerMid =
      Math.floor((smallerArray.length + largerArray.length + 1) / 2) -
      smallerMid;
    console.log(
      `Current Smaller array Mid - ${smallerMid} | Larger Array Mid - ${largerMid}`
    );
    /**
     * Concept why to use MIN_SAFE_INTEGER on Max value - because - when Max value un avaialbe and we do Math.max() with this value the other available
     * value will always win - same case for the next line also
     */
    /**
     * Now we are going to maintain One Set with All smaller elements =  Left Set
     * One set with all the larger elements =  Right Set
     * Left Set --------------| Right Set --------------------------
     * [0------- smallerMid-1]| [smallerMid ---------------- smallarlength ]
     * [0------- largerMid-1] | [largerMid ---------------- largerlength ]
     */
    //Finding the Max value of Both Small and large Array  for the left set
    let leftSetSmallerArrayMax =
      smallerMid === 0 ? Number.MIN_SAFE_INTEGER : smallerArray[smallerMid - 1];
    let leftSetLargerArrayMax =
      largerMid === 0 ? Number.MIN_SAFE_INTEGER : largerArray[largerMid - 1];
    //Finding the Min value of Both Small array and large array  for right set
    let rightSetSmallerArrayMin =
      smallerMid === smallerArray.length
        ? Number.MAX_SAFE_INTEGER
        : smallerArray[smallerMid];
    let rightSetLargerArrayMin =
      largerMid === largerArray.length
        ? Number.MAX_SAFE_INTEGER
        : largerArray[largerMid];

    console.log(
      `Left Part Ending elements --  ${leftSetSmallerArrayMax} , ${leftSetLargerArrayMax}`
    );
    console.log(
      `Right Part Beginning Elements -- ${rightSetSmallerArrayMin} , ${rightSetLargerArrayMin}`
    );

    //Because the arrays are already sorted in nature - When we will compare to see if the Left set have some elements which are larger than Right Set
    //We do the Flip comparision - that means do comparison between Left set Small array vs Right Set Large array and Vice versa
    /**
     * Example of such a situation - 
     * Left Set -------------------- | Right Set -----------------
     *Small  ==[10,20]               | [30,40,50]
    *Larger ==[5,15,25,30,35]       | [55,65,75,85]
    Here  we Know in Same array there will never be a case that left part have larger element than Smaller but it can be the case for Cross array 
    Like you can see - Left Set Larger array has [35] which is larger than Right Set Smaller array [30] - Hence we do cross comparison  
     *  */

    if (
      rightSetSmallerArrayMin >= leftSetLargerArrayMax &&
      rightSetLargerArrayMin >= leftSetSmallerArrayMax
    ) {
      //If the above condition pass we can say that left set have all smaller elements than Right Set [For Both Large and Small array ]
      if ((smallerArray.length + largerArray.length) % 2 === 0) {
        //This is a even case - We have to puick element from smaller and larger array both
        //Getting the Min eleent of the Right Set
        let rightSetMinElement = Math.min(
          rightSetSmallerArrayMin,
          rightSetLargerArrayMin
        );
        //Getting the Max eleent of the Left  Set
        let leftSetMaxElement = Math.max(
          leftSetSmallerArrayMax,
          leftSetLargerArrayMax
        );
        //Do the avarage - that is Median of two sorted array
        result = (rightSetMinElement + leftSetMaxElement) / 2;
      } else {
        //Only pick emenet from smaller array - as it has extra element - because thats How we have partitioned it
        //Getting the Max eleent of the Left  Set
        result = Math.max(leftSetSmallerArrayMax, leftSetLargerArrayMax);
      }
      break; //No Further calculation

      //This is the case where we are trying to Shirnk the Start --> end by Binary Search
      /**
       * Suppose a Situation 
       * Here 35 > 30 --- We shoud do something to Move Smaller array [30] to left set 
       * Hence Move Mid pointer to Right 
    * Left Set -------------------- | Right Set -----------------
    *Small  ==[10,20]               | [30,40,50]
    *Larger ==[5,15,25,30,35]       | [55,65,75,85]
       
    Suppose the Situation -- 
    Here  40 > 36 So here we need to do something to move 40 to Right set
    Hence we need to Move Mid pointer toward left 
    * Left Set -------------------- | Right Set -----------------
    *Small  ==[10,20,40]            | [42,45,50]
    *Larger ==[5,15,25,30,35]       | [36,65,75,85]

    * Oviously we want to Move the Small Array Index towards Right so that  - Right set do not contains element smaller [30] that Right Set [35]
    * So we do Binary search on Right Set Small array else Do on Left Set Small array 
    * Larger array Index will get calculated by formula shown above 
       * 
       */
      // In Case smaller array Right Set have smaller element than the Left  Set [Check with Large array as oviously array themshelves are sorted - so no possibility on smaller array itself]
      // - Do Bin search on Right part of small array 
    } else if (smallerArray[smallerMid] <= largerArray[largerMid-1]) {
      //Find next smaller mid on right side
      start = smallerMid + 1;
        // - Do Bin search on Left  part of small array 
    } else if (smallerArray[smallerMid-1] >= largerArray[largerMid]) {
      //Find next smaller mid on left side
      end = smallerMid - 1;
    }
  }
  console.log(`Final Median of two sorted array Efficient ----- ${result}`);
};

// let arr1 = [10, 20, 30, 40, 50];
// let arr2 = [5, 15, 25, 30, 35, 55, 65, 75, 85];
// method0(arr1, arr2);
// method1(arr1, arr2);
// arr1 = [30, 40, 50, 60];
// arr2 = [5, 6, 7, 8, 9];
// method0(arr1, arr2);
// method1(arr1, arr2);
let arr1 = [30, 40, 50, 60, 65, 99, 888];
let arr2 = [3, 55];
method0(arr1, arr2);
console.log(`Efficient Sol ---------------S T A R T----------------->`);
method1(arr1, arr2);
console.log(`Efficient Sol ------------------E N D-------------->`);
arr1 = [30, 40, 50, 60, 65, 99, 888];
arr2 = [55, 999];
method0(arr1, arr2);
console.log(`Efficient Sol ---------------S T A R T----------------->`);
method1(arr1, arr2);
console.log(`Efficient Sol ------------------E N D-------------->`);
