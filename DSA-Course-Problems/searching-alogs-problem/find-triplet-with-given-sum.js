let findPair = function twoPointerApproachtoFindPair(arr, num, initialNum) {
  //console.log(`the given array - ${arr}`);
  let result = [];
  let checker = new Set();
  let start = 0;
  let end = arr.length - 1;
  while (start < end) {
    if (arr[start] + arr[end] === num && !checker.has(start + "-" + end)) {
      result.push("[" + initialNum + "," + arr[start] + "," + arr[end] + "]");
      checker.add(start + "-" + end);
    } else if (arr[start] + arr[end] > num) {
      end--;
    } else {
      start++;
    }
  }
  //console.log(`All available Pairs with Sum - ${num} - ${result}`);
  return result;
};

let method0 = function findtripletwithGivenSumIterative(arr, sum) {
  console.log(`the given array - ${arr}`);
  let result = [];
  let checker = new Set();
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      for (let k = j + 1; k < arr.length; k++) {
        if (
          arr[i] + arr[j] + arr[k] === sum &&
          !checker.has(arr[i] + "-" + arr[j] + "-" + arr[k])
        ) {
          result.push(arr[i] + "-" + arr[j] + "-" + arr[k]);
          checker.add(arr[i] + "-" + arr[j] + "-" + arr[k]);
        }
      }
    }
  }
  console.log(`All available Pairs with Sum - ${sum} - ${result}`);
};

method0(
  [2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3].sort((a, b) => a - b),
  20
);

method1 = function findtripletwithGivenSumWithBinary(arr, sum) {
  console.log(`the given array - ${arr}`);
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    let pair = findPair(arr.slice(i + 1), sum - arr[i], arr[i]);
    if (pair.length > 0) {
      result.push(...pair);
    }
  }
  console.log(
    `All available Pairs with Sum - ${sum} - ${Array.from(new Set(result))}`
  );
};

method1(
  [2, 6, 5, 3, 9, 8, 2, 5, 7, 3, 8, 4, 3].sort((a, b) => a - b),
  20
);
