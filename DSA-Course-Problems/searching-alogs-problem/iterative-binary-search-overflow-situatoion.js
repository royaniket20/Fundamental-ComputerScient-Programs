//Suppose Aray is Too Big - INT MAX length and the Number you are try to Find is at End side
//So in that Case Mind Poinjt Calculation may lead to Over flow

// low + high ==> Will lead to Over Flow situation -
/*
In these case it is better to use an Alternative to calculate Mid 
mid = low + (high-low)/2 [VERY IMPORTANT ]
*/

let method1 = function iterativeBinarySearch(arr, num) {
  let result = -1; // In case we do not Find the Result
  let start = 0;
  let end = arr.length - 1;
  console.log(`The Current Start : [${start}] & end : [${end}]`);
  while (start <= end) {
    // Wait until we  exhausted the Search space
    let mid = Math.floor(start + (end - start) / 2); // Finding out the mid position - May lead to Overflow Use alternative Formula
    console.log(
      `The Current Start : [${start}] & end : [${end}] -- Mid - ${mid}`
    );
    if (arr[mid] === num) {
      result = mid; // exact Number is Found
      break;
    } else if (arr[mid] > num) {
      end = mid - 1; // shifting  the End Point to shorten search space
    } else {
      start = mid + 1; // Shifting the Start Point to shorten the search space
    }
  }

  console.log(`The Result Found at Index : ${result} | Value - ${arr[result]}`);
};
let bigNum = 999999;
console.log(`Big Number - ${bigNum}`);
let arr = new Array(bigNum);
for (let index = 1; index <= bigNum; index++) {
  arr.push(index);
}
console.log(`Array is ready - Calling the actual Binary Search `);
method1(arr, bigNum);

// Time Complexity is always Log N base 2
//Space complexity is 1
