//A peak element in an array is such element whose neighbours are smaller than it
//[5,10,20,15,7] == Here the Peak element is 20
//[10,20,15,5,23,90,67] -- Here peak element is 20/90
//For edge element only check for One side neighbour smaller or Not
//[80,70,90] - peak element id 80/90

let method1 = function iterativeSol(arr) {
  let result = [];
  if (arr.length === 0) {
    result.push(-1);
  } else if (arr.length === 1) {
    result.push(arr[0]);
  } else if (arr.length === 2) {
    if (arr[0] > arr[1]) {
      result.push(arr[0]);
    }
    if (arr[0] < arr[1]) {
      result.push(arr[1]);
    }
  } else {
    for (let i = 1; i < arr.length - 1; i++) {
      if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) result.push(arr[i]);
    }
    //Corenr cases
    if (arr[0] > arr[1]) {
      result.push(arr[0]);
    }
    if (arr[arr.length - 1] > arr[arr.length - 2]) {
      result.push(arr[arr.length - 1]);
    }
  }

  console.log(`The peak elements in Array : ${arr} are ${result}`);
};

method1([5, 10, 20, 15, 7]);
method1([10, 20, 15, 5, 23, 90, 67]);
method1([80, 70, 90]);
method1([80, 770, 90]);
method1([5, 7]);
method1([5]);
method1([]);
//TODO - Pe
//We can use Binary search also - But one of the element will be missed [IMP]
//[120,80,40,30,20,50,60]
//[5,20,40,30,20,50,60]

let method2 = function findPeakBinary(arr) {
  let start = 0;
  let end = arr.length - 1;
  let result = -1;
  if (arr.length === 0) {
    result = -1;
  } else {
    while (start <= end) {
      let mid = Math.floor((start + end) / 2);
      //console.log(start, mid, end);
      //Check Mid is peak or Not
      //If mid is edge case - on that side no neighbour to check
      if (
        (mid === 0 || arr[mid] > arr[mid - 1]) && // checking the left Neighbour
        (mid === arr.length - 1 || arr[mid] > arr[mid + 1])
      ) {
        // checking the right neighbour
        result = arr[mid];
        break;
      } else if (mid > 0 && arr[mid - 1] > arr[mid]) {
        //there are more elements left to mid and mid is smaller to left neighbour
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }
  }
  console.log(` METHOD2 The peak elements in Array : ${arr} are ${result}`);
};

method2([5, 10, 20, 15, 7]);
method2([10, 20, 15, 5, 23, 90, 67]);
method2([80, 70, 90]);
method2([80, 770, 90]);
method2([5, 7]);
method2([5]);
method2([]);
