let method = function firstnNaturalNumsRecursion(num) {
  if (num == 0) return num;

  return num + firstnNaturalNumsRecursion(num - 1);
};

console.log(`Sum of N natural Numbers === ${method(5)} `);

//Time complexity is N
//Space cdomplexity is N 
//T(N) = T(N-1) + C


//Paremeterized Way 

let method1 = function(n , sum){
  if(n===0){
    console.log("Sum in Parameterized way  is "+ sum);
    return;
  }
  sum = sum+n;
  method1(n-1,sum)
}

method1(3,0);

