/**
 * 
A digit string is good if the digits (0-indexed) at even indices are even and the digits at odd indices are prime (2, 3, 5, or 7).

For example, "2582" is good because the digits (2 and 8) at even positions are even and the digits (5 and 2) at odd positions are prime. However, "3245" is not good because 3 is at an even index but is not even.
Given an integer n, return the total number of good digit strings of length n. Since the answer may be large, return it modulo 109 + 7.

A digit string is a string consisting of digits 0 through 9 that may contain leading zeros.

 */


 //Here we know that Possible Digits are  0 - 9 
 //Also we know - Even Digits = 0,2,4,6,8 - 5 count
 //Also we know Primt Digits = 1,3,5,7   - 4 count 
 
 /**'
  * Suppose you have a Digit count = 4 
  * 
  * - - - -
  * For [0] , [2] - We can Put Only Even Digits 
  * For [1] , [3] - We can Only Put Prime Digits 
  * 
  * So total Permutation possible - 5 possiblities * 4 Possibilities *  5 possiblities * 4 Possibilities
  * 1 <= n <= 10^15
  * 
  * Special Note - In most platforms  O(n) only acceptable if N is withing range of 10^7 else try Find Log N solution 
  */


let coutGoodDigitsInterative = function (lengthOfGoodNumber){
    console.log(`Count of Digits Given - ${lengthOfGoodNumber}`);
      let result = 1 ; 
      for (let index = 0; index < lengthOfGoodNumber; index++) {
         if(index%2 === 0 ){
          result = (result*5);
         }else{
          result = (result*4);
         }
         result = result % 1000000007
      }
      console.log(`Total Count of Good Numbers - ${result}`);
}

coutGoodDigitsInterative(50);

//Using Modulo with Large Numbers 

// Use Efficient Power of Recursive Appraoch - To Get Powr in Log Time with Modulo Also 
// x^n = x^[n/2] * x^[n/2] //If Even 
// x^n = x^[n/2] * x^[n/2] * x //If Odd
/**
 * 
The idea is based on below properties.
Property 1: 
(m * n) % p has a very interesting property: 
(m * n) % p =((m % p) * (n % p)) % p
Property 2: 
if b is even: 
(a ^ b) % c = ((a ^ b/2) * (a ^ b/2))%c ? this suggests divide and conquer 
if b is odd: 
(a ^ b) % c = (a * (a ^( b-1))%c
Property 3: 
If we have to return the mod of a negative number x whose absolute value is less than y: 
then (x + y) % y will do the trick
Note: 
Also as the product of (a ^ b/2) * (a ^ b/2) and a * (a ^( b-1) may cause overflow, hence we must be careful about those scenarios 
 */
let moduloPow = function (x , n , mod){
     if(n === 0 ){
      return 1;
     }
     let powData = Math.floor(n/2);
     let temp = moduloPow(x ,powData,mod );
     temp = ((temp% mod)*(temp% mod))% mod;

     if(n%2 ===0){
     return temp;
     }else{
      return  ((x% mod)*(temp% mod))% mod;
     }
}

console.log(`Test Modulo Power - ${moduloPow(5,25,1000000007)} | ${Math.pow(5,25) % 1000000007}`);
console.log(`Test Modulo Power - ${moduloPow(4,25,1000000007)} | ${Math.pow(4,25) % 1000000007}`);


//Count of Good Digits using Modulo 
//Suppose the Index is 0 based 
// So in a Big Number - Even Places - Math.floor(n/2) + n%2
//                      Odd Places - Math.floor(n/2)
//- Cout of Digit = 5 [_ _ _ _ _] - Even Places = 2 + 1
//                    [0 1 2 3 4] - Odd Places =  2 
let coutGoodDigitsEfficient = function (lengthOfGoodNumber){
  let evenPlaces = Math.floor(lengthOfGoodNumber/2) + lengthOfGoodNumber%2;
  let oddPlaces = Math.floor(lengthOfGoodNumber/2);
  console.log(`Count of Digits Given - ${lengthOfGoodNumber}`);
  console.log(`Number of Even Places - ${evenPlaces} | Odd Places - ${oddPlaces}`);
  let evenPermutations =   moduloPow(5,evenPlaces,1000000007) ;
  let oddPermutations =   moduloPow(4,oddPlaces,1000000007) ;
  console.log(evenPermutations , oddPermutations);
   let result = (
    evenPermutations
    *
    oddPermutations
  )  % 1000000007
  console.log(`Total Count of Good Numbers - ${result}`);
}

coutGoodDigitsEfficient(50)






