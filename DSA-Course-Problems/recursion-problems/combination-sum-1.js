/**
 * Intution is - here we need to Pic elements from array which will lead to Sum
 * Also we may need to Pick element multiple times if needed
 * Will start from 0th Index
 * Will carry an Empty array to keep track   
 */

let combinationSum = function (arr, index, sum, initArr) {
    console.log(`Calling with Index - ${index} | Remaining Sum - ${sum} | initArray [${initArr}]`);
    //Base condition
    //When you have exhausted all the Idex of given actual data array 
    if (arr.length === index) {
    if (sum === 0) {
            console.log(`Got one Condition - [${initArr}]`);   
    }
    return;
}
 //RECURSION CALL 1 - LEFT RECURSION
    //Pick the element is its Less than Remaining Sum 
    //Sience I can pick same eelement n number of times I need not to Increase Index  
    if (arr[index] <= sum) {
        initArr.push(arr[index]);
        combinationSum(arr, index, sum - arr[index], initArr);
        initArr.pop();
    }
     //RECURSION CALL 2 - RIGHT  RECURSION
    //This is the Line where qwe are No picking the element of index postion - So going to Next Postion 
    //As we are not picking up anything - nothing is deducted from Sum 
    //Here we are saying that we are Not interested on index element - so we are moving to the Next element
       combinationSum(arr, index + 1, sum , initArr);

}

combinationSum([2,3,6,7] , 0,7,[]);