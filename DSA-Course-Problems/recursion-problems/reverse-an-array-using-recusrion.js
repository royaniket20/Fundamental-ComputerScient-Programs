//Two Pinter Approach 

let reverseArray = function(arr , start , end){
    if(start>end){
        console.log(`Reversed Array - [ ${arr}]`);
        return;
    }
    //Actual swaping 
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
    reverseArray(arr , start , end)

}


let arr = [1,2,3,4,5,6,7,8,9];
let start = 0;
let end = arr.length-1;
console.log(`Original Array - [${arr}]`);
reverseArray(arr , start , end);


 arr = [11,22,33,44,55,66];
 start = 0;
 end = arr.length-1;
console.log(`Original Array - [${arr}]`);
reverseArray(arr , start , end);

console.log("****************************");
//Doing using Single Pointer 
/**
 * Here idea is for Index i we actually Swap n-1-i position Data 
 * 1,2,3,4,5 = So here N = 5 //length of array 
 * I = 0
 * N-1-I = 4
 * ... so on ...
 * 
 */

let reverseArraySinglePointer = function(arr , start ){
    if(start> Math.floor(arr.length/2)-1){
        console.log(`Reversed Array Single POINTER - [ ${arr}]`);
        return;
    }
    //Actual swaping 
    let temp = arr[start];
    let end = arr.length-1-start;
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    reverseArraySinglePointer(arr , start )

}


 arr = [1,2,3,4,5,6,7,8];
 start = 0;
console.log(`Original Array Single Pointer - [${arr}]`);
reverseArraySinglePointer(arr , start );
