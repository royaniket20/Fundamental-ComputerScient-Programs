/**
 * Here for each iteration 1 person get killed
 * Eventually One person is left behind - that is going to be base case for termination the recursion
 *
 * But remember that every time we are Not starting from Oth Postion for killing
 * We ar Shifting the Index  - Based on Pistol Position
 * //Check  ipad Note for more details
 */

let method = function josephusProblem(n, k) {
  console.log(`Calling with Alive Person Count  - ${n} , Kill Pos - ${k}`);
  if (n === 1) {
    //Here pnly 1 person is present - then Thats termination
    return 0;
  }
    /**
     * Explanation -
     * When eventually we reduce this to 1 person - then lastStartPos will be 0
     */
    let lastStartPos = josephusProblem(n - 1, k);
    //On Return For Last pos 0 -> Next Pos will be  0+k , for that it will be (0+K)+K ... so on ...
    let nextStartPos = lastStartPos + k;
    //But the problem is nextStartPos can go beyond the allowed Index  [0,1,2,3... n-1] //Orginal Indexes
    nextStartPos = nextStartPos % n;
    console.log(
      `Last Start Pos : ${lastStartPos} | Next Start Pos - ${nextStartPos}`
    );
    return nextStartPos;
  
};

console.log(`The result is ---- ${method(4, 3)}`);

console.log(`The result is ---- ${method(7, 3)}`);

console.log(`The result is ---- ${method(40, 7)}`);