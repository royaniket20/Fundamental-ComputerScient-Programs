/**
 * You will be given Length of Str = N 
 * now for each Position either it can be 0 nd 1 
 * For each Postion - Try 0 once and Try 1 Once keeping all other Intact 
 *  */ 

/**
 * NOW TO PREVENT CONSECUTIVE 1 - THE INTUTION IS THAT 
 * KILL THE RECURSION BRANCH WHERE YOU AE GOING TO PUT 1 ON A VALUE WHERE LAST VALUE IS ALREADY 1
 */


let printBinaryStr = function(length , initalIndex ,arr){
   //console.log(length , initalIndex , arr);
    if(initalIndex == length){
        //Base case is reached - String is Formed 
        //Print the Binary Str 
        console.log(`Item is - ${arr}`);
        return;
    }

    //one Recursion With Putting 0 Value on Current Index
    arr.push(0);
    printBinaryStr(length,initalIndex+1,arr)
    //Resetting the array 
    arr.pop(0);
    //Another Recursion by Putting 1 Value to Current Index 
    //Need to Put specia Check to Ensure if this Recursion Tree is allowed or Not
    let lastDigit = arr[arr.length-1];
    if(lastDigit === 1) // No need to Do this Branch as it will lead to consecutive one 
   {
     console.log(`Skipping the Branch - ${arr} for Appending 1`);
   }else {
    arr.push(1);
    printBinaryStr(length,initalIndex+1,arr)
    //Resetting the array 
    arr.pop(1);
   }
   
}


let arr  = []
printBinaryStr(4,0,arr)