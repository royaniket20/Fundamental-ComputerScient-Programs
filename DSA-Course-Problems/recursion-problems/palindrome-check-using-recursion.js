let method = function checkPalindrome(str, start, end) {
  console.log(start + "-----" + end);
  if (
    str.substring(start, end).length === 0 ||
    str.substring(start, end).length === 1
  ) {
    //Even String --- Like cabbac --Then Only check Till EMPTY STRING
    //Even String --- Like abbcbba --Then Only check Till c
    console.log(`remaining String - ${str.substring(start, end)}`);
    return true;
  }
  if (str.charAt(start) === str.charAt(end)) {
    start = start + 1;
    end = end - 1;
    return checkPalindrome(str, start, end);
  } else {
    return false;
  }
};

console.log(`The Palindrome Check --- ${method("abbcbba", 0, 6)}`);
console.log(`The Palindrome Check --- ${method("cabbac", 0, 5)}`);

console.log(`The Palindrome Check --- ${method("adbcbba", 0, 6)}`);
console.log(`The Palindrome Check --- ${method("cfbbac", 0, 5)}`);

//Recurrance Sol T(N) = T(N-2) + C
//Time compolexity - O(N) , Space Complexity = O(N)
