/**
 * The below Recursive Call will Calculate Log Value in Floor Mode
 * 16->31 For Base 2 It will Give 4 as resule '
 * For 32 - It will give value as 5 for base 2
 */

let findFloorOfLogBasex = function (num, base) {
  //Base Condition
  if (num < base) {
    console.log("Base Care reached *** " + num);
    return 0;
  }
  return 1 + findFloorOfLogBasex(Math.floor(num / base), base);
};

for (let index = 16; index <= 32; index++) {
  let result = findFloorOfLogBasex(index, 2);

  console.log(`The Result for ${index} Log Base 2 Floor Value is -- ${result}`);
}
