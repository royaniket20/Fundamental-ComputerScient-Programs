/**
 * You will be given Length of Str = N 
 * now for each Position either it can be 0 nd 1 
 * For each Postion - Try 0 once and Try 1 Once keeping all other Intact 
 *  */ 


let printBinaryStr = function(length , initalIndex ,arr){
   //console.log(length , initalIndex , arr);
    if(initalIndex == length){
        //Base case is reached - String is Formed 
        //Print the Binary Str 
        console.log(`Item is - ${arr}`);
        return;
    }

    //one Recursion With Putting 0 Value on Current Index
    arr.push(0);
    printBinaryStr(length,initalIndex+1,arr)
    //Resetting the array 
    arr.pop(0);
    //Another Recursion by Putting 1 Value to Current Index 
    arr.push(1);
    printBinaryStr(length,initalIndex+1,arr)
    //Resetting the array 
    arr.pop(1);
}


let arr  = []
printBinaryStr(4,0,arr)