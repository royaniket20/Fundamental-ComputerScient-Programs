/**
Subsequence can be contigiour or Non contigious - But they will be in Sequence for sure 

[1,2,3] ==> 1,2,3,12,23,13 all are Subsequence 
Subarray can be a subsequence also 
*/
/*
For all index We have do to Logic of Taking that Index and Not taking the Index - But only print with matching Given Sum 
*/
let printAllSubsequenceUsingRecursionWithSum = function (
  arr,
  index,
  tempArr,
  currentSum,
  target
) {
  if (index === arr.length) {
    //Base condition when
    if (currentSum === target) {
      console.log(`Combination : ${tempArr}`);
    }
    return;
  }
  //Picking ith element
  tempArr.push(arr[index]);
  currentSum = currentSum + arr[index];
  printAllSubsequenceUsingRecursionWithSum(
    arr,
    index + 1,
    tempArr,
    currentSum,
    target
  );
  //Not picking ith element
  tempArr.pop();
  currentSum = currentSum - arr[index];
  printAllSubsequenceUsingRecursionWithSum(
    arr,
    index + 1,
    tempArr,
    currentSum,
    target
  );
};
let tempArr = []; //This for calculating the sub sequences
let target = 7;
let currentSum = 0;
printAllSubsequenceUsingRecursionWithSum(
  [1, 2, 4, 3, 5, 7],
  0,
  tempArr,
  currentSum,
  target
);

/**
 * Time Complexity : 
 * 
 For each element we have Two Choice - Not take and take 
 So 2^n ways to Do it  - This is a time complexity 
 Space complexity is n 
 
 */
console.log(
  ` Print Only One Subsequebnce - Here we need to kill recursion tree as we got the result`
);
//Here intution is base case shoud return true if the base case get achieved else return false;
//So we wikk keep calling recursive Functions until we get a true result
let printAllSubsequenceUsingRecursionWithSumAtLeast = function (
  arr,
  index,
  tempArr,
  currentSum,
  target
) {
  if (index === arr.length) {
    //Base condition when
    if (currentSum === target) {
      console.log(`Combination : ${tempArr}`);
      return true;
    }
    return false;
  }
  //Picking ith element
  tempArr.push(arr[index]);
  currentSum = currentSum + arr[index];
  let result = printAllSubsequenceUsingRecursionWithSumAtLeast(
    arr,
    index + 1,
    tempArr,
    currentSum,
    target
  );
  if (result) return true; //One retult achieved - lets kill recursion
  //Not picking ith element
  tempArr.pop();
  currentSum = currentSum - arr[index];
  result = printAllSubsequenceUsingRecursionWithSumAtLeast(
    arr,
    index + 1,
    tempArr,
    currentSum,
    target
  );
  if (result) return true; //One retult achieved - lets kill recursion

  return false; //No returl till achieved - Keep Going - So recursive call will stop when true is returned
};
tempArr = []; //This for calculating the sub sequences
target = 7;
currentSum = 0;
printAllSubsequenceUsingRecursionWithSumAtLeast(
  [1, 2, 4, 3, 5, 7],
  0,
  tempArr,
  currentSum,
  target
);
