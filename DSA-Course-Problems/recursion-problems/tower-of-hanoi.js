/**
 * Rule is We need to move N-1 Disk from A->B
 * then we move the nth disk from A->C
 * then Move N-1 disks from B->C
 *
 * //Base case is if 1 Dick Directly Move from A -> C
 */
//Hypothesis - I shoud make a mthod to Move from A -> C of N element using B as Aux 
//fORMAT IS Func[Disks , SOURCE , AUX , DEST]
let method = function towerOfHanoi(n, a, b, c) {
  //Base case - when Only One lement left just move from A --> C 
  if (n === 1) {
    console.log(`Disk Movement : ${a} ===> ${c}`);
    return;
  }
  if (n === 0) {
    console.log(`Disk Movement : No Movement`);
    return;
  }

  //hYPOTHESIS - Do it forn N-1 elements from A -> B using C as AUX  
  towerOfHanoi(n - 1, a, c, b); //Here Moving from A -> B using C as AUX
  //Induction  - Move the last plat to correct destination 
  console.log(`Disk Movement : ${a} ===> ${c}`);
  //Induction - Putting all remain plates from Helper [B] --> C 
  towerOfHanoi(n - 1, b, a, c); ///Here Moving B-> C using A as AUX
};

method(3, "A", "B", "C");

method(0, "A", "B", "C");

method(64, "A", "B", "C");

//RECURRANCE FORMULA t(n) = 2t(n-1) + c
//Space complexity = O(N)
//Time Complexity = O(2^N) [Because via recurrance sol we can see we need to do 1+2+4+8+16 .. So on ops ] =This is an GP serise =Whose Sume is  Having Term 2^N
