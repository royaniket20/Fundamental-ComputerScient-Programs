 class Stack {
    arr ;
    elements;
    capacity;
    constructor(capacity){
       this.arr = new Array()
       this.elements=0;
       this.capacity=capacity;
    }
      
    push(element){
        if( this.capacity >0){
       this.arr.push(element);
       this.elements++;
       this.capacity--;
       console.log(`Added Element - ${element}`);
        }else{
            console.log(`Stack is already Full - Cannot accomodate More`);
        }
        return this;
    }

    pop(){
        if(this.elements===0){
            console.log(`Stack is already Empty - Cannot pop more`);
            return Number.MIN_SAFE_INTEGER;
        }else {
            let element = this.arr.pop();
            this.elements--;
            this.capacity++;
            console.log(`Removed Element - ${element}`);
            return element;
        }
    }

    peek(){
        if(this.elements===0){
            console.log(`Stack is already Empty - Cannot peek`);
            return Number.MIN_SAFE_INTEGER;
        }else {
            let element = this.arr[this.elements-1];
            console.log(`Peeked Element - ${element}`);
            return element;
        } 
    }
   
 }

 //Hypothesis - This function will sort a stack of N height 
let sortUsingRecursion = function(myStack ,mainArr,tempArr){
    console.log(myStack.arr ,"|", mainArr ,"|", tempArr);
    //Base condition - Stackl is empty nothing to return 
    if(myStack.elements === 0){
        console.log(`Base Case reached - Stack Empty`);
        return ;
    }
    //Hypothesis - Keep one element Side - Make sure We can sort Stack of N-1 Height 
    mainArr.push(myStack.pop());
    sortUsingRecursion(myStack ,mainArr,tempArr);
    //Actual Sorting - Induction 
    //When Stack is already Empty  - Just Put the First element on Array 
    if(myStack.elements === 0 ){
        myStack.push(mainArr.pop());
    }else {
        //Pop Elements Until We find the Suitable Place 
          while(myStack.peek()> mainArr[mainArr.length-1]){
            tempArr.push(myStack.pop());
          }
        //Insert in Correct Place 
        myStack.push(mainArr.pop());
        //Push Back remaing Elements
        while(tempArr.length>0){
            myStack.push(tempArr.pop());
        }
    }
    console.log(`Sorted Stack Upto ${myStack.elements} is ==> [${myStack.arr}]`);

}
let arr = [5,3,1,7,8,5,4,3,9,0,5,4,3,2,1,6];
let myStack = new Stack(arr.length);
arr.forEach(item=> myStack.push(item));
let mainArr = [];
let tempArr = [];
sortUsingRecursion(myStack ,mainArr,tempArr)
