/**
 * Here First Numbers printed from 10 -> 1 anf then It printed 1->10
 */
let simpleRecursion1 = function (num) {
  //Base Condition
  if (num === 0) {
    console.log("Base Care reached -- " + num);
    return;
  }
  console.log(`The Number is -- ${num}`);
  simpleRecursion1(num - 1);
  console.log(`The Number is -- ${num}`);
};

simpleRecursion1(10);







