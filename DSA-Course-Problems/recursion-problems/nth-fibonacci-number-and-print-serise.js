let nthFibbonaci = function (pos) {
  // Assuming 0 based index System
  if (pos === 0) {
    return pos;
  } else if (pos === 1) {
    return 1;
  }

  return nthFibbonaci(pos - 1) + nthFibbonaci(pos - 2);
};

console.log(`The result is --- ${nthFibbonaci(6)}`);

console.log(`The result is --- ${nthFibbonaci(2)}`);

let printSeriseUptoN = function (pos) {
  for (let index = 0; index < pos; index++) {
    console.log(nthFibbonaci(index));
  }
};
//Time complexity N*2^N
//cALL STACK n

printSeriseUptoN(7);

//Better solution is Interative
let printFibonacciRecuirsive = function (n) {
  let firstnum = 0;
  let secondNum = 1;
  if (n <= 0) {
    console.log(`No number to print`);
    return;
  } else if (n == 1) {
    console.log(firstnum);
    return;
  } else if (n == 2) {
    console.log(firstnum);
    console.log(secondNum);
    return;
  } else {
    console.log(firstnum);
    console.log(secondNum);
    for (let index = 0; index < n-2 ; index++) {
      let temp = firstnum + secondNum;
      console.log(temp);
      firstnum = secondNum;
      secondNum = temp;
    }
  }
};
console.log("******************");
printFibonacciRecuirsive(0);
console.log("******************");
printFibonacciRecuirsive(1);
console.log("******************");
printFibonacciRecuirsive(2);
console.log("******************");
printFibonacciRecuirsive(3);
console.log("******************");
printFibonacciRecuirsive(4);
console.log("******************");
printFibonacciRecuirsive(12);
