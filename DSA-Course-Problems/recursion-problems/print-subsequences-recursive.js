/**
Subsequence can be contigiour or Non contigious - But they will be in Sequence for sure 

[1,2,3] ==> 1,2,3,12,23,13 all are Subsequence 
Subarray can be a subsequence also 
*/
/*
For all index We have do to Logic of Taking that Index and Not taking the Index
*/
let printAllSubsequenceUsingRecursion = function (arr, index, tempArr) {
  if (index === arr.length) {
    //Base condition when
    console.log(`Combination : ${tempArr}`);
    return;
  }
  //Picking ith element
  tempArr.push(arr[index]);
  printAllSubsequenceUsingRecursion(arr, index + 1, tempArr);
  //Not picking ith element
  tempArr.pop();
  printAllSubsequenceUsingRecursion(arr, index + 1, tempArr);
};
let tempArr = []; //This for calculating the sub sequences
printAllSubsequenceUsingRecursion([1, 2, 3], 0, tempArr);

/**
 * Time Complexity : 
 * 
 For each element we have Two Choice - Not take and take 
 So 2^n ways to Do it  - This is a time complexity 
 Space complexity is n 
 
 */
