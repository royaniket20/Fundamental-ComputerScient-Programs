let method = function getCountOfSubsetSum(arr, sum, index, initArr) {
  console.log(`Calling with Sum - ${sum} | For Index - ${index} | initArr - [${initArr}]`);
  if (arr.length == index) {
    if (sum === 0) {
      console.log(`Item Where Sum is ${3} is ------------------------> [${initArr}]`);
    } else {
      console.log(`Item Where Sum is Not ${3} is --------------------> [${initArr}]`);
    }

    return;
  }
  console.log(`Init Array for Sum Calculation - [${initArr}] | Current Sum - ${sum}`);
  //First try by including the element 
  initArr.push(arr[index])
  getCountOfSubsetSum(arr, sum - arr[index], index + 1, initArr);
  //Then Try by No including the Item 
  initArr.pop(); //Removing the element from array as added previously 
  getCountOfSubsetSum(arr, sum - 0, index + 1, initArr);

};

method([1, 2, 3], 3, 0, []);

/*** Now Modify the program to Print Only First occurance of the sub sequence 
Here the idea is use of Functional Return Value 

Generally we do like below 
1.Do a recursion call with Number included 
2.Then return Back 
3.Then do a recursoive call with Number excluded 

Here what we can do every time we come back from a recursion - check if its return a boolean data mentioning it achieve 
the recursion goal - then stop calling further 


**/


let method2 = function getFirstCountOfSubsetSum(arr, sum, index, initArr) {
  if (arr.length == index) {
    if (sum === 0) {
      console.log(`Item Where Sum is ${3} is ------------------------> [${initArr}]`);
      return true ; //Now we are indicating First One found 
    } else {
      return false; //We are indicating to Keep Digging
    }
  }
  //First try by including the element 
  initArr.push(arr[index])
  let result1 = getFirstCountOfSubsetSum(arr, sum - arr[index], index + 1, initArr);
  if(result1){
     console.log(`Goal achieved - breaking recursion in mid - even though all other branches not reached base condition`);
     return true;
  }
  //Then Try by No including the Item 
  initArr.pop(); //Removing the element from array as added previously 
  let result2 = getFirstCountOfSubsetSum(arr, sum - 0, index + 1, initArr);
  if(result2){
    console.log(`Goal achieved - breaking recursion in mid - even though all other branches not reached base condition`);
    return true;
 }
 //If none of them Till Now reached the Goal - keep continue by rewtuning false 
 return false;
}

console.log("**************************************************************");
method2([1, 2, 3], 3, 0, []);

console.log("//////////////////////////////////////////////");

//Rerturn Count of Sub sequence whose Sum is K 

let method3 = function getCountNumberOfSubsetSum(arr, sum, index) {
  if (arr.length == index) {
    if (sum === 0) {
       return 1;
    } else {
       return 0;
    }
  }
  //First try by including the element 
  let count1 = getCountNumberOfSubsetSum(arr, sum - arr[index], index + 1);
  //Then Try by No including the Item 
  let count2 = getCountNumberOfSubsetSum(arr, sum - 0, index + 1);
  return count1+count2;

};

console.log(`Total Count of Sub Sequences with Sum ${3} is - ${method3([1, 2, 3], 3, 0)}`);