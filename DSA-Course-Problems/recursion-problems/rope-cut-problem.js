/**
 * Our task is cutting Rope of Given length in such that
 * allowed Piece Length - x,y,z
 * Find Max Cuts possible
 * n = 5 | a = 2 , b = 5 , c =1
 * Here Possible Ways - 5 | 2,2,1 | 2,1,1,1 | 1,1,1,1,1
 * So Max possible Count to cut in allowed length is 5 [All piecec are of 1 length ]
 *
 *  * n = 5 | a = 2 , b = 4 , c =6
 * Here No way We can cut the Ropw in allowed size piecec - So result is [-1]
 *
 */

let method = function ropeCutting(len, a, b, c) {
  //console.log(`Total Roap - ${len} | Cutting Piece Allowed - ${a,b,c}`);
  if (len === 0) {
    return 0; //reached End of rope
  } else if (len < 0) {
    return -1; // Exceeding length of Rope
  }
  //Here idea is that for A given Length we will try to Cut in every Possible way and See which actually prefectly cut the Roap
  //Eventually
  let aa = ropeCutting(len - a, a, b, c);
  let bb = ropeCutting(len - b, a, b, c);
  let cc = ropeCutting(len - c, a, b, c);
  let result = Math.max(aa, bb, cc);

  if (result === -1) {
    //If all scenario Give you -1 then Max is also -1 which is Not valid [That means you tried to Cut in A , B , C way for N - but not a single Cut is possible lIKE  3 ->[4,7,8]]
    return -1;
  } else {
    //Atleast One cut is possible Now //Add it to result
    return result + 1;
  }
};

console.log(method(81, 11, 9, 12));

console.log(method(9, 2, 2, 2));

console.log(method(23, 11, 9, 12));

//Time complexity is based on
