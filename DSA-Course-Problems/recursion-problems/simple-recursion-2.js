let simpleRecursion2 = function (num) {
  //Base Condition
  if (num === 0) {
    //console.log("Base Care reached *** " + num);
    return;
  }
  simpleRecursion2(num - 1);
  process.stdout.write(`${num} -> `);
  simpleRecursion2(num - 1);
  
};
console.log("1st Call with Input 3");
simpleRecursion2(3);
console.log();
console.log("2nd Call with Input 5");
simpleRecursion2(5);
console.log();