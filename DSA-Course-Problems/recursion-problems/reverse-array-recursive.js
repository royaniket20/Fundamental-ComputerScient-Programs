let reverseArr = function(arr , start , end){

  if(start>end){
    return arr;
  }
  //Swap elements 
  let temp = arr[start];
  arr[start] = arr[end];
  arr[end] = temp;
  return reverseArr(arr , start + 1 , end -1);
}

let arr = [1,2,3,4,5];
console.log(`Initial - ${arr}`);
let res = reverseArr(arr , 0 , arr.length-1);
console.log(`Result is - ${res}`);