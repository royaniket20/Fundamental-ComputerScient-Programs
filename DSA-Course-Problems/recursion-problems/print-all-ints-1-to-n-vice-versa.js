let method1 = function methodtoPrint(num) {
  if (num == 0) return;
  methodtoPrint(num - 1);
  console.log(num);
};

//Here the Speace complexity is going to be N - As that is Max call Stack
//length

//Here time Complexity
// T(N) === T(N-1) + C
//Space complexty is O(N)
method1(15);

//tAIL RECURSION OPTIMISATION
let method2 = function methodtoPrintTail(num, init) {
  if (num == 0) return;
  console.log(init);
  init++;
  methodtoPrintTail(num - 1, init);
};

console.log(`Tail recursion way of doing Stuff`);
method2(15, 1);

console.log(`**********************************`);
let method11 = function methodtoPrint(num) {
  if (num == 0) return;
  console.log(num);
  methodtoPrint(num - 1);
};

//Here the Speace complexity is going to be N - As that is Max call Stack
//length

//Here time Complexity
// T(N) === T(N-1) + C
//Space complexty is O(N)
method11(15);
