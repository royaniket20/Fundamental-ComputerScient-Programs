let swap = function swap(str, pos1, pos2) {
  // console.log(`Input : ${str}`);
  let arr = [...str];
  let temp = arr[pos1];
  arr[pos1] = arr[pos2];
  arr[pos2] = temp;
  let result = arr.join("");
  // console.log(`Output : ${result}`);
  return result;
};

let method = function generateAllPermutation(str, index) {
  if (index + 1 === str.length) {
    //Print Data
    console.log(`Item is : ${str}`);
  }
  //Having Loop inside Recursion
  //Here the idea is for each Posuition from the String - SWAP with all other Pos characters At its.'s left 
  //Then try tothe same for all position from left to right 
  /**
   * ABC --> 
   * A -> B ,C == BAC , CBA 
   */
  for (let i = index; i < str.length; i++) {
    //Swap the Current Index position character with Remaining Right Side characters one by one
    str = swap(str, index, i);
    generateAllPermutation(str, index + 1);
    //On Return of Recursion You must Fix the Position so that Other Recursion Trees are Not affected
    str = swap(str, i, index);
  }
};

method("abcd", 0);
