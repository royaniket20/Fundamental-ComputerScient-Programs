 function swap(arr , i ,j){
    //console.log(`Start`,arr,i,j);
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
    //console.log(`End`,arr,i,j);
    return;
}


//Hypothesis is this will sort an array of Size n 
let sortUsingRecursion = function(arr , lastIndex){
    //Baase condition 
    if(lastIndex === 0){
        console.log(`Base Case reached`);
        return ;
    }
    //Hypothesis - this will sort an array of N-1 length 
    sortUsingRecursion(arr , lastIndex-1);
    //Actual Soering - this is called Induction 
    //Everytime we increase one index my responsibility is too Just put that element in correct position 
    console.log(`Sorting Upto - ${lastIndex}`);
    for (let index = lastIndex; index >0; index--) {
        if(arr[index]<arr[index-1]){
          swap(arr,index,index-1);
        }
    }
    console.log(`Sorted Array Till [${lastIndex}] ==> [${arr.slice(0,lastIndex+1)}]`);

}
let arr = [5,3,1,7,8,5,4,3,9,0,5,4,3,2,1,6];
sortUsingRecursion(arr ,arr.length-1 )
