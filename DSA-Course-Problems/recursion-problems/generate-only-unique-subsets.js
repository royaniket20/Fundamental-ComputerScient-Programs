/**
 * Given an integer array nums that may contain duplicates, return all possible 
subsets
 (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.

 

Example 1:

Input: nums = [1,2,2]
Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
Example 2:

Input: nums = [0]
Output: [[],[0]]
 */

function generateUniqueSubSet(array , currentIndex , tempArray) {
    //Base condition -  Every call is a sub set getting generated 
    //Now for some language we can Do a duplicat check here - Better apparoah to prevet the recursive call at all 
        console.log("Found one Subset - " + tempArray);

   
    //Now Do it for Smaller input 
    //For each element of array we have 2 choice - To take it or nOT TO TAKE IT 
    for(let i = currentIndex ; i <array.length ; i++){
        if(i>currentIndex && (array[i] == array[i-1])) continue
        tempArray.push(array[i])
        generateUniqueSubSet(array , i+1  , tempArray)
        tempArray.pop();
    }
   

}

let arr = [1,2,2];
arr.sort((a,b)=> a-b);
console.log(`Sorted array - ${arr}`);

generateUniqueSubSet( arr , 0 , [])

