 class Stack {
    arr ;
    elements;
    capacity;
    constructor(capacity){
       this.arr = new Array()
       this.elements=0;
       this.capacity=capacity;
    }
      
    push(element){
        if( this.capacity >0){
       this.arr.push(element);
       this.elements++;
       this.capacity--;
       console.log(`Added Element - ${element}`);
        }else{
            console.log(`Stack is already Full - Cannot accomodate More`);
        }
        return this;
    }

    pop(){
        if(this.elements===0){
            console.log(`Stack is already Empty - Cannot pop more`);
            return Number.MIN_SAFE_INTEGER;
        }else {
            let element = this.arr.pop();
            this.elements--;
            this.capacity++;
            console.log(`Removed Element - ${element}`);
            return element;
        }
    }

    peek(){
        if(this.elements===0){
            console.log(`Stack is already Empty - Cannot peek`);
        }else {
            let element = this.arr[this.elements-1];
            console.log(`Peeked Element - ${element}`);
            return element;
        } 
    }
   
 }

 //Hypothesisl - It shoud reverse  stack of Length N 
let reverseUsingRecursion = function(myStack ,mainArr){
    console.log(myStack.arr ,"|", mainArr);
    if(myStack.elements === 0){
        console.log(`Base Case reached - Stack Empty`);
        return ;
    }
     //Hypothesisl - It shoud reverse  stack of Length N -1
    mainArr.push(myStack.pop());
    reverseUsingRecursion(myStack ,mainArr);

    //Induction 
    //Actual Reversion happening here 
    //When Stack is already Empty  - Just Put the First element on Array 
      myStack.push(mainArr[myStack.elements]);
    console.log(`Reversed  Stack Upto ${myStack.elements} is ==> [${myStack.arr}]`);

}
let arr = [1,2,3,4,5,6,7,8];
let myStack = new Stack(arr.length);
arr.forEach(item=> myStack.push(item));
let mainArr = [];
reverseUsingRecursion(myStack ,mainArr)
