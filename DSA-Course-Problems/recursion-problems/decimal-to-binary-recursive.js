let decimalToBinary = function (num) {
  console.log(`Calling with Num - ${num}`);
  if (num === 0) {
    return;
  }
  decimalToBinary(Math.floor(num / 2)); //Same can be done using Right Shift Operator also - FASTER
  process.stdout.write((num % 2) + "");
};
let input = 13;
console.log(`The numbner is ${13} ----> ${input.toString(2)}`);
decimalToBinary(13);

/**
 * Here the calculation is Like that -
 * 13 = Input
 * Function is getting called for 13,6,3,1
 * On Return It print -
 * 1 ->1
 * 1 ->3
 * 0 ->6
 * 1 ->13
 *
 * 13 = 1101
 */
