let generateSubsetRecursion = function generateSubsetRecursion(
  str,
  init,
  index
) {
  //Base case
  if (str.length == index) {
    console.log(index + " Item : " + init);
    return;
  }
  generateSubsetRecursion(str, init + str.charAt(index), index + 1); //Including Element
  generateSubsetRecursion(str, init + "", index + 1); //Not Including Element
};

generateSubsetRecursion("abcd", "", 0);


generateSubsetRecursion("AB", "", 0);