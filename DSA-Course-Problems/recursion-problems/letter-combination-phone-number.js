/**
 * 17. Letter Combinations of a Phone Number

Given a string containing digits from 2-9 inclusive, 
return all possible letter combinations that the number could represent. 
Return the answer in any order.

A mapping of digits to letters 
(just like on the telephone buttons) is given below. 
Note that 1 does not map to any letters.

2 = abc
3 = def
4 = ght
5 = jkl
6 = mno
7 = pqrs
8 = tuv
9 = wxyz

 

Example 1:

Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]
Example 2:

Input: digits = ""
Output: []
Example 3:

Input: digits = "2"
Output: ["a","b","c"]
 

Constraints:

0 <= digits.length <= 4
digits[i] is a digit in the range ['2', '9'].
 */

//This problem is all about finding all combination of certain length
function letterCombination(arr, tempArr, limit, outerIndex) {
  //Base condition - when Limit is reached  
  if (limit == 0) {
    console.log(`Found one combination --------------> ${tempArr.join("")}`);
    return;
  }
  console.log(`outer - ${outerIndex}`);

  //Do for all smaller input - for each set we loop through 
  for (let Outindex = outerIndex; Outindex < arr.length; Outindex++) {
    const elements = arr[Outindex];
    console.log(`Processing for elements - [${elements}]`);
    //Do Recursive call for each element of the array 
    for (let Inindex = 0; Inindex < elements.length; Inindex++) {
      const innerElement = elements[Inindex];
      console.log(`Processing for element recursively- ${innerElement}`);
      tempArr.push(innerElement);
      //we increase Outer Loop Index so that we cna take Next set on recursive call 
      //Limit is reduced as One element pushed to rresult temp array 
      letterCombination(arr, tempArr, limit - 1, Outindex + 1);
      tempArr.pop();
    }
  }
}

let digits = "23";
let convertedResultArr = [];
Array.from(digits).forEach((item) => {
  let arr = Array.from(convertdigitsToAlphabet(item));
  convertedResultArr.push(arr);
});

console.log(`Input ----`);
convertedResultArr.forEach((item) => {
  console.log(item);
});

let tempArr = [];
let limit = digits.length;
let outerIndex = 0;

letterCombination(convertedResultArr, tempArr, limit, outerIndex);
console.log("-----------------------------------------------------------------------------------");


 digits = "9823";
 convertedResultArr = [];
Array.from(digits).forEach((item) => {
  let arr = Array.from(convertdigitsToAlphabet(item));
  convertedResultArr.push(arr);
});

console.log(`Input ----`);
convertedResultArr.forEach((item) => {
  console.log(item);
});


 tempArr = [];
 limit = digits.length;
 outerIndex = 0;

letterCombination(convertedResultArr, tempArr, limit, outerIndex);

function convertdigitsToAlphabet(digits) {
  let digitlatterMapping = new Map();
  digitlatterMapping.set("2", "abc");
  digitlatterMapping.set("3", "def");
  digitlatterMapping.set("4", "ghi");
  digitlatterMapping.set("5", "jkl");
  digitlatterMapping.set("6", "mno");
  digitlatterMapping.set("7", "pqrs");
  digitlatterMapping.set("8", "tuv");
  digitlatterMapping.set("9", "wxyz");
  let result = "";
  Array.from(digits).forEach((item) => {
    result = result + digitlatterMapping.get(item);
  });
  console.log(`Converted from ${digits} to --> ${result}`);
  return result;
}
