/**
 * 
 * Given a array arr of integers, return the sums of all subsets in the list.  Return the sums in any order.

Examples:

Input: arr[] = [2, 3]
Output: [0, 2, 3, 5]
Explanation: When no elements are taken then Sum = 0. When only 2 is taken then Sum = 2. When only 3 is taken then Sum = 3. When elements 2 and 3 are taken then Sum = 2+3 = 5.
Input: arr[] = [1, 2, 1]
Output: [0, 1, 1, 2, 2, 3, 3, 4]
Explanation: The possible subset sums are 0 (no elements), 1 (either of the 1's), 2 (the element 2), and their combinations.
Input: arr[] = [5, 6, 7]
Output: [0, 5, 6, 7, 11, 12, 13, 18]
Explanation: The possible subset sums are 0 (no elements), 5, 6, 7, and their combinations.
Constraints:
1 ≤ arr.size() ≤ 15
0 ≤ arr[i] ≤ 104


 * 
 */

function generateSumOfAllSubSet(array , currentIndex , results , tempArray) {
    //Base condition - when We reached end of Original array 
    if(currentIndex == array.length){
        console.log("Found one Subset - " + tempArray);
        const sum = tempArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
        results.push(sum);
        return
    }

    //Now Do it for Smaller input 
    //For each element of array we have 2 choice - To take it or NOT  TO TAKE IT 
    tempArray.push(array[currentIndex])
    generateSumOfAllSubSet(array , currentIndex+1 , results , tempArray)
    tempArray.pop();
    generateSumOfAllSubSet(array , currentIndex+1 , results , tempArray)

}

let results = [];
generateSumOfAllSubSet([5, 6, 7] , 0,results , [])
console.log(`Sum is - ${results}`);
