/**
 * Givean a String a1B2 - expected Oututs - 
 * A1B2 ,
 * A1b2 
 * a1B2 , 
 * a1b2  
 */

/**
 * create a ip op choice tree 
 */


function permutationwithLetterCaseChangeWithMixNumbers(input , currentIndex  )
{
    //Base case 
    if(currentIndex == input.length )
    {
        console.log("Item : ",input);
        return;
    }
    //Choice  -  we need to carry the modified  String 

    if(input.charAt(currentIndex) >= '0' &&  input.charAt(currentIndex) <= '9'){
        permutationwithLetterCaseChangeWithMixNumbers(input , currentIndex+1 )
    }else{
        let choice1 = input.charAt(currentIndex).toLowerCase();
        let choice2 = input.charAt(currentIndex).toUpperCase();
        let result1 = input.substr(0,currentIndex)+choice1+ input.substr(currentIndex+1)
        let result2 = input.substr(0,currentIndex)+choice2+ input.substr(currentIndex+1)
        permutationwithLetterCaseChangeWithMixNumbers(result1 , currentIndex+1 )
        permutationwithLetterCaseChangeWithMixNumbers(result2 , currentIndex+1 )
    }

}
let input = "ab123c";

permutationwithLetterCaseChangeWithMixNumbers(input , 0 );
console.log("------------------------------------");

input = "a1b2";

permutationwithLetterCaseChangeWithMixNumbers(input , 0 );
