/**
 * 779. K-th Symbol in Grammar
Medium
Topics
Companies
Hint
We build a table of n rows (1-indexed). We start by writing 0 in the 1st row. Now in every subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each occurrence of 1 with 10.

For example, for n = 3, the 1st row is 0, the 2nd row is 01, and the 3rd row is 0110.
Given two integer n and k, return the kth (1-indexed) symbol in the nth row of a table of n rows.

 

Example 1:

Input: n = 1, k = 1
Output: 0
Explanation: row 1: 0
Example 2:

Input: n = 2, k = 1
Output: 0
Explanation: 
row 1: 0
row 2: 01
Example 3:

Input: n = 2, k = 2
Output: 1
Explanation: 
row 1: 0
row 2: 01
 */

/**
 * N = 4 , K = 4 
 * N = 4 , K ==4 is redived from N = 3 , K =2 which is Derived from N = 2 K = 1 
 * which is Derived from N =1 , K = 0 
 * (0)           
 * (0)1
 * 0(1)10
 * 011(0)1001
 * Some observation - N can be reduced in Recusover call by 1 time 
 * But K shoud be reduced in Previous call by K/2 times 
*/

//TBH method 

//Hypothesis - given N and K it shoud Give bcck the Bit value 
function findKthGrammer(n , k ){
  console.log("Calling from ", n , k );
  
//Base condition 
if(n == 1 && k == 0  )
{
  console.log(`Came to base condition ${n} | ${k}`);
  console.log("Formed Str - ", "0");
  return "0";
}
//Hypothesis - Do this for smaller Input 
let result = findKthGrammer(n-1 , Math.floor(k/2))
//Induction - 
//First make every 0 as O1 and every 1 as 10
let resStr  = ""
Array.from (result).forEach(char =>{
if(char === '0')
  resStr = resStr+"01"
else
resStr = resStr + "10"
})
console.log("Formed Str - ", resStr);
//Now Find the Kth element from here 
return resStr

} 

let lastStr = findKthGrammer(4,4)
console.log("Output - " ,lastStr[4-1] );
