let generateSubsetRecursion = function generateSubsetRecursion(
  str,
  init,
  index,
  arr
) {
  //Base case
  if (str.length == index) {
    console.log(index + " Item : " + init);
    arr.push(init)
    return;
  }
  generateSubsetRecursion(str, init + str.charAt(index), index + 1,arr); //Including Element
  generateSubsetRecursion(str, init + "", index + 1,arr); //Not Including Element
};
let arr = []
generateSubsetRecursion("abcd", "", 0,arr);
arr.sort()
console.log("Result is = ",arr);


