//We will start with a array with all participiants 
//Then as we Kill we will make array shorter on every call
//eventually when only one element left thats the person 

//Hypothesis - It will somehow Give me the Original Index of last man standing
function josephusProblem(numOfPeople , arrayOfPeople , jumpLength, startIndex ){

    //Base condition - when One person left 
    //Return his actual position in array 
    //Here it will be value of Oth index
    if(numOfPeople == 1){
        console.log(`Last man standing - ${arrayOfPeople[0]}`);
        return;
    }
        //Induction -- On solving Problem 
    let start = startIndex;
    let killPos = (startIndex+jumpLength-1)%numOfPeople; // -1 as Zero based Index , % to do cisrcular 
    arrayOfPeople.splice(killPos,1)
    //console.log(start , killPos ,arrayOfPeople);
    startIndex = killPos; //As Item Now shifted to KillPos 
    
    //Hypothesis for smaller Input 
    josephusProblem(numOfPeople-1, arrayOfPeople,jumpLength ,startIndex )


}

let numOfPeople = 40;
let jumpLength = 7;
let arrayOfPeople = [];
let startIndex = 0;
for (let index = 0; index < numOfPeople; index++) {
    arrayOfPeople.push(index);
}
console.log(arrayOfPeople);

josephusProblem(numOfPeople , arrayOfPeople , jumpLength , startIndex )

