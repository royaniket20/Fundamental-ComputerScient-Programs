let generateSubsetRecursion = function generateSubsetRecursion(
  str,
  init,
  index,
  set
) {
  //Base case
  if (str.length == index) {
    set.add(init);
    return;
  }
  generateSubsetRecursion(str, init + str.charAt(index), index + 1, set); //Including Element
  generateSubsetRecursion(str, init + "", index + 1, set); //Not Including Element
};

let set = new Set();
generateSubsetRecursion("aab", "", 0, set);
console.log("Unique Subset -", set);
