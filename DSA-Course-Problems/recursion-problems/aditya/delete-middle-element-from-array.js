let stackEven = [1, 2, 3, 4, 5, 6];
let stackOdd = [1, 2, 3, 4, 5];

//Hypothesis - We want this method delete element of Kth Pos  - Where K is the Delete  Position 

function deleteMidElementFromStack(stack, tempStack , deleteIndex , isEven) {
   console.log(`Calling with Stack ${stack} | tempStack - ${tempStack} | isEven ${isEven} | DeleteIndex - ${deleteIndex}`);
  //Base Condtion
  if (deleteIndex + 1  ==  stack.length ) {
    console.log("Reached the delete Position  ");
    stack.pop()
    if(isEven)  //In case of is Even remove two elements 
      stack.pop()
    return stack;
  }

  //Hypothesis - do the sme for smaller Input
  tempStack.push(stack.pop());
  deleteMidElementFromStack(stack, tempStack , deleteIndex , isEven );

  //Induction - Move base lement from temp stack to main stack 
  stack.push(tempStack.pop())  
  return stack ;
}

let isEven = stackEven.length%2 ==0 ? true : false
let result = deleteMidElementFromStack(stackEven, [] , Math.floor(stackEven.length/2)  ,  isEven  );
console.log(`Result Stack - ${result}`);
isEven = stackOdd.length%2 ==0 ? true : false
 result = deleteMidElementFromStack(stackOdd, [] , Math.floor(stackOdd.length/2)  ,  isEven  );
console.log(`Result Stack - ${result}`);

