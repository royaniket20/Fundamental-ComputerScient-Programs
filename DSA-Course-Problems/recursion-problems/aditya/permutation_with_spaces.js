/**
 * Givean a String ABC - expected Oututs - A_BC  AB_C ABC A_B_C
 */

/**
 * create a ip op choice tree 
 * Easily we can see given n length Input We have to take decision for first n-1 elements at right side we need
 * to append space or Not 
 */


function permutationwithDash(input , currentIndex ,substitutionIndex, limit)
{
    //Base case 
    if(currentIndex == limit-1)
    {
        console.log("Item : ",input);
        return;
    }
    //Choice  -  we need to carry the modified  String 

//substitutionIndex tells which character we need to to cosider to make X_ / X
    let choice1 = input.charAt(substitutionIndex);
    let choice2 = input.charAt(substitutionIndex)+"_";
    let result1 = input.substr(0,substitutionIndex)+choice1+ input.substr(substitutionIndex+1)
    let result2 = input.substr(0,substitutionIndex)+choice2+ input.substr(substitutionIndex+1)
    permutationwithDash(result1 , currentIndex+1 ,substitutionIndex+1, limit )
    //substitutionIndex is increased by 2 because in this case _ added and we need to consider beyond that for further 
    //choice calculation 
    permutationwithDash(result2 , currentIndex+1 ,substitutionIndex+2, limit)
}
let input = "ABC";

permutationwithDash(input , 0 ,0,input.length);
console.log("------------------------------------");

input = "ABCDEF";

permutationwithDash(input , 0 ,0,input.length);