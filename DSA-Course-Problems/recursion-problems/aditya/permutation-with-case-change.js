/**
 * Givean a String ABC - expected Oututs - A_BC  AB_C ABC A_B_C
 */

/**
 * create a ip op choice tree 
 */


function permutationwithCasechange(input , currentIndex)
{
    //Base case 
    if(currentIndex == input.length )
    {
        console.log("Item : ",input);
        return;
    }
    //Choice  -  we need to carry the modified  String 

    let choice1 = input.charAt(currentIndex);
    let choice2 = input.charAt(currentIndex).toUpperCase();
    let result1 = input.substr(0,currentIndex)+choice1+ input.substr(currentIndex+1)
    let result2 = input.substr(0,currentIndex)+choice2+ input.substr(currentIndex+1)
    permutationwithCasechange(result1 , currentIndex+1 )
    permutationwithCasechange(result2 , currentIndex+1 )
}
let input = "abc";

permutationwithCasechange(input , 0 );
console.log("------------------------------------");

input = "ab";

permutationwithCasechange(input , 0 );
console.log("------------------------------------");

input = "abcd";

permutationwithCasechange(input , 0 );