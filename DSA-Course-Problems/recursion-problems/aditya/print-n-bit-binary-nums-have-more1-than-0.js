function generateAllBinary(len, arr, numOfOne, numOfZero) {
    //console.log(len , arr , numOfOne , numOfZero );
    
  //Base condition reached
  if (len == 0) {
    console.log(arr);
    return;
  }

  //Choice where we can have One
  arr.push(1);
  generateAllBinary(len - 1, arr, numOfOne + 1, numOfZero);
  arr.pop(); //Important step as using same array
  //Choice where we can have Zero - here we shoud filter invalid branches
  if (numOfOne > numOfZero + 1) {
    arr.push(0);
    generateAllBinary(len - 1, arr, numOfOne, numOfZero + 1);
    arr.pop(); //Important step as using same array
  }
}

generateAllBinary(5, [], 0, 0);
console.log("Code completed ");
