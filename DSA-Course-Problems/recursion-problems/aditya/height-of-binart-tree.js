class BinaryTreeNode {
  value;
  left;
  right;
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

let root = new BinaryTreeNode(5)

root.left = new BinaryTreeNode(3)

root.left.left = new BinaryTreeNode(2)
root.left.right = new BinaryTreeNode(4)

root.right = new BinaryTreeNode(8)

//Height of Binar Tree 

//Hypothesis - I will have a method wihich will give height of Tree 

function  giveHeight(root){

  //Base Condition 
  if(root == null)
    return 0;

  let leftData  = giveHeight(root.left);
  let rightData  = giveHeight(root.right);
   
  //Induction 
  return 1+Math.max(leftData , rightData);
}

console.log("Height of Tree - "+ giveHeight(root));
