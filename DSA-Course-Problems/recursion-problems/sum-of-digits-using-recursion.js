//This is NON TAIL RECURSION 
let method1 = function sumOfDigitsUsingRecursion(num) {
  //Base case
  if (num === 0) return num;

  let lsb = num % 10;
  let remainingNum = Math.floor(num / 10); //RECURSION IS NOT LAST LINE
  return lsb + sumOfDigitsUsingRecursion(remainingNum);
};

console.log("The sum of nums digits = " + method1(123));

console.log("The sum of nums digits = " + method1(9987));

//TAIL RECURSIVE WAY

let method2 = function sumOfDigitsUsingRecursionTail(num, sum) {
  //Base case
  if (num === 0) return sum;

  let lsb = num % 10;
  let remainingNum = Math.floor(num / 10);
  sum = sum + lsb ;
  return sumOfDigitsUsingRecursionTail(remainingNum, sum); //recurdion is last line here 
};

console.log("The sum of nums digits = " + method2(123, 0));

console.log("The sum of nums digits = " + method2(9987, 0));


//SIMPLE ITERATIVE SOLUTION 

let method3 = function simpleIterativeSol(num) {
    let sum = 0;
    //Base case
    while (num>0){
        let lsb = num % 10;
        num = Math.floor(num / 10);
        sum = sum + lsb ;
    }

    return sum;
  };
  
  console.log("The sum of nums digits = " + method3(123));
  
  console.log("The sum of nums digits = " + method3(9987));
  

