let atoi = function(str){
console.log(`Current Input - ${str}`);
//This is the Base Case
if(str.length === 0){
   return 0; 
}
let LSB = str.substring( str.length-1)
let remaining = str.substring(0, str.length-1)
console.log(`Calculating for Num - ${LSB}`);
let result = parseInt(LSB) + 10*atoi(remaining) 
return result;
}

let input = '145287456';
let result = atoi(input);

console.log(`Atoi of ${input} => ${result}`);
