// javascript program for the above approach
function power(x, n) {
  console.log(`Recursive Call to ${x}^${n}`);
  // If x^0 return 1
  if (n == 0) return 1;

  if (x == 0) return 0;

  // For all other cases
  return x * power(x, n - 1);
}

// Driver Code

let result = power(5, 5);
console.log(`Result is = ${result}`);
// This code is contributed by Rajput-Ji
