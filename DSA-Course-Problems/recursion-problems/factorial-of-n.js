let method1 = function NonTailRecursive(n){
     if(n===0 || n===1)
     {
         return 1;
     }
   
     return n * NonTailRecursive(n-1);

}

console.log(`NON TAIL RECURSIVE ${5} is ${method1(5)}`);

let method2 = function TailRecursive(n,result){
    if(n===0 || n===1)
    {
        return result;
    }
    result = result*n; 
    console.log(result);
    return TailRecursive(n-1,result);

}

console.log(` TAIL RECURSIVE ${5} is ${method2(5,1)}`);