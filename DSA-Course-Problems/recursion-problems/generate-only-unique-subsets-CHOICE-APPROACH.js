/**
 * Given an integer array nums that may contain duplicates, return all possible 
subsets
 (the power set).

The solution set must not contain duplicate subsets. Return the solution in any order.

 

Example 1:

Input: nums = [1,2,2]
Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
Example 2:

Input: nums = [0]
Output: [[],[0]]
 */

function generateUniqueSubSet(array, currentIndex, tempArray , isPreviousElementPicked) {
    //Base condition - when We reached end of Original array 
    if (currentIndex == array.length) {
        console.log("Found one Subset - " + tempArray);
        return
    }

    //Now Do it for Smaller input 
    //For each element of array we have 2 choice -

    let   isprevousElementSame = currentIndex >0 && array[currentIndex] == array[currentIndex-1];
    if(isprevousElementSame && !isPreviousElementPicked ) {
      console.log(`Going to Duplicate path - Stop Here `);
    }else{
    //  To take it   - hence isPreviousElementPicked is true 
    tempArray.push(array[currentIndex])
    generateUniqueSubSet(array, currentIndex + 1, tempArray,true)
    tempArray.pop();

    }


         
    //Not to take it  - hence isPreviousElementPicked is false 
    generateUniqueSubSet(array, currentIndex + 1, tempArray,false)

}


let arr = [1, 2, 2];
arr.sort((a, b) => a - b);
console.log(`Sorted array - ${arr}`);
let isPreviousElementPicked = false;
generateUniqueSubSet(arr, 0, [] , isPreviousElementPicked) //false beacuse there is no previous eklement 

