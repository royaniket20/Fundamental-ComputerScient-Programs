/**
 * Given a string s, partition s such that every 
substring
 of the partition is a 
palindrome
. Return all possible palindrome partitioning of s.

 

Example 1:

Input: s = "aab"
Output: [["a","a","b"],["aa","b"]]
Example 2:

Input: s = "a"
Output: [["a"]]
 

Constraints:

1 <= s.length <= 16
s contains only lowercase English letters.
 */

function isPalindrome(data) {
  if (data.length == 0 || data.length == 1) {
    return true;
  }
  let start = 0;
  let end = data.length - 1;
  while (start < end) {
    if (data[start] != data[end]) {
      return false;
    }
    start++;
    end--;
  }
  return true;
}

//Palidrom Function checke
let data = "abcba";
console.log(`is ${data} palidrome ==  ${isPalindrome(data)}`);
data = "abfgba";
console.log(`is ${data} palidrome ==  ${isPalindrome(data)}`);

function palindormicPartition(data) {
  let tempResults = [];
  console.log(`Calculating recursively for Data - ${data}`);
  recursivePartition(data, tempResults, 0);
}

function recursivePartition(data, tempResults, currIndex) {
  //Base condition
  if (currIndex == data.length) {
    console.log(`***************************Got one combination - ${tempResults}`);
    return;
  }
  for (let index = currIndex; index < data.length; index++) {
    let left = data.substring(currIndex, index + 1);
    console.log(`Checking for ${left}`);
    if (left.length > 0 && isPalindrome(left)) {
      console.log(
        `${left} is Palindrome , checking for  - ${data.substring(index + 1)}`
      );
      tempResults.push(left);
      recursivePartition(data, tempResults, index + 1);
      tempResults.pop();
    } else {
      console.log(`${left} is NOT Palindrome, KILL THE TREE}`);
    }
  }
}

palindormicPartition("aabb");
