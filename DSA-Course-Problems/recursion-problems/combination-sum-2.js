/**
 * Combination Sum II
Given a collection of candidate numbers (candidates) and a target number (target), find all unique combinations in candidates where the candidate numbers sum to target.

Each number in candidates may only be used once in the combination.

Note: The solution set must not contain duplicate combinations.


Example 1:

Input: candidates = [10,1,2,7,6,1,5], target = 8
Output: 
[
[1,1,6],
[1,2,5],
[1,7],
[2,6]
]
Example 2:

Input: candidates = [2,5,2,1,2], target = 5
Output: 
[
[1,2,2],
[5]
]
 
 */

//Hypothesis - We will create a Function Given an Input we need to Reach Target 

function combiSum2(array ,currentIndex, tempArr , target) {
   //Base condition when we reach means target  becomes 0

   if(0 == target){
    console.log("Reached end WITH solution ["+tempArr+"]");
    return;
   }
    //further process for smaller Inputs where  from left to Right from 
    //current Index  as they array is already sorted  
    for (let index = currentIndex ; index < array.length; index++) {
           //Avoid duplicate path suppose at certain level you are adding 1 in the tempArray
           // , there is no meaning of adding same Number Again at same level
           if(index>currentIndex && (array[index] == array[index-1])) continue

           tempArr.push(array[index])
           //Chceking for all other piossibility 
           if( target-array[index]>=0) //Only check Good paths 
            {
                combiSum2(array ,index+1, tempArr , target-array[index])  
            }
            tempArr.pop();  
    }

}
//Sort the array so that you get result in sorted way 
let arr = [2,5,2,1,2];
arr.sort((a,b)=> a-b);
console.log(arr , 'Sorted Array ');

combiSum2( arr ,0, [] , 5)
arr = [10,1,2,7,6,1,5];
arr.sort((a,b)=> a-b);
console.log(arr , 'Sorted Array ');
combiSum2( arr ,0, [] , 8)

arr = [1,1,1,2,2];
arr.sort((a,b)=> a-b);
console.log(arr , 'Sorted Array ');
combiSum2( arr ,0, [] , 4)
