//Space Efficient Solution
class Node {
  node;
  constructor() {
    this.node = new Array(26).fill(null);
  }
  isCharPresent(char) {
    return this.node[this.getIndex(char)] != null ? true : false;
  }
  insertChar(char, newNode) {
    this.node[this.getIndex(char)] = newNode;
  }
  getNextNode(char) {
    return this.node[this.getIndex(char)];
  }

  getIndex(char) {
    return char.charCodeAt() - "a".charCodeAt();
  }
}

class Trie {
  root;
  count;
  constructor() {
    this.root = new Node(); //Initially Started with the Empty Root Node
    this.count = 0; //For Sub String Count - actually New Node creation Count
  }

  getSubStringCount() {
    return this.count + 1; //One is for Empty String which is Also Valid Sub String
  }

  getRootNode() {
    return this.root;
  }

  //Insert Word In the Trie
  insertItem(char, currentNode) {
    console.log(`Inserting Character - ${char}`);
    //Starting from the Root
    let index = char.charCodeAt() - "a".charCodeAt();
    console.log(
      `Is Char - ${char} Present  at Index [${index}]?  : ${currentNode.isCharPresent(
        char
      )}`
    );
    if (currentNode.isCharPresent(char)) {
      currentNode = currentNode.getNextNode(char);
    } else {
      let freshNode = new Node();
      currentNode.insertChar(char, freshNode);
      currentNode = freshNode;
      this.count++;
    }
    return currentNode;
  }
}

let trieObj = new Trie();
let word = "abab";
let arr = Array.from(word);

for (let i = 0; i < arr.length; i++) {
  let currentNode = trieObj.getRootNode();
  for (let j = i; j < arr.length; j++) {
    console.log(`Push Char to Trie - ${arr[j]}`);
    currentNode = trieObj.insertItem(arr[j], currentNode);
  }
}
console.log(`Number of Uniqueue SubString - ${trieObj.getSubStringCount()}`);
