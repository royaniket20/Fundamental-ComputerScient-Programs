class Node {
  node; //This Holds A-Z chars
  flag; //Then Indicate its end of an Word at this Node 
  constructor() {
    this.node = new Array(26).fill(null);
    this.flag = false;
  }
  isCharPresent(char) {
    return this.node[this.getIndex(char)] != null ? true : false;
  }
  insertChar(char, newNode) {
    this.node[this.getIndex(char)] = newNode;
  }
  getNextNode(char) {
    return this.node[this.getIndex(char)];
  }

  getIndex(char) {
    return char.charCodeAt() - "a".charCodeAt();
  }
  setFlag(value) {
    this.flag = value;
  }

  getFlag() {
    return this.flag;
  }
}

class Trie {
  root;
  constructor() {
    this.root = new Node(); //Initially Started with the Empty Root Node
  }

  //Insert Word In the Trie
  insertWord(word) {
    console.log(`Inserting Word - ${word}`);
    //Starting from the Root
    let currentNode = this.root;
    Array.from(word).forEach((char) => {
      let index = char.charCodeAt() - "a".charCodeAt();
      console.log(
        `Is Char - ${char} Present  at Index [${index}]?  : ${currentNode.isCharPresent(
          char
        )}`
      );
      if (currentNode.isCharPresent(char)) {
        currentNode = currentNode.getNextNode(char);
      } else {
        let freshNode = new Node();
        currentNode.insertChar(char, freshNode);
        currentNode = freshNode;
      }
    });
    //Now Just have the Leaf Node Make Flag = true
    currentNode.setFlag(true);

    return this;
  }
  printTrie() {
    console.log(JSON.stringify(this));
    return this;
  }

  searchPartialWord(word) {
    let current = this.root;
    let isPresent = false;
    console.log(`Searching Partialliy For the Word - ${word}`);
    Array.from(word).every((char) => {
      console.log(`Searching for ${char}`);
      if (current.isCharPresent(char)) {
        current = current.getNextNode(char);
        isPresent = true;
        return true;
      } else {
        isPresent = false;
        return false; //Now it Will break
      }
    });
    if (isPresent) {
      console.log("Word Is Present Partially !!!");
    } else {
      console.log("Word Is NOT  Present Partially !!!");
    }
    return this;
  }

  findallWords(prefix){
    console.log(`Given Prefix = ${prefix}`);

    return this;
  }

  searchFullWord(word) {
    let current = this.root;
    let isPresent = false;
    console.log(`Searching Fully For the Word - ${word}`);
    Array.from(word).every((char) => {
      console.log(`Searching for ${char}`);
      if (!current.isCharPresent(char)) {
        isPresent = false;
        return false;
      } else {
        current = current.getNextNode(char);
        isPresent = true;
        return true;
      }
    });
    if (isPresent && current.getFlag()) {
      console.log(`Word Found Fully In Trie DS`);
    } else {
      console.log(`Word NOT Found Fully In Trie DS`);
    }
    return this;
  }
}

let trieObj = new Trie();
trieObj
  .insertWord("aniket")
  .insertWord("apple")
  .insertWord("apps")
  .insertWord("apxl")
  .insertWord("bac")
  .insertWord("bat")
  .insertWord("application")
  .insertWord("appropriate")
  .insertWord("apline")
  .insertWord("apple")
  .insertWord("apollo")
  .insertWord("aplabhabet")
  .insertWord("apk")
  .findallWords("ap")
  // .printTrie();

console.log("******************************************");

// trieObj
//   .searchFullWord("bac")
//   .searchFullWord("applet")
//   .searchFullWord("apxl")
//   .searchFullWord("ba");

// console.log("******************************************");
// trieObj
//   .searchPartialWord("appxl")
//   .searchPartialWord("b")
//   .searchPartialWord("app")
//   .searchPartialWord("apl")
//   .searchPartialWord("ba");
