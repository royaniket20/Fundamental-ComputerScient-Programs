class Node {
  node;
  constructor() {
    this.node = new Array(2).fill(null);
  }
  isCharPresent(char) {
    return this.node[this.getIndex(char)] != null ? true : false;
  }
  insertChar(char, newNode) {
    this.node[this.getIndex(char)] = newNode;
  }
  getNextNode(char) {
    return this.node[this.getIndex(char)];
  }

  getIndex(char) {
    return char.charCodeAt() - "0".charCodeAt();
  }
}

class Trie {
  root;
  inserCount;
  constructor() {
    this.root = new Node(); //Initially Started with the Empty Root Node
    this.inserCount = 0; //Special Condition Check when Running for Quiries  -Because Trie Get created Gradually
  }

  //Insert Word In the Trie
  insertWord(word) {
    console.log(`Inserting Word - ${word}`);
    //Starting from the Root
    let currentNode = this.root;
    Array.from(word).forEach((char) => {
      let index = char.charCodeAt() - "0".charCodeAt();
      // console.log(
      //   `Is Char - ${char} Present  at Index [${index}]?  : ${currentNode.isCharPresent(
      //     char
      //   )}`
      // );
      if (currentNode.isCharPresent(char)) {
        currentNode = currentNode.getNextNode(char);
      } else {
        let freshNode = new Node();
        currentNode.insertChar(char, freshNode);
        currentNode = freshNode;
      }
    });
    this.inserCount++;
    return this;
  }
  printTrie() {
    console.log(JSON.stringify(this));
    return this;
  }

  findMaxXor(x) {
    if (this.inserCount === 0) {
      // If all integers are greater than ‘Ai’ in array/list ‘ARR’  then the answer to this ith query will be -1.
      return -1;
    }
    let result = "";
    let value = x.toString(2).padStart(8, "0");
    //console.log(`Input - ${value}`);
    let current = this.root;
    Array.from(value).forEach((item) => {
      let target = ("1".charCodeAt() - item.charCodeAt()).toString();
      // console.log(
      //   "Original Bit :  " +
      //     item +
      //     " || Reverse Bit to Maximise XOR : " +
      //     target
      // );

      if (current.isCharPresent(target)) {
        //console.log(`Wish Found : ${target}`);
        result = result.concat(target);
        current = current.getNextNode(target);
      } else {
        // console.log(`Wish Not Found : ${target}`);
        target = ("1".charCodeAt() - target.charCodeAt()).toString();
        //console.log(`Compormised Wish : ${target}`);
        result = result.concat(target);
        current = current.getNextNode(target);
      }
    });
    console.log(`Max XOR element - ${result} ===> ${parseInt(result, 2)}`);
    console.log(
      `${result}[${parseInt(result, 2)}] ^ ${value}[${parseInt(
        value,
        2
      )}] ==> ${(parseInt(result, 2) ^ parseInt(value, 2)).toString(2)} [${
        parseInt(result, 2) ^ parseInt(value, 2)
      }]`
    );
    return parseInt(result, 2);
  }

  findMaxXOROfQuiries(queries, arr, resultArr) {
    arr.sort((a, b) => a - b);
    console.log(`Sorted Input Array : ${JSON.stringify(arr)}`);
    queries.sort((a, b) => a[1] - b[1]);
    console.log(
      `Sorted Query Array Based on Arr element part of the Query : ${JSON.stringify(
        queries
      )}`
    );
    let CurrentIndexOfArray = 0;
    let CurrentItemOfArray = arr[CurrentIndexOfArray];
    queries.forEach((item) => {
      let xi = item[0];
      let ai = item[1];
      let pi = item[2];
      let res = -1;
      console.log(`Inserting in Trie Upto : ${ai}`);
      while (CurrentItemOfArray <= ai) {
        this.insertWord(CurrentItemOfArray.toString(2).padStart(8, "0"));
        CurrentIndexOfArray++;
        CurrentItemOfArray = arr[CurrentIndexOfArray];
      }
      res = this.findMaxXor(xi);
      console.log(`Searching for Max Xor  - ${xi} Is --> ${res} `);
      resultArr[pi] = res === -1 ? res : res ^ xi;
    });
  }
}

let arr1 = [6, 6, 0, 6, 8, 5, 6];
let arr2 = [1, 7, 1, 7, 8, 0, 2];
arr1 = arr1.map((item) => {
  return item.toString(2).padStart(8, "0");
});
console.log(`Input Elements Of Trie : ${arr1}`);
let trieObj = new Trie();
arr1.forEach((item) => {
  trieObj.insertWord(item);
});

//trieObj.printTrie();
console.log("****************************");
let x = 8;
trieObj.findMaxXor(x);
console.log("*********************************");
let maxResult = Number.MIN_SAFE_INTEGER;
let resArr = [];
arr2.forEach((item) => {
  let result = trieObj.findMaxXor(item);
  let tempXOR = result ^ item;
  if (tempXOR > maxResult) {
    maxResult = tempXOR;
    resArr[0] = item;
    resArr[1] = result;
    resArr[2] = `--->${maxResult}`;
  }
});
console.log(`Final Result ------${resArr}`);

console.log("**************** MAX XOR QUERY *******************");

let maxXorArr = [6, 6, 3, 5, 2, 4];
//QUERY [Xi , A[i] , Sequence]
let queries = [
  [6, 3, 0],
  [8, 1, 1],
  [12, 4, 2],
];
let result = [];
trieObj = new Trie();
trieObj.findMaxXOROfQuiries(queries, maxXorArr, result);
console.log(`Max Query Result ` + result);

maxXorArr = [0, 0, 0, 0, 0];
//QUERY [Xi , A[i] , Sequence]
queries = [
  [1, 0, 0],
  [1, 1, 1],
];
result = [];
trieObj = new Trie();
trieObj.findMaxXOROfQuiries(queries, maxXorArr, result);
console.log(`Max Query Result ` + result);
