class Node {
  node;
  endWord;
  countPrefix;
  constructor() {
    this.node = new Array(26).fill(null);
    this.endWord = 0;
    this.countPrefix = 0;
  }
  isCharPresent(char) {
    return this.node[this.getIndex(char)] != null ? true : false;
  }
  insertChar(char, newNode) {
    this.node[this.getIndex(char)] = newNode;
  }
  getNextNode(char) {
    return this.node[this.getIndex(char)];
  }

  getIndex(char) {
    return char.charCodeAt() - "a".charCodeAt();
  }
  addToendWord(value) {
    this.endWord = this.endWord + value;
  }

  addToCountPrefix(value) {
    this.countPrefix = this.countPrefix + value;
  }
  getendWord() {
    return this.endWord;
  }
  getCountPrefix() {
    return this.countPrefix;
  }
}

class Trie {
  root;
  constructor() {
    this.root = new Node(); //Initially Started with the Empty Root Node
  }

  //Insert Word In the Trie
  insertWord(word) {
    console.log(`Inserting Word - ${word}`);
    //Starting from the Root
    let currentNode = this.root;
    Array.from(word).forEach((char) => {
      //   let index = char.charCodeAt() - "a".charCodeAt();
      //   console.log(
      //     `Is Char - ${char} Present  at Index [${index}]?  : ${currentNode.isCharPresent(
      //       char
      //     )}`
      //   );
      if (currentNode.isCharPresent(char)) {
        currentNode = currentNode.getNextNode(char);
      } else {
        let freshNode = new Node();
        currentNode.insertChar(char, freshNode);
        currentNode = freshNode;
      }
      currentNode.addToCountPrefix(1); //Increase Prifix Count to the Next Node
    });
    //Now Just have the Leaf Node Make End With Increment by 1
    currentNode.addToendWord(1);

    return this;
  }
  printTrie() {
    console.log(JSON.stringify(this));
    return this;
  }

  deleteFullWord(word) {
    let current = this.root;
    console.log(`Assuming Entry Given always present - Deleting  - ${word}`);
    Array.from(word).every((char) => {
      //  console.log(`Searching for ${char}`);
      current = current.getNextNode(char);
      current.addToCountPrefix(-1);
      return true;
    });
    current.addToendWord(-1);
    return this;
  }

  searchPartialWord(word) {
    let current = this.root;
    let isPresent = false;
    console.log(`Searching Partialliy For the Word - ${word}`);
    Array.from(word).every((char) => {
      // console.log(`Searching for ${char}`);
      if (current.isCharPresent(char)) {
        current = current.getNextNode(char);
        isPresent = true;
        return true;
      } else {
        isPresent = false;
        return false; //Now it Will break
      }
    });
    if (isPresent) {
      console.log(
        `Word Is Present Partially & Count - ${current.getCountPrefix()}`
      );
    } else {
      console.log("Word Is NOT  Present Partially !!!");
    }
    return this;
  }

  searchFullWord(word) {
    let current = this.root;
    let isPresent = false;
    console.log(`Searching Fully For the Word - ${word}`);
    Array.from(word).every((char) => {
      //  console.log(`Searching for ${char}`);
      if (!current.isCharPresent(char)) {
        isPresent = false;
        return false;
      } else {
        current = current.getNextNode(char);
        isPresent = true;
        return true;
      }
    });
    if (isPresent && current.getendWord() > 0) {
      console.log(
        `Word Found Fully In Trie DS & Count - ${current.getendWord()}`
      );
    } else {
      console.log(`Word NOT Found Fully In Trie DS`);
    }
    return this;
  }
}

let trieObj = new Trie();
trieObj
  .insertWord("aniket")
  .insertWord("apple")
  .insertWord("apps")
  .insertWord("apxl")
  .insertWord("bac")
  .insertWord("apple")
  .insertWord("bat")
  .insertWord("apple")
  .printTrie();

console.log("******************************************");

trieObj
  .searchFullWord("bac")
  .searchFullWord("applet")
  .searchFullWord("apxl")
  .searchFullWord("apple")
  .searchFullWord("ba");

console.log("******************************************");
trieObj
  .searchPartialWord("appxl")
  .searchPartialWord("b")
  .searchPartialWord("app")
  .searchPartialWord("apl")
  .searchPartialWord("ba");

console.log("******************************************");

trieObj
  .searchFullWord("apple")
  .deleteFullWord("apple")
  .searchFullWord("apple")
  .deleteFullWord("apple")
  .deleteFullWord("apple")
  .searchFullWord("apple");
