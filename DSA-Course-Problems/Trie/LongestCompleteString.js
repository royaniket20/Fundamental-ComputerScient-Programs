class Node {
  node;
  flag;
  constructor() {
    this.node = new Array(26).fill(null);
    this.flag = false;
  }
  isCharPresent(char) {
    return this.node[this.getIndex(char)] != null ? true : false;
  }
  insertChar(char, newNode) {
    this.node[this.getIndex(char)] = newNode;
  }
  getNextNode(char) {
    return this.node[this.getIndex(char)];
  }

  getIndex(char) {
    return char.charCodeAt() - "a".charCodeAt();
  }
  setFlag(value) {
    this.flag = value;
  }

  getFlag() {
    return this.flag;
  }
}

class Trie {
  root;
  constructor() {
    this.root = new Node(); //Initially Started with the Empty Root Node
  }

  //Insert Word In the Trie
  insertWord(word) {
    console.log(`Inserting Word - ${word}`);
    //Starting from the Root
    let currentNode = this.root;
    Array.from(word).forEach((char) => {
      let index = char.charCodeAt() - "a".charCodeAt();
      console.log(
        `Is Char - ${char} Present  at Index [${index}]?  : ${currentNode.isCharPresent(
          char
        )}`
      );
      if (currentNode.isCharPresent(char)) {
        currentNode = currentNode.getNextNode(char);
      } else {
        let freshNode = new Node();
        currentNode.insertChar(char, freshNode);
        currentNode = freshNode;
      }
    });
    //Now Just have the Leaf Node Make Flag = true
    currentNode.setFlag(true);

    return this;
  }
  printTrie() {
    console.log(JSON.stringify(this));
    return this;
  }

  searchFullWordComplete(word) {
    let current = this.root;
    let isPresent = false;
    console.log(`Searching Completeness For the Word - ${word}`);
    Array.from(word).every((char) => {
      console.log(`Searching for ${char}`);
      if (current.isCharPresent(char) && current.getNextNode(char).getFlag()) {
        current = current.getNextNode(char);
        isPresent = true;
        return true;
      } else {
        isPresent = false;
        return false;
      }
    });
    if (isPresent && current.getFlag()) {
      console.log(`Word Prefix Found Fully In Trie DS`);
      return true;
    } else {
      console.log(`Word Prefix NOT Found Fully In Trie DS`);
      return false;
    }
  }

  findLongestCompleteString(arr) {
    let longest = "";
    arr.forEach((item) => {
      let result = this.searchFullWordComplete(item);
      console.log(result);
      if (result) {
        longest = item.length > longest.length ? item : longest;

        //Purely For Lexiographical Check
        if (longest.length === item.length && item < longest) {
          longest = item;
        }
      }
    });

    return longest;
  }
}

let trieObj = new Trie();
let inputArray = [
  "n",
  "ninja",
  "ning",
  "nini",
  "ni",
  "nin",
  "ninga",
  "ninj",
  "nenj",
];
inputArray.forEach((item) => {
  trieObj.insertWord(item);
});

console.log("******************************************");

console.log(
  `The Longest Complete String - ${trieObj.findLongestCompleteString(
    inputArray
  )}`
);

console.log("ninga".localeCompare("ninja"));
console.log("******************************************");
