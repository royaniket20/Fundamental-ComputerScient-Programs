import BST from "./BST.js";

let bst = new BST();
bst
  .insert(10)
  .insert(55)
  .insert(40)
  .insert(30)
  .insert(25)
  .insert(29)
  .insert(65)
  .insert(77)
  .insert(88)
  .insert(85)
  .insert(87)
  .insert(62)
  .insert(39)
  .insert(42);

/**
 * Going  Visit Left Right
 */
let preOrderTraversal = function preOrder(current, arr) {
  if (current != null) {
    arr.push(current.value);
    preOrder(current.left, arr);
    preOrder(current.right, arr);
  }
};

let preOrderTraversalIterative = function preOrder(current, arr) {
  let stack = []; // Use it to Store Future Traversal Points
  //Important Condition - Keep on going Until Stack is Exhausted - But to start with we have currect!=null
  //When Current becomes Null atleast stack will have some entry to travel other areas - when that becomes Empty -
  //we must have traversed all area
  while (current != null || stack.length > 0) {
    if (current != null) {
      arr.push(current.value); //Print Data
      stack.push(current); //Store For Return Back When needed to travel other direction
      current = current.left; //Traversing to the Left
    } else {
      //Now current Becomes Null - Thean Means Now its time to Go Right
      current = stack.pop(); //We are Poping the Top element - But No need to Print it Because its get printed during Pushing
      current = current.right; //Now Explore the right path
    }
  }
};
/**
 * Going   Left Visit Right
 */
let inOrderTraversal = function inOrder(current, arr) {
  if (current != null) {
    inOrder(current.left, arr);
    arr.push(current.value);
    inOrder(current.right, arr);
  }
};

let inOrderTraversalIterative = function inOrder(current, arr) {
  let stack = [];
  while (current != null || stack.length > 0) {
    if (current != null) {
      stack.push(current); //Stopre for future Print
      current = current.left; // Keep on traaversing Left
    } else {
      current = stack.pop(); //Now left Route exhausted - Now Pop and Print
      arr.push(current.value);
      current = current.right; //Expore the Right path
    }
  }
};

/**
 * Going   Left  Right Visit
 */
let postOrderTraversal = function postOrder(current, arr) {
  if (current != null) {
    postOrder(current.left, arr);
    postOrder(current.right, arr);
    arr.push(current.value);
  }
};

let postOrderTraversalIterative = function postOrder(current, arr) {
  let stack = [];
  while (current != null || stack.length > 0) {
    if (current != null) {
      current.flag = 1; // to mark you have visited the Tag 
      stack.push(current); //Store for future Print 
      current = current.left; //Keep traversing Left 
    } else {  //Now current becomes Null Explore Right 
      let temp = stack.pop(); //Pop the Top elelemnt 
      if (temp.flag === 1 ) { //That means I have visited it - Poped Once to Get the reference to traverse the Right and Keep it back 
        temp.flag = 0;  // Next time I will Pop It I will print it  - So marking 0 
        stack.push(temp); // Keep back 
        current = temp.right; //Keep traversing Right 
      } else {
        arr.push(temp.value); //Data Printing as its marked 0 means - I have visited already for traversal - Now time to Print 
      }
    }
  }
};

/**
 * Process of Level Order traversal
 * First take Root element - Push into Queue
 * Then Until Queus is empty - Print then Element and Push Left [If available] and Right Child [If available] inside the queue
 * Repet the same until you traverse all
 */
function levelOrderTraversal(current, arr) {
  let queue = [];
  //Use Push for Enqueue and shift for Dequeu using Array
  //Push the First element - Most probably Root
  queue.push(current); //Enqueueing
  while (queue.length > 0) {
    let temp = queue.shift(); //Dqueue from End
    arr.push(temp.value); //Print the Data
    if (temp.left != null) {
      queue.push(temp.left); //Enqueueing
    }
    if (temp.right != null) {
      queue.push(temp.right); //Enqueueing
    }
  }
}

//Start from Root Node
let current = bst.root;
let arr = [];

preOrderTraversal(current, arr);
console.log(`PRE ORDER RECURSIVE -- ${arr}`);

arr = [];
preOrderTraversalIterative(current, arr);
console.log(`PRE ORDER ITERATIVE -- ${arr}`);
arr = [];
inOrderTraversal(current, arr);
console.log(`IN ORDER RECURSIVE -- ${arr}`);

arr = [];
inOrderTraversalIterative(current, arr);
console.log(`IN ORDER ITERATIVE -- ${arr}`);
arr = [];
postOrderTraversal(current, arr);
console.log(`POST ORDER RECURSIVE -- ${arr}`);

arr = [];
postOrderTraversalIterative(current, arr);
console.log(`POST ORDER ITERATIVE -- ${arr}`);
console.log("******************************************");
bst = new BST();
bst.insert(30).insert(29).insert(31).insert(28).insert(35);
current = bst.root;
arr = [];
levelOrderTraversal(current, arr);
console.log(`LEVEL ORDER USING QUEUE -- ${arr}`);
