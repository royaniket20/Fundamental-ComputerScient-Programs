import BST from "./BST.js";
let bst = new BST();
bst
  .insert(60)
  .insert(50)
  .insert(75)
  .insert(25)
  .insert(55)
  .insert(56)
  .insert(57)
  .insert(58)
  .insert(70)
  .insert(80)
  .insert(69)
  .insert(71)
  .insert(82)
  .insert(83)
  .insert(84);
//For Left View we want to Visit the Left Nodes on Each Level First time so that we can POut that On Map
//In future if we again come back to this Level we will Jut Ignore - as Left Most Element of that level is
//already in the Map
let leftView = function leftView(node, arr, map, level) {
  if (node == null) {
    return; //Base condition for recursion
  }
  //Keep track of Level Nodes First Visit
  if (!map.has(level)) {
    map.set(level, node.value);
    arr.push(level + "--->" + node.value + "|");
  }
  //Do  Post Order --- Left->Right->Root
  leftView(node.left, arr, map, level + 1);
  leftView(node.right, arr, map, level + 1);
};
//For Right  View we want to Visit the Right  Nodes on Each Level First time so that we can POut that On Map
//In future if we again come back to this Level we will Jut Ignore - as Right  Most Element of that level is
//already in the Map
let rightView = function rightView(node, arr, map, level) {
  if (node == null) {
    return; //Base condition for recursion
  }
  //Keep track of Level Nodes First Visit
  if (!map.has(level)) {
    map.set(level, node.value);
    arr.push(level + "--->" + node.value + "|");
  }
  //Do reverse PreOrder --- Right->Left-->Root
  rightView(node.right, arr, map, level + 1);
  rightView(node.left, arr, map, level + 1);
};
/**
 * For Top View we need to something like that -
 * Here we need to decide Vertical Levels for keep track for Top View
 * Like Root element is at Vertical Level 0
 * Root.left == Vertical Level -1
 * Root.right == Vertical Level +1
 * Root.left.left == Vertical Level -2
 * Root.right.right == Vertical Level +2
 *
 * Root.left.right == Vertical Level 0
 * Root.right.left == Vertical Level 0
 *
 * Now idea is for each Vertical Level Keep track of First element you Visit
 * Do a normal DFS traversal
 *
 */

let topView = function topView(current, arr, map, level) {
  if (current == null) {
    return; //Base condition reached
  }
  if (!map.has(level)) {
    //Instead of Pushing Blindly to the logic array using some basic sorting logic
    // Printing will be in this order ...,-2,-1,0,+1,+2,...
    if (level === 0) {
      arr.push(level + "--->" + current.value);
    } else if (level > 0) {
      arr.push("|" + level + "--->" + current.value);
    } else {
      arr.unshift(level + "--->" + current.value + "|");
    }
    map.set(level, current.value);
  }
  topView(current.right, arr, map, level + 1);
  topView(current.left, arr, map, level - 1);
};

let buttomView = function buttomView(current, arr, map, level) {
  if (current == null) {
    return; //Base condition reached
  }
  //Keep  Updating Values - when  we will reach the Leaf Node thats will be buttom View
  map.set(level, [current.value]);
  buttomView(current.left, arr, map, level - 1);
  buttomView(current.right, arr, map, level + 1);
};

let map = new Map();
let arr = [];
leftView(bst.root, arr, map, 0);
console.log(`The Left View -   ${arr}`);
map.clear();
arr = [];
rightView(bst.root, arr, map, 0);
console.log(`The Right View -   ${arr}`);
map.clear();
arr = [];
topView(bst.root, arr, map, 0);
console.log(`The Top View -   ${arr}`);
map.clear();
arr = [];
buttomView(bst.root, arr, map, 0);
//Instead of Pushing Blindly to the logic array using some basic sorting logic
// Printing will be in this order ...,-2,-1,0,+1,+2,...
new Map([...map.entries()].sort((a, b) => a[0] - b[0])).forEach(
  (value, key) => {
    arr.push(key + "--->" + value + "|");
  }
);
console.log(`The Button View -   ${arr}`);
