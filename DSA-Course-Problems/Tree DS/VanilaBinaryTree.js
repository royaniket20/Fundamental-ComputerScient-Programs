class BinaryTreeNode {
  value;
  left;
  right;
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

export default class BinaryTree {
  root;
  constructor() {
    this.root = null;
  }

  //Insert Data to Binary Tree
  insert(value, current, flag) {
    console.log(`Inserting Node ${value} at Specific Node `);
    if (current != null) {
      if (flag === "LEFT") {
        if (current.left != null) {
          console.log(`Some Value Already Present!! - ${current.left.value}`);
        } else {
          //New Value getting added at Left
          let node = new BinaryTreeNode(value);
          current.left = node;
          current = current.left;
          console.log(` Left Value Inserted !!`);
        }
      } else if (flag === "RIGHT") {
        if (current.right != null) {
          console.log(
            `Some Value Already Present!! --  ${current.right.value}`
          );
        } else {
          //New Value getting added at Right
          let node = new BinaryTreeNode(value);
          current.right = node;
          current = current.right;
          console.log(` Right Value Inserted !!`);
        }
      } else {
        console.log(`NO VALID OPTION GIVEN!!`);
      }
    } else {
      //First Value getting added
      let node = new BinaryTreeNode(value);
      this.root = node;
      current = this.root;
      console.log(`New Value Inserted !!`);
    }
    return current;
  }
/**
 * Process of Level Order traversal 
 * First take Root element - Push into Queue 
 * Then Until Queus is empty - Print then Element and Push Left [If available] and Right Child [If available] inside the queue 
 * Repet the same until you traverse all 
 */
  levelOrderTraversal() {
    let current = this.root;
    let arr = [];
    let queue = [];
    //Use Push for Enqueue and shift for Dequeu using Array
    //Push the First element - Most probably Root
    queue.push(current);
    while (queue.length > 0) {
      let temp = queue.shift();
      arr.push(temp.value);
      if (temp.left != null) {
        queue.push(temp.left);
      }
      if (temp.right != null) {
        queue.push(temp.right);
      }
    }
    console.log(`Level Order Traversal --- ${arr}`);
  }
}
