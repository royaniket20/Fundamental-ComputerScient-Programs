import BST from "./BST.js";

let bst = new BST();
bst
  .insert(10)
  .insert(20)
  .insert(30)
  .insert(40)
  .insert(50)
  .insert(49)
  .insert(55)
  .insert(48)
  .insert(47)
  .insert(46)
  .insert(3)
  .insert(32)
  .insert(31)
  .insert(310)
  .insert(33);

let findMaxNodeOfBST = function findMax(node) {
  if (node == null) {
    return Number.MIN_SAFE_INTEGER;
  }
  return Math.max(node.value, findMax(node.right));
};

let findMinNodeOfBST = function findMin(node) {
  if (node == null) {
    return Number.MAX_SAFE_INTEGER;
  }
  return Math.min(node.value, findMin(node.left));
};

console.log(`The Max Node***************-  ${findMaxNodeOfBST(bst.root)}`);
console.log(`The Min Node***************-  ${findMinNodeOfBST(bst.root)}`);
