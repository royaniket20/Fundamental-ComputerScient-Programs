import BinaryTree from "./VanilaBinaryTree.js";
let bt = new BinaryTree();
let current = bt.insert(-10, bt.root, null);
let currentL = bt.insert(9, current, "LEFT");
let currentR = bt.insert(20, current, "RIGHT");
let currentR1 = bt.insert(15, currentR, "RIGHT");
// currentR1 = bt.insert(15, currentR1, "RIGHT");
// currentR1 = bt.insert(8, currentR1, "RIGHT");
// currentR1 = bt.insert(9, currentR1, "RIGHT");
// currentR1 = bt.insert(10, currentR1, "RIGHT");
let currentL1 = bt.insert(7, currentR, "LEFT");
// currentL1 = bt.insert(5, currentL1, "LEFT");
// currentL1 = bt.insert(9, currentL1, "LEFT");
// currentL1 = bt.insert(10, currentL1, "LEFT");

bt.levelOrderTraversal();
let maxPathSumFunc = function maxPathSum(node, maxSum) {
  if (node == null) {
    return 0;
  }
  let leftSum = maxPathSum(node.left, maxSum);
  let rightSum = maxPathSum(node.right, maxSum);
  // Here we are using the calculated Sum of Left and Right Sub tree to Find Max path Sum
  maxSum[0] = Math.max(maxSum[0], leftSum + rightSum + node.value);
  console.log(`Till Now Max Path Sum - ${maxSum[0]}`);
  //Returning the Max Sum for the Tree
  return Math.max(leftSum, rightSum) + node.value;
};
let maxSum = new Array(1).fill(0);
maxPathSumFunc(bt.root, maxSum);
console.log(`Final Max Sum ############ ${maxSum}`);
