import BST from "./BST.js";

let bst = new BST();
bst
  .insert(55)
  .insert(40)
  .insert(30)
  .insert(25)
  .insert(29)
  .insert(65)
  .insert(77)
  .insert(88)
  .insert(85)
  .insert(87)
  .insert(62)
  .insert(39)
  .insert(42);

let levelOrderTraversal = function (current, arr) {
  let queue = [];
  //Use Push for Enqueue and shift for Dequeu using Array
  //Push the First element - Most probably Root
  queue.push(current);
  while (queue.length > 0) {
    let temp = queue.shift();
    arr.push(temp.value);
    if (temp.left != null) {
      queue.push(temp.left);
    }
    if (temp.right != null) {
      queue.push(temp.right);
    }
  }
};
/**
 * Here the Idea is you are doing Normal Level order traversal - That is Just for each Node - Finding child Nodes and Pushing thme into Queue
 * But because we need to switch printing direction when we move from one level to another - So we collect childs of next level in a temp Ququq
 * Miantain a flag to detect level - hence printing direction
 * Collection result of each level on a tempesult array
 * When one level is exhaused
 * 1.Based on level we push the result to main array
 * 2.Dump all the nodes for next level from temp queue to main queue
 * 3.Increase Level
 * clear temp array and temp queue
 */
let zigzagOrderTraversal = function (current, arr) {
  let queue = [];
  //Use Push for Enqueue and shift for Dequeu using Array
  //Push the First element - Most probably Root
  queue.push(current);
  let level = 0;
  let tempArr = [];
  let tempQueue = [];
  while (queue.length > 0) {
    console.log(`I am right Now at level - ${level}`);
    let temp = queue.shift();
    tempArr.push(temp.value); //Printing Data
    //Now if you are at level 0
    /**
     * Ar level 1 you will traverse from right to left
     * at level 2 you will traverse from left to Right
     * so on ...
     */
    if (temp.left != null) {
      tempQueue.push(temp.left);
    }
    if (temp.right != null) {
      tempQueue.push(temp.right);
    }
    //Increase Level Once the All nodes traversed on that level
    if (queue.length === 0) {
      console.log(`Current Value of tempArray  - ${tempArr}`);
      //Copy to main array
      if (level % 2 == 0) {
        //0,2,4,6
        for (let index = 0; index < tempArr.length; index++) {
          arr.push(tempArr[index]);
        }
      } else {
        //1,3,5,7
        for (let index = tempArr.length - 1; index >= 0; index--) {
          arr.push(tempArr[index]);
        }
      }
      tempArr = [];
      //Now one level is completed
      level++; //Increase the level Now
      //Fill the main queue with Collected elements of Next Queue
      queue = [...tempQueue];
      tempQueue = [];
    }
  }
};

let verticalOrderTraversal = function (current, map) {
  let queue = [];
  //Use Push for Enqueue and shift for Dequeu using Array
  //Push the First element - Most probably Root
  queue.push({ node: current, level: 0 });
  while (queue.length > 0) {
    let temp = queue.shift();
    let level = temp.level;

    if (map.has(level)) {
      map.get(level).push(temp.node.value);
    } else {
      let arr = new Array();
      arr.push(temp.node.value);
      map.set(level, arr);
    }
    if (temp.node.left != null) {
      queue.push({ node: temp.node.left, level: level - 1 });
    }
    if (temp.node.right != null) {
      queue.push({ node: temp.node.right, level: level + 1 });
    }
  }
};

//Start from Root Node
let current = bst.root;
let arr = [];
console.log(
  `Doing Level Order Traversal of Binary Search Tree - ${current.value}`
);
levelOrderTraversal(current, arr);
console.log(`LEVEL ORDER USING QUEUE -- ${arr}`);

current = bst.root;
let map = new Map();
console.log(
  `Doing Vertical Order Traversal of Binary Search Tree - ${current.value}`
);
verticalOrderTraversal(current, map);
console.log(`VERTICAL ORDER USING QUEUE --`);
console.log(map);

current = bst.root;
arr = [];
console.log(
  `Doing ZigZag  Order Traversal of Binary Search Tree - ${current.value}`
);
zigzagOrderTraversal(current, arr);
console.log(`ZIG-ZAG ORDER TRAVERSAL -- ${arr}`);
