class BinaryTreeNode {
  value;
  left;
  right;
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  root;
  constructor() {
    this.root = null;
  }

  //Insert Data to BST
  insert(value) {
    console.log(`Inserting Node ${value}`);
    let current = this.root;
    if (current != null) {
      while (current != null) {
        if (current.value > value) {
          if (current.left != null) {
            current = current.left;
          } else {
            //New Value getting added at Left
            let node = new BinaryTreeNode(value);
            current.left = node;
            console.log(` Left Value Inserted !!`);
            break;
          }
        } else if (current.value < value) {
          if (current.right != null) {
            current = current.right;
          } else {
            //New Value getting added at Right
            let node = new BinaryTreeNode(value);
            current.right = node;
            console.log(` Right Value Inserted !!`);
            break;
          }
        } else {
          console.log(`Value Already Present!!`);
          break;
        }
      }
    } else {
      //First Value getting added
      let node = new BinaryTreeNode(value);
      this.root = node;
      console.log(`New Value Inserted !!`);
    }
    return this;
  }

  //Delete Data from BST
  delete(value) {
    console.log(`Deleting Node ${value}`);
    let current = this.root;
    let parent = this.root;
    while (true) {
      if (current == null) {
        console.log(`Value Not Found !`);
        break;
      }
      if (current.value > value) {
        parent = current;
        current = current.left;
      } else if (current.value < value) {
        parent = current;
        current = current.right;
      } else {
        //Value is Found - Not check for Position
        if (current.left == null && current.right == null) {
          console.log(`Found a leaf Node for deletion`);
          if (parent.left === current) {
            parent.left = null;
          } else {
            parent.right = null;
          }
        } else if (current.left == null || current.right == null) {
          console.log(`Found a Single Child Node for deletion`);
          if (parent.left === current) {
            parent.left = current.left != null ? current.left : current.right;
          } else {
            parent.right = current.left != null ? current.left : current.right;
          }
        } else {
          console.log(`Found a Double Child Node for deletion`);
          this._deleteNodeWithTwoChild(current);
        }
        break;
      }
    }
    return this;
  }

  _deleteNodeWithTwoChild(current) {
    console.log(`Double Child Node to be deleted - ${JSON.stringify(current)}`);
    //First Go to Right Child
    let rightChild = current.right;
    let extrementLeftChildOfRightChild = rightChild;
    //Now go till Extrement Left Child
    while (extrementLeftChildOfRightChild.left != null) {
      rightChild = extrementLeftChildOfRightChild;
      extrementLeftChildOfRightChild = extrementLeftChildOfRightChild.left;
    }
    console.log(
      `${rightChild.left != null ? rightChild.left.value : null}<-----${
        rightChild.value
      }------>${rightChild.right != null ? rightChild.right.value : null}`
    );
    console.log(
      `${
        extrementLeftChildOfRightChild.left != null
          ? extrementLeftChildOfRightChild.left.value
          : null
      }<-----${extrementLeftChildOfRightChild.value}------>${
        extrementLeftChildOfRightChild.right != null
          ? extrementLeftChildOfRightChild.right.value
          : null
      }`
    );
    //Replace Value of the Target Node with Intended Node
    current.value = extrementLeftChildOfRightChild.value;

    //Now Delete Extreme Left Child of Right Child of Target Node
    //Remember this Node always Have Left Child is Null and Right Child May or May Not repsent
    if (extrementLeftChildOfRightChild != rightChild) {
      rightChild.left = extrementLeftChildOfRightChild.right;
    } else {
      //Here Right Node of the Target Node is leaf Node
      current.right = null;
    }
    //Making  Totally ready for garbage Collection - All reationship broken from Tree
    extrementLeftChildOfRightChild.left = null;
    extrementLeftChildOfRightChild.right = null;
  }

  //Traverse the Tree
  traverse() {
    console.log(JSON.stringify(this.root));
    return this;
  }
  //Lookup for a Value
  lookup(value) {
    console.log(`Serching Node ${value}`);
    let current = this.root;
    let result = "Root";
    let isResultFound = false;
    while (current != null) {
      if (current.value > value) {
        result = result + "--" + current.value + "-->LEFT";
        current = current.left;
      } else if (current.value < value) {
        result = result + "--" + current.value + "-->RIGHT";
        current = current.right;
      } else {
        result = result + "--" + current.value + "-->Found!!";
        isResultFound = true;
        break;
      }
    }
    if (isResultFound) {
      console.log(result);
    } else {
      result = result + "--" + value + "-->Not Found!!";
      console.log(result);
    }
    return this;
  }
}

let bst = new BinarySearchTree();
bst
  .insert(9)
  .insert(4)
  .insert(6)
  .insert(20)
  .insert(170)
  .insert(15)
  .insert(1)
  .insert(20)
  .lookup(15)
  .lookup(155)
  .traverse()
  .delete(1)
  .delete(4)
  .insert(169)
  .insert(168)
  .insert(160)
  .insert(165)
  .insert(164)
  .insert(167)
  .insert(13)
  .insert(12)
  .traverse()
  .delete(20)
  .traverse();
