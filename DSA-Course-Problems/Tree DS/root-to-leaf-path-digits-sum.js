// 1. Given root of a binary tree having nodes with numbers from 0 to 9 [both inclusive],
//    return sum of numbers constructed if we travel from root to leaf numbers.

//    5
//  3   6
// 2 4

// 124+125+13 = 262 


import BST from "./BST.js";

let bst = new BST();
bst
  .insert(5)
  .insert(6)
  .insert(3)
  .insert(4)
  .insert(2);

function findTreeSum(current){
  let result = doGetNumString(current , 0)
  console.log(`Final result is ====` + result);
}

function doGetNumString(current , num){
  //In case empty tree 
if(current === null){
  return 0;
}
console.log("-----"+current.value);
num = num*10 + current.value;
//if its a leaf Node Now return the Number to be added eventually 
if(current.left === null && current.right === null ){
  return num;
}
//In case Not a leaf Node do recursive call 
let leftNum = doGetNumString(current.left , num)
console.log(`Left Number = ${leftNum}`);

let rightNum = doGetNumString(current.right , num)
console.log(`Right Number = ${rightNum}`);

let sum =  leftNum + rightNum;
console.log(`Sum is Now = `+ sum);
return sum;
}


findTreeSum(bst.root);

