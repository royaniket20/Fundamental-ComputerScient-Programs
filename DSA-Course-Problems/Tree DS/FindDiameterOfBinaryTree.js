/**
 * Here though Process is Find the Max Height Formula - Not for each Node whne you Find left Height and Right Height - at that Point
 * Calculate Diameter also by adding LH + RH + 1 [1 for the Node itself ]
 * p.s - diameter of the Tree - Logest path between two Nodes of a binary tree 
 * Eventually Send back Max Path 
 */

import BinaryTree from "./VanilaBinaryTree.js";
let bt = new BinaryTree();
let current = bt.insert(1, bt.root, null);
let currentL = bt.insert(2, current, "LEFT");
let currentR = bt.insert(3, current, "RIGHT");
let currentR1 = bt.insert(6, currentR, "RIGHT");
currentR1 = bt.insert(7, currentR1, "RIGHT");
currentR1 = bt.insert(8, currentR1, "RIGHT");
currentR1 = bt.insert(9, currentR1, "RIGHT");
currentR1 = bt.insert(10, currentR1, "RIGHT");
let currentL1 = bt.insert(4, currentR, "LEFT");
currentL1 = bt.insert(5, currentL1, "LEFT");
currentL1 = bt.insert(9, currentL1, "LEFT");
currentL1 = bt.insert(10, currentL1, "LEFT");

bt.levelOrderTraversal();

let diameterOfBinaryTree = function diameterOfBinaryTree(node, maxDiameter) {
  if (node == null) {
    return 0;
  }
  //Here we are tring to Find the Height of Left Path  from current Node
  let leftHeight = diameterOfBinaryTree(node.left, maxDiameter);
  //Find the height of Right Path from current node
  let rightHeight = diameterOfBinaryTree(node.right, maxDiameter);
  //Keeping track of Max diaameter of each Node Until we get Final result 
  maxDiameter[0] = Math.max(maxDiameter[0], leftHeight + rightHeight);

  //Returning the Max Height for the Tree - Here idea is we know at current Point whats the left path Height 
  //and whats the right path height - so we decide if we want to go left path or Right path to attend max path 
  let maxHeight = Math.max(leftHeight, rightHeight) + 1; // +1 to add current Nodes contribution to the Height 
  console.log(`Till Now Max Path - ${maxDiameter[0]}`);
  console.log(`Till Now Max Height - ${maxHeight}`);
  return maxHeight; //Returning the Max height we can achieve for current Node 
};
let maxDiameter = new Array(1).fill(0);
diameterOfBinaryTree(bt.root, maxDiameter);
console.log(`Final Max Diameter ############ ${maxDiameter}`);
