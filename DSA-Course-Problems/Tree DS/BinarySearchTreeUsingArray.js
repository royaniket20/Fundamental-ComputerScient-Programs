class BinarySearchTree {
  arr;
  nodeCount;
  capacity;
  constructor(size) {
    this.arr = new Array();
    this.nodeCount = 0;
    this.capacity = size;
  }
  /**
   * For Array  implementation it is Rule that
   * If an Element is in Nth Position in Array - Left child will be at 2*n position
   * Right child will be at 2*n +1 position in array
   * For this calculation it is better to assume array start with 1 index rather than 0 index - else 2*0 == 0 = Will give wrong position
   */
  //Insert Data to BST
  insert(value) {
    console.log(`Inserting Node ${value}`);
    if (this.capacity === this.nodeCount) {
      console.log(`Tree has Reached Capacity`);
    } else {
      let currentIndex = 1;
      let current = this.arr[currentIndex];
      if (current != undefined) {
        //Means - this have left or Right child - we need to traverse
        while (current != undefined) {
          if (current > value) {
            //Traverse the left child
            let leftIndex = currentIndex * 2; //Finding left child index
            if (this.arr[leftIndex] != undefined) {
              currentIndex = leftIndex; //Updating Current Index so that Next iteration can start from here
              current = this.arr[currentIndex];
            } else {
              //New Value getting added at Left
              this.arr[leftIndex] = value;
              this.nodeCount++;
              console.log(` Left Value Inserted !!`);
              break;
            }
          } else if (current < value) {
            //Traverse the Right  child
            let rightIndex = currentIndex * 2 + 1;
            if (this.arr[rightIndex] != undefined) {
              currentIndex = rightIndex; //Updating Current Index so that Next iteration can start from here
              current = this.arr[currentIndex];
            } else {
              //New Value getting added at Right
              this.arr[rightIndex] = value;
              this.nodeCount++;
              console.log(` Right Value Inserted !!`);
              break;
            }
          } else {
            console.log(`Value Already Present!!`);
            break;
          }
        }
      } else {
        //First Value getting added
        this.arr[currentIndex] = value;
        this.nodeCount++;
        console.log(`New Value Inserted !!`);
      }
    }
    return this;
  }

  //Delete Data from BST
  delete(value) {
    //TODO - Similar to Linked List One -Need to Implement
    return this;
  }
  //Traverse the Tree
  traverse() {
    console.log(JSON.stringify(this.arr.slice(1)));
    return this;
  }
  //Lookup for a Value
  lookup(value) {
    console.log(`Serching Node ${value}`);

    let currentIndex = 1;
    let current = this.arr[currentIndex];
    let result = "Root";
    let isResultFound = false;
    while (current != undefined) {
      if (current > value) {
        result = result + "--" + current + "-->LEFT";
        let leftIndex = currentIndex * 2;
        currentIndex = leftIndex;
        current = this.arr[currentIndex];
      } else if (current < value) {
        result = result + "--" + current + "-->RIGHT";
        let rightIndex = currentIndex * 2 + 1;
        currentIndex = rightIndex;
        current = this.arr[currentIndex];
      } else {
        result = result + "--" + current + "-->Found!!";
        isResultFound = true;
        break;
      }
    }
    if (isResultFound) {
      console.log(result);
    } else {
      result = result + "--" + value + "-->Not Found!!";
      console.log(result);
    }
    return this;
  }
}

let bst = new BinarySearchTree(12);
bst
  .traverse()
  .insert(9)
  .insert(4)
  .insert(6)
  .insert(20)
  .insert(170)
  .traverse()
  .insert(170)
  .insert(15)
  .insert(1)
  .insert(20)
  .traverse()
  // .delete(1)
  // .delete(4)
  // .delete(170)
  // .delete(999)
  // .delete(1)
  // .traverse();
  .lookup(15)
  .lookup(155)
  .insert(155)
  .insert(166)
  .insert(177)
  .insert(188)
  .insert(180)
  .lookup(155)
  .lookup(180)
  .lookup(188)
  .traverse();
