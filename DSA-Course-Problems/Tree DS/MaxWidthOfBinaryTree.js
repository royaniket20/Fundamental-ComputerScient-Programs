//width of a Binary tree is finding a level bounded by extrement left and extreme Right Nodes - And then Find Number of Nodes Possible to that Level

import BST from "./BST.js";

let bst = new BST();
bst.insert(30).insert(25).insert(35).insert(24).insert(26).insert(38);

let findMaxWidthOfBinaryTree = function findWidth(node, tracker, level) {
  if (node == null) {
    return; //Base case for recuresion
  }
  console.log(
    `Tracker Content Internal= ${JSON.stringify(tracker)} | ${level}`
  );
  if (level < 0) {
    tracker.leftMax = Math.max(Math.abs(level), tracker.leftMax); // Because we are traversing Left
  }
  findWidth(node.left, tracker, level - 1);
  if (level > 0) {
    tracker.rightMax = Math.max(Math.abs(level), tracker.rightMax); // Because we are traversing Right
  }
  findWidth(node.right, tracker, level + 1);
};
//0 based level Number
function findMaxNodeOfALevel(level) {
  let count = 1; //For level 0
  for (let i = 1; i <= level; i++) {
    count = count * 2;
  }
  return count;
}

let tracker = {
  leftMax: 0,
  rightMax: 0,
};
findMaxWidthOfBinaryTree(bst.root, tracker, 0);
console.log(`Tracker Content = ${JSON.stringify(tracker)}`);
console.log(
  `Max Width ------------------- ${findMaxNodeOfALevel(
    Math.min(tracker.leftMax, tracker.rightMax)
  )}`
);

bst = new BST();
bst.insert(10).insert(8).insert(7).insert(11);

tracker = {
  leftMax: 0,
  rightMax: 0,
};
findMaxWidthOfBinaryTree(bst.root, tracker, 0);
console.log(`Tracker Content = ${JSON.stringify(tracker)}`);
console.log(
  `Max Width ------------------- ${findMaxNodeOfALevel(
    Math.min(tracker.leftMax, tracker.rightMax)
  )}`
);

bst = new BST();
bst
  .insert(10)
  .insert(8)
  .insert(6)
  .insert(4)
  .insert(12)
  .insert(14)
  .insert(16)
  .insert(7)
  .insert(5)
  .insert(11);

tracker = {
  leftMax: 0,
  rightMax: 0,
};
findMaxWidthOfBinaryTree(bst.root, tracker, 0);
console.log(`Tracker Content = ${JSON.stringify(tracker)}`);
console.log(
  `Max Width ------------------- ${findMaxNodeOfALevel(
    Math.min(tracker.leftMax, tracker.rightMax)
  )}`
);
