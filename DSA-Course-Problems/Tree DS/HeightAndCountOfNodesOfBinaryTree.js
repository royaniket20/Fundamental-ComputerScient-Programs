import BST from "./BST.js";

let bst = new BST();
bst
  .insert(10)
  .insert(20)
  .insert(30)
  .insert(40)
  .insert(50)
  .insert(49)
  .insert(55)
  .insert(48)
  .insert(47)
  .insert(46)
  .insert(3)
  .insert(32)
  .insert(31)
  .insert(310)
  .insert(33);

let findMaxHeightOfBinaryTree = function findHeight(node) {
  if (node == null) {
    return 0;
  } else {
    let leftHeight = findHeight(node.left);
    let rightHeight = findHeight(node.right);
    let height = Math.max(leftHeight, rightHeight) + 1; //+1 for Current Height Addtiton
    return height;
  }
};
let findTotalNodesOfBinaryTree = function findCount(node) {
  if (node == null) {
    return 0;
  } else {
    let leftCount = findCount(node.left);
    let rightCount = findCount(node.right);
    let count = leftCount + rightCount + 1; //+1 for Current Node Count Addtiton
    return count;
  }
};

let findTotalLeafNodesOfBinaryTree = function findLeafCount(node) {
  if (node == null) {
    return 0;
  } else if (node.left == null && node.right == null) {
    return 1;
  } else {
    let leftCount = findLeafCount(node.left);
    let rightCount = findLeafCount(node.right);
    let leafCount = leftCount + rightCount;
    return leafCount;
  }
};

console.log(`Max Height of the Tree = ${findMaxHeightOfBinaryTree(bst.root)}`);
console.log(
  `Max Count Nodes of the Tree = ${findTotalNodesOfBinaryTree(bst.root)}`
);
console.log(
  `Leaf Count Nodes of the Tree = ${findTotalLeafNodesOfBinaryTree(bst.root)}`
);
bst = new BST();
bst.insert(10).insert(8).insert(11);
console.log(`Max Height of the Tree = ${findMaxHeightOfBinaryTree(bst.root)}`);
console.log(
  `Max Count Nodes of the Tree = ${findTotalNodesOfBinaryTree(bst.root)}`
);
console.log(
  `Leaf Count Nodes of the Tree = ${findTotalLeafNodesOfBinaryTree(bst.root)}`
);
