class BinaryTreeNode {
  value;
  left;
  right;
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  root;
  constructor() {
    this.root = null;
  }
  insert(value) {
    console.log(`Inserting Node ${value}`);
    this.root = this.insertRec(value, this.root);
    return this;
  }
  //Insert Data to BST
  insertRec(value, node) {
    //Base Case
    if (node == null) {
      //First Value getting added
      let node = new BinaryTreeNode(value);
      console.log(`Creating New Node - ${node.value}`);
      return node;
    }
    if (node.value > value) {
      console.log(`Travel to Left for ${node.value}`);
      node.left = this.insertRec(value, node.left);
    } else if (node.value < value) {
      console.log(`Travel to Right for ${node.value}`);
      node.right = this.insertRec(value, node.right);
    } else {
      console.log(`Value already Present`);
    }
    //This Will always Return Same Node that you send Initially
    //This is required so that Root can be initialized for an Empty tree and Consecutive calls
    //can Build Up tree by starting from Rooy

    return node;
  }

  //Delete Data from BST
  delete(value) {
    console.log(`Deleting Node ${value}`);
    this.root = this.deleteRec(this.root, value);
    return this;
  }

  deleteRec(node, value) {
    if (node == null) {
      console.log(`Value Not Found`);
      return node;
    }
    if (node.value > value) {
      console.log(
        `Calling Left Subtree for the Deletion ${value}<--- ${node.value} `
      );
      node.left = this.deleteRec(node.left, value);
    } else if (node.value < value) {
      console.log(
        `Calling Right Subtree for the Deletion ${node.value} --> ${value}`
      );
      node.right = this.deleteRec(node.right, value);
    } else {
      //Now Value is Found we need to Delete It
      if (node.left == null && node.right == null) {
        console.log(
          `Leaf Node Found for deletion - ${node.left}<----${node.value} --->${node.right}`
        );
        return null; //This will break the Link with Parent
      } else if (node.left == null || node.right == null) {
        console.log(
          ` Node With Signle Child  Found for deletion - ${node.left}<----${node.value} --->${node.right}`
        );
        //This Will Skip the Node and Connect Its Parent with It's Child
        if (node.left == null) {
          return node.right;
        } else {
          return node.left;
        }
      } else {
        console.log(
          ` Node with Both Left and Right Child  Found for deletion - ${node.left}<----${node.value} --->${node.right}`
        );
        //Now Find the Minimum Value in Right SubTree [InOrder Successor of this Node]
        let value = this._findMin(node.right);
        node.value = value; //Udating Value for the Current Node - Now It is Deleted -But Need to Delete that Min value Node
        //Simply Call Delete for this Node in the Sub Tree
        node.right = this.deleteRec(node.right, value);
      }
    }

    //Sending the Original Node Back -WE DO NOT MODIFY THIS
    return node;
  }

  _findMin(node) {
    let value = node.value;
    while (node != null) {
      value = node.value;
      node = node.left;
    }
    return value;
  }

  //Traverse the Tree
  traverse() {
    console.log(JSON.stringify(this.root));
    return this;
  }
  //Lookup for a Value
  lookup(value) {
    console.log(`Serching Node ${value}`);
    this.lookUpRecursive(value, this.root);
    return this;
  }

  lookUpRecursive(value, node) {
    if (node == null) {
      console.log(`Value Not Found - ${value}`);
      return;
    }
    if (value > node.value) {
      console.log(`Traversing Toward Right ${node.value} --> ${value}`);
      this.lookUpRecursive(value, node.right);
    } else if (value < node.value) {
      console.log(`Traversing Toward Left ${value} <---${node.value}  `);
      this.lookUpRecursive(value, node.left);
    } else {
      console.log(`Final Value has been Found - ${value} ==== ${node.value} `);
      return;
    }
  }
}

let bst = new BinarySearchTree();
let node = bst.root;
bst
  .insert(9)
  .insert(4)
  .insert(6)
  .insert(20)
  .insert(170)
  .insert(15)
  .insert(1)
  .insert(20)
  .insert(169)
  .insert(168)
  .insert(160)
  .insert(165)
  .insert(164)
  .insert(155)
  .insert(167)
  .traverse()
  .delete(20)
  .traverse()
  .delete(1)
  .traverse()
  .delete(4)
  .traverse()
  .lookup(15)
  .lookup(163)
  .lookup(162)
  .traverse();
