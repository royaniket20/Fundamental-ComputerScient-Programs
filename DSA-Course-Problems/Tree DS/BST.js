class BinaryTreeNode {
    value;
    left;
    right;
    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  export default class BinarySearchTree {
    root;
    constructor() {
      this.root = null;
    }
  
    //Insert Data to BST
    insert(value) {
      console.log(`Inserting Node ${value}`);
      let current = this.root;
      if (current != null) {
        while (current != null) {
          if (current.value > value) {
            if (current.left != null) {
              current = current.left;
            } else {
              //New Value getting added at Left
              let node = new BinaryTreeNode(value);
              current.left = node;
              console.log(` Left Value Inserted !!`);
              break;
            }
          } else if (current.value < value) {
            if (current.right != null) {
              current = current.right;
            } else {
              //New Value getting added at Right
              let node = new BinaryTreeNode(value);
              current.right = node;
              console.log(` Right Value Inserted !!`);
              break;
            }
          } else {
            console.log(`Value Already Present!!`);
            break;
          }
        }
      } else {
        //First Value getting added
        let node = new BinaryTreeNode(value);
        this.root = node;
        console.log(`New Value Inserted !!`);
      }
      return this;
    }
  
  }
  
 