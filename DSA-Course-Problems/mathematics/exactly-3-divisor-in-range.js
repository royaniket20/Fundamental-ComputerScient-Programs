//Basic approach - Find all the Numbers in range - who have exactly 3 divisors

let bruteforce = function (range) {
  console.log(`given Range is ----------> ${range}`);
  for (let index = 1; index <= range; index++) {
    console.log(`Checking for Number -> ${index}`);
    let dataSet = checkDivisors(index);
    console.log(dataSet);
  }
};

let checkDivisors = function checFor3Divisor(a) {
  let isExactlyThree = false;
  let results = [];
  let dataSet = {};
  for (let i = 1; i <= a; i++) {
    if (a % i === 0) {
      results.push(i);
    }
  }
  if (results.length === 3) {
    isExactlyThree = true;
  }
  if (isExactlyThree) {
    dataSet.decision = isExactlyThree;
    dataSet.data = results;
  } else {
    dataSet.decision = isExactlyThree;
    dataSet.data = [];
  }
  return dataSet;
};

bruteforce(6);
bruteforce(25);

//Efficiet Approach -
/**
 * Here the Idea is P is a prime Number and 3 divisor is possible in A number is a perfect Squre of P ==>
 *  4 -[1,2,4]
 * 9-[1,3,9]
 * 25 =[1,5,25]
 * etc ...
 */

import { checkPrimeSqrt } from "./check-prime-or-not.js";
function exactThreeDivisors(range) {
  console.log(`Given Range is -----------> ${range}`);
  for (let index = 2; index <= range; index++) {
    console.log(`Checking for Number -> ${index}`);
    let isPrime = checkPrimeSqrt(index);
    if (isPrime) {
      if (Math.pow(index, 2) <= range) {
        console.log(`${Math.pow(index, 2)} is having Perfect 3 divisors`);
        console.log(checkDivisors(Math.pow(index, 2)).data);
      } else {
        console.log(
          "No need to check Further - Becuase Perfect Squre going Out of Range"
        );
        break;
      }
    }
  }
}

console.log("///////////////*********************///////////////////");
exactThreeDivisors(6);
exactThreeDivisors(25);
