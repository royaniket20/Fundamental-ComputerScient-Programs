/**
 * Module Multiplication Rule  - Refer to Note 
 * You are given two numbers a and b. You need to find the multiplication of a and b under modulo M (M as 109+7).
 * 
 */

let moduloMultiplication = function(num1,num2,mod){
    console.log(`Given Number a - ${num1} | b - ${num2}`);
    console.log(`Given Mod - ${mod}`);
     let a = BigInt(num1);
     let b = BigInt(num2);
     mod = BigInt(mod);
     a = a%mod;
     b = b%mod;
     let res = (a*b)%mod;
     console.log(`Result is - ${res}`);

}

moduloMultiplication(92233720368547758n,92233720368547758n ,Math.pow(10,9)+7 )

