//If Sums and Digits Count is Given Find the Max Possible Number
//Start filling position from MSB -> LSB from 1->9 Until goal
let maxNumber = function (s, n) {
  console.log(`Given Sum - ${s}`);
  console.log(`Given Digit Count - ${n}`);
  let arr = new Array(n).fill(0);
  console.log(`Computation Space -- ${arr}`);

  let result = 0;
  let startIndex = 0;
  let startElement = 0;
  while (true) {
    //To prevent Array Overflow
    if (startIndex < n) {
      arr[startIndex] = startElement;
    }
    //Processing Until target sum is achieved
    if (findArraySum(arr) < s) {
      //If target sum is not reached but digit count exceeded [Sumpose allowd Digits is 3 and Arr Index[0 based] reached 3 [0-1-2-3]]
      //Then No meaning of calculation as Digit Cout is Not enough to accomodate the Target sum even if all Digits 9 like 999
      if (startIndex === n) {
        result = -1;
        break;
      }
      //If for a Place - 9 is reached move to next Bit toward Right and again start from 0
      if (startElement === 9) {
        startIndex++;
        startElement = 0;
      }
      startElement++;
      continue;
    } else {
      if (findArraySum(arr) == s) {
        result = parseInt(arr.join(""));
      } else {
        result = -1;
      }
      break;
    }
  }

  console.log(`The Given Number is -- ${result}`);
};

let findArraySum = function (arr) {
  console.log(`Current Arr - ${arr}`);
  return arr.reduce((a, b) => a + b, 0);
};

maxNumber(20, 3);
maxNumber(9, 2);
maxNumber(19, 6);

maxNumber(325, 3);
