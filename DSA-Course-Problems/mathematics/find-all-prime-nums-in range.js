let isPrime = function checkPrime(a) {
  for (let i = 2; i <= Math.sqrt(a); i++) {
    if (a % i === 0) {
      return false;
    }
  }
  return true;
};

let apprach1 = function findAllPrimes(a) {
  let results = [];
  for (let i = 2; i <= a; i++) {
    console.log(` Num -- ${i} id Prime -- ${isPrime(i)}`);
    if (isPrime(i)) {
      results.push(i);
    }
  }
  console.log("Results ---- " + results);
};

apprach1(20);

let apprach2 = function findAllPrimesEfficient(a) {
  let results = [];
  //Because Array is Zero Based Index -For making 1 based Index usage - We took array of a+1
  let placeholder = new Array(a + 1).fill(true);

  placeholder[0] = false; //Not in Use - So discarded
  placeholder[1] = false;

  console.log(
    `The Inital Array : ${placeholder.toString()} ---- ${placeholder.length}`
  );
  console.log(`The Inital Result : ${results}`);
  for (let i = 2; i < Math.sqrt(placeholder.length); i++) {
    console.log(i + "***loop value*****");
    if (isPrime(i)) {
      let temp = i;
      let multiplier = 2;
      temp = temp * multiplier;
      console.log("Detected val : " + i);
      while (temp <= a) {
        console.log("Marked false : " + temp);
        placeholder[temp] = false;
        temp = i * ++multiplier;
      }
    }
  }
  for (let i = 2; i < placeholder.length; i++) {
    if (placeholder[i] === true) results.push(i);
  }

  console.log(`The Final Array : ${placeholder}`);
  console.log("Results ---- " + results);
};

apprach2(60);
