/**
 * Armstrong Numbers are the numbers having the sum of digits raised 
 * to power no. of digits is equal to a given number. 
 * Examples- 0, 1, 153, 370, 371, 407, and 1634 are some of the Armstrong Numbers.
 * 1^3 + 5^3 + 3^3 = 153
**/
let armsNum = function(num){
   // console.log(`Given numer is ${num}`);
    let result = 0;
    let initial = num;
    let digitCount = Math.floor(Math.log10(num))+1;
    while(num>0){
      result = result + Math.pow(num%10 , digitCount);
      num = Math.floor(num/10);
    }
    if(result === initial){
      console.log(`${initial} is an Armstrong Number`);
    }else{
        //console.log(`Its Not an ArmString Number`);
    }
}

console.log(`All armstrong Nums in Range 0 - 2000 ARE - `);
for (let index = 0; index < 2000 ; index++) {
 
  armsNum(index);
}