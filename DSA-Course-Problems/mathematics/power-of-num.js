let approach1 = function simpleSol(x, n) {
  if (n == 0) return 1;
  let result = 1;
  for (let i = 1; i <= n; i++) {
    console.log(`Current result : ${result}`);
    result = result * x;
  }
  return result;
};

console.log(`Power of Numer is ${approach1(3, 8)}`);

let approach2 = function efficientSol(x, n) {
  if (n == 0) {
    return 1;
  }
  let power = Math.floor(n / 2);
  console.log(`Value of N = ${n} ==> n/2 => ${n / 2}`);
  console.log("Power value : " + power);
  let temp = efficientSol(x, power);
  if (n % 2 === 0) {
    return temp * temp;
  } else {
    return x * temp * temp;
  }
};

console.log(`Power of Numer is Efficient Way  ${approach2(2, 10)}`);
