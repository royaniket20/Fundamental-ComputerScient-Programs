//GCD of Two Number cannot be larger than Smalles Number
let gcdofNums = function gcdOfTwoNums(a, b) {
  let result = 1;
  let min = Math.min(a, b);
  while (min > 0) {
    if (a % min === 0 && b % min === 0) {
      result = min;
      break;
    } else {
      min--;
      console.log("Current value of Min : " + min);
    }
  }

  return result;
};

console.log(`Gcd of two Numbers : ${gcdofNums(35, 180)}`);

let gcdofNumsEu = function gcdOfTwoNumsEuclidian(a, b) {
  console.log(`The value of Current GCD  : GCD (${a} , ${b})`);
  if (a === 0) {
    return b;
  }
  if (a > b) {
    return gcdOfTwoNumsEuclidian(a - b, b);
  } else {
    return gcdOfTwoNumsEuclidian(b - a, a);
  }
};

console.log(`Gcd of two Numbers : ${gcdofNumsEu(35, 180)}`);

console.log(`Gcd of two Numbers : ${gcdofNumsEu(23, 19)}`);
