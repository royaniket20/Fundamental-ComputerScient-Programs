let isPrime = function checkPrime(a) {
  for (let i = 2; i <= Math.sqrt(a); i++) {
    if (a % i === 0) {
      return false;
    }
  }
  return true;
};

let findAllPrimeFactors = function finding(a) {
  if (a <= 1) return;

  let allPrimes = [];
  let temp = a;
  for (let i = 2; i < a; i++) {
    console.log("Loop Runs for -- " + i);
    if (a % i === 0 && isPrime(i)) {
      while (temp % i === 0) {
        allPrimes.push(i);
        temp = temp / i;
      }
    }
  }
  if (temp > 1) {
    allPrimes.push(temp);
  }
  console.log(`All the Primes : ${allPrimes}`);
};

//***********Efficient Solution */

/**
 * We will find First prime factor then Clean It out Then go for next prime factor Until Number ends
 */

let findAllPrimeFactors2 = function findingEfficient(a) {
  if (a <= 1) return;

  let allPrimes = [];
  for (let i = 2; i <= Math.sqrt(a); i++) {
    console.log("Loop Runs for -- " + i);
    //First Few Numbers like 2,3 are already prime numbers - and we are claing out all its multiples already , So no need to do explicit Prime check
    //When 4 , 8 Appear - If Block will anyway Not execute
    if (a % i === 0) {
      while (a % i === 0) {
        console.log(`Pushed Item - ${i}`);
        allPrimes.push(i);
        a = a / i;
      }
    }
  }
  if (a > 1) {
    allPrimes.push(a);
  }
  console.log(`All the Primes Efficient : ${allPrimes}`);
};

findAllPrimeFactors2(12);
findAllPrimeFactors2(532);
findAllPrimeFactors(4);
