console.log('**********');

let approach1 = function iterativeApproach(number){
    console.log('Number is '+number);
let result = 0;
while(number!=0){
    number = Math.floor(number/10);
    result++;
    console.log(`The values : ${number} --- ${result}`);
}
return result;
}

let approach2 = function recursiveApproach(number){
    console.log('Number is '+number);
if(number === 0)
{
return 0;
}
return 1 + recursiveApproach( Math.round(number/10));
}

let approach3 = function mathApproach(number){
    console.log('Number is '+number);


return Math.floor(Math.log10(number)+1);

}


let result1 = approach1(112233558877);

console.log("---Result 1 is : "+result1);

let result2 = approach2(112233558877);

console.log("---Result 2 is : "+result2);


let result3 = approach3(112233558877);

console.log("---Result 3 is : "+result3);