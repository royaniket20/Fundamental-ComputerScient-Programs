//LCM of Two Numbers always greater than or Equal to larger Number
let approach1 = function lcmOfTwoNums(a, b) {
  let result = -1;
  let maxNum = Math.max(a, b);

  while (true) {
    if (maxNum % a === 0 && maxNum % b === 0) {
      result = maxNum;
      break;
    } else {
      console.log("Current Iteration ---- " + maxNum);
      maxNum++;
    }
  }
  return result;
};

console.log(`LCM of two Numbers : ${approach1(7, 3)}`);

let gcdofNumsEu = function gcdOfTwoNumsEuclidian(a, b) {
  console.log(`The value of Current GCD  : GCD (${a} , ${b})`);
  if (a === 0) {
    return b;
  }
  if (a > b) {
    return gcdOfTwoNumsEuclidian(a - b, b);
  } else {
    return gcdOfTwoNumsEuclidian(b - a, a);
  }
};
//Using Formula GCD * LCM = A*B
let approach2 = function lcmOfTwoNumsByFormula(a, b) {
  //a*b = gcd(a,b)*lcm(a,b)
  let result = -1;
  let multiplicationofNums = a * b;
  let gcdOfNums = gcdofNumsEu(a, b);

  result = multiplicationofNums / gcdOfNums;

  return result;
};

console.log(`LCM of two Numbers : ${approach2(7, 3)}`);
