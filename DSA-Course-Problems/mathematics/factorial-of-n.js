
let approach1 = function factorialRecursive(n){

    if(n===0)
    {
        return 1;
    }
    return n* factorialRecursive(n-1);

}

let approach2 = function factorialIterative(n){

    let result = 1;
    while(n!=0)
    {
        result = result*n;
        n--;
    }
    return result;

}


console.log(`The Approach 1 : ${approach1(5)}`);

console.log(`The Approach 2 : ${approach2(15)}`);





