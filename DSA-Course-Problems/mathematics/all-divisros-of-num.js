/***
 * Divisor of a Number is the numbers that can devide the number fully
 * 1,5,25 are divisor of 25
 */
let approach1 = function findAllDicisors(a) {
  let results = [];
  for (let i = 1; i <= a; i++) {
    if (a % i === 0) {
      results.push(i);
    }
  }
  console.log(`The result : ${results}`);
};

let approach2 = function findAllDicisorsEfficientNotSorted(a) {
  let results = [];
  //As we know A number is just multiplation of 2 numbers , one of them is less than sqrt n
  for (let i = 1; i <= Math.sqrt(a); i++) {
    if (a % i === 0) {
      results.push(i);
      if (i != a / i) {
        //Avoid perfect squre to print value twitce - 25 --1, 5 ,5,25 is wrong -- It houd be 1 , 5, 25
        results.push(a / i); //Adding the other Pair
      }
    }
  }
  console.log(`The result : ${results}`);
};

let approach3 = function findAllDicisorsEfficientSorted(a) {
  let results = [];
  for (let i = 1; i <= Math.sqrt(a); i++) {
    if (a % i === 0) {
      results.push(i);
    }
  }
  console.log(`The result First Round : ${results}`);

  for (let i = Math.round(Math.sqrt(a)); i >= 1; i--) {
    if (a % i === 0) {
      if (i != a / i) {
        //Avoid perfect squre to print value twitce - 25 --1, 5 ,5,25 is wrong -- It houd be 1 , 5, 25
        results.push(a / i);
      }
    }
  }
  console.log(`The result Final Round : ${results}`);
};

approach1(539);
approach1(25);

approach2(539);
approach2(25);

approach3(539);
approach3(25);
