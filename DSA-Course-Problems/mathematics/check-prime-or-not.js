/**
 * Here check for each Number from 2->n-1 If n is divisible by any number - then Non prime else prime
 * @param {*} a
 * @returns
 */
let approach1 = function checkPrime(a) {
  let result = true;

  for (let i = 2; i < a; i++) {
    if (a % i === 0) {
      console.log("Number divisible by " + i);
      return false;
    }
  }

  return true;
};
console.log(`Is the Number Prime ${approach1(4)}`);
/**
 * Here check for each Number from 2->sqrt n  If n is divisible by any number - then Non prime else prime
 * Here idea is One of the factor of a Given number must be present in below sqrt(n) if Not prime
 * @param {*} a
 * @returns
 */
export function checkPrimeSqrt(a) {
  let result = true;

  for (let i = 2; i <= Math.sqrt(a); i++) {
    if (a % i === 0) {
      console.log("Number divisible by " + i);
      return false;
    }
  }

  return true;
}

console.log(`Is the Number Prime ${checkPrimeSqrt(4)}`);
