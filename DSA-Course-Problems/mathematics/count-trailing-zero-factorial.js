console.log("**************");

//Finding the Tailing Zero in a small Number where overflow will not happen

let method1 = function findTailingZero(num) {
  let result = 0;
  while (num % 10 == 0) {
    result++;
    console.log(`The Data : ${result} --- ${num}`);
    num = Math.floor(num / 10);
  }
  return result;
};

console.log("Number of Zeros : " + method1(58046000));

//Finding the Tailing Zero for Factorial Of large Nums

let method2 = function findTailingZeroinFactorial(num) {
  let result = 0;
  let divisor = 5;
  while (num > divisor) {
    result = result + Math.floor(num / divisor);
    divisor = divisor * 5;
  }
  return result;
};

console.log("Number of Zeros in Factorial : " + method2(256));

console.log("Number of Zeros in Factorial: " + method2(27));

console.log("Number of Zeros in Factorial: " + method2(1000));
