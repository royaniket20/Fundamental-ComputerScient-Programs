/**
 * modular multiplicative inverse Rule  - Refer to Note
 * Given two integers ‘a’ and ‘m’. The task is to find the smallest modular multiplicative inverse of ‘a’ under modulo ‘m’.
 *
 */

let moduloMultiplicativeInverse = function (num1, mod) {
  console.log(`Given Number a - ${num1} `);
  console.log(`Given Mod - ${mod}`);
  let result = -1;
  for (let index = 1; index < mod; index++) {
    if (((num1 % mod) * (index % mod)) % mod == 1) {
      result = index;
      break;
    }
  }
  console.log(`Modulo Multicative Inverse is - ${result}`);
};

moduloMultiplicativeInverse(3, 11);
moduloMultiplicativeInverse(10, 17);
