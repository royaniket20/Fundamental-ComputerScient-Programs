/**
 * Module Sum Rule  - Refer to Note 
 * Given two numbers a and b, find the sum of a and b. Since the sum can be very large, find the sum modulo 10^9+7.
 * 
 */

let moduloSum = function(num1,num2,mod){
    console.log(`Given Number a - ${num1} | b - ${num2}`);
    console.log(`Given Mod - ${mod}`);
     let a = BigInt(num1);
     let b = BigInt(num2);
     mod = BigInt(mod);
     a = a%mod;
     b = b%mod;
     let res = (a+b)%mod;
     console.log(`Result is - ${res}`);

}

moduloSum(9223372036854775807n,9223372036854775807n ,Math.pow(10,9)+7 )

