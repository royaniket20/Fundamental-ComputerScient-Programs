//Some Important Math to Remember 
/**
 * LOG 10 (A*B) = LOG 10(A) + LOG 10(B)
 * Number of Digits in a Number n = Math.floor (Log 10 (n))+1
 */


let countDigits = function (num) {
  let logSum = 1;
  for (let index = 1; index <= num; index++) {
    logSum = logSum + Math.log10(index);
  }
  logSum = Math.floor(logSum);
  console.log(`Number of Digits in Factorial of ${num} is ==> ${logSum}`);
};

countDigits(5);
//12! = 479001600
countDigits(12);
