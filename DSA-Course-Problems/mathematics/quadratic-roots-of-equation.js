let quadaticRoots = function (a, b, c) {
  let determinant = Math.pow(b, 2) - 4 * a * c;
  console.log(`Calculated discriminant is - ${determinant}`);
  if (determinant < 0) {
    console.log(`Roots are Imaginary`);
  } else {
    console.log(`Roots are real `);
    let root1 = (-b + Math.sqrt(determinant)) / (2 * a);
    let root2 = (-b - Math.sqrt(determinant)) / (2 * a);
    console.log(`Roots are ${root1} , ${root2}`);
  }
};

quadaticRoots(1, -7, 12);

quadaticRoots(1, -2, 12);
