/**
 * inversion of array is when i<j abd a[i]>a[j]
 * That means when smaller element appears after larger element 
 */

let arr1 = [2, 4, 1, 3, 5];

let method1 = function theEasySolution(arr) {
  console.log(`The Given Array - ${arr}`);
  let result = 0;
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        console.log(`The Inversion Pair - [${arr[i]}, ${arr[j]}]`);
        result++;
      }
    }
  }
  console.log(`The total Inversion - ${result}`);
};

//Time Complexity O(n^2)
method1(arr1);
method1(arr1.sort((a, b) => a - b));
method1(arr1.sort((a, b) => b - a));
