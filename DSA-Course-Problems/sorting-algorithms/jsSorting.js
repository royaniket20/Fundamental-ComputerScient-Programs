let arr = [33, 66, 88, 11, 33, 9, 2, 4, 6, 11, 65, 43];
console.log(`Before Sort -- ${arr}`);
arr.sort((a, b) => a - b);
console.log(`After Sort -- ${arr}`);

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

let objArr = [];
for (let i = 0; i < 10; i++) {
  objArr.push({
    id: i,
    name: "Aniket" + i,
    age: getRandomInt(30),
  });
}

console.log(objArr);

objArr.sort((a, b) => {
  return parseInt(a.age) - parseInt(b.age);
});

console.log(objArr);
