//Unit of two array - Is all  elements of those arrays - But excluding Duplicates 

let methodEfficient = function findUnionsBetweenTwoSortedArray(arr1, arr2) {
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  let newArr = [];
  let maxLength = arr1.length + arr2.length;
  let arr1Pos = arr1.length > 0 ? 0 : -1;
  let arr2Pos = arr2.length > 0 ? 0 : -1;
  for (let i = 0; i < maxLength; i++) {
    //Duplicate removal
    if (newArr.length > 0) {
      if (
        arr1[arr1Pos] != undefined &&
        newArr[newArr.length - 1] === arr1[arr1Pos]
      ) {
        arr1Pos++;
        continue;
      }
      if (
        arr2[arr2Pos] != undefined &&
        newArr[newArr.length - 1] === arr2[arr2Pos]
      ) {
        arr2Pos++;
        continue;
      }
    }
    if (arr2[arr2Pos] != undefined && arr1[arr1Pos] != undefined) {
      if (arr1[arr1Pos] < arr2[arr2Pos]) {
        //arr1 smaller
        //For uNION sotre it

        newArr.push(arr1[arr1Pos]);

        arr1Pos++;
      } else if (arr2[arr2Pos] < arr1[arr1Pos]) {
        //arr 2 smaller
        //For Union store Operation

        newArr.push(arr2[arr2Pos]);

        arr2Pos++;
      } else {
        //both are equal
        //For merging store both
        //For Union pick anyone - Also prevent DuplicATE
        newArr.push(arr1[arr1Pos]);
        arr1Pos++;
        arr2Pos++;
      }
    } else {
      if (arr2[arr2Pos] != undefined) {
        newArr.push(arr2[arr2Pos]);
        arr2Pos++;
      } else if (arr1[arr1Pos] != undefined) {
        newArr.push(arr1[arr1Pos]);
        arr1Pos++;
      }
    }
  }

  console.log(
    "--UNIONS OF THE TWO SORTED ARRAYS------" + JSON.stringify(newArr)
  );
};

methodEfficient(
  [1, 3, 2, 4, 8, 4, 3, 2, 1, 34, 5, 4, 3, 21, 2, 34, 4].sort((A, B) => A - B),
  [1, 7, 8, 3, 2, 2, 2, 1, 7, 34].sort((A, B) => A - B)
);

let method1 = function findUnions(arr1, arr2) {
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  let set = new Set();
  arr1.forEach((item) => {
    set.add(item);
  });
  arr2.forEach((item) => {
    set.add(item);
  });

  console.log(`The Given UNION is = ${Array.from(set)}`);
};

method1(
  [1, 3, 2, 4, 4, 3, 2, 1, 34, 5, 4, 3, 21, 2, 34, 4],
  [1, 7, 8, 3, 2, 2, 2, 1, 7]
);
