let method1 = function mergeTwoSortedArray(arr1, arr2) {
  let newArr = [];
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  for (let i = 0; i < arr1.length; i++) {
    newArr.push(arr1[i]);
  }
  for (let i = 0; i < arr2.length; i++) {
    newArr.push(arr2[i]);
  }
  console.log(`Updated merged Array Before sorting - ${newArr}`);
  newArr.sort((a, b) => a - b);
  console.log(`Updated merged Array After sorting - ${newArr}`);
};

let arr1 = [11, 33, 55, 77, 88];
let arr2 = [22, 33, 44, 77, 88, 99, 122, 133, 144, 155, 667, 889];

// method1(arr1, arr2);

export function mergeTwoSortedArrayEfficient(arr1, arr2) {
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  let newArr = [];
  let maxLength = arr1.length + arr2.length;
  let arr1Pos = arr1.length > 0 ? 0 : -1;
  let arr2Pos = arr2.length > 0 ? 0 : -1;
  for (let i = 0; i < maxLength; i++) {
    if (arr1Pos >= 0 && arr1Pos < arr1.length) {
      arr1[arr1Pos] <= arr2[arr2Pos] || arr2[arr2Pos] === undefined
        ? newArr.push(arr1[arr1Pos++])
        : null;
    } else {
      //  console.log(`Array 1 Ended`);
    }
    if (arr2Pos >= 0 && arr2Pos < arr2.length) {
      arr2[arr2Pos] <= arr1[arr1Pos] || arr1[arr1Pos] === undefined
        ? newArr.push(arr2[arr2Pos++])
        : null;
    } else {
      // console.log(`Array 2 Ended`);
    }
  }

  console.log(`Updated merged Array After sorting - ${newArr}`);
  return newArr;
}

//mergeTwoSortedArrayEfficient(arr1, arr2);
// arr1 = [11, 33, 55, 77, 88];
// arr2 = [22];
// mergeTwoSortedArrayEfficient(arr1, arr2);

let methodEfficient = function mergeTwoSortedArrayEasyEfficient(arr1, arr2) {
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  let newArr = [];
  let maxLength = arr1.length + arr2.length;
  let arr1Pos = arr1.length > 0 ? 0 : -1;
  let arr2Pos = arr2.length > 0 ? 0 : -1;
  for (let i = 0; i < maxLength; i++) {
    if (arr2[arr2Pos] != undefined && arr1[arr1Pos] != undefined) {
      if (arr1[arr1Pos] < arr2[arr2Pos]) {
        //arr1 smaller
        //For merging sotre it
        newArr.push(arr1[arr1Pos]);
        arr1Pos++;
      } else if (arr2[arr2Pos] < arr1[arr1Pos]) {
        //arr 2 smaller
        //For Merging store Operation
        newArr.push(arr2[arr2Pos]);
        arr2Pos++;
      } else {
        //both are equal
        //For merging store both
        newArr.push(arr1[arr1Pos]);
        newArr.push(arr2[arr2Pos]);
        arr1Pos++;
        arr2Pos++;
      }
    } else {
      if (arr2[arr2Pos] != undefined) {
        newArr.push(arr2[arr2Pos]);
        arr2Pos++;
      } else if (arr1[arr1Pos] != undefined) {
        newArr.push(arr1[arr1Pos]);
        arr1Pos++;
      }
    }
  }

  console.log("--THE MERGED SORTED ARRAY EASILY-----" + JSON.stringify(newArr));
};

//methodEfficient(arr1, arr2);
