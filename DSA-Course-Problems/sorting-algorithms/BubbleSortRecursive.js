function swap(arr, i, j) {
  let temp = arr[j];
  arr[j] = arr[i];
  arr[i] = temp;
}
/**
 * Idea of Bubble Sort Resursion is For each Call we bubble Up one element to last of the array - larest element 
 * Then we Do recusrisve call on next N-1 Length array - Becau Nth element is the Bubbled Up element 
 *  Do this Until Only One element left 
 * @param {*} arr 
 * @param {*} n 
 * @returns 
 */
let bubbleSortResursive = function BubbleSort(arr, n) {
  //Base case
  if (n === 1) {
    return;
  }
  for (let j = 0; j < n - 1; j++) {
    if (arr[j] > arr[j + 1]) swap(arr, j, j + 1);
  }
  BubbleSort(arr, n - 1);
};
let arr = [55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, -5];
bubbleSortResursive(arr, arr.length);
console.log(`Result ---- ${arr}`);
