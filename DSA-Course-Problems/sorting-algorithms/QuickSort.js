/**
 * Main Idea of Quick Sort is that - We use partitioning Algo to do the partions of Array Until it Becomes Single element Array
 * Then Sort and merge Back as usual
 * Its a devide concure algo - But do not take additional spoace
 * But time complexity can get a Hit of O(N^2). - avarage case is O(nLog n )
 * Its a tail recursive Function  - Merge Sort is not tail Resursive function
 * Better for CPU instruction optimization
 * Partition is Key Function in Quick Sort - Stable
 * If efficient Naive Partition is used - Quick sort will be stable
 * Java use Dual Pivot Quick Sort for Premitive Data - as stability do not matter
 * Java use Merge Sort for the non premitive Data as its table
 *
 */
import { lombutoPartition, HoarePartition } from "./partition-of-array.js";
//First we will use Lomuto Partition - We Know for Lomuto High is the Pivot element
let quickSortLomuto = function doQuickSortLomuto(arr, low, high) {
  if (low < high) {
    let pivot = lombutoPartition(arr, low, high, high); //Because we know High is the Pivot considered in Lomuto
    console.log(`Got Pivot Lomuto - ${pivot}`);
    //Sort the Left array [all elements smaller than pivot ]
    doQuickSortLomuto(arr, low, pivot - 1); //Now we know that Pivot element is already placed in Correct Position - So Ignore that
    //Sort the Right array [All element greater than Pivot]
    doQuickSortLomuto(arr, pivot + 1, high); //Now we know that Pivot element is already placed in Correct Position - So Ignore that
  }
};
let arr = [55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, -70, 80, -90];
console.log(`Given array for Lomuto Partition Quick Sort - ${arr}`);
quickSortLomuto(arr, 0, arr.length - 1);
console.log(`Quick Sort Using Lomuto Partition Result - ${arr}`);

//First we will use Hoare Partition - We Know for Hoare low  is the Pivot element
let quickSortHoare = function doQuickSortHoare(arr, low, high) {
  if (low < high) {
    let pivot = HoarePartition(arr, low, high, low); //Because we know Low is the Pivot considered in Hoare
    console.log(`Got Pivot Hoare - ${pivot}`); //Hoare partion Just give out the Higher Boundary of Smaller array w.r.t to pivot element
    doQuickSortHoare(arr, low, pivot); //Sort the Left array [all elements smaller than pivot ]
    doQuickSortHoare(arr, pivot + 1, high); //Sort the Right array [All element greater than Pivot and Pivot itself included ]
  }
};
arr = [55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, -70, 80, -90];
console.log(`Given array for Hoare partition Quick Sort  - ${arr}`);
quickSortHoare(arr, 0, arr.length - 1);
console.log(`Quick Sort Using Hoare Partition Result - ${arr}`);
