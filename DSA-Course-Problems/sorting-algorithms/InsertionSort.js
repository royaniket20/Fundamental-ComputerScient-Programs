function swap(arr, i, j) {
  let temp = arr[j];
  arr[j] = arr[i];
  arr[i] = temp;
}
/**
 *
 * Takes an element and place it to the Correct position on the array
 * it start with One element array thats the first element
 *
 * for each element find out where it shoud be in the array from 0 to  that position
 * Once you find the position right shif all elements to make place there
 * and then place the meement there
 *
 * we also assume first element is already at correct position as therre is nothing left to It to compare its real position
 */

let insertionSort = function InsertionSort(arr) {
  console.log(`The Initial Array - ${arr}`);
  for (let i = 1; i < arr.length; i++) {
    //Consider first element  as single element sorted arrtay
    let element = arr[i];
    let elementPosition = i; //Take the Position where the element already situated
    console.log(`Checking for element - ${element}`);
    console.log(`Checking in the range arr [ ${i - 1} -> ${0}]`);
    for (let j = i - 1; j >= 0; j--) {
      if (element < arr[j]) {
        //If we use <= SYMBOL THE STABILITY WILL BE BROKEN
        arr[j + 1] = arr[j]; //Shifting element to Right to widen the sorted array and make position for Insertion of element
        elementPosition = j; //Point to the position where extra space created for insertion
      } else {
        //Found position and placement done - Now no need to traverse further toward start
        console.log(`Element already in correct position`);
        break;
      }
    }
    console.log(`Current Insetion Position = ${elementPosition}`);
    arr[elementPosition] = element;
    console.log(`The partially Sorted Array - ${arr}`);
  }
  console.log(`The Sorted Array - ${arr}`);
};

insertionSort([55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, -33]);
