import { mergeTwoSubArray as merge } from "./merge-function-sorted-sub-arrays.js";

let arr = [
  15, 10, 20, 11, 30, 45, 67, 12, 65, -90, -34, 2, 1, 3, 4, 6, 5, 5, 6, 4, 6, 4,
  6, 46, 3, 2, 3, 5, 6, 7, 8,
];

function mergeSort(arr, low, high) {
  if (high > low) {
    let mid = low + Math.floor((high - low) / 2);
    mergeSort(arr, low, mid);
    mergeSort(arr, mid + 1, high);
    merge(arr, low, mid, high);
  }
}

console.log(`BEFORE MERGE SORT THE ORIGINAL ARRAY - ${arr}`);
mergeSort(arr, 0, arr.length - 1);
console.log(`AFTER  MERGE SORT THE ORIGINAL ARRAY - ${arr}`);
