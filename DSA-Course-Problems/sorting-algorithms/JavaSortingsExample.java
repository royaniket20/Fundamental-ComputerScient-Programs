import java.util.*;

public class JavaSortingsExample {

    public static void main(String[] argv) throws Exception {

        // Try block to check for exceptions
        try {

            // Creating Arrays of String type
            Integer a[] = { 3, 8, 2, -1, 7, -9, 0, -3, 5, 8, 7 };
            System.out.println("The list is Before Sorting: " + Arrays.asList(a));
            Arrays.sort(a);
            // Getting the list view of Array
            List<Integer> list = Arrays.asList(a);

            // Printing all the elements in list object
            System.out.println("The list is After Sorting: " + list);
        }

        // Catch block to handle exceptions
        catch (NullPointerException e) {

            // Print statement
            System.out.println("Exception thrown : " + e);
        }
    }
}