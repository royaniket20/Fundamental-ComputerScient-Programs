function swap(arr, i, j) {
  let temp = arr[j];
  arr[j] = arr[i];
  arr[i] = temp;
}

let bubbleSort = function BubbleSort(arr) {
  console.log(`The Initial Array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    //This many Number of swaps required
    console.log(`Pass - ${i + 1} Started ....`);
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) swap(arr, j, j + 1);
    }
    console.log(`Pass - ${i + 1} Completed ....`);
    console.log(`The Partial Sorted Array- ${arr}`);
  }
  console.log(`The Sorted Array - ${arr}`);
};

let bubbleSortModified = function BubbleSortMod(arr) {
  console.log(`The Initial Array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    let isSwapped = false;
    //This many Number of swaps required
    console.log(`Pass - ${i + 1} Started ....`);
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        isSwapped = true;
      }
    }
    console.log(`Pass - ${i + 1} Completed ....`);
    console.log(`The Partial Sorted Array- ${arr}`);
    if (!isSwapped) {
      console.log(`No More Iteration Required`);
      break;
    }
  }
  console.log(`The Sorted Array - ${arr}`);
};

bubbleSort([55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, 90]);
bubbleSortModified([55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, 90]);
