/**
 * Intersection of Two array - Only Give common elements - excluding duplicates
 * @param {*} arr1
 * @param {*} arr2
 */

let methodEfficient = function findIntersectionBetweenTwoSortedArray(
  arr1,
  arr2
) {
  console.log(`The Given Array - ${arr1} And ${arr2}`);
  let newArr = [];
  let maxLength = arr1.length + arr2.length;
  let arr1Pos = arr1.length > 0 ? 0 : -1;
  let arr2Pos = arr2.length > 0 ? 0 : -1;
  for (let i = 0; i < maxLength; i++) {
    if (arr2[arr2Pos] != undefined && arr1[arr1Pos] != undefined) {
      if (arr1[arr1Pos] < arr2[arr2Pos]) {
        //arr1 smaller
        //For merging sotre it
        arr1Pos++;
      } else if (arr2[arr2Pos] < arr1[arr1Pos]) {
        //arr 2 smaller
        //For Merging store Operation
        arr2Pos++;
      } else {
        //both are equal
        //For merging store both
        //For Intersection pick anyone - Also prevent DuplicATE - You can simply Use the Set also
        if (
          newArr.length === 0 ||
          (newArr.length > 0 && newArr[newArr.length - 1] != arr1[arr1Pos])
        ) {
          newArr.push(arr1[arr1Pos]);
        }
        arr1Pos++;
        arr2Pos++;
      }
    }
  }

  console.log(
    "--INTERSECTION OF THE TWO ARRAYS------" + JSON.stringify(newArr)
  );
};
//This will Only Work when We see that Two Given arrays are already sorted
methodEfficient(
  [1, 3, 2, 4, 8, 4, 3, 2, 1, 34, 5, 4, 3, 21, 2, 34, 4].sort((A, B) => A - B),
  [1, 7, 8, 3, 2, 2, 2, 1, 7, 34].sort((A, B) => A - B)
);
//Here we use set to store the Small array , Then use Large array to check if element matching - then thats part of final result array
let method1 = function findIntersection(arr1, arr2) {
  let set = new Set();
  let smallerarray, largerArray;
  arr1.length > arr2.length
    ? ((smallerarray = arr2), (largerArray = arr1))
    : ((smallerarray = arr1), (largerArray = arr2));

  smallerarray.forEach((item) => {
    set.add(item);
  });
  let result = new Set();
  largerArray.forEach((item) => {
    if (set.has(item)) {
      result.add(item);
    }
  });

  console.log(`The Given Intersection is = ${Array.from(result)}`);
};

method1(
  [1, 3, 2, 4, 4, 3, 2, 1, 34, 5, 4, 3, 21, 2, 34, 4],
  [1, 7, 8, 3, 2, 2, 2, 1, 7]
);
