import { mergeTwoSortedArrayEfficient } from "./merge-2-sortd-arr.js";

export function mergeTwoSubArray(arr, low, mid, high) {
  console.log(`The Given Array -- ${arr}`);
  let leftArray = arr.slice(low, mid + 1);
  let rightArray = arr.slice(mid + 1, high + 1);
  console.log(leftArray, rightArray);
  let result = mergeTwoSortedArrayEfficient(leftArray, rightArray);
  for (let i = low, j = 0; i <= high; i++) {
    arr[i] = result[j++];
  }
  console.log(`The Sorted array Fully --- ${arr}`);
}

// let arr = [10, 15, 20, 11, 30];
// let low = 0;
// let mid = 2;
// let high = 4;

// mergeTwoSubArray(arr, low, mid, high);
