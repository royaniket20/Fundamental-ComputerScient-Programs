function swap(arr, i, j) {
  let temp = arr[j];
  arr[j] = arr[i];
  arr[i] = temp;
}
//from i = 0 -> n
/**
 * For each Index find the smallest element of the remaining elemet from that index till end
 * Swap the min element with current Index and move to next index
 */

let selectionSort = function selectionSort(arr) {
  console.log(`The Initial Array - ${arr}`);
  for (let i = 0; i < arr.length; i++) {
    //This many Number of Passes required
    let Initialmin = arr[i]; // Now Fuind who is actually minimum
    let initialMinIndex = i;
    for (let j = i; j < arr.length; j++) {
      if (arr[j] < Initialmin) {
        //As we are not using <= - stability of the sort will be maintained
        Initialmin = arr[j];
        initialMinIndex = j;
      }
    }
    swap(arr, initialMinIndex, i);
    console.log(`Pass - ${i + 1} Completed ....`);
    console.log(`The Partially Sorted Array- ${arr}`);
  }
  //If you want you can copy the temp again back to original
  console.log(`The Sorted Array - ${arr}`);
};

selectionSort([55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, 90]);
