function swap(arr, i, j) {
  let temp = arr[j];
  arr[j] = arr[i];
  arr[i] = temp;
}
/**
 *
 * Takes an element and place it to the Correct position on the array
 * it start with One element array thats the first element
 *
 * for each element find out where it shoud be in the array from 0 to  that position
 * Once you find the position right shif all elements to make place there
 * and then place the meement there
 *
 * we also assume first element is already at correct position as therre is nothing left to It to compare its real position
 */
/**
 * Idea behind the Resursive Insertion Sort is Put 2nd element to [0]->[1] array in correct Postion
 * Then Do resursive Call for 3rd , 4th ... Upto Last element
 * Then Return
 * @param {*} arr
 * @param {*} n
 * @returns
 */
let insertionSortRecursive = function InsertionSort(arr, n) {
  //No more lement pending anymore ....
  if (n === arr.length) {
    return;
  }
  let element = arr[n];
  let elementPosition = n;
  for (let j = n - 1; j >= 0; j--) {
    if (element < arr[j]) {
      arr[j + 1] = arr[j];
      elementPosition = j;
    } else {
      break;
    }
  }
  arr[elementPosition] = element;
  InsertionSort(arr, n + 1);
};
let arr = [55, 77, 12, 34, 10, 8, 20, 5, 30, 40, 50, 60, 70, 80, -33];
insertionSortRecursive(arr, 1); //Start from Second Element
console.log(`Result is - ${arr}`);
