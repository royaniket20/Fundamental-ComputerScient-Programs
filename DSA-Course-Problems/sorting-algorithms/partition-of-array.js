/**
 * In Case of native partition - stability is maintained But we are going to use N extra space.
 * @param {*} arr
 * @param {*} low
 * @param {*} high
 * @param {*} pivot
 * Here we first Copy all elements less than Pivot to aux array .
 * Then Put all equals to Pivot to aux array
 * Then Put More than Pivot  elements to aux array
 * Return It
 */

export function naivePartition(arr, low, high, pivot) {
  let updatedPivorIndex = -1;
  let pivotElement = arr[pivot];
  // console.log(
  //   `The Given Array - ${arr} , LOW : ${low} , HIGH : ${high} , PIVOT : ${pivot} , PIVOT-ELEMENT - ${pivotElement}`
  // );
  let temp = [];
  for (let i = low; i <= high; i++) {
    if (arr[i] < pivotElement) {
      temp.push(arr[i]);
    }
  }
  for (let i = low; i <= high; i++) {
    if (arr[i] === pivotElement) {
      temp.push(arr[i]);
      updatedPivorIndex = temp.length - 1;
    }
  }
  for (let i = low; i <= high; i++) {
    if (arr[i] > pivotElement) {
      temp.push(arr[i]);
    }
  }

  //Copy back to Original Array
  for (let i = low; i <= high; i++) {
    arr[i] = temp[i];
  }
  console.log(
    `Updated array after partition Native - ${arr} and Updated PIVOT INDEX : ${updatedPivorIndex} , Updated PIVOT-ELEMENT - ${arr[updatedPivorIndex]}`
  );
}

function swap(arr, i, j) {
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}
/**
 * Here we can do the partition without E xtra space
 * Here first we assume that high element is pivot element
 * In case user give the Pivot element position swap High and Pivot element - which will again make sure High element is the pivot element
 * Now we keep window Size Index to Low-1.
 * Interate  I from low -- > high-1 // Because we know High is the pivot element
 * When we get an element which is <pivot element Just increate Windoiw Size index  by 1 and swap ith pos and Windown Index
 * Evenatuall All elements less than Pivot will be placed at Upto Window Index
 * Then Swap High and Window Index +1 to Put Pivot back to correct position .
 * @param {*} arr
 * @param {*} low
 * @param {*} high
 * @param {*} pivot
 */
export function lombutoPartition(arr, low, high, pivot) {
  let smallWindow = low - 1;
  let pivotElement = arr[pivot];
  console.log(
    `The Given Array - ${arr} , LOW : ${low} , HIGH : ${high} , PIVOT : ${pivot} , PIVOT-ELEMENT - ${pivotElement}`
  );
  if (pivot < high) {
    swap(arr, high, pivot); //swap to run standard lumbuto Partition
  }
  pivotElement = arr[high]; //Because if required we swapped

  for (let i = low; i <= high - 1; i++) {
    if (arr[i] < pivotElement) {
      //increase Window Size to Keep the Smaller element than Pivot
      smallWindow++;
      swap(arr, smallWindow, i);
    }
  }
  swap(arr, smallWindow + 1, high); //Putting Pivot to the End of smaller Window
  //Automatically it ill be ensured that Larger element is At right side of Pivot

  console.log(
    `Updated array after partition Lomuto - ${arr} and Updated PIVOT INDEX : ${
      smallWindow + 1
    } , Updated PIVOT-ELEMENT - ${arr[smallWindow + 1]}`
  );
  return smallWindow + 1;
}
/**
 * Here we will Keep Pivot to  Low Position
 * Now we keep Large window Index = High + 1 [Out of Index]
 * Now we keep Small window Index - Low -1 [Out of Index]
 * Then what we do is in do while Loop - Increase Small Window Index Until elements <pivot
 * Then what we do is in do while Loop - Decrease large  Window Index Until elements >pivot
 *  When syuch condition reach - Do check if Small window Index  cross Large window Index - If So break
 * Else do swap of both Index and then Go further
 *
 * Above whole thing will happen in white(true)
 * @param {*} arr
 * @param {*} low
 * @param {*} high
 * @param {*} pivot
 */
export function HoarePartition(arr, low, high, pivot) {
  let smallWindow = low - 1;
  let higherWindow = high + 1;
  let pivotElement = arr[pivot];
  // console.log(
  //   `The Given Array - ${arr} , LOW : ${low} , HIGH : ${high} , PIVOT : ${pivot} , PIVOT-ELEMENT - ${pivotElement}`
  // );
  if (pivot > low) {
    swap(arr, low, pivot); //swap to run standard Hoare Partition
  }
  pivotElement = arr[low]; //Because if required we swapped
  // console.log(`Updated Array - ${arr}`);
  // console.log(`Pivot Element --- ${pivotElement}`);
  let smallWindowFinalPosition = -1;
  while (true) {
    do {
      smallWindow++;
    } while (arr[smallWindow] < pivotElement);
    //console.log(`Smaller Window --- ${smallWindow}`);
    do {
      higherWindow--;
    } while (arr[higherWindow] > pivotElement);
    //console.log(`Higher Window --- ${higherWindow}`);
    if (smallWindow >= higherWindow) {
      break; //No need to Swap -IMPORTANT
    }
    // console.log(`Before Swap - ${arr}`);
    swap(arr, smallWindow, higherWindow);
    // console.log(`After Swap -  ${arr}`);
  }
  // console.log(`Pivot Element --- ${pivotElement}`);
  console.log(
    `The Left Partition ${arr.slice(
      0,
      higherWindow + 1
    )} and Right Partition: ${arr.slice(higherWindow + 1)} `
  );
  console.log(`Final High End of Smaller array - ${higherWindow}`);
  return higherWindow;
}
let arr = [1, 7, 3, 5, 9, 4, 8, 3, 2, 4, 9, 5, 2, 9, 4, 3, 2, 8];
let arr2 = [1, 7, 3, 5, 9, 4, 8, 3, 2, 4, 9, 5, 2, 9, 4, 3, 2, 8];
let arr3 = [1, 7, 3, 5, 9, 4, 8, 3, 2, 7, 4, 7, 9, 5, 2, 9, 4, 3, 2, 8];
// naivePartition(arr, 0, arr.length - 1, 1);
// lombutoPartition(arr2, 0, arr2.length - 1, 1);
//HoarePartition(arr3, 0, arr3.length - 1, 1);

//Testing for quick Sort Algo
// let arr4 = [8, 4, 7, 9, 3, 10, 5];
// HoarePartition(arr4, 0, arr4.length - 1, 0);
