import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * One array will be given and It need to be sorted 
 * Special case - Its K Sorted / Nearly Sorted - This means all element is only Upto K place misplaces 
 * Left or Right 
 * Meaning of K sorted array - Any element is displaces Upto K place from its Original Position 
 * Here we will use a Logic called Gap Sort ....
 * Heap can be used - But How we can solve using Heap 
 * Lets K = 3 
 * So for array - 6,5,3,2,8,10,9
 * we can say to decuide the value of Arr[0] we need to check Arr[0]---> Arr[3]
 * we can say to decuide the value of Arr[1] we need to check Arr[1]---> Arr[4]
 * we can say to decuide the value of Arr[2] we need to check Arr[2]---> Arr[5]
 * So on .... 
 */
public class NearlySortedArray {
  public static void main(String[] args) {
      int arr[] = new int[]{6,5,3,2,8,10,9};
      int k = 3;
  System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));
  PriorityQueue<Integer> minHeap = new PriorityQueue<>();
  int start = 0; //For Pusing First K+1 elements in Heap 
  while (minHeap.size()<k+1) {
    minHeap.offer(arr[start]);
    start++;
  }
  start = 0; //Actual Array Manipulation Index tracker 
  for (int i = minHeap.size(); i < arr.length; i++) {
     arr[start] = minHeap.poll(); //Put Min element to correct Pos
     minHeap.offer(arr[i]); //Fill next lement in Heap 
     start++ ; //Updating Index Pos for array Update 
  }
  //For remaining elements lets Do 
  while (!minHeap.isEmpty()) {
    arr[start] = minHeap.poll(); 
    start++ ;
  }
  System.out.println("Nearly  Sorted Array  : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));
  
  }
}
