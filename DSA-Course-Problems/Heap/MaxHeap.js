/**
 * Max Heap - parent is greater than its Two Childs
 * HeaP is represented in array SAME start from index 1
 * for ith element
 * Left child 2i
 * right child - 2i+1
 * parent for any child  i Math.floor(i/2)
 *
 *
 */

export default class MaxHeap {
  arr;
  length;
  constructor() {
    this.arr = new Array();
    this.arr.push(Number.MIN_SAFE_INTEGER);
    this.length = 0;
  }
  /**
   * During insertion Put the value to the Last available Box
   * Then Just do swap operations by comparing its parent to eventually shift it to correct position
   */
  insert(value) {
    this.arr.push(value);
    this.length++;
    let index = this.arr.length - 1;
    //We need to make Heap alignment - >1 because if Only 1 element is there no meaning of heap fixture
    while (index > 1) {
      let parent = Math.floor(index / 2);
      if (this.arr[parent] <= value) {
        //Swap them
        this.#swap(index, parent);
        index = parent;
      } else {
        break; // reached correct position
      }
    }
    return this;
  }

  #swap(source, target) {
    let temp = this.arr[source];
    this.arr[source] = this.arr[target];
    this.arr[target] = temp;
  }
  /**
   * During delete it shoud happen only frm the top of the Heap - that means Root element is deleted - which is nothing But the 1st element of array
   * Then Take the last element from the array and Put it here - Now we need to make it again a max heap
   * from Root element compare with Both child and then swap with largest child and go on ....
   */
  delete() {
    this.#swap(1, this.arr.length - 1);
    this.arr.pop();

    this.length--;
    let index = 1;
    while (index < this.arr.length) {
      if (index === this.arr.length - 1) break; // Means You have already reached last element

      let leftChild = index * 2;
      let rightChild = index * 2 + 1;
      let largest = Number.MIN_SAFE_INTEGER;
      let largestIndex = Number.MIN_SAFE_INTEGER;
      if (leftChild < this.arr.length && this.arr[leftChild] >= largest) {
        largestIndex = leftChild;
        largest = this.arr[leftChild];
      }
      if (rightChild < this.arr.length && this.arr[rightChild] >= largest) {
        largestIndex = rightChild;
        largest = this.arr[rightChild];
      }
      if (this.arr[index] <= this.arr[largestIndex]) {
        this.#swap(largestIndex, index);
        index = largestIndex;
      } else {
        break;
      }
    }
    return this;
  }

    /**
   * When we delete an element from the Heap - Just store it on some other array - that will be Sorted by default
   * Because Heap root is always Smallets
   */
    maxHeapSort() {
      let temparr = new Array();
      while (this.arr.length > 1) {
        temparr.push(this.arr[1]);
        this.delete();
        this.printHeap();
      }
      console.log(`Sorted array - ${temparr}`);
      return this;
    }

  printHeap() {
    console.log(`The Printed Heap is - ${this.arr.slice(1)}`);
    return this;
  }
}

let maxHeap = new MaxHeap();
maxHeap
  .insert(6)
  .insert(12)
  .insert(15)
  .insert(20)
  .insert(10)
  .insert(5)
  .insert(30)
  .printHeap()
  .insert(40)
  .printHeap();
console.log("***************************************");
maxHeap = new MaxHeap();
maxHeap
  .insert(10)
  .insert(20)
  .insert(30)
  .insert(25)
  .insert(5)
  .insert(40)
  .insert(35)
  .printHeap().maxHeapSort();

console.log("***************************************");
maxHeap
  .delete()
  .delete()
  .delete()
  .delete()
  .delete()
  .delete()
  .delete()
  .delete()
  .printHeap()
  .insert(40)
  .insert(25)
  .insert(5)
  .insert(40)
  .printHeap()
  .delete()
  .printHeap();
