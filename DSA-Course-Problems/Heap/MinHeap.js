/**
 * Min Heap is a Heap Structure Where Parent is less than or equal to it's decendents
 * Here also we shouduse 1 based Index
 * for Parent i the Left child - 2i and Right child - 2i + 1
 * for a child 1 parent is Math.floor(i/2)
 */

class MinHeap {
  length;
  arr;
  constructor() {
    this.arr = new Array();
    this.arr.push(null); //1 based Index
    this.length = 0;
  }
  /**
   * First we Put element at last of the array
   * Then we start comparing it with it's parent for larger  parent and move Upward via swapping
   * Eventually it will be placed in such place that it is smaller or equal to it's decendents
   * @param {*} element
   * @returns
   */
  insert(element) {
    //First insert element at last availave spot
    this.arr.push(element);
    this.length++;
    let index = this.arr.length - 1;
    //Now Move it to correct position - >1 because if Index is already 1 - Means Only 1 element - No sense of making heap fixture  
    while (index > 1) {
      let parentIndex = Math.floor(index / 2);
      if (this.arr[parentIndex] > this.arr[index]) {
        this.#swap(index, parentIndex);
        index = parentIndex;
      } else {
        break;
      }
    }
    return this;
  }

  /**
   *First  Overwrite last  element  to  Root element  and delete last element - technically deleting root element and replace it with last element 
   Then Do check from parent toward child and keep moving down with smaller child until you are at correct place 
   * @returns 
   */
  delete() {
    this.#swap(1, this.arr.length - 1);
    this.arr.pop();
   
    this.length--;
    let index = 1;
    while (index < this.arr.length) {
      if (index === this.arr.length - 1) break;

      let leftChild = index * 2;
      let rightChild = index * 2 + 1;
      let smallest = Number.MAX_SAFE_INTEGER;
      let smallestIndex = Number.MAX_SAFE_INTEGER;
      if (leftChild < this.arr.length && this.arr[leftChild] <= smallest) {
        smallestIndex = leftChild;
        smallest = this.arr[leftChild];
       
      }
      if (rightChild < this.arr.length && this.arr[rightChild] <= smallest) {
        smallestIndex = rightChild;
        smallest = this.arr[rightChild];
       
      }
      if (this.arr[index] >= this.arr[smallestIndex]) {
        this.#swap(smallestIndex, index);
        index = smallestIndex;
      }else{
        break;
      }
    }
    return this;
  }
  printArr() {
    console.log(`Current Array - ${this.arr.slice(1)}`);
    return this;
  }
  /**
   * When we delete an element from the Heap - Just store it on some other array - that will be Sorted by default
   * Because Heap root is always Smallets
   */
  minHeapSort() {
    let temparr = new Array();
    while (this.arr.length > 1) {
      temparr.push(this.arr[1]);
      this.delete();
      this.printArr();
    }
    console.log(`Sorted array - ${temparr}`);
    return this;
  }

  #swap(source, target) {
    let temp = this.arr[source];
    this.arr[source] = this.arr[target];
    this.arr[target] = temp;
  }
}

new MinHeap()
  .insert(20)
  .insert(30)
  .insert(5)
  .insert(15)
  .insert(25)
  .printArr()
  .delete()
  .printArr()
 .minHeapSort();
