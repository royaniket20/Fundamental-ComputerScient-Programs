import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConnectRopes {
  /**
   * Given an Array which are length of Ropes 
   * arr = [1,2,3,4,5]
   * Now the Cost of connecting two Ropes = Length of Both Ropes 
   * Suppose You connect 2,3 -- Length 5 , Cost - 5 
   * 
   * Example - 
   * 1+2 = [Len = 3][Cost = 3]
   * 3+ 3= [Len = 6][Cost = 6]
   * 6+4 = [Len = 10][Cost = 10]
   * 10+5 =[Len = 15][Cost = 15]
   * ---------------------------
   * Total Cost = 3+6+10+15 - We need to Minimise this Cost 
   * We can observeOne pattern - If with remaining Rops if we try to always Connect two Minimum 
   * Ropes then We can Keep cost Minimum 
   * 
   * [1,2,3,4,5]
   * Min - 1 ,2 ---Cost = 3
   * [3,3,4,5]
   * Min - 3,3 --Cost --6
   * [6,4,5]
   * Min - 4,5 --Cost - 9 
   * [6,9]
   * Cost ------------15
   * Total Cost - 15 + 9 + 6 + 3 = 33 - This is Minimum cost 
   */
  public static void main(String[] args) {
      int arr[] = new int[]{1,2,3,4,5};
  System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));
  PriorityQueue<Integer> minHeap = new PriorityQueue<>();
  for (int i = 0; i < arr.length; i++) {
      minHeap.offer(arr[i]);
  }
  int totalCost = 0;
  while (minHeap.size()>=2) {
    int firstMin = minHeap.poll();
    int secondMin = minHeap.poll();
    int currentCost =  firstMin + secondMin;
    minHeap.offer(currentCost);
    System.out.println("Adding "+firstMin+" & "+secondMin);
    totalCost = totalCost + currentCost;
  }
  System.out.println("Rope Length = " + minHeap.poll());
  System.err.println("Minimum Cost = "+totalCost);

  }
}
