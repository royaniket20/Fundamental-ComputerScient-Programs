import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TopKFrequentNumber {
  /**
   * We need Top K frequent elements 
   * [1,1,1,3,2,2,4]
   * K =2 
   * then Output is 1 , 2 
   * 
   * Appraoch - This we can use Hash and Heap Together to Do this 
   * @param args
   */
  public static void main(String[] args) {
       int arr[] = new int[]{1,3,2,1,2,1,4};
       int k = 2;
       Map<Integer , Integer> freqMap = new HashMap<>();
       PriorityQueue<Pair> minHeap = new PriorityQueue<>((a,b)-> a.freq.compareTo(b.freq));
  System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));

  for (int i = 0; i < arr.length; i++) {
    if(freqMap.containsKey(arr[i])){
      freqMap.put(arr[i], freqMap.get(arr[i])+1);
    }else{
      freqMap.put(arr[i], 1);
    }
  }
  freqMap.entrySet().stream().forEach(entry ->{
    minHeap.offer(new Pair(entry.getKey(),entry.getValue()));
    if(minHeap.size()>k){
     minHeap.poll(); //Keep[ing Size always to 2 
    }
  });
  System.out.println("Showing Top "+k+" Most Frequest element : ");
  while (!minHeap.isEmpty()) {
    System.out.println("Element : "+minHeap.peek().num +" : [Count :  "+minHeap.poll().freq+"]");
  }
  }

  static class Pair {
     Integer num;
     Integer freq;

     public Pair(Integer num ,Integer freq){
this.num = num;
this.freq = freq;
     }

    @Override
    public String toString() {
      return "Pair [num=" + num + ", freq=" + freq + "]";
    }

     
  }
}
