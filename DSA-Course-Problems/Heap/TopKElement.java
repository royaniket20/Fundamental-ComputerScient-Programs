import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TopKElement {
  public static void main(String[] args) {
  int arr[] = new int[]{7,10,4,3,20,15};
    System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));

  int k = 3;
 //If we go for sorting way - then Complexity of NLogN
 //We can reduce ti NlogK - k length Heap will be used 
   PriorityQueue<Integer> minHeap = new PriorityQueue<>();
   for (int i = 0 ; i< arr.length ; i++) {
    minHeap.add(arr[i]);
    if(minHeap.size()>k){
      minHeap.poll();
    }
   }
   System.out.println(k+"th Largest   Element is - "+ minHeap.peek());
   System.out.println("K largest elements Printing");
   while (!minHeap.isEmpty()) {
    System.out.println("Element : "+minHeap.poll());
   }
  }
}