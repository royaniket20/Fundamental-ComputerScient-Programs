import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ButtomKElement {
  public static void main(String[] args) {
  int arr[] = new int[]{7,10,4,3,20,15};
  System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));
  int k = 3;
 //If we go for sorting way - then Complexity of NLogN
 //We can reduce ti NlogK - k length Heap will be used 
   PriorityQueue<Integer> maxHeap = new PriorityQueue<>((a,b)-> b.compareTo(a));
   for (int i = 0 ; i< arr.length ; i++) {
    maxHeap.add(arr[i]);
    if(maxHeap.size()>k){
      maxHeap.poll();
    }
   }
   System.out.println(k+"th Smallest   Element is - "+ maxHeap.peek());
   System.out.println("K Smallest elements Printing");
   while (!maxHeap.isEmpty()) {
    System.out.println("Element : "+maxHeap.poll());
   }
  }
}