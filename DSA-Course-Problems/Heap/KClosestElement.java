import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KClosestElement {
  /**
   * Given an Array Find out K closest element for the Given Number S   - Here What we will do is 
   * For each element - Find out the Diff between Element of Array  and S and Store it in Max Heap 
   * So that we will Fetch Kth Smallest Diff  
   * @param args
   */
  public static void main(String[] args) {
     int arr[] = new int[]{5,6,7,8,9};
     int k = 3;
     int x = 7;
  System.out.println(" Input : "+ IntStream.of(arr).boxed().collect(Collectors.toList()));
  PriorityQueue<Pair> maxHeap = new PriorityQueue<>((a,b)-> b.diff.compareTo(a.diff));
  for (int i = 0; i < arr.length; i++) {
    maxHeap.offer(new Pair(arr[i], Math.abs(arr[i]-x)));
    if(maxHeap.size()>k){
      maxHeap.poll();
    }
  }
  System.out.println(k+" Closest elemt to "+x+" is - ");
  while (!maxHeap.isEmpty()) {
    System.out.println(" Element : "+maxHeap.poll().num);
  }

}

static class Pair{
  Integer num;
  Integer diff;
  public Pair(Integer num , Integer diff){
    this.diff = diff;
    this.num = num;
  }
}
}
