/**
 * everytime you insert into a Heap - you need to do adjustment
 * This adjustment for each Insert take log n time wrost case
 * for N element max heap - it will take nLog n time
 *
 * But what we can do is Blindly add the elements in the array and then Just Call heapify
 * Heapify can do the max heap creation on Just N time
 */

class MaxHeap {
  arr;
  constructor() {
    this.arr = new Array();
    this.arr.push(null);
  }
  /**
   * During insertion Put the value to the Last available Box
   * Then Just do swap operations by comparing its parent to eventually shift it to correct position
   */
  insert(value) {
    this.arr.push(value);
    let index = this.arr.length - 1;
    //We need to make Heap alignment - >1 because if Only 1 element is there no meaning of heap fixture
    while (index > 1) {
      let parent = Math.floor(index / 2);
      if (this.arr[parent] <= value) {
        //Swap them
        this.#swap(index, parent);
        index = parent;
      } else {
        break; // reached correct position
      }
    }
    return this;
  }

  #swap(source, target) {
    let temp = this.arr[source];
    this.arr[source] = this.arr[target];
    this.arr[target] = temp;
  }
  /**
   *  Heapify will do the traversal from Right to Left
   * Because doing from Right to Left - First we are encoutering the leaf elements -
   * Heapify we generally going to do look downwards - so for leaf node there is no below elements
   * As we know for a full bimnary tryy of n nodes n/2 elements are already leaf node
   * We are going to work only for rest of the n/2 elemnts
   * In Heapify we adjust elements to downwards
   */
  heapify() {
    console.log(`Current State Before Heapify - ${this.arr.slice(1)}`);
    for (let i = this.arr.length - 1; i > 0; i--) {
      this.#heapifyRecursive(i, this.arr);
    }
    console.log(`Current State After Heapify - ${this.arr.slice(1)}`);
    return this;
  }

  #heapifyRecursive(index) {
    console.log(
      `Calling Heapify For element arr[${index}] = ${this.arr[index]}`
    );
    //Base case
    if (index < 1) {
      return; // All entries covered
    }
    let currentElement = this.arr[index];
    let largestAmongChilds = Number.MIN_SAFE_INTEGER;
    let largestAmongChildIndex = -1;
    let leftChildIndex = index * 2;
    let rightChildIndex = index * 2 + 1;

    if (leftChildIndex < this.arr.length) {
      console.log(
        `Left Child arr[${leftChildIndex}] = ${this.arr[leftChildIndex]}`
      );
      largestAmongChildIndex =
        this.arr[leftChildIndex] > currentElement ? leftChildIndex : index;
      largestAmongChilds =
        this.arr[leftChildIndex] > currentElement
          ? this.arr[leftChildIndex]
          : currentElement;
    }
    if (rightChildIndex < this.arr.length) {
      console.log(
        `Right Child arr[${rightChildIndex}] = ${this.arr[rightChildIndex]}`
      );
      largestAmongChildIndex =
        this.arr[rightChildIndex] > largestAmongChilds
          ? rightChildIndex
          : largestAmongChildIndex;
      largestAmongChilds =
        this.arr[rightChildIndex] > largestAmongChilds
          ? this.arr[rightChildIndex]
          : largestAmongChilds;
    }
    if (largestAmongChildIndex != -1) {
      console.log(
        `Largest among parent and it's children - arr[${largestAmongChildIndex}] =  ${largestAmongChilds}`
      );
    }

    if (index != largestAmongChildIndex && largestAmongChildIndex != -1) {
      //Swap Elements
      this.#swap(index, largestAmongChildIndex);
      console.log(
        ` Swapped Postion between arr[${index}] <==> arr[${largestAmongChildIndex}]`
      );
      console.log(`Calling recursively for - arr[${largestAmongChildIndex}]`);

      this.#heapifyRecursive(largestAmongChildIndex);
    } else {
      console.log(` No swapping moving to next element !`);
    }
  }

  /**
   * Adding value manually to be heapified later
   */
  add(value) {
    this.arr.push(value);
    this.length++;
    return this;
  }

  printHeap() {
    console.log(`The Printed Heap is - ${this.arr.slice(1)}`);
    return this;
  }
}

let maxHeap = new MaxHeap();
maxHeap
  .insert(6)
  .printHeap()
  .insert(12)
  .printHeap()
  .insert(15)
  .printHeap()
  .insert(20)
  .printHeap()
  .insert(10)
  .printHeap()
  .insert(5)
  .printHeap()
  .insert(30)
  .printHeap()
  .insert(40)
  .printHeap();
console.log("***************************************");
maxHeap = new MaxHeap();
maxHeap
  .add(6)
  .add(12)
  .add(15)
  .add(20)
  .add(10)
  .add(5)
  .add(30)
  .add(40)
  .heapify()
  .printHeap();

console.log("***************************************");
