let countOfRepetedelement = function countOfRepetedelement(arr, element) {
  console.log(`Given array - ${arr} , Searched Element - ${element}`);
  arr.sort((a, b) => a - b);
  console.log(`Sorted Array - ${arr}`);
  //Because we are using Open addressing  - Have an Array with largest Element Number value + 1
  let hashTable = new Array(arr[arr.length - 1] + 1).fill(-1);
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (hashTable[element] < 0) {
      hashTable[element] = 1;
    } else {
      hashTable[element] = hashTable[element] + 1;
    }
  }
  console.log(
    `Element - ${element} appreas  ${
      hashTable[element] ?? 0
    } times in the array`
  );
};

let arr = [2, 5, 7, 4, 3, 2, 9, 8, 7, 6, 4, 2, 1, 5];

countOfRepetedelement(arr, 5);
countOfRepetedelement(arr, 1);
countOfRepetedelement(arr, 10);

//************ String use case **************** */

let countOfRepetedelementForString = function countOfRepetedelementForString(
  arr,
  element
) {
  console.log(`Given array - ${arr} , Searched Element - ${element}`);
  //We know there is only 255 ascii chars ranging from 0 - 255 - So have an array of 256
  let hashTable = new Array(256).fill(null);
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (!hashTable[element]) {
      hashTable[element] = 1;
    } else {
      hashTable[element] = hashTable[element] + 1;
    }
  }
  console.log(
    `Element - ${element} appreas  ${
      hashTable[element] ?? 0
    } times in the array`
  );
};

countOfRepetedelementForString("ghyoirne4525@#$@!#$2342", "5");

countOfRepetedelementForString("ghyoirne4525@#$@!#$2342", "@");
