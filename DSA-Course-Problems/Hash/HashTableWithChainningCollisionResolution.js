class Node {
  value;
  key;
  next;
  constructor(key, value) {
    this.value = value;
    this.key = key;
    this.next = null;
  }
}

class HashTable {
  data;
  constructor(size) {
    this.data = new Array(size).fill(null);
  }

  //Its a private method
  _hash(key) {
    let keyData = 0;
    for (let i = 0; i < key.length; i++) {
      keyData = keyData + key[i].charCodeAt();
    }
    keyData = keyData % this.data.length;
    return keyData;
  }
  //Put Operation
  put(key, value) {
    let index = this._hash(key);
    if (this.data[index] == null) {
      console.log(`Adding Data First Time`);
      let node = new Node(key, value);
      this.data[index] = node;
    } else {
      // console.log(`Either Hash Collision or Duplicate Entry`);
      let node = this.data[index];
      while (true) {
        if (node.key === key) {
          console.log(`Key Matched - Replace the Value`);
          node.value = value;
          break;
        }
        if (node.next != null) {
          node = node.next;
        } else {
          console.log(`Its a New Value Addition to Collisioned Node`);
          let temp = new Node(key, value);
          node.next = temp;
          break;
        }
      }
    }
  }

  //get Function
  get(key) {
    let index = this._hash(key);

    if (this.data[index] != null) {
      let node = this.data[index];
      while (node != null) {
        if (node.key === key) {
          return `Key[${node.key}]== Value [${node.value}]`;
        }
        node = node.next;
      }
      return "No Data Found";
    } else {
      return "No Data Found";
    }
  }

  keys() {
    let result = [];
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i] != null) {
        let node = this.data[i];
        while (node != null) {
          result.push(node.key);
          node = node.next;
        }
      }
    }
    return result;
  }

  delete(key) {
    let index = this._hash(key);

    if (this.data[index] != null) {
      let start = this.data[index];
      let node = start;
      let temp = start;
      while (node != null) {
        if (node.key === key) {
          let res = `Key[${node.key}]==> Value [${node.value}] Deleted`;
          if (node === start) {
            //Removing the First Element Itself
            this.data[index] = start.next;
          } else {
            //Removing Element from Middle or Last
            temp.next = node.next;
            delete node.next;
            node = null;
          }
          return res;
        }
        temp = node;
        node = node.next;
      }
      return "No Data Found To Delete";
    } else {
      return "No Data Found To Delete";
    }
  }

  printHashTable() {
    console.log(JSON.stringify(this.data));
  }
}

let obj = new HashTable(3);
obj.put("Aniket", 23);
obj.put("Aniket", 25);
console.log(obj.get("Aniket"));
obj.put("Aniket", 24);
obj.put("Aniket", 29);
obj.put("Amit", 23);
obj.put("Amit", 25);
obj.put("Sriparna", 24);
obj.put("Rimi", 23);
obj.put("Rimpa", 23);
obj.put("Runa", 23);

obj.printHashTable();
console.log(obj.get("Aniket"));
console.log(obj.get("Rimi"));
console.log(obj.get("Sri"));
console.log(obj.keys());

console.log(obj.delete("Anike"));
console.log(`-------------BEFORE ANY DELETE OPERATION ----------`);
obj.printHashTable();
console.log(obj.delete("Rimpa"));
obj.printHashTable();
console.log(obj.delete("Runa"));
obj.printHashTable();
console.log(obj.delete("Aniket"));
obj.printHashTable();
console.log(obj.delete("Sriparna"));
obj.printHashTable();
console.log(obj.delete("Amit"));
obj.printHashTable();
console.log(obj.delete("Rimi"));
obj.printHashTable();
