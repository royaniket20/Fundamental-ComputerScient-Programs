/**
 * When you have keys beyong 10^7 its better to go for channing approach
 * That means dont go for array - rather Go for something called Map
 */

let countOfRepetedelement = function countOfRepetedelement(arr, element) {
  console.log(`Given array - ${arr} , Searched Element - ${element}`);
  let map = new Map();
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (map.has(element)) {
      map.set(element, 1 + map.get(element));
    } else {
      map.set(element, 1);
    }
  }
  console.log(
    `Element - ${element} appreas  ${map.get(element) ?? 0} times in the array`
  );
};

let arr = [2, 5, 7, 4, 3, 2, 9, 8, 7, 6, 4, 2, 1, 5];

countOfRepetedelement(arr, 5);
countOfRepetedelement(arr, 1);
countOfRepetedelement(arr, 10);
