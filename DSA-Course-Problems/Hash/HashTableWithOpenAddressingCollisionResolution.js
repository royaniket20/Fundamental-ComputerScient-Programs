class Node {
  value;
  key;
  constructor(key, value) {
    this.value = value;
    this.key = key;
  }
}

class HashTable {
  data;
  constructor(size) {
    this.data = new Array(size).fill(null);
  }

  //Its a private method
  _hash(key, nextIndex) {
    let keyData = 0;
    for (let i = 0; i < key.length; i++) {
      keyData = keyData + key[i].charCodeAt();
    }
    keyData = (keyData + nextIndex) % this.data.length;
    return keyData;
  }
  //Put Operation
  put(key, value) {
    let index = this._hash(key, 0);
    // console.log(`Hash Collision Happened - Fix Next Empty /Deleted Location`);
    let incrementer = 1;
    let initialIndex = index;
    do {
      if (this.data[index] == null) {
        console.log(`Fresh Entry Made  -- ${key}==> ${value}`);
        let node = new Node(key, value);
        this.data[index] = node;
        return;
      }
      if (this.data[index].key === key) {
        console.log(`Key Matched - Replace  -- ${key}==> ${value}`);
        this.data[index].value = value;
        return;
      }
      if (this.data[index].value == "DELETED") {
        console.log(`DELETED Node - Replace  -- ${key}==> ${value}`);
        let node = new Node(key, value);
        this.data[index] = node;
        return;
      }
      index = this._hash(key, incrementer++);
    } while (initialIndex != index);
    console.log(`No space to Enter New Data -- ${key}==> ${value}`);
    return;
  }
  //Fetch all Keys Operation
  keys() {
    let result = [];
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i] != null || this.data[i].value != "DELETED") {
        let node = this.data[i];
        result.push(node.key);
      }
    }
    return result;
  }
  //Delete Operations
  delete(key) {
    let index = this._hash(key, 0);
    let tempIndex = index;
    console.log(`Starting at Index - ${index}`);
    let incrementer = 1;
    do {
      if (this.data[index] == null) {
        return `Key[${key}] -- Not Found for Deletion`;
      }
      if (this.data[index].key === key && this.data[index].value != "DELETED") {
        this.data[index].value = "DELETED";
        return `Key[${key}] -- Deleted `;
      }
      index = this._hash(key, incrementer++);
      console.log(`Next Open Addr Index - ${index}`);
    } while (tempIndex != index);
    return `Key[${key}] -- Not Found for Deletion`;
  }

  //get Function
  get(key) {
    let index = this._hash(key, 0);
    let tempIndex = index;
    console.log(`Starting at Index - ${index}`);
    let incrementer = 1;
    do {
      if (this.data[index] == null) {
        return `Key[${key}] -- Not Found for Search`;
      }
      if (this.data[index].key === key && this.data[index].value != "DELETED") {
        return `Key[${key}] ==> Value[${this.data[index].value}] -- Found For Search `;
      }
      index = this._hash(key, incrementer++);
      console.log(`Next Open Addr Index - ${index}`);
    } while (tempIndex != index);
    return `Key[${key}] -- Not Found for Search`;
  }

  printHashTable() {
    console.log(JSON.stringify(this.data));
  }
}

let obj = new HashTable(7);
console.log("-----INSSERT OPRTATIONS --------------");
obj.put("Anikei", 22);
obj.put("Amikej", 33);
obj.put("Aniket", 44);
obj.put("Rimi", 55);
obj.put("Amit", 66);
obj.put("Amit", 77);
obj.put("Sriparna", 88);
obj.put("Rimi", 99);
obj.put("Rimpa", 111);
obj.put("Runa", 222);
console.log("-----FIND KEYS -------------");
console.log(obj.keys());
console.log("-----PRINT  OPRTATIONS --------------");
obj.printHashTable();
console.log("-----DELETE OPRTATIONS --------------");
console.log(obj.delete("Anike"));
console.log(obj.delete("Amikej"));
obj.put("Amikel", 555);
obj.printHashTable();
console.log("-----GET OPRTATIONS --------------");
console.log(obj.get("Aniket"));
console.log(obj.get("Rimi"));
console.log(obj.get("Sriparna"));
console.log(`-------------DELETE GET PRINT MIXTURE ----------`);
console.log(obj.delete("Aniket"));
console.log(obj.get("Aniket"));
console.log(obj.delete("Rimpa"));
console.log(obj.delete("Runa"));
console.log(obj.delete("Rimi"));
obj.printHashTable();
console.log(obj.get("Amikel"));
console.log(`---RE FILL DATA AGAIN -----`);
obj.printHashTable();
obj.put("AAA", 1);
obj.put("AAA", 1);
obj.put("BBB", 1);
obj.put("CCC", 1);
obj.put("DDD", 1);
obj.printHashTable();
