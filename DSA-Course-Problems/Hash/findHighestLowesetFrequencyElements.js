/**
 * When you have keys beyong 10^7 its better to go for channing approach
 * That means dont go for array - rather Go for something called Map
 */

let findHighLowFreq = function findHighLowFreq(arr) {
  console.log(`Given array - ${arr}`);
  let map = new Map();
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    if (map.has(element)) {
      map.set(element, 1 + map.get(element));
    } else {
      map.set(element, 1);
    }
  }

  let currentMaxFreq = Number.MIN_SAFE_INTEGER;
  let currentMaxFreqElement = null;
  let currentMinFreq = Number.MAX_SAFE_INTEGER;
  let currentMinFreqElement = null;
  map.forEach((v, k) => {
    //For max Frequency Case
    if (currentMaxFreq && currentMaxFreq < v) {
      currentMaxFreq = v;
      currentMaxFreqElement = k;
    }
    //For min Frequency Case
    if (currentMinFreq && currentMinFreq > v) {
      currentMinFreq = v;
      currentMinFreqElement = k;
    }
  });
  console.log(map);
  console.log(
    `Max Frequency - ${currentMaxFreqElement} : Count - ${currentMaxFreq}`
  );
  console.log(
    `Min Frequency - ${currentMinFreqElement} : Count - ${currentMinFreq}`
  );
};

let arr = [2, 5, 7, 4, 3, 2, 9, 8, 7, 6, 4, 2, 1, 5, 9, 9, 9, 7];

findHighLowFreq(arr);
