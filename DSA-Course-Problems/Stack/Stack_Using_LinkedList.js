/**
 * When you need to create stack using LL - you make sure 
 * Your head Node always Point to last Node --> which Point to 2nd last node 
 * So on .... 
 */

class Node {
  value;
  next;
  constructor(value) {
    this.value = value;
    this.next = null;
  }
  printNode() {
    return ` ${this.value} --> ${this.next != null ? this.next.value : "null"}`;
  }
}

class Stack {
  top;
  buttom;
  length;
  constructor() {
    this.length = 0;
    this.top = null;
    this.buttom = null;
  }

  //To Add element to Stack
  push(value) {
    console.log(`Pushed Element to Stack - ${value}`);
    let node = new Node(value);
    if (this.length === 0) {
      this.top = node;
      this.buttom = node;
    } else {
      let current = this.top;
      this.top = node;
      this.top.next = current;
    }
    this.length++;
    return this;
  }
  //To Remove Element
  pop() {
    let current = this.top;
    if (current != null) {
      this.top = this.top.next;
      delete current.next;
      this.length--;
      if (this.length === 0) {
        this.buttom = null;
      }
      console.log(`Poped Element to Stack - ${current.value}`);
    } else {
      console.log(`Nothing to Pop From Stack -EMPTY STACK`);
    }

    return this;
  }
  //To Check Top Element
  peek() {
    console.log(
      `Peeked Element to Stack Top - ${
        this.top != null ? this.top.value : "STACK EMPTY"
      }`
    );
    return this;
  }

  printStack() {
    let current = this.top;
    let arr = [];
    arr.push(" Top <- ");
    arr.push("  V  ");
    while (current != null) {
      arr.push(`  ${current.value}  `);
      arr.push("  V  ");
      current = current.next;
    }
    arr.push("Buttom [Null] <- ");
    console.log(`Height Of Stack ---- ${this.length}`);
    arr.forEach((item) => {
      console.log(item);
    });
    return this;
  }
}

let stackObj = new Stack();

stackObj.peek();

stackObj.push(5).push(6).push(5).push(3).printStack();

stackObj.peek();

stackObj.push(55).pop().printStack();

stackObj.pop().pop().pop().pop().pop().pop().printStack();
stackObj.peek();

console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.push(6);
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.push(8);
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.pop();
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.pop();
console.log("Stack Config : " + JSON.stringify(stackObj));
