/**
 * 
 * One Liner - Keep Pushing Operator to Stack -- If less important Operator come for stack insert - Pop All Import ones and add to Output / If Bracker close comes - Pop all ones til open bracket -- then continue 
 * 
 *  
1. Scan the infix expression from left to right. 

2. If the scanned character is an operand, Print it. 

3. Else, 

If the precedence of the scanned operator is greater than the precedence of the operator in the stack or the stack is empty or the stack contains a ‘(‘, push the character into the stack. 
Else [Smaller or Equal ], Pop all the operators from the stack which are greater than or equal to in precedence than that of the scanned operator. After doing that Push the scanned operator to the stack. 
4. If the scanned character is an ‘(‘, push it into the stack. 

5. If the scanned character is an ‘)’, pop the stack and output it until a ‘(‘ is encountered, and discard both the parenthesis. 

6. Repeat steps 2-5 until the entire infix expression is scanned. 

7. Print the output.

8. Pop and print the output from the stack until it is not empty.} exp 
 */

let conversion = function (exp) {
  console.log(`Given expression - ${exp}`);
  let operators = new Map();
  operators.set("+", 1);
  operators.set("-", 1);
  operators.set("*", 2);
  operators.set("/", 2);
  operators.set("^", 3);
  operators.set("(", 4);
  operators.set(")", 4);
  console.log(`Allowed operators -`);
  console.log(operators);
  let tracker = [];
  let result = [];
  for (let index = 0; index < exp.length; index++) {
    const element = exp[index];
    if (operators.has(element)) {
      //This is Operator
      //If the Operator is opening bracket  ( - then Just push it
      if (element === "(") {
        tracker.push(element);
      }
      //If Operator is Closing bracket ) then Keep popping until you pop the openeing bracket and add to result if Not a bracket
      //Brackets are ignored in Postfix
      else if (element === ")") {
        let popedElement = tracker.pop();
        while (popedElement != "(") {
          result.push(popedElement);
          popedElement = tracker.pop();
        }
      }
      //If Top elelemnt is ( or Simply stack is Empty Push operand freely
      else if (tracker.length === 0 || tracker[tracker.length - 1] === "(") {
        tracker.push(element);
      }
      //if the above condition not met  Pop all the operators from the stack which are greater than or equal to in precedence than that of the scanned operator
      // After doing that Push the scanned operator to the stack.
      else {
        let peekedElement = tracker[tracker.length - 1];
        while (operators.get(peekedElement) >= operators.get(element)) {
          peekedElement = tracker.pop();
          result.push(peekedElement);
          if (tracker.length == 0) break; // Stack becomes empty
        }
        tracker.push(element);
      }
    } else {
      result.push(element); //This is Just operand
    }
  }
  //Pop everything Until Stack is empty
  while (tracker.length > 0) {
    result.push(tracker.pop());
  }
  console.log(`Result  of Infix to Postfix - ${result}`);
  return result;
};

conversion("(a+b)-(c-d)/d^c");

conversion("(a+b)/(c-d)-(e*f)");

conversion("(a+b)");
