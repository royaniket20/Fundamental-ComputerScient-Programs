/**
 * Operators - ^ * / + - 
 * Operands - A-Z | a-z | 0-9
 * Priority Order 
 * () - 4
 * ^ = 3
 * * / = 2
 * + - = 1
 * Any other = 0 
 * 
 * InFix expression  -- General Programming Language 
 * (p+q)*(m-n)
 *  Prefix Expression - LISP , Tree DS use It 
 *  -- (+pq)*(-mn) --- *+pq-mn
 * Postfix expression --Stack Based Calculator 
 * 
 * (pq+)*(mn-) --- pq+mn-*
 * 
 */