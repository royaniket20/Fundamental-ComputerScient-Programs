/**
 * Given a Postfix Make It Infix
 * we get an Operand - Put on Stack
 * When we get a Operator - Pop 2 elements - create an element , wrap in Bracker
 * Push Back in Stack
 * This will work
 *
 */

let input = `AB-DE+F*/`;

function postfixToInfix(input) {
  let Operators = new Set();
  Operators.add("+");
  Operators.add("-");
  Operators.add("*");
  Operators.add("/");

  console.log(`Given Postfix - ${input}`);
  let stack = [];
  for (let index = 0; index < input.length; index++) {
    const element = input[index];
    if (Operators.has(element)) {
      //In case of Operator
      let operand1 = stack.pop();
      let operand2 = stack.pop();
      let data = "(" + operand2 + element + operand1 + ")";
      stack.push(data);
    } else {
      //In case of Operand
      stack.push(element);
    }
  }
  console.log(`Final Result - ${stack.pop()}`);
}

postfixToInfix(input);
