/**
 * Create a stack such that
 * It can support Push , Pop , Top and
 * get Min Operation
 *
 * GetMin - How is the Intution -
 * So hre what we can do is We can store a pair in the stack
 * One element will be the actual element and other element will be what I
 * see as Minimum till Now - So any time Stack Top 2nd element will give Minimum
 *
 * P.S - Even when we Pop then also Top 2nd element will aways have the Minimum
 *
 *
 *
 * Now this will take 2N spoace in Stack - How to Space Optimiae It
 * We can Keep Min variable and Track it on Push Or Pop Both Operation
 * Here Also a mathematical Formula to rememebr
 *
 * When we get a new Minimum on a non empty stack -
 * We do not store that in Stack rather we store it in Minimum
 * and using the below Formula we store the newVal in Stack
 * 2*currentVal - currentMinimum = newVal
 * So whener we  pop we go back to previous Mimimum and Pop also correctly
 * */

class MinStack {
  stack;
  top;
  length;
  constructor() {
    this.stack = [];
    this.top = null;
    this.length = 0;
  }

  push(element) {
    if (this.length === 0) {
      this.stack.push([element, element]);
      this.length++;
    } else {
      let currentMin = this.stack[this.length - 1][1];
      let nextelement = [element, element < currentMin ? element : currentMin];
      this.stack.push(nextelement);
      this.length++;
    }
    return this;
  }

  pop() {
    console.log(`Poped Element ${this.stack[this.length - 1][0]}`);
    this.length--;
    return this;
  }

  peek() {
    console.log(`Top  Element ${this.stack[this.length - 1][0]}`);

    return this;
  }

  Min() {
    console.log(`Min Element ${this.stack[this.length - 1][1]}`);

    return this;
  }
}

let specialStack = new MinStack();
specialStack.push(10).push(5).push(9).push(1);

specialStack.Min();

specialStack.pop().pop().Min();

console.log(`---------------------- advanced --------------------`);
class MinStackAdvance {
  stack;
  top;
  length;
  min;
  constructor() {
    this.stack = [];
    this.top = null;
    this.length = 0;
    this.min = 0;
  }

  push(element) {
    if (this.length === 0) {
      this.stack.push(element);
      this.length++;
      this.min = element;
    } else {
      if (this.min <= element) {
        //No change in Minimum
        this.stack.push(element);
      } else {
        //New Minimum is received
        let modifiedElement = 2 * element - this.min;
        this.min = element;
        this.stack.push(modifiedElement);
      }
      this.length++;
    }
    return this;
  }

  pop() {
    let popedElement = this.stack[this.length - 1];
    if (popedElement < this.min) {
      let modifiedElement = popedElement;
      let actualelement = this.min;
      //element is Modified we need to send back Min val - which is Original Open
      //also we need to track back to previous Minimum
      // let modifiedElement = 2*element - this.min;
      //Here we need to Now extract the  this.min which was previous Minimum
      this.min = 2 * actualelement - modifiedElement;
      popedElement = actualelement;
    } else {
      //element is Not modified
      //Nothing to Do
    }
    console.log(`Poped Element ${popedElement}`);
    this.length--;
    return this;
  }

  peek() {
    console.log(`Top  Element ${this.stack[this.length - 1]}`);

    return this;
  }

  Min() {
    console.log(`Min Element ${this.min}`);

    return this;
  }
}

specialStack = new MinStackAdvance();
specialStack.push(10).push(5).push(9).push(1);

specialStack.Min();

specialStack.pop().pop().Min();
