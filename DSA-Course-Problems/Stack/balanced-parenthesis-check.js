/**
 * Here is approach is when there is an Opening  bracket - Just push to Stack
 * When you get a closing bracket - top of stack shoud match the corresponding opening bracket
 * Else it is not balanced
 * @param {*} str
 */

let checkisValidParenthesis = function (str) {
  console.log(`Given Str - ${str}`);
  let isBalanced = true;
  let stack = [];
  for (let index = 0; index < str.length; index++) {
    let currentBracket = str[index];
    if (
      currentBracket === "(" ||
      currentBracket === "[" ||
      currentBracket === "{"
    ) {
      //They are opening bracket Keep pushing them on Stack
      stack.push(currentBracket);
    } else {
      //They are closing bracket - If balanced it shoud match with Top
      if (currentBracket === ")" && stack.pop() === "(") {
        continue;
      } else if (currentBracket === "}" && stack.pop() === "{") {
        continue;
      } else if (currentBracket === "]" && stack.pop() === "[") {
        continue;
      } else {
        isBalanced = false;
        break;
      }
    }
  }
  if (stack.length > 0) {
    isBalanced = false;
  }
  console.log(`Is the parenthesis are balanced - ${isBalanced}`);
};

checkisValidParenthesis("(())");

checkisValidParenthesis("(({[()]}))");
checkisValidParenthesis("(({[()]}){)}");
