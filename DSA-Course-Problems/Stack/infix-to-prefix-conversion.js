/**
 *
0 - First reverse the expression   
1. Scan the infix expression from left to right. - We will do Infix to PostFix Under controller Env 

2. If the scanned character is an operand, Print it
If Operand is Not ^ Then If stack empty or similar precedence Operator - Just Push It //Remeber for Infix to Postfix - we generally Push strictly greater precedence 

For the ^ We just need to Pop Out everything Upto ^ precedence 

If the precedence of the scanned operator is greater than or equals to the precedence of the operator in the stack or the stack is empty or the stack contains a ‘)‘, push the character into the stack. 
Else, Pop all the operators from the stack which are greater than in precedence than that of the scanned operator. After doing that Push the scanned operator to the stack. 
4. If the scanned character is an ‘)‘, push it into the stack. 

5. If the scanned character is an ‘(’, pop the stack and output it until a ‘)‘ is encountered, and discard both the parenthesis. 

6. Repeat steps 2-5 until the entire infix expression is scanned. 
 Pop and print the output from the stack until it is not empty exp 
6.1 - Reverse the result 
7. Print the output.
8.
 */

let conversion = function (exp) {
  console.log(`Given expression - ${exp}`);
  exp = exp.split("").reverse().join("");
  console.log(`Given expression reversed  - ${exp}`);
  let operators = new Map();
  operators.set("+", 1);
  operators.set("-", 1);
  operators.set("*", 2);
  operators.set("/", 2);
  operators.set("^", 3);
  operators.set("(", -1); //Just to make sure that theu dont get involved in precedence calculation
  operators.set(")", -1); //Just to make sure that theu dont get involved in precedence calculation
  console.log(`Allowed operators -`);
  console.log(operators);
  let tracker = [];
  let result = [];
  for (let index = 0; index < exp.length; index++) {
    const element = exp[index];
    if (operators.has(element)) {
      //This is Operator
      //If the Operator is closing bracket  ) - then Just push it
      if (element === ")") {
        tracker.push(element);
      }
      //If Operator is opening bracket ( then Keep popping until you pop the closing bracket  ) and add to result if Not a bracket
      else if (element === "(") {
        let popedElement = tracker.pop();
        while (popedElement != ")") {
          result.push(popedElement);
          popedElement = tracker.pop();
        }
      }
      //If Simply stack is Empty Push operand freely
      else if (tracker.length === 0) {
        tracker.push(element);
      }
      //if the above condition not met  Pop all the operators from the stack which are greater than  in precedence than that of the scanned operator
      // After doing that Push the scanned operator to the stack.
      else {
        let peekedElement = tracker[tracker.length - 1];
        while (operators.get(peekedElement) > operators.get(element)) {
          tracker.pop(); //poping the lement
          result.push(peekedElement); //Pushing the poped element in result - same as peekedElement
          peekedElement = tracker[tracker.length - 1]; //Getting next top for checking
          if (tracker.length === 0) break; //Stack becomes empty
        }
        tracker.push(element);
      }
    } else {
      result.push(element); //This is Just operand
    }
  }
  //Pop everything Until Stack is empty
  while (tracker.length > 0) {
    result.push(tracker.pop());
  }
  result = result.reverse();
  console.log(`Result  of Infix to Prefix =  [${result}]`);
  return result;
};

conversion("(a+b)");

conversion("K+L-M*N+(O^P)*W/U/V*T+Q");
