/**
 * Given a Postfix Make It Prefix
 * we get an Operand - Put on Stack
 * When we get a Operator - Pop 2 elements make One element - Operator | operand1  | operand2
 * Push Back in Stack
 * This will work
 *
 */

let input = `AB-DE+F*/`;

function postfixToPrefix(input) {
  let Operators = new Set();
  Operators.add("+");
  Operators.add("-");
  Operators.add("*");
  Operators.add("/");

  console.log(`Given Postfix - ${input}`);
  let stack = [];
  for (let index = 0; index < input.length; index++) {
    const element = input[index];
    if (Operators.has(element)) {
      //In case of Operator
      let operand1 = stack.pop();
      let operand2 = stack.pop();
      let data = element + operand2 + operand1;
      console.log(`Prepared Data - ${data}`);

      stack.push(data);
    } else {
      //In case of Operand
      console.log(`Pushing element - ${element}`);

      stack.push(element);
    }
  }
  console.log(`Final Result - ${stack.pop()}`);
}

postfixToPrefix(input);
