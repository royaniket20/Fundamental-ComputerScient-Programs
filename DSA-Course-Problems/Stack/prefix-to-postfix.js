/**
 * Given a Prefix Make It Postfix
 * This time We will start from last toward first
 * we get an Operand - Put on Stack
 * When we get a Operator - Pop 2 elements make One element -  operand1  | operand2 | Operator
 * Push Back in Stack
 * This will work
 *
 */

let input = `/-AB*+DEF`;

function postfixToPrefix(input) {
  let Operators = new Set();
  Operators.add("+");
  Operators.add("-");
  Operators.add("*");
  Operators.add("/");

  console.log(`Given Postfix - ${input}`);
  let stack = [];
  for (let index = input.length - 1; index >= 0; index--) {
    const element = input[index];
    if (Operators.has(element)) {
      //In case of Operator
      let operand1 = stack.pop();
      let operand2 = stack.pop();
      let data = operand1 + operand2 + element;
      console.log(`Prepared Data - ${data}`);

      stack.push(data);
    } else {
      //In case of Operand
      console.log(`Pushing element - ${element}`);

      stack.push(element);
    }
  }
  console.log(`Final Result - ${stack.pop()}`);
}

postfixToPrefix(input);
