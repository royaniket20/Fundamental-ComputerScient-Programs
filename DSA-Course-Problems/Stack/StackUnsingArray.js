 class Stack {
  arr;
  elements;
  capacity;
  constructor(capacity) {
    this.arr = [];
    this.capacity = capacity;
    this.elements =0;
  }

  //To Add element to Stack
  push(value) {
    if(this.capacity === 0){
      console.log(`Stack is Not Full !!`);
    }else {
      console.log(`Pushed Element to Stack - ${value}`);
      this.arr[this.elements] = value;
      this.elements++;
      this.capacity--;
    }
  
    return this;
  }
  //To Remove Element
  pop() {
    if (this.elements > 0) {
      let element = this.arr[this.elements-1];
      this.arr[this.elements-1] = null;
      console.log(`Poped Element to Stack - ${element}`);
      this.elements--;
      this.capacity++;
    } else {
      console.log(`Nothing to Pop From Stack -EMPTY STACK`);
    }

    return this;
  }
  //To Check Top Element
  peek() {
    console.log(
      `Peeked Element to Stack Top - ${
        this.elements != 0 ? this.arr[this.elements - 1] : "STACK EMPTY"
      }`
    );
    return this;
  }

  printStack() {
    let result = this.arr.filter((item)=>item!=null);
    console.log(result);     
    return this;
  }
}

let stackObj = new Stack(3);

stackObj.peek();

stackObj.push(5).push(6).push(55).push(3).printStack();

stackObj.peek();

stackObj.push(55).pop().printStack();

stackObj.pop().pop().pop().pop().pop().pop().printStack();
stackObj.peek();

console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.push(6);
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.push(8);
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.pop();
console.log("Stack Config : " + JSON.stringify(stackObj));
stackObj.pop();
console.log("Stack Config : " + JSON.stringify(stackObj));
