/**
 * Stack using Queue
 * initally you put value on 1 queue
 * when Next element come we have to take another Queue - Put all elements
 * in a 2nd Queue  - Put the Original element in Queue
 * Then again fill q1 with all elements of Q2
 */

/**
 * iF  TOO MANY PUSH oPERATION WE CAN oPTIMISE pUSH AND
 * degarde performance of pop and TOp  -Its exactly same
 * Here during push  we just use a simple queu
 * However when we do Top/Pop - we use 2nd Queue
 */

import QueueImpl from "./Queue.js";

class Stack {
  queue1;
  queue2;
  constructor() {
    this.queue1 = new QueueImpl(1);
    this.queue2 = new QueueImpl(2);
  }

  //To Add element to Stack
  push(value) {
    console.log(`Pushed Element to Stack - ${value}`);
    if (this.queue1.lengthOfQueue() == 0) {
      this.queue1.enqueue(value);
    } else {
      while (this.queue1.lengthOfQueue() > 0) {
        let element = this.queue1.dequeue();
        this.queue2.enqueue(element);
      }
      this.queue1.enqueue(value);
      while (this.queue2.lengthOfQueue() > 0) {
        let element = this.queue2.dequeue();
        this.queue1.enqueue(element);
      }
    }
    return this;
  }
  //To Remove Element
  pop() {
    if (this.queue1.lengthOfQueue() > 0) {
      let element = this.queue1.dequeue();
      console.log(`Poped Element to Stack - ${element}`);
    } else {
      console.log(`Nothing to Pop From Stack -EMPTY STACK`);
    }

    return this;
  }
  //To Check Top Element
  peek() {
    console.log(
      `Peeked Element to Stack Top - ${
        this.queue1.lengthOfQueue() > 0 ? this.queue1.peek() : "STACK EMPTY"
      }`
    );
    return this;
  }

  printStack() {
    console.log("Stack  Config : " + JSON.stringify(this));
    return this;
  }
}

let stackObj = new Stack();

stackObj.peek();

stackObj.push(5).push(6).push(55).push(3).printStack();

stackObj.peek();

stackObj.push(55).pop().printStack();

stackObj.pop().pop().pop().pop().pop().pop().printStack();
stackObj.peek();

stackObj.printStack();
stackObj.push(6);
stackObj.printStack();
stackObj.push(8);
stackObj.printStack();
stackObj.pop();
stackObj.printStack();
stackObj.pop();
stackObj.printStack();
