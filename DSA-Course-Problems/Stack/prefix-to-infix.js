/**
 * Given a Prefix Make It Infix
 * This time we start Iteration from Last to First
 * we get an Operand - Put on Stack
 * When we get a Operator - Pop 2 elements - create an element , wrap in Bracker
 * Push Back in Stack
 * This will work
 *
 */

let input = `*+PQ-MN`;

function prefixToInfix(input) {
  let Operators = new Set();
  Operators.add("+");
  Operators.add("-");
  Operators.add("*");
  Operators.add("/");

  console.log(`Given Prefix - ${input}`);
  let stack = [];
  for (let index = input.length - 1; index >= 0; index--) {
    const element = input[index];
    if (Operators.has(element)) {
      //In case of Operator
      let operand1 = stack.pop();
      let operand2 = stack.pop();
      let data = "(" + operand1 + element + operand2 + ")";
      stack.push(data);
    } else {
      //In case of Operand
      stack.push(element);
    }
  }
  console.log(`Final Result - ${stack.pop()}`);
}

prefixToInfix(input);
