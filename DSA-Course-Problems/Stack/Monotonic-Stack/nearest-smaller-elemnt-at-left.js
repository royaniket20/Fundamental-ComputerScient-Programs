/**
 * This is Sub probolem from NGE - Monotponic stack
 *
 * Given an array find nearest smaller element to the left of the array or give -1
 *
 * [4,5,2,10,8] ===> [-1,4,-1,2,2]
 */

/**
 * It is Opposoite of NGE
 * here iteration start from left to right
 * push element in empty array
 * if the top of the stack is Not the NSE --> thne Pop
 */

let array = [4, 5, 2, 10, 8];

function nearestSmallerElement(array) {
  console.log(`Original Array - ${array}`);
  let stack = [];
  stack.push(array[0]);
  array[0] = -1;
  for (let index = 1; index < array.length; index++) {
    const element = array[index];
    if (stack[stack.length - 1] < element) {
      array[index] = stack[stack.length - 1];
    } else {
      while (stack.length > 0 && stack[stack.length - 1] >= element) {
        stack.pop();
      }
      array[index] = stack.length > 0 ? stack[stack.length - 1] : -1;
    }
    stack.push(element);
    console.log(`Modified  Array - ${array}`);
  }

  console.log(`Modified Final  Array - ${array}`);
}

nearestSmallerElement(array);
