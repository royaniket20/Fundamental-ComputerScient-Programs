/**
 * Monotonic Stack - when we store element in Stack is a certain Order
 * Some increasing Order / decresing order or osme special order
 */

let stack = [6, 0, 8, 1, 3];
// NGE    == [8,8,-1,3,-1]

//When we stand at 6 next greater element is 8
//When we stand at 8 we return -1 [no greater element ]

/**
 * Intution - If we traverse from Ebnd to start -
 * atleast at any Index - I must have travelled all the right habnd elements
 * Now here the tric is as we get element push   in stack - use stack top as Next greater element
 * If that is Not the case Pop it until you get the next greater element for this
 * then push this current element also  - this will work
 *
 * Here the Stack is Monotonic Stack because elements at any point of time
 * Is stored in Decresing Order
 *
 */

let stackInput = [4, 12, 5, 3, 1, 2, 5, 3, 1, 2, 4, 6];

function NGE(stackInput) {
  let stack = [];
  console.log(`Actual Array    - ${stackInput}`);

  //Use the stack to Kerep track of Next greater element
  stack.push(stackInput[stackInput.length - 1]); //Pushing the last element in Stack to be used as NGE for previous elements toward the Left traversal
  stackInput[stackInput.length - 1] = -1; //Updated NGE for right Most element
  console.log(
    `Input[${
      stackInput.length - 1
    }] NGE is calculated ! - In Stack Elements - ${stack}`
  );
  console.log(`Modified  Array - ${stackInput}`);
  for (let index = stackInput.length - 2; index >= 0; index--) {
    const element = stackInput[index];
    if (stack[stack.length - 1] > element) {
      stackInput[index] = stack[stack.length - 1];
    } else {
      while (stack.length > 0 && stack[stack.length - 1] <= element) {
        console.log(stack.pop(), " Poped as Less that or equals to ", element);
      }
      stackInput[index] = stack.length > 0 ? stack[stack.length - 1] : -1;
    }
    stack.push(element);
    console.log(`Arr[${index}] NGE is calculated | Stack Elements - ${stack}`);
    console.log(`Modified  Array - ${stackInput}`);
  }
}

NGE(stackInput);
