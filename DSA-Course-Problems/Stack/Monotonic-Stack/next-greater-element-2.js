/**
 * Monotonic Stack - when we store element in Stack is a certain Order
 * Some increasing Order / decresing order or osme special order
 */
//Here we not only see at right but circle back and check in left portion also
//We have to traver whole array in Circular Mode
let stack = [2, 10, 12, 1, 11];
// NGE    == [10,12,-1,11,12]

//When we stand at 11 next greater element is 12
//When we stand at 8 we return -1 [no greater element ]

/**
 * Intution -Here Intution is We double the array Virtually for getting the NGE for the last element
 * That technically means we traverse array from n -> 1 to get the monotonic stack filled
 * Thren start from last element and follow same rule as NGE
 * Then Its just finding NGE as is for other elements
 * Here the Stack is Monotonic Stack because elements at any point of time
 * Is stored in Decresing Order
 *
 */

let stackInput = [2, 10, 12, 1, 11];

function NGE2(stackInput) {
  let stack = [];
  console.log(`Actual Array    - ${stackInput}`);
  //We Virually assume the array is Double - Now find the NGE for the last lement
  for (let index = stackInput.length - 1; index >= 0; index--) {
    const element = stackInput[index];
    if (stack.length == 0) {
      stack.push(element);
    } else {
      if (stack[stack.length - 1] > element) {
        stack.push(element);
      } else {
        while (stack.length > 0 && stack[stack.length - 1] <= element) {
          stack.pop();
        }
        stack.push(element);
      }
    }
  }
  console.log(`Now the Intial Stack - ${stack}`);

  for (let index = stackInput.length - 1; index >= 0; index--) {
    const element = stackInput[index];
    if (stack[stack.length - 1] > element) {
      stackInput[index] = stack[stack.length - 1];
    } else {
      while (stack.length > 0 && stack[stack.length - 1] <= element) {
        stack.pop();
      }
      stackInput[index] = stack.length > 0 ? stack[stack.length - 1] : -1;
    }
    stack.push(element);
    console.log(`Modified  Array - ${stackInput}`);
  }
}

NGE2(stackInput);
