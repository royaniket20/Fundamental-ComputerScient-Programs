//Queue is a First In First Out 
//Enqueu
//Dequeu 
//Peek - for getting the head element 


export default class Queue {
  arrInternal;
  index;
  constructor(index) {
    this.arrInternal = [];
    this.index = index;
  }

  //To Add element to Queue Rear
  enqueue(value) {
    console.log(`Add element to the Queue(${this.index}) - ${value}`);
    this.arrInternal.push(value);
    return this;
  }
  //To Remove Element
  dequeue() {
    if (this.arrInternal.length > 0) {
      let value = this.arrInternal.shift();
      console.log(`Removed Element from Queue(${this.index}) - ${value}`);
      return value;
    } else {
      console.log(`Nothing to Dequeue From QUEUE(${this.index}) -EMPTY QUEUE`);
      return null;
    }
  }
  //To Check First Element To be Exiting Next
  peek() {
    console.log(
      `Peeked Element to Queue(${this.index}) - ${
        this.arrInternal.length > 0 ? this.arrInternal[0] : null
      }`
    );
    return this.arrInternal.length > 0 ? this.arrInternal[0] : null;
  }

  lengthOfQueue() {
    // console.log(`Length of Queue(${this.index}) : ${this.arrInternal.length}`);
    return this.arrInternal.length;
  }
}
