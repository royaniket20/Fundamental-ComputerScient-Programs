class Queue {
  //Here we have Not used any Javascript Specific Methoods also Size mentioned
  arrInternal;
  front; //For Deletion
  rear; // For Insertion
  count;
  size;
  constructor(length) {
    this.arrInternal = new Array(length).fill("null");
    this.front = this.rear = 0;
    this.count = 1; //We are presering One empty Space
    this.size = length;
  }

  //To Add element to Queue Rear
  enqueue(value) {
    //Remmemeber in Circular Queue always One Place is Kept Empty
    if (this.count < this.size) {
      this.count++;
      console.log(`Add element to the Queue - ${value}`);
      this.rear++;
      this.arrInternal[this.rear % this.size] = value;
    } else {
      console.log(`Queue is already Full`);
    }
    console.log(this.arrInternal);
    return this;
  }
  //To Remove Element
  dequeue() {
    //Remmemeber in Circular Queue always One Place is Kept Empty
    if (this.count > 1) {
      this.count--;
      this.front++;
      let value = this.arrInternal[this.front % this.size];
      this.arrInternal[this.front % this.size] = "null";
      console.log(`Removed Element from Queue - ${value}`);
    } else {
      console.log(`Queue is Already Empty`);
    }

    return this;
  }
  //To Check First Element To be Exiting Next
  peek() {
    console.log(
      `Peeked Element to Queue - ${
        this.arrInternal[(this.front + 1) % this.size]
      }`
    );
    return this;
  }

  printQueue() {
    let arr = [];

    [...this.arrInternal].forEach((item, index) => {
      arr.push(item);
    });
    arr[this.rear % this.size] = "[Rear]" + arr[this.rear % this.size];
    arr[this.front % this.size] = "[Front]" + arr[this.front % this.size];
    console.log(arr.join("--"));

    console.log("Queue Config : " + JSON.stringify(queueObj));
    return this;
  }
}

let queueObj = new Queue(5);
queueObj.printQueue();
queueObj
  .enqueue(5)
  .enqueue(6)
  .enqueue(7)
  .enqueue(8)
  .enqueue(9)
  .enqueue(10)
  .enqueue(11)
  .enqueue(12)
  .enqueue(13)
  .enqueue(14)
  .enqueue(15)
  .printQueue();

queueObj
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .printQueue();

queueObj.enqueue(5).enqueue(6).enqueue(7).dequeue().printQueue();
queueObj.peek();

queueObj.dequeue().dequeue().printQueue();
