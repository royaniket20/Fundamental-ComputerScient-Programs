class Queue {
  //Here we have Not used any Javascript Specific Methoods also Size mentioned
  arrInternal;
  front; //For Deletion & Insertion
  rear; // For Insertion & Deletion
  size;
  constructor(length) {
    this.arrInternal = new Array(length).fill("null");
    this.front = this.rear = -1;
    this.size = length;
  }

  //To Add element to Queue Rear
  enqueueRear(value) {
    let pos = this.rear + 1;
    if (pos < this.size && pos != this.front) {
      console.log(`Add element to the Queue End - ${value}`);
      this.arrInternal[pos] = value;
      this.rear = pos;
    } else {
      console.log(`Queue is already Full At Ending`);
    }
    return this;
  }
  //To Remove Element From Queue  Rear
  dequeueRear() {
    let pos = this.rear;
    if (pos > -1 && pos != this.front) {
      let value = this.arrInternal[pos];
      this.arrInternal[pos] = "null";
      pos--;
      this.rear = pos;
      console.log(`Removed Element from Queue End - ${value}`);
    } else {
      console.log(`Queue is Already Empty`);
    }

    return this;
  }

  //To Add element to Queue Front
  enqueueFront(value) {
    let pos = this.front;
    if (pos > -1) {
      console.log(`Add element to the Queue Front - ${value}`);
      this.arrInternal[pos] = value;
      pos--;
      this.front = pos;
    } else {
      console.log(`Queue is already Full At Beginning`);
    }
    return this;
  }

  //To Remove Element From Queue Front
  dequeueFront() {
    let pos = this.front + 1;
    if (pos <= this.rear) {
      let value = this.arrInternal[pos];
      this.arrInternal[pos] = "null";
      this.front = pos;
      console.log(`Removed Element from Queue Front - ${value}`);
    } else {
      console.log(`Queue is Already Empty`);
    }

    return this;
  }

  printQueue() {
    let arr = [];

    [...this.arrInternal].forEach((item, index) => {
      arr.push(item);
    });
    console.log(arr.join("--"));

    console.log("Queue Config : " + JSON.stringify(queueObj));
    return this;
  }
}

let queueObj = new Queue(5);
queueObj.printQueue();
console.log("**********************************");
queueObj
  .enqueueRear(5)
  .enqueueRear(6)
  .enqueueRear(7)
  .enqueueRear(10)
  .printQueue()
  .enqueueRear(11)
  .enqueueRear(12)
  .enqueueRear(13)
  .printQueue();

console.log("**********************************");
queueObj
  .dequeueFront()
  .dequeueFront()
  .dequeueFront()
  .dequeueFront()
  .printQueue()
  .dequeueFront()
  .dequeueFront()
  .dequeueFront()
  .printQueue();

console.log("**********************************");
queueObj
  .dequeueRear()
  .dequeueRear()
  .dequeueRear()
  .printQueue()
  .dequeueRear()
  .dequeueRear()
  .dequeueRear()
  .printQueue();

console.log("**********************************");
queueObj
  .enqueueFront(55)
  .enqueueFront(66)
  .enqueueFront(77)
  .printQueue()
  .enqueueFront(88)
  .printQueue()
  .enqueueFront(11)
  .enqueueFront(12)
  .enqueueFront(13)
  .printQueue();
console.log("************ MIXTURE OF OPS **********************");

queueObj = new Queue(3);

queueObj.printQueue();

queueObj.enqueueFront(55).dequeueFront().dequeueRear().printQueue();

console.log("####################################################");

queueObj.enqueueRear(55).dequeueFront().dequeueRear().printQueue();

console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

queueObj
  .enqueueRear(55)
  .enqueueRear(66)
  .enqueueFront(77)
  .dequeueFront()
  .dequeueRear()
  .printQueue();
