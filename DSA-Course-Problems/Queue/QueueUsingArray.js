class Queue {
  arrInternal;
  capacity;
  elements;
  constructor(capacity) {
    this.arrInternal = [];
    this.capacity = capacity;
    this.elements = 0;
  }

  //To Add element to Queue Rear
  enqueue(value) {
    if(this.capacity === 0 ){
      console.log(`Queue is Now FULL`);
    }else {
      console.log(`Add element to the Queue - ${value}`);
      this.arrInternal[this.elements] = value;
      this.capacity--;
      this.elements++;
    }
    
    return this;
  }
  //To Remove Element
  dequeue() {
    if (this.elements > 0) {
      let value = this.arrInternal[0];
      //SHIFTING THE ELEMENTS 
      this.arrInternal.shift();
      this.capacity++;
      this.elements--;
      console.log(`Removed Element from Queue - ${value}`);
    } else {
      console.log(`Nothing to Dequeue From QUEUE -EMPTY QUEUE`);
    }

    return this;
  }
  //To Check First Element To be Exiting Next
  peek() {
    if(this.elements>0){
      console.log(
        `Peeked Element to Queue - ${
          this.arrInternal.length > 0 ? this.arrInternal[0] : "QUEUE EMPTY"
        }`
      );
    }else {
      console.log(`Nothing to Peek From QUEUE -EMPTY QUEUE`);
    }
   
    return this;
  }

  printQueue() {
    console.log(this.arrInternal);
    return this;
  }
}

let queueObj = new Queue(3);
console.log("Queue Config : " + JSON.stringify(queueObj));
queueObj.enqueue(5).enqueue(6).enqueue(7).enqueue(8).printQueue();

console.log("Queue  Config : " + JSON.stringify(queueObj));

queueObj
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .printQueue();

console.log("Queue Config : " + JSON.stringify(queueObj));

queueObj.peek().dequeue();
console.log("Queue Config : " + JSON.stringify(queueObj));
queueObj.enqueue(5).enqueue(6).enqueue(7).dequeue().printQueue();
console.log("Queue Config : " + JSON.stringify(queueObj));
