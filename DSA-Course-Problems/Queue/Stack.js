/**
 * Stack follow List in First Out 
 * Push - Add element 
 * Pop - Remove element 
 * top/peek - For checking top element 
 */

export default class Stack {
  arr;
  index;
  constructor(index) {
    this.arr = [];
    this.index = index;
  }

  //To Add element to Stack
  push(value) {
    // console.log(
    //   `Pushed Element to Stack ${this.index} - ${JSON.stringify(value)}`
    // );
    this.arr.push(value);
    return value;
  }
  //To Remove Element
  pop() {
    if (this.arr.length > 0) {
      let element = this.arr.pop();
      // console.log(
      //   `Poped Element to Stack ${this.index} - ${JSON.stringify(element)}`
      // );
      return element;
    } else {
      // console.log(`Nothing to Pop From Stack ${this.index} -EMPTY STACK`);
      return null;
    }
  }
  //To Check Top Element
  peek() {
    // console.log(
    //   `Peeked Element to Stack ${this.index} Top - ${
    //     this.arr.length != 0 ? this.arr[this.arr.length - 1] : "STACK EMPTY"
    //   }`
    // );
    return this.arr.length != 0 ? this.arr[this.arr.length - 1] : "STACK EMPTY";
  }
}
