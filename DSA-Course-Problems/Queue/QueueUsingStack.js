import StackImpl from "./Stack.js";
/**
 * Similar to the Using 2 Queue to create 1 Stack 
 * Here it is Just Opposite - Approach is Same 
 */
class Queue {
  Stack1;
  Stack2;
  length;

  constructor() {
    this.Stack1 = new StackImpl(1);
    this.Stack2 = new StackImpl(1);
    this.length = 0;
  }

  //To Add element to Queue Rear
  enqueue(value) {
    console.log(`Add element to the Queue - ${value}`);
    if (this.length === 0) {
      //First Element is Getting Inserted
      this.Stack1.push(value);
    } else {
      let popped1 = this.Stack1.pop();
      while (popped1 != null) {
        this.Stack2.push(popped1);
        popped1 = this.Stack1.pop();
      }
      this.Stack2.push(value);

      let popped2 = this.Stack2.pop();
      while (popped2 != null) {
        this.Stack1.push(popped2);
        popped2 = this.Stack2.pop();
      }
    }
    this.length++;
    return this;
  }
  //To Remove Element
  dequeue() {
    if (this.length > 0) {
      let value = this.Stack1.pop();
      console.log(`Removed Element from Queue - ${JSON.stringify(value)}`);
      this.length--;
    } else {
      console.log(`Nothing to Dequeue From QUEUE -EMPTY QUEUE`);
    }

    return this;
  }
  //To Check First Element To be Exiting Next
  peek() {
    if (this.length > 0) {
      let value = this.Stack1.peek();
      console.log(`Peeked Element to Queue -${JSON.stringify(value)}`);
    } else {
      console.log(`Peeked Element to Queue - EMPTY QUEUE`);
    }
    return this;
  }

  printQueue() {
    console.log("Queue  Config : " + JSON.stringify(this));
    return this;
  }
}

let queueObj = new Queue();
queueObj.printQueue();
queueObj.enqueue(5).enqueue(6).enqueue(7).enqueue(8).printQueue();

queueObj
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .printQueue();

queueObj.peek();

queueObj.printQueue();

queueObj.enqueue(5).enqueue(6).enqueue(7).enqueue(8).dequeue().printQueue();
queueObj.peek();
queueObj.dequeue().dequeue();
queueObj.peek();
queueObj.dequeue().peek();
