/**
 * Start from single Node with start and end start with same Node 
 * Then you keep moving End Node and pointing to last element 
 */

class Node {
  value;
  next;
  constructor(value) {
    this.value = value;
    this.next = null;
  }
  printNode() {
    return ` ${this.value} --> ${this.next != null ? this.next.value : "null"}`;
  }
}

class Queue {
  first;
  last;
  length;
  constructor() {
    this.length = 0;
    this.first = null;
    this.last = null;
  }

  //To Add element to Queue Rear
  enqueue(value) {
    console.log(`Add element to the Queue - ${value}`);
    let node = new Node(value);
    if (this.length === 0) {
      this.first = node;
      this.last = node;
    } else {
      let current = this.last;
      this.last = node;
      current.next = this.last;
    }
    this.length++;
    return this;
  }
  //To Remove Element
  dequeue() {
    let current = this.first;
    if (current != null) {
      this.first = this.first.next;
      delete current.next;
      this.length--;
      if (this.length === 0) {
        this.last = null;
      }
      console.log(`Removed Element from Queue - ${current.value}`);
    } else {
      console.log(`Nothing to Dequeue From QUEUE -EMPTY QUEUE`);
    }

    return this;
  }
  //To Check First Element To be Exiting Next
  peek() {
    console.log(
      `Peeked Element to Queue - ${
        this.first != null ? this.first.value : "QUEUE EMPTY"
      }`
    );
    return this;
  }

  printQueue() {
    let current = this.first;
    let arr = [];
    arr.push("EXIT FROM QUEUE <-");
    while (current != null) {
      arr.push(current.value);
      arr.push("<-");
      current = current.next;
    }
    arr.push(" <-ENTRY INTO QUEUE");
    console.log(`Length Of Queue ---- ${this.length}`);
    console.log(arr.join(" "));
    return this;
  }
}

let queueObj = new Queue();
console.log("Queue Config : " + JSON.stringify(queueObj));
queueObj.enqueue(5).enqueue(6).enqueue(7).enqueue(8).printQueue();

console.log("Queue  Config : " + JSON.stringify(queueObj));

queueObj
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .dequeue()
  .printQueue();

console.log("Queue Config : " + JSON.stringify(queueObj));

queueObj.peek().dequeue();
console.log("Queue Config : " + JSON.stringify(queueObj));
queueObj.enqueue(5).enqueue(6).enqueue(7).dequeue().printQueue();
console.log("Queue Config : " + JSON.stringify(queueObj));
