let palindromeCheckBasic = function check(str) {
  console.log(`Given String - ${str}`);
  let temp = Array.from(str).reverse().join("");
  return temp === str;
};

console.log(
  `The String is Palindrome BASIC ? = ${palindromeCheckBasic("AnikinA")}`
);

console.log(
  `The String is Palindrome BASIC ? = ${palindromeCheckBasic("AniinA")}`
);

console.log(
  `The String is Palindrome BASIC ? = ${palindromeCheckBasic("AnikrnA")}`
);

let palindromeCheck = function check(str) {
  console.log(`Given String - ${str}`);
  let end = str.length - 1;
  let start = 0;
  let result = true;
  while (start <= end) {
    console.log(str[start], str[end]);
    if (str[start] === str[end]) {
      start++;
      end--;
    } else {
      result = false;
      break;
    }
  }
  return result;
};

console.log(`The String is Palindrome ? = ${palindromeCheck("AnikinA")}`);

console.log(`The String is Palindrome ? = ${palindromeCheck("AniinA")}`);

console.log(`The String is Palindrome ? = ${palindromeCheck("AnikrnA")}`);

let palindromeCheckRecursive = function check(str) {
  console.log(`Given String - ${str}`);
  if (str.length === 0 || str.length === 1) {
    return true;
  }
  let flag = str[0] === str[str.length - 1];
  if (flag) return flag && check(str.substring(1, str.length - 1));
  else return flag;
};

console.log(
  `The String is Palindrome RECURSIVE ? = ${palindromeCheckRecursive(
    "AnikinA"
  )}`
);

console.log(
  `The String is Palindrome RECURSIVE ? = ${palindromeCheckRecursive("AniinA")}`
);

console.log(
  `The String is Palindrome RECURSIVE ? = ${palindromeCheckRecursive(
    "AnikrnA"
  )}`
);
