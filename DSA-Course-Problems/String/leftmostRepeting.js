let leftMostrepeting = function leftmostRepeting(str) {
  let map = new Map();
  let result = -1;
  for (let index = 0; index < str.length; index++) {
    let char = str[index];
    if (map.has(char)) {
      map.set(char, map.get(char) + 1);
    } else {
      map.set(char, 1);
    }
  }
  for (let index = 0; index < str.length; index++) {
    let char = str[index];
    if (map.get(char) > 1) {
      result = index;
      break;
    }
  }
  console.log(`The result is REPETING LEFT MOST - arr[${result}]`);
};

leftMostrepeting("abbcc");
leftMostrepeting("abcd");
leftMostrepeting("abcdeefghhijja");
