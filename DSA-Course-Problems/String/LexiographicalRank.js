let lexiograpicalRank = function (str) {
  let rank = 1;
  console.log(`Given String - ${str}`);
  for (let i = 0; i < str.length - 1; i++) {
    let combination = str.length - 1 - i;
    let counter = 0;
    console.log(str[i], str[i].charCodeAt(), "Combi -> ", combination);
    for (let j = i + 1; j < str.length; j++) {
      if (str[j].charCodeAt() < str[i].charCodeAt())
        console.log("-->", str[j], str[j].charCodeAt());
      counter++;
    }
    let fact = calculateFactorial(combination);
    rank = rank + counter * fact;
  }
  console.log(`The Lexiographical Rank --- ${rank}`);
};

function calculateFactorial(num) {
  if (num === 0) {
    return 1;
  } else {
    let res = 1;
    while (num > 1) {
      res = res * num;
      num--;
    }
    return res;
  }
}

lexiograpicalRank("STRING");
lexiograpicalRank("DCBA");
lexiograpicalRank("CBA");
lexiograpicalRank("BAC");
lexiograpicalRank("ABC");
