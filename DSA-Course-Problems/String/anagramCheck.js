let anagramCheck = function check(str, anagram) {
  str = Array.from(str).sort().join("");

  anagram = Array.from(anagram).sort().join("");
  console.log(anagram, str);
  if (str === anagram) {
    console.log(`The String - is anagram`);
  } else {
    console.log(`The String -  is Not anagram`);
  }
};

anagramCheck("aniket", "naikte");

anagramCheck("aniket", "nahikte");

let anagramCheckEnhanced = function check(str, anagram) {
  if (str.length != anagram.length) {
    console.log(`The String -  is Not anagram`);
  } else {
    let map = new Map();
    for (let index = 0; index < str.length; index++) {
      map.set(
        str[index],
        map.get(str[index]) != undefined ? map.get(str[index]) + 1 : 1
      );
      console.log(map);
      map.set(
        anagram[index],
        map.get(anagram[index]) != undefined ? map.get(anagram[index]) - 1 : -1
      );
      console.log(map);
    }
    let result = true;
    map.forEach((val, key) => {
      if (val != 0) {
        result = false;
      }
    });

    if (result) {
      console.log(`The String - is anagram`);
    } else {
      console.log(`The String -  is Not anagram`);
    }
  }
};

anagramCheckEnhanced("bcdrfgha", "bacfegdh");
anagramCheckEnhanced("aniket", "nahikte");
