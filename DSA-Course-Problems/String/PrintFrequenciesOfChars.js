let printCharCounts = function (str) {
  console.log(`Original String - ${str}`);
  //Here it is Considered that only small letter alphabet is there
  let arr = new Array(26).fill(0);
  Array.from(str).forEach((item) => {
    let index = item.charCodeAt() - "a".charCodeAt();
    arr[index] = arr[index] + 1;
  });
  arr.forEach((data, index) => {
    if (data > 0)
      console.log(
        "Count " + data + "===>" + String.fromCharCode(index + "a".charCodeAt())
      );
  });
};

printCharCounts("geeksforgeeks");
