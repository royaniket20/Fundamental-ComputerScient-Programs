/**
 * Find the longest Substring withouit repeting characters
 * s = cadbzabcd
 * Here idea is we use two Pointer - Fill a 256 Size array
 * and then Makr the array with Index pos of char that is appearing
 * now Move end pointer one by one ..
 * However Always check if the char is repeting - then Move start to next to repeting char Index
 * and Remove entry from the Array
 *
 */

let findLongestSubStr = function (str) {
  console.log(`Given String is - ${str}`);
  let start = 0;
  let end = 0;
  let filler = new Array(256).fill(null);
  while (end < str.length) {
    if (filler[str[end].charCodeAt()] === null) {
      //That means till Now no repeting Chars Found
      filler[str[end].charCodeAt()] = end; //Marked with char Index
      end++;
      if (end > str.length - 1) {
        end--;
        break;
      }
    } else {
      console.log(
        `Duplicate detected time to Shift start pointer only if Start has Not laready crossed that Point  `
      );
      //That Means already a repeting Char Found - Move start towards new Start
      if (filler[str[end].charCodeAt()] >= start) {
        start = filler[str[end].charCodeAt()] + 1;
      }
      filler[str[end].charCodeAt()] = null;
    }
    console.log(
      `Current Start str[${start}]: ${str[start]} ==> Current End str[${end}] : ${str[end]} `
    );
  }
  console.log(`The Final Length = ${end - start + 1}`);
};

findLongestSubStr("cadbzabcd");
