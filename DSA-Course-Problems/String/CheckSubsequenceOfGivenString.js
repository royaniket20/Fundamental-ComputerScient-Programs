let findSubsequence = function (str, target) {
  let mainArr = Array.from(str);
  let checkArr = Array.from(target);
  let index = 0;
  let dest = checkArr.length;
  for (let i = 0; i < mainArr.length; i++) {
    if (mainArr[i] === checkArr[index]) {
      console.log(`${mainArr[i]} -- Found !!`);
      index++;
    }
  }
  if (index === dest) {
    console.log(`Sub Sequence is Found`);
  } else {
    console.log(`Sub Sequence Not Found`);
  }
};

findSubsequence("Aniket Roy is Good", "ikety iod");
findSubsequence("Aniket Roy is Good", "iekty iod");

let findSubsequenceRecursive = function findSubSeq(
  str,
  target,
  strIndex,
  targetIndex
) {
  // console.log(strIndex, targetIndex);
  if (targetIndex === 0) {
    //Whole Target String is Covered
    return true;
  }
  if (strIndex === 0) {
    //Whole Target String is NOT Covered But original String exhausted
    return false;
  }
  if (str[strIndex - 1] === target[targetIndex - 1]) {
    return findSubSeq(str, target, strIndex - 1, targetIndex - 1);
  } else {
    return findSubSeq(str, target, strIndex - 1, targetIndex);
  }
};

console.log(
  "Is SubSequence Recursive ---- " +
    findSubsequenceRecursive(
      "Aniket Roy is Good",
      "ikety iod",
      "Aniket Roy is Good".length,
      "ikety iod".length
    )
);
console.log(
  "Is SubSequence Recursive ---- " +
    findSubsequenceRecursive(
      "Aniket Roy is Good",
      "iekty iod",
      "Aniket Roy is Good".length,
      "iekty iod".length
    )
);
