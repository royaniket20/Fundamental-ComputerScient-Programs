let reverseWordsUsingStack = function (sentence) {
  console.log(`Given sentence is - ${sentence}`);
  let initialIndex = 0;
  let stack = [];
  for (let index = 0; index < sentence.length; index++) {
    if (sentence[index] === " ") {
      let word = sentence.substring(initialIndex, index);
      stack.push(word);
      initialIndex = index + 1;
    } else if (index == sentence.length - 1) {
      //Last word
      let word = sentence.substring(initialIndex, sentence.length);
      stack.push(word);
    }
  }
  sentence = "";
  console.log(`Stack is -- ${stack}`);
  while (stack.length > 1) {
    sentence = sentence + stack.pop() + " ";
  }
  sentence = sentence + stack.pop();

  console.log(`Reversed sentence is USING STACK - ${sentence}`);
};

reverseWordsUsingStack("aniket is a good boy");

let reverseWordsEfficient = function (sentence) {
  console.log(`Given sentence is - ${sentence}`);
  let initialIndex = 0;
  let arr = Array.from(sentence);
  for (let index = 0; index < sentence.length; index++) {
    if (sentence[index] === " ") {
      reverse(arr, initialIndex, index - 1);
      initialIndex = index + 1;
    } else if (index == sentence.length - 1) {
      //Last word Reversal
      reverse(arr, initialIndex, index);
    }
  }
  reverse(arr, 0, arr.length - 1);
  console.log(
    `Reversed sentence is Using In Place Twice Reversal - ${arr.join("")}`
  );
};

let reverse = function (arr, start, end) {
  while (start < end) {
    let temp = arr[start];
    arr[start] = arr[end];
    arr[end] = temp;
    start++;
    end--;
  }
  console.log(arr + "--Reversed Word");
};
reverseWordsEfficient("aniket is a good boy");
