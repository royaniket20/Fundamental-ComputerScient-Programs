let naiveSlidingWindowDistinct = function (str, pattern) {
  let loopCount = 0;
  let result = [];
  if (pattern.length <= str.length) {
    let start = 0;
    let end = pattern.length - 1;
    while (end < str.length) {
      loopCount++;
      let token = str.substring(start, end + 1);
      let searchIndexStart = start;

      for (let i = 0; i < token.length; i++) {
        loopCount++;
        if (token[i] === pattern[i]) {
          start++;
          end++;
          if (i == token.length - 1) {
            //Last Loop means Token Matched
            console.log(token);
            result.push(searchIndexStart);
          }
        } else {
          start++;
          end++;
          break;
        }
      }
    }
  }
  console.log(
    `The Start Index with Matching Pattern Distinct - ${result} -- loopCount : ${loopCount}`
  );
};

naiveSlidingWindowDistinct(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "in Be"
);
naiveSlidingWindowDistinct(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "Industries"
);

naiveSlidingWindowDistinct("AAAAA", "A");
