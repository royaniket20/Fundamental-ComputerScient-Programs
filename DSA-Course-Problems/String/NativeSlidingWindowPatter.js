let naiveSlidingWindow = function (str, pattern) {
  let loopCount = 0;
  let result = [];
  if (pattern.length <= str.length) {
    let start = 0;
    let end = pattern.length - 1;
    while (end < str.length) {
      loopCount++;
      let token = str.substring(start, end + 1);
      for (let i = 0; i < token.length; i++) {
        loopCount++;
        if (token[i] === pattern[i]) {
          if (i == token.length - 1) {
            //Last Loop means Token Matched
            console.log(token);
            result.push(start);
          }
        } else {
          break;
        }
      }
      start++;
      end++;
    }
  }
  console.log(
    `The Start Index with Matching Pattern - ${result} -- Loop Count -- ${loopCount}`
  );
};

naiveSlidingWindow(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "in Ben"
);
naiveSlidingWindow(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "Industries"
);

naiveSlidingWindow("AAAAA", "A");

naiveSlidingWindow("AAAAA", "B");
