let leftMostNonrepeting = function leftmostRepeting(str) {
  let map = new Map();
  let result = -1;
  for (let index = 0; index < str.length; index++) {
    let char = str[index];
    if (map.has(char)) {
      map.set(char, map.get(char) + 1);
    } else {
      map.set(char, 1);
    }
  }
  for (let index = 0; index < str.length; index++) {
    let char = str[index];
    if (map.get(char) == 1) {
      result = index;
      break;
    }
  }
  console.log(`The result is NON REPETING LEFT MOST - arr[${result}]`);
};

leftMostNonrepeting("abbcca");
leftMostNonrepeting("abcd");
leftMostNonrepeting("abcdeefghhijja");

let leftMostNonrepetingSingleTraversal = function leftmostRepeting(str) {
  console.log("***************************************************");
  console.log(`Given String is - ${str}`);
  //Fill array with -1
  let asciiArr = new Array(127).fill(-1);
  for (let index = 0; index < str.length; index++) {
    let charCode = str[index].charCodeAt();
    console.log(charCode);
    if (asciiArr[charCode] == -1) {
      //Firt time Visit
      asciiArr[charCode] = index;
    } else if (asciiArr[charCode] > -1) {
      //Already repeting ---Mark for Repeted
      asciiArr[charCode] = -2;
    }
  }
  let init = Number.MAX_SAFE_INTEGER;
  let result = asciiArr
    .filter((item) => item > -1)
    .reduce((prev, curr) => {
      let result = Math.min(prev, curr);
      console.log(`previous: ${prev}, current: ${curr}, returns: ${result}`);
      return result;
    }, init);
  result = result === Number.MAX_SAFE_INTEGER ? -1 : result;
  console.log(`The result is NON REPETING LEFT MOST - arr[${result}]`);
};

leftMostNonrepetingSingleTraversal("abbacbd");
leftMostNonrepetingSingleTraversal("abcd");
leftMostNonrepetingSingleTraversal("abcdeefghhijja");
leftMostNonrepetingSingleTraversal("abcdabcd");
