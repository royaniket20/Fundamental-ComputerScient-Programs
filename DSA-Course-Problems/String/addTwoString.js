// let x = 99
//let y = 239
// Sum -- 338

var addStrings = function (num1, num2) {
  let i = num1.length - 1;
  let j = num2.length - 1;
  let carry = 0;
  let sum = "";
  let temp = 0;
  while (i >= 0 || j >= 0) {
    if (i >= 0) {
      temp = temp + Number(num1[i]);
      i--;
    }

    if (j >= 0) {
      temp = temp + Number(num2[j]);
      j--;
    }
    if (carry > 0) {
      temp = temp + carry;
    }
    carry = Math.floor(temp / 10);
    sum = sum + (temp % 10);
    temp = 0;
  }
  if (carry > 0) {
    sum = sum + carry; //If carry left over Just append
  }
  return sum.split("").reverse().join("");
};

let result = addStrings("99", "239");
console.log(`Sum is - ${result}`);
