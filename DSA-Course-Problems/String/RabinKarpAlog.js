let rabinKarpSearch = function search(str, pattern) {
  let patternHash = calculateSimpleHash(pattern, 0, pattern.length - 1);
  let loopCount = 0;
  let result = [];
  if (pattern.length <= str.length) {
    let start = 0;
    let end = pattern.length - 1;
    let tokenHash = null;
    while (end < str.length) {
      loopCount++;
      let token = str.substring(start, end + 1);
      tokenHash = calculaterollingHash(str, start, end, tokenHash);
      for (let i = 0; i < token.length && tokenHash === patternHash; i++) {
        loopCount++;
        if (token[i] === pattern[i]) {
          if (i == token.length - 1) {
            //Last Loop means Token Matched
            console.log(token);
            result.push(start);
          }
        } else {
          break;
        }
      }
      start++;
      end++;
    }
  }
  console.log(
    `The Start Index with Matching Pattern - ${result} -- Loop Count -- ${loopCount}`
  );
};

function calculateSimpleHash(str, start, end) {
  //console.log("calculateSimpleHash ---> ", start, end);
  let val = null;
  for (let index = start; index <= end; index++) {
    val = val + str[index].charCodeAt();
  }
  return val;
}

function calculaterollingHash(str, start, end, hash) {
  //console.log("calculaterollingHash --> ", start, end, hash);
  if (hash === null) {
    return calculateSimpleHash(str, start, end);
  } else {
    hash = hash - str[start - 1].charCodeAt() + str[end].charCodeAt();
  }
  return hash;
}

rabinKarpSearch(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "in Be"
);
rabinKarpSearch(
  "Aniket roy is a Good Boy in Bengaluru. He leaves in Bengal for Long Time.There is Many Industries in Bengladesh",
  "Industries"
);

rabinKarpSearch("AAAAA", "A");
