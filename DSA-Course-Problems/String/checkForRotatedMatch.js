let naiveMethod = function (str, target) {
  let arr = str.split("");
  let check = target.split("");
  console.log(`Given String - ${arr}`);
  let rotation = str.length;
  while (rotation > 0) {
    let char = arr.pop();
    arr.unshift(char);
    console.log(`Rotated Array at Rotation ${rotation} ==> ${arr}`);
    if (JSON.stringify(arr) === JSON.stringify(check)) {
      console.log(`Match is Found Target - ${check}`);
      break;
    }
    rotation--;
  }
  if (rotation == 0) console.log(`Match is NOT Found Target - ${check}`);
};

naiveMethod("abcd", "cdab");

naiveMethod("abcd", "cdba");

let efficientMethod = function (str, target) {
  console.log(`Given String - ${str} || Target String - ${target}`);
  let strData = str + str;
  console.log("Concatinated String- " + strData);
  if (strData.includes(target)) {
    console.log(`Match is Found Target - ${target}`);
  } else {
    console.log(`Match is Found Target - ${target}`);
  }
};

efficientMethod("abcd", "cdab");

efficientMethod("abcd", "cdba");
